#-*- coding:utf-8 -*- 
import pandas
from atraOuter import *
from atraBot import *
import pandas as pd
import threading, argparse, configparser

def parseAtraArg():
    argp = argparse.ArgumentParser(
        description='manage atra')
    argp.add_argument('--config-keys',
                      default="./setting/keys.ini",
                      help="Use different config //->keys setting file (default: %(default)s)")
    argp.add_argument('--add-secret', action="store_true",
                      help="prompt for API secret for each Exchanges, encrypt them and then exit")
    argp.add_argument('--get-secret', action="store_true",
                      help="show specified exchange saved decrypted_secret_key using password")
    argp.add_argument('--password', action="store", default=None,
                      help="password for decryption of stored key. This is a dangerous option" +
                      "~/.bash_history 에 남을수 있으니 생각하고 사용할것 ")

    args = argp.parse_args()
    config_keys_filename = args.config_keys
    print(config_keys_filename)
    #//->그냥 configparser를 상속받아서 만든 ini파일 관리 class임
    config = AtraConfig(config_keys_filename)
    # config.read(config_keys_filename)
    k_manager = KeyManager(config)
    k_manager.password_from_commandline_option = args.password
    # --add-secret 모드 일때
    if args.add_secret:
        k_manager.prompt_exchanges()
        os._exit(1)
    elif args.get_secret:
        k_manager.prompt_decrypt(display = True)
        os._exit(1)
    else:
        #비번을 제대로 치면 시작 할 수 있도록
        decrypted_result = k_manager.prompt_decrypt(display = False)
        print("decrypted_result", decrypted_result)
        if decrypted_result == k_manager.S_OK:
            return k_manager
        #todo 각 mode view 모드 virtual-trading 모드 cycle 모드 등등을 어떻게 할지 지금은 curses를 안쓰니 크게 고려하지 않아도 될수있을듯듯
        else:
            pass


def atraStart():
    key_manager = parseAtraArg()
    SharedStorage.__init__(key_manager)
    pairing_exchangeName_dict = {"IOExchange": "bithumb", "EExchange": "bitfinex"}
    # money_role_eachExchange = {"IOExchange": "KRW", "EExchange": "BTC"}
    # a= ExchangePairing("BTC", pairing_exchangeName_dict, money_role_eachExchange)
    money_role_eachExchange = {"IOExchange": "KRW", "EExchange": "BTC"}
    a = ExchangePairing("BTC", pairing_exchangeName_dict, money_role_eachExchange)

    # io가 어디고 EExchange이 어디고는 생성자 생성할때만 딱 넣고 나머지는 다 exchange Type하나로만 구분하여 호출
    a.atraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    a.atraCycle_setting_val["THREASHOLD_RATE"] = 0.002
    a.atraCycle_setting_val["THREASHOLD_PROFIT"] = 3000
    a.atraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] =  4000000
    a.atraCycle_setting_val["PERMITTED_COIN"] = ["ETC", "DASH","LTC","XRP"]

    #단일호출도 가능(쓰레드 없이)
    # AtraRepetitiveInformer.eachExchangeOrderInfoBot("poloniex",False)
    # AtraRepetitiveInformer.eachExchangeOrderInfoBot("bithumb",False)

    #각 exchange의 balance를 주기적으로 받아오는 쓰레드 시작
    balance_autoBot_bithumb = threading.Thread(name="bithumb_balance",
                                               target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot,
                                               args=("bithumb", True), kwargs={"delay": 2})
    balance_autoBot_bithumb.daemon = True
    balance_autoBot_bithumb.start()

    balance_autoBot_bitfinex = threading.Thread(name="bithumb_balance",
                                                target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot,
                                                args=("bitfinex", True), kwargs={"delay": 2})
    balance_autoBot_bitfinex.daemon = True
    balance_autoBot_bitfinex.start()
    #각 마켓의 orderbook을 주기적으로 받아 오는  쓰레드 시작
    orderBook_autoBot_bithumb = threading.Thread(name="bithumb_orderBook",
                                                 target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot,
                                                 args=("bithumb", True))
    orderBook_autoBot_bithumb.daemon = True
    orderBook_autoBot_bithumb.start()

    orderBook_autoBot_bitfinex = threading.Thread(name="bitfinex_orderBook",
                                                  target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot,
                                                  args=("bitfinex", True))
    orderBook_autoBot_bitfinex.daemon = True
    orderBook_autoBot_bitfinex.start()

    while True:
    #     print("is poloniex_orderBook_Exist" + str(AtraUTIL.threadController('find','poloniex_orderBook')))
    #     print("is bithumb_orderBook_Exist" + str(AtraUTIL.threadController('find','bithumb_orderBook')))
    #     clear_output()
        #pairingObj 객체의 costAndProfit_basedOn_inOutCoin_latest 에 값이 들어가 있다는것은 시작 가능하다는 의미 이므로
        if  a.costAndProfit_basedOn_inOutCoin_latest["whenInCoin"] :
            # whenInCoin whenOutCoin 둘 모두 동일한 timestamp 가 들어있음
            timestamp = a.costAndProfit_basedOn_inOutCoin_latest["whenOutCoin"]["timestamp"]
            earning_rate = a.costAndProfit_basedOn_inOutCoin_latest["max_earningRate_info"]["earning_rate"]
    #         if earning_rate >=a.atraCycle_setting_val["THREASHOLD_RATE"]:
    #             time.sleep(100)
    #             break
    #         break
        time.sleep(3)

if __name__ == "__main__":
    try:
        atraStart()
    except KeyboardInterrupt as e:
        print(e)
        exit()    