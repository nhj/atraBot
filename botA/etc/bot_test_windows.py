#-*- coding:utf-8 -*-
import pandas
from atraBot import *
from IPython import display
import pandas as pd
import threading
import atraOuter
from atraStart_trade import parseAtraArg



# key_manager = parseAtraArg()
# SharedStorage.__init__(key_manager)
# pairing_exchangeName_dict = {"IOExchange": "bithumb", "EExchange": "poloniex"}
# money_role_eachExchange = {"IOExchange": "KRW", "EExchange": "BTC"}
#
# a= ExchangePairing("BTC", pairing_exchangeName_dict, money_role_eachExchange)
# # io가 어디고 EExchange이 어디고는 생성자 생성할때만 딱 넣고 나머지는 다 exchange Type하나로만 구분하여 호출
# # a.trade_setting_val["IS_TRADE_PERMITTED"] = True
# #
# # 단일호출도 가능(쓰레드 없이)
# # AtraRepetitiveInformer.eachExchangeOrderBookInfoBot("poloniex",False)
# # AtraRepetitiveInformer.eachExchangeOrderBookInfoBot("bithumb",False)

#각 exchange의 orderbook을 주기적으로 받아 오는  쓰레드 시작
# orderBook_autoBot_poloniex = threading.Thread(name="poloniex_orderBook", target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot, args=("poloniex", True), kwargs={"delay" : 0.1})
# orderBook_autoBot_bithumb = threading.Thread(name="bithumb_orderBook", target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot, args=("bithumb", True))
#
# orderBook_autoBot_bithumb.start()
# orderBook_autoBot_poloniex.start()
#각 exchange의 balance를 주기적으로 받아오는 쓰레드 시작
# balance_autoBot_poloniex = threading.Thread(name="poloniex_balance", target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot, args=("poloniex", True), kwargs={"delay" : 1})
# balance_autoBot_bithumb = threading.Thread(name="bithumb_balance", target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot, args=("bithumb", True),kwargs={"delay" : 2})
# balance_autoBot_bithumb.start()
# balance_autoBot_poloniex.start()


#
#
# # 'bid_highest_wonPrice_perEachCoin', 'bid_highest_baseCoinRatioPrice_perEachCoin' 등은 view에서는 안쓰니까 제외
# exchange_baseinfo_viewIndex = ['ask_lowest_krwPrice_perEachCoin',
#               'baseCoin_ratios_perEachCoin',
#               'ask_lowest_usdtPrice_perEachCoin',
#               'ask_lowest_baseCoinRatioPrice_perEachCoin']
#
# inout_baseinfo_viewIndex = ["inOut_coin_direction",
#                             "theoretical_outCoin_amount_if_noArbitrage_perEachCoin",
#                             "theoretical_outCoin_transferredAmount_perEachCoin",
#                             "theoretical_outCoin_transferredAmount_greedy_perEachCoin",
#                             "theoretical_atraCycle_value_grouping_perEachCoin",
#                             "theoretical_outCoin_sellMoney_perEachCoin",
#                             "theoretical_outCoin_sellMoney_greedy_grouping_perEachCoin",
#                             "theoretical_earningRate_perEachCoin",
#                             "theoretical_earningRate_greedy_perEachCoin",
#                             "theoretical_profit_byAtraCycleValue_perEachCoin"]
#
# while True:
#     time.sleep(1)
#     clear_output()
#     #pairingObj 객체의 costAndProfit_basedOn_inOutCoin_latest 에 값이 들어가 있다는것은 시작 가능하다는 의미 이므로
#     if  a.costAndProfit_basedOn_inOutCoin_latest["whenInCoin"] :
#            #summarized_orderBook은 순수히 view를 위한것 calculate랑 아무 관련없음
#         summarized_orderBook_eachExchange = AtraRepetitiveInformer.summarizeOrderBook(a)
#         exchangeBase_df_view = AtraUTIL.processView(a, summarized_orderBook_eachExchange["IOExchange"],
#                                                   summarized_orderBook_eachExchange["EExchange"],
#                                                   view_index=exchange_baseinfo_viewIndex)
#
#         #역시 calculate도 스레드에서 알아서 돌아가므로 df 로 빼는것도 모두 view를 위한 것임
#         inCoinBaseInfo_df_view = AtraUTIL.processView(a, a.costAndProfit_basedOn_inOutCoin_latest["whenInCoin"],
#                                                       view_index=inout_baseinfo_viewIndex)
#         outCoinBaseInfo_df_view = AtraUTIL.processView(a, a.costAndProfit_basedOn_inOutCoin_latest["whenOutCoin"],
#                                                        view_index=inout_baseinfo_viewIndex)
#
#         # whenInCoin whenOutCoin 둘 모두 동일한 timestamp 가 들어있음
#         timestamp = a.costAndProfit_basedOn_inOutCoin_latest["whenOutCoin"]["timestamp"]
#         # print("view only timestamp : ", str(time.strftime("%y%m%d %H:%M:%S", time.localtime(timestamp))))
#
#         # display.display(exchangeBase_df_view)
#         display.display(inCoinBaseInfo_df_view)
#         display.display(outCoinBaseInfo_df_view)
#     time.sleep(3)


# inCoin_baseInfo, outCoin_baseInfo = a.calculateCostAndProfit()
#
# inout_baseinfo_viewIndex = ["inOut_coin_direction",
#                             "theoretical_outCoin_amount_if_noArbitrage_perEachCoin",
#                             "theoretical_outCoin_transferredAmount_perEachCoin",
#                             "theoretical_outCoin_transferredAmount_greedy_perEachCoin",
#                             "theoretical_outCoin_sellMoney_perEachCoin",
#                             "theoretical_outCoin_sellMoney_greedy_perEachCoin",
#                             "theoretical_earningRate_perEachCoin",
#                             "theoretical_earningRate_greedy_perEachCoin"]
# inCoinBaseInfo_df_view = processView(inCoin_baseInfo,view_index=inout_baseinfo_viewIndex)
# outCoinBaseInfo_df_view = processView(outCoin_baseInfo,view_index=inout_baseinfo_viewIndex)
#
#
# display.display(inCoinBaseInfo_df_view)
# display.display(outCoinBaseInfo_df_view)




#!! thread Test !!##
# key_manager = parseAtraArg()
# SharedStorage.__init__(key_manager)
# balance_autoBot_poloniex = threading.Thread(name="poloniex_balance", target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot, args=("poloniex", True), kwargs={"delay" : 1})
# balance_autoBot_bithumb = threading.Thread(name="bithumb_balance", target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot, args=("bithumb", True),kwargs={"delay" : 2})
# balance_autoBot_bithumb.start()
# balance_autoBot_poloniex.start()
# time.sleep(4)
# # orderBook thread 테스트
# orderBook_autoBot_poloniex = threading.Thread(name="poloniex_orderBook", target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot, args=("poloniex", True), kwargs={"delay" : 0.1})
# orderBook_autoBot_bithumb = threading.Thread(name="bithumb_orderBook", target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot, args=("bithumb", True))
# orderBook_autoBot_bithumb.start()
# orderBook_autoBot_poloniex.start()




# #!!각 작동부 단일호출 테스트!!##   SharedStorage.__init__()필요
# key_manager = parseAtraArg()
# SharedStorage.__init__(key_manager)
# pairing_exchangeName_dict = {"IOExchange": "bithumb", "EExchange": "poloniex"}
# money_role_eachExchange = {"IOExchange": "KRW", "EExchange": "BTC"}
#
# a= ExchangePairing("BTC", pairing_exchangeName_dict, money_role_eachExchange)
# #1회용 orderBook 테스트
eExchange_baseinfo = AtraRepetitiveInformer.eachExchangeOrderBookInfoBot("poloniex",False)
ioExchange_baseinfo = AtraRepetitiveInformer.eachExchangeOrderBookInfoBot("bithumb",False)
# print(SharedStorage.orderbook_eachExchange["bithumb"])
# print(SharedStorage.orderbook_eachExchange["poloniex"])

# #1회용 balance 테스트
# print(AtraRepetitiveInformer.eachExchangeBalanceInfoBot("poloniex",False))
# print(AtraRepetitiveInformer.eachExchangeBalanceInfoBot("bithumb",False))

# print(AtraRepetitiveInformer.summarizeOrderBook(a))

# #buy시에
# result = AtraUTIL.calculateTradingGreedily("poloniex","BTC" ,"buy","ETH",money_role_sum=2)
#
# #sell시에
# AtraUTIL.calculateTradingGreedily("poloniex","BTC","sell","ETH",commodity_role_coin_amount=20)



# #transpose하는 함수
# # df = pd.DataFrame(np.array(data).T,columns=['B','P','F','I','FP','BP','2','M','3','1','I','L'])
#
# display.display(df_info)

