#-*- coding:utf-8 -*-
import json, copy, logging
import os
import pycurl
import socket
import threading
import time
import traceback
import urllib2
from pprint import pformat,pprint
from deepdiff import DeepDiff
import atraLogging
import pandas as pd
from exchangeAPI.xcoin_api_client_studied import *
from exchangeAPI import bitfinex_api_client_studied as bitfinexAPI
from exchangeAPI import poloniex_api_client_studied as poloAPI
from exchangeAPI import bittrex_api_client_studied as bittrexAPI
#websocketClient
from exchangeAPI.btfxwss import BtfxWss

import inspect
#jupyter같은곳에서 test로 불러오면 오류가 발생 할 수 있기 때문에 atraStart에서 불러온것만 log 셋팅함
if inspect.getframeinfo(inspect.getouterframes(inspect.currentframe())[1][0])[0].find("Start") > 0:
    logging_normal = atraLogging.configureAtraLogger('normal')
    logging_notify = atraLogging.configureAtraLogger('notify_error')
    logging_atraCycle = atraLogging.configureAtraLogger('atraCycle')
    btfx_log = atraLogging.configureAtraLogger('btfx_log')


# logging.basicConfig(level=logging.DEBUG, format='(%(threadName)-9s) %(message)s',)
#todo:commodityRole Coin 과 counter coin은 엄밀히 말하자면 다른것임 이것에 대해서 찾아서 바꿀것
#todo: commodityRole Coin은 pairing neutral 하며 개별 exchange 안에서 정의되는 개념이며
#todo : counterCoin은 exchange  pairing이 성립된 이후에 base_coin의 상대 개념으로 성립되는 개념임
#하나의 ioExchange과 다수의 EExchange이 결부될수 있으므로 각 exchange 이름별로 정보를 저장하는 공간과 관련 메소드
#각 collector 객체들은 객체 생성시 매개변수로 받은 ioExchange명과 EExchange 명을 기반으로  sharedStorage에서 정보를 받아오거나
# sharedStorage저장시킨다
class SharedStorage:
    atraCycleObj = None
    pairingObj_connectedWith_atraCycle = None

    #각 exchange pair 에 의해서 orederbook에 정해진 시간(초) 텀으로 orderbook을 받아와서 저장 시킨다
    # 각 paitObj는  view나 calculateCostandProfit 같은 연산을할때 여기에서 최신 정보를 받아간다
    #아래와 같은 식으로 AtraUTIL.makeAtraSyleOrderBook() 에서 만든뒤 저장시킴
    #  "poloniex" : {"BTC{moneyrole임}" : { "ETH{commodityrole임}" : {"price" :~ , "quantity" : ~} , ETC : {..}} , ETH{moneyrole임}:~}}
    #  "bithum" : {"WON{moneyrole임}" : { "ETH{commodityrole임}" : {"price" :~ , "quantity" : ~} , ETC : {..}}
    #이런식으로 마켓이름 안에 money_role 로 묶인 기축 코인 또는 실물화폐 WON 이나 USD 등으로 해놓고 각각의 moneyRole안에서 해당 exchange에서 거래되는
    #commodity 역할 코인들을 집어 넣는다  atra에서 인정하는 money_role은 money_role_in_eachExchange에서 정해진것만임
    orderbook_eachExchange = {}

    #orderBook api요청 파라미터로 all 을 사용 못하는 exchange들(e.g bitfinex, bittrex)은 다중 쓰레드를 사용해서 거의 동시에 여러 coin
    #orderBook 정보를 받아오게 되는데이때  공유공간이 필요하다
    orderbook_eachExchange_temp = {}

    balance_eachExchange = {}
    # watcher_condition = None

    #설정에서 불러와 각 exchange의 각 coin  주소를 dict로 저장
    coin_address_eachExchange = {}
    #설정에서 불러와 각 exchange의 각 fee 정보를 dict로 저장
    feeInfo_eachExchange = {}
    #atraBot에서 이용하는 각 exchange에서 moneyRole을 하는 것들 원화(\)나  달러($) 혹은 major 한 BTC,ETH 같은 코인들임
    #실제 exchange에서는 더 많을수 있지만 atra에서 취급하는 것들만 추림 #설정에서 불러와 각 exchange의 각 fee 정보를 dict로 저장
    coin_roleList_eachExchange = {}
    # _initExchangeClientApi 에서  coin_roleList_eachExchange 의 key인 exchangeName 기준으로 뽑아와서 저장시킨다
    client_api_eachExchange = {}
    #pairingObj가 만들어지면 여기로 들어감 각 pairing "KRW_bithumb__BTC__poloniex_BTC" 식의 이름을 생성시켜서 넣음
    activated_pairingObj = {}

    @classmethod
    def __init__(cls, key_manager, config_sharedJson_filename, atra_module_interactor):
        #feeInfo_eachExchange 초기화, lock class Member class를 초기화 시킨다
        cls.config_sharedJson_filename = config_sharedJson_filename
        #./setting/shared_~.json 을 불러옴 기본적으로
        with open (config_sharedJson_filename, 'r') as f:
            shared_config = json.load(f)
        # print(shared_config["coin_address_eachExchange"], type(shared_config["coin_address_eachExchange"]))
        # print(shared_config["feeInfo_eachExchange"], type(shared_config["feeInfo_eachExchange"]))
        # print(shared_config["coin_roleList_eachExchange"], type(shared_config["coin_roleList_eachExchange"]))

        cls.coin_address_eachExchange = AtraUTIL.convert(shared_config["coin_address_eachExchange"])
        cls.feeInfo_eachExchange = AtraUTIL.convert(shared_config["feeInfo_eachExchange"])
        cls.coin_roleList_eachExchange = AtraUTIL.convert(shared_config["coin_roleList_eachExchange"])
        cls.convertible_symbol_eachExchage= AtraUTIL.convert(shared_config["convertible_symbol"])

        # shared.json에서 정의된 각 exchange별 api_client정보 (해당 exchange가 restful websocket
        # 어떤 client 객체를 만들지  두객체를 모두 만들었다면 각 api 명령(ex. orderBook transfer)이
        # 어떤 client를 이용할건지 정의 되어있음
        cls.api_client_info = AtraUTIL.convert(shared_config["api_client_info"])
        #atraBot과 atra module은 완전히 독립적으로 운용되며 atraBot 외부의 interactor를 통해서만 모든 module 정보와 함수를
        #접근할 수 있게 함.
        cls.atra_module_interactor = atra_module_interactor

        #secret key 관리 인스턴스인 key_manager에 저장된 값을 가져옴
        keys = AtraUTIL.convert(key_manager.keys_eachExchange)
        #coin_roleList가 정의된것들은 무조건 clientAPI를 만들어 놓아야 된다고 생각하면됨 즉 기준은 coinRoleList임
        exchange_list = cls.coin_roleList_eachExchange.keys()
        #module로 db나 server에 접근시 client 명으로 쓰임
        cls.client_name = os.uname()[1]

        api_exchange_list = cls.api_client_info.keys()
        #client_api_list 에서 정의된 것들과 coin_role_list에서 정의된 exchange가 차이날때를 대비
        none_defined_exchanges = list(set(exchange_list) - set(api_exchange_list))
        if len(none_defined_exchanges) != 0:
            raise Exception("you should define api_client_info and coin_role_list altogether" + str(none_defined_exchanges))

        for exchange_name in api_exchange_list:
            if exchange_name not in cls.client_api_eachExchange:
                cls.client_api_eachExchange[exchange_name] = {}
            client_type_list = cls.api_client_info[exchange_name]["type"]
            for api_client_type in client_type_list:
                cls.client_api_eachExchange[exchange_name][api_client_type] = cls._initExchangeClientApi(exchange_name, keys[exchange_name], api_client_type)
                logging_normal.info(api_client_type+ " api client of "  + exchange_name +",has been successfully created\n" + str(cls.client_api_eachExchange[exchange_name][api_client_type]))
        # print(pprint(cls.client_api_eachExchange))

    @classmethod
    def _initExchangeClientApi(cls, exchange_name, keys, api_client_type):
        '''각 exchange의 client api 객체를 만들어 놓고 AtraBot 전체에서 쓴다'''
        #todo: no-secret 모드 일때 어떻게 할 지 해서 api_key = "" 로 만들것
        api_key = keys["api_key"]
        secret_key = keys["secret_key"]
        # print(api_key, secret_key)
        client_api = None

        if api_client_type == "restful":
            if exchange_name == "bithumb":
                client_api = XCoinAPI(api_key, secret_key);

            elif exchange_name == "poloniex":
                # -> 1
                # apiKey = "CPOW7JI5-ETRDZR6A-REWVO7NR";
                # secretKey = "caf2ab635a2389b55602447ff";
                client_api = poloAPI.poloniex(api_key, secret_key)
            elif exchange_name == "bitfinex":
                client_api = bitfinexAPI.BitfinexAPI(api_key, secret_key)

            elif exchange_name == "bittrex":
                client_api = bittrexAPI.bittrexApi(api_key, secret_key)
        #api_client_type 이 websocket일때
        elif api_client_type == "websocket":
            if exchange_name == "bithumb":
                pass
            elif exchange_name == "poloniex":
                pass
            elif exchange_name == "bitfinex":
                try :
                    client_api = BtfxWss(key=api_key, secret=secret_key)
                    client_api.start()
                except Exception as e:
                    logging_notify.exception("btfx initial error")
            elif exchange_name == "bittrex":
                pass



        if client_api == None:
            raise Exception("//->you didn't define client_api method for " + exchange_name + ",client type : " + api_client_type)
        else:
            return client_api

    @classmethod
    def _initAtraCycleThread(cls):
        '''atraCycle Thread를 만들고 SharedStorage에서 AtraCycle을 저장시킨다'''
        print("atraCycle initiated on SharedStorage _initAtraCycleThread Func")
        atraCycle_threadName = "atraCycle"
        # if not AtraUTIL.threadController("find", atraCycle_threadName) and is_trade_permitted:
        #     if not AtraUTIL.threadController("find", atraCycle_threadName):
        #         print("trading thread start.............")
        cls.atraCycle_thread_event = threading.Event()

        # self.atraCycle_thread_event = atraCycle_thread_event
        cls.atraCycleObj = AtraCycle(name=atraCycle_threadName, args=(cls.atraCycle_thread_event,))
        cls.atraCycleObj.start()

    @classmethod
    def requestAtraCycle(cls, pairingObj):
        '''계산을 끝난 각 pairingObj의 calculate 메소드는 여기를 훑고 지나가서 거래를 요청한다'''
        atraCycle_lock = threading.Lock()
        print("//->before lock")
        with atraCycle_lock :
            if cls.pairingObj_connectedWith_atraCycle == None:
                #atraCycle 종료시점이나 finalize 시점에 다시 None으로 바꿔줌
                cls.pairingObj_connectedWith_atraCycle = pairingObj
                print("-->trading_SETED..... on SharedStorage requestAtraCycle Func")
                cls.atraCycle_thread_event.set()
                # clear를 너무 일찍 해주면 atracycle쓰레드의 wait에서 나오기도 전에 다시 block되므로 살짝 띄어줌
                time.sleep(0.001)
                # eventset  직후에  다시 clear을 해줘야  trading 쓰레드가 wait에서 대기 하고 있음 set 한걸 clear하지
                # 않는다면 wait을 만나도 안멈추고 그냥 계속 돌아버리게됨 진행해 버리게됨
                cls.atraCycle_thread_event.clear()
                #찰나의 순간에 lock에 걸린 exchangePairing들은 걸러짐
            else:
                print("already other exchange pairing is on atraCycle")

    @classmethod
    def setExchangeOrderBook(cls,exchange_name,order_book):

        #todo: 거래하는 exchange Pairing이 여러개가 되면 Aexchange - Bexchange 간의 orderbook 수집이 완료되면
        #관련된 모든 pairingObj 애들이 calculteCostAndProfit 을 시작해서 조건이 맞으면 trading을 시작하게 만들것
        # is_orderBook_watcher_exist = AtraUTIL.threadController("find","orderBook_watcher")
        # if not is_drderBook_watcher_exist:
        #     condition = threading.Condition()
        #     #일단 threading.condition을 모든 paringObj 안에 집어넣고 아래 if 문으로 조건이 완료되면
        #     SharedStorage.watcher_condition = condition
        #     orderBook_watcher = threading.Thread(target=AtraRepetitiveInformer.watchOrderBook,name="orderBook_watcher",
        #                                          args=(condition,))

        #최초 들어올때 체크해서  start 날려줌
        if exchange_name not in cls.orderbook_eachExchange.keys()  :
            logging_normal.info(exchange_name + " orderBook start...")
            cls.orderbook_eachExchange[exchange_name] = order_book
        else:
            #개 좆같은 빗썸 중복되서 오는지 비교 이전 orderBook 내용과 중복되면 갱신하지 않는다
            if exchange_name == "bithumb":
                old_orderBook = cls.orderbook_eachExchange[exchange_name]
                new_orderBook  = order_book

                diff_result = DeepDiff(old_orderBook, new_orderBook)
                if "values_changed" in diff_result :
                    # print(len(diff_result["values_changed"]), "stuff has changed")
                    # print(diff_result["values_changed"].keys())
                    #오직 timestamp만 변했을경우 orderBook 내용은 동일하므로 갱신하지 않는다
                    if "timestamp" in diff_result["values_changed"].keys()[0] and len(diff_result["values_changed"]) == 2:
                        pass
                        # print("only thing changed is timestamp so keep old orderBook")
                    #전체 내용이 변했을 경우 갱신시킴
                    else:
                        # print("bithumb orderBook changed so change new orderBook")
                        cls.orderbook_eachExchange[exchange_name] = order_book
            else :
                cls.orderbook_eachExchange[exchange_name] = order_book



        #지금 들어온 마켓이름말고 counterExchange의 이름
        # print(cls.orderbook_eachExchange[exchange_name])
        # 특정exchange(ex.poloniex)은 보통 해외에 있기 때문에(주로 EExchange) restful로 데이터 요청하고 받는데 시간이 대체로 국내에 있는 IOExchange보다  오래 걸림
        #따라서 최신IOExchange orderBook이 0. 몇초 이전것 이라도 EExchange의 주고 받는시간이 더 오래 걸리기 때문에
        #최신 EExchange 기준으로 비교하면 조금더 서로 비슷한 시간대라고 가정할수 있음 (IOExchange을 기준으로 하는것보다)
        if exchange_name == "poloniex" or exchange_name == "bitfinex" or exchange_name == "bittrex": #주로 해외의 exchange들이 reference(비교) 마켓의 후보가 됨
            #기준마켓중에 하나라면 조건을 맞추기 위해 연산을 들어감
            referenceExchange_name = exchange_name
            referenceExchange_orderBook_timestamp = order_book["timestamp"]

            #todo trading thread는 딱 하나만 만드는 것으로 하자  pairing이 얼마가 많던간에 현재 있는 그 하나
            #todo : 그리고 trade 끝나고 transfer 가 성공하자마자 바로 다른거래도 할 수 있게(그래야 혼돈이 없을듯)
            for pairingObj_name,pairingObj in SharedStorage.activated_pairingObj.iteritems():
                #pairing된 조합들을 순회 하면서 현재 기준이 되기로 한 reference Exchange 이 pairing 을 구성 하는것을 찾아냄 (해외 마켓이 될 가능성이 높음)
                #지금은 일단 해외의 EExchange 들만 reference Exchange 이 될수 있기 때문에 EExchange만 찾지만 나중엔 IOExchange도 어떻게 될지 모름 그땐 둘다 검색
                paired_eExchange_name = pairingObj.pairing_exchangeName_dict["EExchange"]
                paired_ioExchange_name = pairingObj.pairing_exchangeName_dict["IOExchange"]

                #여기서는 있는지 없는지만 체크
                is_paired_eExchange_balance_exist = paired_eExchange_name in cls.balance_eachExchange.keys()
                is_paired_ioExchange_balance_exist = paired_ioExchange_name in cls.balance_eachExchange.keys()

                if paired_eExchange_name == referenceExchange_name and paired_ioExchange_name in cls.orderbook_eachExchange.keys():
                    #현재넘어온 비교기준마켓과 관련된 모든pairing 의 IOExchange 최신 orderBook timestamp와 지금 EExchange
                    #timestamp를 비교하여 0.5초 이내라면  그 paringObj는 calculate 들어감
                    paired_ioExchange_latestOrderBook_timestamp = cls.orderbook_eachExchange[paired_ioExchange_name]["timestamp"]
                    #trading 결과 logging 뿌릴때 이거 나오면 존나 짜증나니까
                    # if pairingObj.is_atraCycle_now == False:
                    print("sec difference between orderbooks  :" + str(referenceExchange_orderBook_timestamp - paired_ioExchange_latestOrderBook_timestamp))
                    if 0 < abs(referenceExchange_orderBook_timestamp - paired_ioExchange_latestOrderBook_timestamp) < 1 and \
                            is_paired_eExchange_balance_exist and is_paired_ioExchange_balance_exist:
                        calc_thread_name = pairingObj_name + "Calc"
                        if not AtraUTIL.threadController("find",thread_name=calc_thread_name):
                        # if pairingObj.is_calculating_now == False:
                        #     print(pairingObj_name + " is about to calculating")
                            prepared_pairing = threading.Thread(name=calc_thread_name,target=pairingObj.calculateCostAndProfit )
                            prepared_pairing.start()
                            #
                        else:
                            print(pairingObj_name + " is on Calculating so can't calculating now")
                        #원래 exchangePairing을 하나만 운용할때는 기준 orderBook 와 연결된 paired_orderBook이 단 하나 이므로 쓰레스 사용 안하고 바로 계산시켰음
                        # pairingObj.calculateCostAndProfit()

    @classmethod
    def setExchangeBalance(cls,exchange_name,balance_info):
        if exchange_name not in cls.balance_eachExchange.keys() :
            logging_normal.info(exchange_name + " balance info start...")
        cls.balance_eachExchange[exchange_name] = balance_info
        AtraUTIL.putAtraModuleData("atraBalance_DB", {exchange_name : balance_info})


class AtraUTIL:
    '''주로 공통적으로 단일 호출해서 사용하는 메소드들을 모음 staticMethod나 classMethod이기 때문에 그냥 nameSpace개념임'''
    @staticmethod
    def notifyInfoUsingBeep(info_type):
        def normal():
            try:
                import winsound
            except ImportError:
                import os

                for i in range(1):
                    os.system('echo -ne "\a"')
                    time.sleep(1)
            else:
                for i in range(1):
                    winsound.Beep(300,500)
                    time.sleep(1)

        def warning():
            try:
                import winsound
            except ImportError:
                import os
                for i in range(2):
                    os.system('echo -ne "\a"')
                    time.sleep(0.3)
            else:
                for i in range(2):
                    winsound.Beep(300, 500)

        if info_type == "normal":
            notify = threading.Thread(target=normal,)
        elif info_type == "warning":
            notify = threading.Thread(target=warning, )
        notify.start()

    @staticmethod
    def groupingFiatNumber(num):
        '''fiat(통화 moneyRole 200000 -> 200,000 으로 자릿수 구분해서 돌려줌'''
        return '{:20,}'.format(num)
        # return  '{:20,.2f}'.format(num) //->소수점 두 자릿수 까지 표시

    @staticmethod
    def processView(pairingObj,*atraBot_baseInfos_dict, **view_index):
        '''# atrabot info style 들을 받아서 #dataFrame으로 뱉어낸다 pandasview에서 쓰임 
        atrabot info style 이라 함은 각각의  commodity_role_coin 이 key  인 하나의 정보 set을 의미
        commodity_role_coin 하나가 column이 되고  정보set 은 (ex. ask_lowest_krwPrice) index가 된다
        즉  {"ask_lowest_krwPrice" : BTC: ~ , ETH : ~, ETC,   ......   } 같은것이 list로 있는것을 뜻함 
        '''
        commodityRole_coin_sequence = pairingObj.paired_commodityRole_coin_sequence

        # 복수의 키워드로 받은 view_index를 빼줌
        view_index = view_index["view_index"]

        # 만약 여러개가 들어온다면 합쳐줌
        all_info_dict = {}
        for i, info in enumerate(atraBot_baseInfos_dict):
            all_info_dict.update(info)

        # 요놈은 view_index순으로 만들어지고 결국 그 순서대로  index가 dataFrame에 뿌려짐
        all_infoList_bySequence = []

        # view_index 만들기 위해 쓴것임 필요할때마다 쓰면됨(이거로 뽑아서 복붙)
        # all_info_keys = [key for key in all_info_dict]
        # print(all_info_keys)

        # 내가 지정한 view_index 순서대로 만들어진 list가 df에 쓰인다
        for info in view_index:
            all_infoList_bySequence.append(all_info_dict[info])

        df_info = pd.DataFrame(all_infoList_bySequence, columns=commodityRole_coin_sequence, index=view_index,dtype=float)

        return df_info

    #api 로 땡겨오는 json  내용은 모두 unicode로 되어 있기에 이를 utf-8로 재귀적으로 바꿔주는 함수
    @staticmethod
    def convert(input):

        if isinstance(input, dict):
            return {AtraUTIL.convert(key): AtraUTIL.convert(value) for key, value in input.iteritems()}
        elif isinstance(input, list):
            return [AtraUTIL.convert(element) for element in input]
        elif isinstance(input, unicode):
            input = input.encode('utf-8')
            #//->utf-8로 변환한뒤 각 형식에 맞추어 int float str로 변환하는 로직
            def parseType(input):
                try:
                    return float(input)
                except ValueError as e:
                    try:
                        return int(input)
                    except ValueError as e:
                        return str(input)
            return parseType(input)
        else:
            return input
    # convert = staticmethod(convert)

    #이걸 만약에 ExchangePairing에서만 쓴다면 다시 ExchangePairing의 내부 함수로 옮길것
    @staticmethod
    def calculateTradingGreedily(exchange_name,exchange_money_role ,trade_type, commodity_role_coin_type, money_role_sum=0, commodity_role_coin_amount=0):
        '''정확한 계산을 위해서는 greedy하게 order layer를 넘어가며 계산을 해야함 살때와 팔때 무슨 수치를 넣어야 하는지 꼭명심
        buy 할때는 당연히 money_role인 화폐가 필요하고  sell할때는 팔고자하는 commodity_role_coin_amount 가 필요하다
        '''
        # 이런식으로 class로 안 감싸면 inner function Scope에서 참조못함(정확히 말하자면 i의 변동을 추적못한다는뜻임
        # inner function 호출시 만들어진 외곽 변수 값만 접근 가능) 안에서는 꼭 local.~ 붙여야함
        class local:
            #orderPrice_for_actualTrade
            theoretical_greedy_traded_result = {"layer_trade_info": [], "traded_price_average": None, "traded_quantity_sum": None
                , "orderPrice_for_actualTrade": None, "is_layer_outOfLength": False}
            i = 0

        if trade_type == "buy":
            if money_role_sum == 0:
                raise Exception("when Buy , it should have money_roleSum")
            #buy 상황이므로 orderBook의 ask 를 불러옴
            order_book = SharedStorage.orderbook_eachExchange[exchange_name][exchange_money_role][commodity_role_coin_type]["asks"]
            orderBook_layerLength = len(order_book)
            # print(order_book)
            def calculateOrderLayer(money_for_calculating):
                i = local.i
                # print(i, local.theoretical_greedy_traded_result)
                #처음은 ask lowest price이며 i 가 늘어날수록 차순위 거래대상 layer로 이동
                layer_price = float(order_book[i]["price"])
                layer_quantity = float(order_book[i]["quantity"])

                # 하나의 layer를 전부 사는데 필요한 화폐역할의 총액
                money_for_allCoin_inALayer = layer_price * layer_quantity
                #현재 layer로 넘어온 buyMoney로 살수 있는 coin
                available_amount = money_for_calculating / layer_price
                #ask lowest 기준으로 따지면 전체 buyMoney 로 살수 있는 coin과 현재 layer의 coin 갯수를 비교해서 충분히 살수 있을때를
                #뜻함, 당연히 다음 layer로 넘어 가야함(더 비싼 가격을 주고서라도 buyMoney를 쓸려면 더사야 하므로)
                if layer_quantity < available_amount:
                    # 현재 ask layer는 전부 구매 하였으므로 남는돈으로 더 구매 해야함 이론적 구매 결과는
                    #  layer 전체를 theoretical_greedy_traded_result[layer_trade_info][i]에 넣어줌
                    local.theoretical_greedy_traded_result["layer_trade_info"].append({"price": layer_price, "quantity": layer_quantity})
                    # 현재 layer 위에 다음 orderBook layer 가 있다면 i 를 늘려주고 호출하고 아니라면 마지막 layer를 더 사용함(계속될 수 있음)
                    if local.i < (orderBook_layerLength - 1):
                        local.i = local.i + 1
                        calculateOrderLayer(money_for_calculating - money_for_allCoin_inALayer)
                    elif local.i == (orderBook_layerLength - 1):
                        local.theoretical_greedy_traded_result["is_layer_outOfLength"] = True
                        # print(exchange_name, commodity_role_coin_type, local.i, local.theoretical_greedy_traded_result["is_layer_outOfLength"])

                else:
                    #이전 layer를 모두 구매하고 넘어돈(혹은 최초layer계산시) buyMoney로 현재 layer의 coin을 전부 살수 없을
                    # 때임 따라서 이번 layer에서 살수있을만큼 산 뒤 greedy한 계산은 종료 됨(물론 ask lowest에서 한방에 종료될수 있음)
                    layer_conclusion_quantity = money_for_calculating / layer_price
                    local.theoretical_greedy_traded_result["layer_trade_info"].append({"price": layer_price, "quantity": layer_conclusion_quantity})

            calculateOrderLayer(money_role_sum)
            theoretical_greedy_traded_result = local.theoretical_greedy_traded_result

            #theoretical 일때는 최초 구매목적으로 넘어온 money_role_sum과 대부분 동일하다.
            #그러나 orderBook layer가 몇개 없어서 모두 사용을 못할경우(ex.bithumb은 ALL 일결우 coin당 5개 호가창밖에 안줌)
            #마지막 layer까지 다 쓴 수치만큼만 빼서 초기 넘어온 buy_role_sum 에서 얼마나 하락 하는지를 확인후
            #그 %만큼 calculateCostAndProfit의 initial Money를 재조정하고 해야함
            theoretical_buyMoneyRole_sum_afterTrade =sum([dictObj["price"] * dictObj["quantity"] for i, dictObj  in enumerate(theoretical_greedy_traded_result["layer_trade_info"])])
            #이론상 orderBook을 통해서 moneyRole_sum 으로 구매한 총 코인의 갯수
            # print("decreasing rate when buy",commodity_role_coin_type ,money_role_sum, theoretical_buyMoneyRole_sum_afterTrade, theoretical_buyMoneyRole_sum_afterTrade/money_role_sum)
            decreasing_rate = theoretical_buyMoneyRole_sum_afterTrade/money_role_sum
            theoretical_coin_sum_afterTrade = sum([ dictObj["quantity"] for i, dictObj  in enumerate(theoretical_greedy_traded_result["layer_trade_info"])])
            #코인의 갯수로 나눠진  평균 매수가격, layer quantity에 따른 가중평균이 된다

            theoretical_average_price_afterGreedy = theoretical_buyMoneyRole_sum_afterTrade / theoretical_coin_sum_afterTrade

            theoretical_greedy_traded_result["traded_price_average"] = theoretical_average_price_afterGreedy
            theoretical_greedy_traded_result["traded_quantity_sum"] = theoretical_coin_sum_afterTrade
            #실제 주문할때 ask lowest가 아닌 이론적으로 greedy하게 계산해서 가장끝까지 간 가격 즉 buy상황에선 먹어 들어간 layer중 가장 비싼 가격을
            #불러야 금액을 다쓰고 계산된 코인을 살 수 있다(물론 그럼에도 불구하고  실제 거래에선 달라질수 있음)
            layer_trade_info = theoretical_greedy_traded_result["layer_trade_info"]
            theoretical_greedy_traded_result["orderPrice_for_actualTrade"] = \
                theoretical_greedy_traded_result["layer_trade_info"][len(layer_trade_info) - 1]["price"]
            # greedilly 하게 계산후 어느정도 깊이까지 파고 들어 갔는지임 orderBook_layerLength는 api로 받은 orderBook의 full 깊이임
            theoretical_greedy_traded_result["used_layer_length"] = len(layer_trade_info)
        elif trade_type == "sell":
            if commodity_role_coin_amount == 0:
                raise Exception("when sell , it should have commodity_role_coin_amount!")
            # sell 상황이므로 orderBook의 bids 를 불러옴
            order_book = SharedStorage.orderbook_eachExchange[exchange_name][exchange_money_role][commodity_role_coin_type]["bids"]
            orderBook_layerLength = len(order_book)
            def calculateOrderLayer(commodityCoin_for_layer_calculating):
                i = local.i
                # print(i, local.theoretical_greedy_traded_result)
                #첫 layer는 bid highest price 이며 i 가 늘어날수록 사겠다는 가격이 낮은 차순위 매수자 layer로 이동
                layer_price = float(order_book[i]["price"])
                layer_quantity = float(order_book[i]["quantity"])

                if layer_quantity < commodityCoin_for_layer_calculating :
                    # 현재 layer에서 모든 코인을 다 팔고도 남는 코인을 더 팔아야 할 때임 따라서 모든 코인을 팔고 다음 layer로 넘어 가야함
                    local.theoretical_greedy_traded_result["layer_trade_info"].append({"price": layer_price, "quantity": layer_quantity})
                    #현재 layer 위에 다음 orderBook layer 가 있다면 i 를 늘려주고 호출하고 아니라면 마지막 layer를 더 사용함(계속될수 있음)
                    if local.i < (orderBook_layerLength - 1):
                        local.i = local.i + 1
                        calculateOrderLayer(commodityCoin_for_layer_calculating - layer_quantity)
                    elif local.i == (orderBook_layerLength -1):
                        # print(exchange_name, commodity_role_coin_type, local.i,
                        #       local.theoretical_greedy_traded_result["is_layer_outOfLength"])
                        local.theoretical_greedy_traded_result["is_layer_outOfLength"] = True

                else :
                    #현재 layer에서 넘어온 모든 코인을 매도할 수 있을 때임 따라서 이번 layer에서 팔수 있을만큼 판 뒤 greedy한 계산은 종료됨(물론 bid highest에게 한방에 팔 수 있음)
                    #(layer_conclusion_quantity = layer_quantity - commodityCoin_for_layer_calculating)  <- 이값은 필요없음
                    #즉 위의 값이 result 에 들어가는게 아니라 넘어온 값 그 자체가 팔린것 이므로 result에는 이전 layer에서 넘어온 값이 들어가야함
                    local.theoretical_greedy_traded_result["layer_trade_info"].append({"price": layer_price, "quantity": commodityCoin_for_layer_calculating})

            calculateOrderLayer(commodity_role_coin_amount)
            theoretical_greedy_traded_result = local.theoretical_greedy_traded_result

            #판매가 모두 종료된후 orderBook을 greedy하게 계산하여 도출해낸 이론적인 총 판매액  poloniex같이 coin by coin exchange의 경우
            #만약 inCoin 이 countercoin 으로써 ETH이고 outCoin이 baseCoin이자 화폐역할인 BTC라면  sellMoney는 화폐역할인 BTC가 된다
            theoretical_sellMoney_sum_afterTrade =  sum([dictObj["price"] * dictObj["quantity"] for i, dictObj  in enumerate(theoretical_greedy_traded_result["layer_trade_info"])])
            #이론상으론 당연히 최초 넘어온 commodity_role_coin_amount 과 동일하다 그러나 orderBook layer갯수의 한계로 달라질수 있으며 이것으로 decreasing rate를 만든다다
            theoretical_coin_sum_afterTrade = sum([dictObj["quantity"] for i, dictObj in enumerate(theoretical_greedy_traded_result["layer_trade_info"])])
            decreasing_rate = theoretical_coin_sum_afterTrade / commodity_role_coin_amount
            # print("decreasing rate when sell",commodity_role_coin_type, commodity_role_coin_amount, theoretical_coin_sum_afterTrade,
            #       theoretical_coin_sum_afterTrade / commodity_role_coin_amount)
            # 코인의 갯수로 나눠진  평균 매수가격, layer quantity에 따른 가중평균이 된다
            # print(exchange_name, trade_type, commodity_role_coin_type, money_role_sum, commodity_role_coin_amount)
            theoretical_average_price_afterGreedy = theoretical_sellMoney_sum_afterTrade / commodity_role_coin_amount
            theoretical_greedy_traded_result["traded_price_average"] = theoretical_average_price_afterGreedy
            theoretical_greedy_traded_result["traded_quantity_sum"] = theoretical_coin_sum_afterTrade
            theoretical_greedy_traded_result["traded_sellMoney_sum"] = theoretical_sellMoney_sum_afterTrade

            # 실제 주문할때 bid highest가 아닌 이론적으로 greedy하게 계산해서 가장끝까지 간 가격 즉 sell상황에선 들어간 layer중 가장 싼가격을
            # orderPrice_for_actualTrade 을 불러야 코인을 다 팔고 계산된 sellPrice를 거머쥘 수 있다(물론 그럼에도 불구하고 달라질수 있음)
            layer_trade_info = theoretical_greedy_traded_result["layer_trade_info"]
            theoretical_greedy_traded_result["orderPrice_for_actualTrade"] = \
                theoretical_greedy_traded_result["layer_trade_info"][len(layer_trade_info) - 1]["price"]
            #greedilly 하게 계산후 어느정도 깊이까지 파고 들어 갔는지임 orderBook_layerLength는 api로 받은 orderBook의 full 깊이임
            theoretical_greedy_traded_result["used_layer_length"] = len(layer_trade_info)
        #종종 계산때문에 decreasing이 필요없는데 0.99999999999 가찍히는경우 때문에 0.99이하만 orderBook 문제가 있다고 판단
        if theoretical_greedy_traded_result["is_layer_outOfLength"] == True and decreasing_rate < 0.99:
            #무조건 반내림을 해서 소숫점 네자릿수까지지 반환
           return decreasing_rate
        else :
            return local.theoretical_greedy_traded_result

    @classmethod
    def makeAtraStyleBalance(cls, exchange_name, raw_balance_info):
        '''각 exchange에서 받아오는 balance를 AtraStyle로 재 구성한다'''
        utf8_converted_balance_result = cls.convert(raw_balance_info)
        roleList_eachExchange = SharedStorage.coin_roleList_eachExchange[exchange_name]
        timestamp = time.time()

        #balance는 commodity_role_coin이든 moneyRole이든 모든 것들에 대한 정보가 필요 하므로
        #SharedStorage.coin_roleList 의 구조(즉 {money_role : [CR,CR]}에 상관없이 exchange내의  krw든 btc든 eth든 모든것을 하나의 list로 만들어준다
        #money_role과 commodity_coin 이 하나의  [] 안에 들어감[krw: "~", BTC: "~", ETH : "~" , ~]
        flatten_allCurrencyList_inExchange = []
        #money_role은  교환가능매개체인 Btc,eth 가 될 수도 있고 krw나 usdt 모두가능
        for money_role, commodity_role_list in roleList_eachExchange.iteritems():
            if money_role != "fiat":
                if money_role not in flatten_allCurrencyList_inExchange:
                    flatten_allCurrencyList_inExchange.append(money_role)
                for commodity_coin in commodity_role_list:
                    if commodity_coin not in flatten_allCurrencyList_inExchange:
                        flatten_allCurrencyList_inExchange.append(commodity_coin)
        atraStyle_balance = {}

        if exchange_name == "poloniex":
            #currency 는 Fiat , moneyRoleCoin, commodity_roleCoin 을 모두 포함함
            for currency in flatten_allCurrencyList_inExchange:
                atra_currency_balance = {"available" : 0, "btcValue" : 0}
                if currency in utf8_converted_balance_result.keys():
                    exchange_currency_balance_available = utf8_converted_balance_result[currency]["available"]
                    exchange_currency_balance_btcValue = utf8_converted_balance_result[currency]["btcValue"]
                    atra_currency_balance["available"] = exchange_currency_balance_available
                    atra_currency_balance["btcValue"] = exchange_currency_balance_btcValue
                atraStyle_balance[currency] = atra_currency_balance

        elif exchange_name == "bithumb":
            data = utf8_converted_balance_result["data"]
            for currency in flatten_allCurrencyList_inExchange:
                atra_currency_balance = {"available": 0, "btcValue": 0}
                if "available_" + str.lower(currency) in data.keys():
                    #btcValue나 fiatValue는 일단 보류
                    atra_currency_balance["available"] = data["available_" + str.lower(currency)]
                atraStyle_balance[currency] = atra_currency_balance
            # print(atraStyle_balance)

        elif exchange_name == "bitfinex":
            atraStyle_balance = {}
            for currency in flatten_allCurrencyList_inExchange:
                coin_lower_chr = str.lower(currency)
                #bitfinex는  balance 호출하면 전체 coin balance가 오는게 아니라 한번이라도 deposit 되었던 coin만 오기 때문에
                #일단 coin Role list 에 있는 모든 코인은  available 0 으로 기본적으로 만들어 놓는다
                atraStyle_balance[currency] = {"available": 0, "btcValue": 0}

                for balance_perCoin in utf8_converted_balance_result:
                    coinName_in_result = balance_perCoin["currency"]
                    #bitfinex dash 심볼은  dash 가 아니라 dsh 이므로 바꿔줌
                    if coinName_in_result == "dsh" : coinName_in_result = "dash"
                    coinAvailable_in_result = balance_perCoin["available"]
                    if coin_lower_chr == coinName_in_result:
                        atraStyle_balance[currency]["available"] = coinAvailable_in_result
                        
        elif exchange_name == "bittrex":
            atraStyle_balance = {}
            for coin in flatten_allCurrencyList_inExchange:
                atraStyle_balance[coin] = {"available": 0, "btcValue": 0}

                for balance_perCoin in utf8_converted_balance_result:
                    coinName_in_result = balance_perCoin["Currency"]
                    coinAvailable_in_result = balance_perCoin["Available"]
                    if coin == coinName_in_result:
                        atraStyle_balance[coin]["available"] = coinAvailable_in_result

        atraStyle_balance["timestamp"] = timestamp
        return atraStyle_balance

    @classmethod
    def makeAtraStyleOrderBook(cls, exchange_name, raw_orderBook):
        '''각 exchange에서 받아오는 orderBook을 AtraStyle로 재 구성한다'''
        utf8_converted_orderBook = cls.convert(raw_orderBook)
        roleList_eachExchange = SharedStorage.coin_roleList_eachExchange[exchange_name]
        timestamp = time.time()

        #얘안에 각각의 moneyrole이 dict key 로  들어가고 각각
        atraStyle_orderBook = {}

        if exchange_name == "bitfinex":
            pass

        elif exchange_name == "poloniex":
            # money_role == 각 exchange의 moneyrole들임
            for money_role, commodity_coin_list in roleList_eachExchange.iteritems():

                atraStyle_orderBook_in_eachBaseCoin = {}
                for coin_type in commodity_coin_list:
                    if money_role + "_" + coin_type in utf8_converted_orderBook.keys():

                        new_in_CoinPair_data = {}
                        # 얘안에 {asks:[[price,volume],[..] ,...[..]] , bids :[[],[] ,...[]]} 식으로 구성
                        raw_in_CoinPair_data = utf8_converted_orderBook[money_role + "_" + coin_type]
                        for key, in_asks_bids in raw_in_CoinPair_data.iteritems():
                            # 중간에 isfrozen 같은 정수형 값이 들어가 있음
                            if isinstance(in_asks_bids, list):
                                new_asks_bids = []
                                # poloniex는 orderbook price quantity가 list로 되어있음 이걸 {price: ~, quantity: ~}식으로 바꿈꿈
                                for i, raw_price_quantity_list in enumerate(in_asks_bids):
                                    new_price_quantity_dict = {}
                                    new_price_quantity_dict["price"] = float(raw_price_quantity_list[0])
                                    new_price_quantity_dict["quantity"] = float(raw_price_quantity_list[1])
                                    new_asks_bids.append(new_price_quantity_dict)
                                new_in_CoinPair_data[key] = new_asks_bids
                        atraStyle_orderBook_in_eachBaseCoin[coin_type] = new_in_CoinPair_data
                atraStyle_orderBook[money_role] = atraStyle_orderBook_in_eachBaseCoin

        elif exchange_name == "bithumb":
            #bithumb는  반환값 {"data"} key에 담아져서 옴
            atraStyle_orderBook_in_eachBaseCoin = utf8_converted_orderBook["data"]
            for i, item in atraStyle_orderBook_in_eachBaseCoin.iteritems():
                if not isinstance(item, list):
                    del(item)
            #bithumb 에서는 money_role이 원화(\)밖에 없으므로
            atraStyle_orderBook["KRW"] = atraStyle_orderBook_in_eachBaseCoin
        atraStyle_orderBook["timestamp"] = timestamp
        atraStyle_orderBook["used"] = False

        return atraStyle_orderBook

    @classmethod
    def makeAtraStyleTradeResult(cls, exchange_name, raw_result, ordered_price ,ordered_amount, ordered_money_role, ordered_commodity_role, trade_type, client_api = None):
        is_trade_succeed = False
        converted_raw_result = AtraUTIL.convert(raw_result)
        atraStyle_result = {}
        ordered_amount = float(ordered_amount)

        # print("converted_raw_result :" + str(converted_raw_result))
        if exchange_name == "poloniex":
            #poloniex에서 거래성공 판단기준            amountUnfilled
            if "error" not in converted_raw_result.keys() :
                if len(converted_raw_result["resultingTrades"]) != 0 :
                    atraStyle_result["is_succeeded"] = is_trade_succeed = True
                    atraStyle_result["order_id"] = converted_raw_result["orderNumber"]
                    #detailed_result , overall_result
                    #amount = commodityRole amount , total = moneyRole total
                    overall_result = {"amount" : 0, "price_rate" : 0, "total" : 0}
                    overall_amount = 0
                    overall_priceRate_sum = 0
                    overall_total = 0
                    detailed_resultLength = len(converted_raw_result["resultingTrades"])
                    for i, detail_layer in enumerate(converted_raw_result["resultingTrades"]):
                        overall_amount = overall_amount + detail_layer["amount"]
                        #이름을 맞추기 위해서 바꾼다
                        detail_layer["price_rate"] = detail_layer.pop("rate")
                        overall_priceRate_sum +=  detail_layer["price_rate"]
                        overall_total = overall_total + detail_layer["total"]

                    atraStyle_result["detailed_result"] = converted_raw_result["resultingTrades"]
                    overall_result["amount"] = overall_amount
                    overall_result["price_rate"] = overall_priceRate_sum / detailed_resultLength
                    overall_result["total"] = overall_total

                    if overall_amount / ordered_amount > 0.5:
                        atraStyle_result["overall_result"] = overall_result
                    else:
                        '''fillorkill 이나 시장가 trade를 한다면  부분거래가 없겠지만 immediateOrCancel 같은식의 거래가 이루어 진다면
                     부분거래가 이루어 진다 근데 80%이하로 ordered_amount 가 거래 됐다면 예측과 많이 빗나갔다는 뜻 이므로 거래를 실패했다고 판단한다 
                     '''
                        atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                      "raw_result": raw_result,
                                      "raw_message" : "order : " + str(ordered_amount) + ",filled : " + str(overall_amount) + ", lower than 50%", "retryable": True, "is_fatal_error" : False}
                else:
                    '''가격 급변동으로 주문넣은 orderPrice로 전혀 구매가 안될때(라고 추정함) resultingTrades': []식으로 옴'''
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                  "raw_result": raw_result, "raw_message" : "resultingTrades': []", "retryable": True, "is_fatal_error" : False}
                #exchange_fail에서 주문부분 성공 외에는 모두 아래 else 문에서 처리함
            else:
                #그 외의 에러메세지는 다음과 같은 식 으로 옴
                err_message = converted_raw_result["error"]
                print("err_message", err_message)
                print("enough", err_message.find("enough"))
                print("Unable", err_message.find("Unable"))
                print("to", err_message.find("to"))
                print("fill", err_message.find("fill"))
                print("'Unable to fill", err_message.find("Unable to fill"))
                if err_message.find("Unable to fill") >= 0: # {  'error': 'Unable to fill order completely.'} 일때임 retryable 가능
                    print("catched on Unable to fill")
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                  "raw_result": converted_raw_result, "raw_message" : err_message ,"retryable": True, "is_fatal_error" : False}
                elif err_message.find("enough") >= 0 : #{u'error': u'Not enough BTC.'} 일때임  뭔가 잘못 됐으므로 후처리 후 종료시킴
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                  "raw_result": converted_raw_result, "raw_message" : err_message ,"retryable": False, "is_fatal_error" : True}

                else:
                    print("unknown exchange_fail")
                    #확인되지 않은 에러임 후처리후 종료시킴
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                  "raw_result": converted_raw_result, "raw_message": err_message, "retryable": False, "is_fatal_error" : True}


        elif exchange_name == "bithumb":
            #todo:이거확인
            #bithumb에서 거래성공 기준
            if converted_raw_result["status"] == 0:
                atraStyle_result["is_succeeded"] = is_trade_succeed = True
                atraStyle_result["order_id"] = converted_raw_result["order_id"]
                #detailed_result , overall_result
                #amount = commodityRole amount , total = moneyRole total
                overall_result = {"amount" : 0, "price_rate" : 0, "total" : 0}
                overall_amount = 0
                overall_priceRate_sum = 0
                overall_total = 0
                detailed_resultLength = len(converted_raw_result["data"])
                for i, detail_layer in enumerate(converted_raw_result["data"]):
                    overall_amount +=  detail_layer["units"]
                    #이름이 key가 "price"로 담아져서 오니 detail도 "price_rate"로 바꿔줌
                    detail_layer["price_rate"] = detail_layer.pop("price")
                    overall_priceRate_sum += detail_layer["price_rate"]
                    overall_total = overall_total + detail_layer["total"]

                atraStyle_result["detailed_result"] = converted_raw_result["data"]
                overall_result["amount"] = overall_amount
                overall_result["price_rate"] = overall_priceRate_sum / detailed_resultLength
                overall_result["total"] = overall_total
                atraStyle_result["overall_result"] = overall_result

            else:
                #에러메세지는 다음과 같은 식 으로 옴 한글이므로 convert안시킴
                err_message = raw_result["message"]
                utf8_converted_err_message = converted_raw_result["message"]
                if converted_raw_result["status"] == 5600 or converted_raw_result["status"] == "5600":
                    print('err_message.find("9900")', err_message.find("9900"))
                    if err_message.find("9900") >= 0 or err_message.find("9999") >= 0:
                        # print(type(converted_raw_result["status"]), type(raw_result["status"]))
                        #{u'status': u'5600',u'message': u'\uc7a0\uc2dc \ud6c4 \uc774\uc6a9\ud574 \uc8fc\uc2ed\uc2dc\uc624.[9900]'} //->잠시후이용해 주세요
                        atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                      "raw_result": raw_result, "raw_message": err_message, "retryable": True, "is_fatal_error" : False}
                    elif utf8_converted_err_message.find("try again") >= 0:  #{  u'message': u'Please try again', u'status': u'5600'} 한글에서 바뀐것으로 추정
                        atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                            "raw_result": raw_result, "raw_message": err_message, "retryable": True,
                                            "is_fatal_error": False}
                    elif err_message.find(u"\uc2dc\ub3c4\ud574 \uc8fc\uc138\uc694") >= 0:  #{  u'message': u'Please try again', u'status': u'5600'} 한글에서 바뀐것으로 추정
                        atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                            "raw_result": raw_result, "raw_message": err_message, "retryable": True,
                                            "is_fatal_error": False}


                    else:
                        # 확인되지 않은 에러임 후처리후 종료시킴
                        #todo :일단 9900(잠시후 이용해 주세요) 외에는 모두 retryable False로 두고 후처리후 종료시킴
                        atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                      "raw_result": raw_result, "raw_message": err_message, "retryable": False, "is_fatal_error" : True}
                else:
                    # 확인되지 않은 에러임 후처리후 종료시킴
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                  "raw_result": raw_result, "raw_message": err_message, "retryable": False, "is_fatal_error" : True}

        elif exchange_name == "bitfinex":
            # bitfinex 이 좆같은곳은 단순 order result로 거래 성공을 판단할 수 있으므로 추가 정보를 다시 요구 해야함 아 개빡치네진짜
            order_id = converted_raw_result["order_id"]
            client_api = SharedStorage.client_api_eachExchange[exchange_name]["restful"]
            more_specific_trade_result = None
            #0.5초정도 쉴것
            time.sleep(0.5)
            # order 주문이 완료된 상황에서(성공했던 실패했던 network는 작동되었음)
            # 일단 5번까지는 무조건 more_specific_trade_result 를 가져 올 수 있다고 가정
            print("fingding order_id" + str(order_id))
            for i in range(20 ):

                try:
                    time.sleep(i)
                    rgParams = {
                        "order_id": order_id,
                    };
                    more_specific_trade_result = client_api.api_query("order/status", rgParams)
                    converted_raw_specific_result = AtraUTIL.convert(more_specific_trade_result)
                    #좆같은 bifinex는  fill-or-kill 이라도 전체 거래가 취소 되는걸 확인하기 까지 시간이 조금 걸리기 때문에
                    #아직 어떻게 됐는지 확인이 안됐을 경우(결정이 안됐을경우)  is_cancelled는  False 지만 excuted_amount는 0인 상황이 발생함 이때는 아직 fill-or-kill이 어떻게
                    #됐는지 모르니 다시 질의함 즉 fill-or-kill이 실패했을경우는 무조건  is_cancelled는 True가 되며  executed_amount 0이됨
                    if converted_raw_specific_result["is_cancelled"] == False and converted_raw_specific_result["executed_amount"] == 0:
                       logging_atraCycle.warning("bit finex trading is not concluded , need more information")
                       #서버가 바빠지면 bitfinex에서 문제(order status 를 요청해도 계속 404 505 에러를 뱉어냄)가 발생하기에 딜레이를 계속 늘림

                       continue
                except urllib2.HTTPError as err:
                    print(err)
                    continue
                except Exception as err:
                    print(err)
                    continue
                else:
                    break

            # 거래는 성공했으나 more specific trade result 불러오는걸 실패할 수도 있다
            if more_specific_trade_result == None:
                logging_atraCycle.warn("more_specific_trade_result has faild, you should check it manually")
                os._exit(1)

            logging_atraCycle.info('more_specific_trade_result')
            logging_atraCycle.info(pformat(more_specific_trade_result, indent=3))
            excuted_amount = converted_raw_specific_result["executed_amount"]
            if excuted_amount != 0:
                if excuted_amount / float(ordered_amount) > 0.1:
                    # 거래가 성공한 상황임
                    atraStyle_result["is_succeeded"] = is_trade_succeed = True
                    atraStyle_result["order_id"] = converted_raw_specific_result["id"]
                    # bitfinex는 greedy하게 거래를 해도 여러 layer에 걸친 체결 result를 제공하지 않고
                    # 그냥 평균 체결가격과 commodity_coin의체결수량을 제공
                    overall_result = {"amount": 0, "price_rate": 0, "total": 0}
                    # 이용된 commodity_role 수량
                    overall_amount = converted_raw_specific_result["executed_amount"]
                    overall_priceRate = converted_raw_specific_result["avg_execution_price"]
                    # 관련된 money_role 수량
                    overall_total = round(overall_amount * overall_priceRate, 8)

                    atraStyle_result["detailed_result"] = converted_raw_specific_result
                    overall_result["amount"] = overall_amount
                    overall_result["price_rate"] = overall_priceRate
                    overall_result["total"] = overall_total
                    atraStyle_result["overall_result"] = overall_result
                    #종종 이 좆같은 bitfinex는 fill-or-kill 로 order를 냈는데도 아주 적은 일부양이 체결되는 경우가 있음
                else :
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                        "raw_result": converted_raw_specific_result, "raw_message": "only too little amount was excuted",
                                        "retryable": True, "is_fatal_error": False}
            elif excuted_amount == 0:
                '''가격 급변동으로 주문넣은 orderPrice로 전혀 구매가 안될때(라고 추정함) executed_amount :0 로 옴'''
                atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                    "raw_result": converted_raw_specific_result, "raw_message": "", "executed_amount": 0, "retryable": True,
                                    "is_fatal_error": False}

            elif more_specific_trade_result == None:
                #trade order의 network는 성공했으느 more specific request의 network가 실패한 경우임 거래가 성공했는지 안했는지 알 수가 없음 일단 종료시키는 걸로
                atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                    "raw_result": "//->", "raw_message": "//-> bitfinex more specific data requst failed", "retryable": False,
                                    "is_fatal_error": True}


        # client에서 받은시간 찍어줌
        atraStyle_result["client_timestamp"] = time.time()
        atraStyle_result["exchange"] = exchange_name
        atraStyle_result["ordered_money_role"] = ordered_money_role
        atraStyle_result["ordered_commodity_role"] = ordered_commodity_role
        atraStyle_result["trade_type"] = trade_type
        # print("AtraStyle tradeResult : "  + str(atraStyle_result))
        return atraStyle_result

    @classmethod
    def makeAtraStyleTransferResult(cls,raw_result, fromExchange, toExchange,coin_type,
                                     willBe_decreased_amuntOn_FROMEXCHANGE, willBe_deposited_amountOn_TOEXCHANGE):
        '''withdraw 결과를 atraStyle로 바꾼다 poloniex 와 bithumb은'''
        atraStyle_result = {}
        converted_raw_result = AtraUTIL.convert(raw_result)
        atraStyle_result["raw_result"] = converted_raw_result

        if fromExchange == "poloniex":
            if "error" not in converted_raw_result.keys():

                atraStyle_result["is_succeeded"] = True
                # withdraw성공했으므로 willBe_decreased_amountOn_FROMEXCHANGE 는 decreased_amountOn_FROMEXCHANGE 가 됨
                atraStyle_result["decreased_amountOn_FROMEXCHANGE"] = willBe_decreased_amuntOn_FROMEXCHANGE
                atraStyle_result["willBe_deposited_amountOn_TOEXCHANGE"] = willBe_deposited_amountOn_TOEXCHANGE
            else:
                err_message = converted_raw_result["error"]
                print("err_message", err_message)
                if err_message.find("temporarily disabled") != -1:  #{u'error': u'Withdrawals for this currency are temporarily disabled.'} 일때
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                        "raw_result": raw_result,
                                        "raw_message": err_message , "retryable": False,
                                        "is_fatal_error": True}
                else : #확인되지 않은 에러이므로 종료시킴
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                        "raw_result": raw_result,
                                        "raw_message": err_message, "retryable": False,
                                        "is_fatal_error": True}

        elif fromExchange == "bithumb":
            if converted_raw_result["status"] == 0:
                atraStyle_result["is_succeeded"] = True
                # withdraw성공했으므로 willBe_decreased_amountOn_FROMEXCHANGE 는 decreased_amountOn_FROMEXCHANGE 가 됨
                atraStyle_result["decreased_amountOn_FROMEXCHANGE"] = willBe_decreased_amuntOn_FROMEXCHANGE
                atraStyle_result["willBe_deposited_amountOn_TOEXCHANGE"] = willBe_deposited_amountOn_TOEXCHANGE
            else:
                # bithumb 에러메세지는 다음과 같은 식 으로 옴 한글이므로 convert안시킴
                #{u'status': u'5600', u'message': u'\ucd9c\uae08\uc561\uc774 \ucd9c\uae08\uac00\ub2a5\uc561\uc744 \ucd08\uacfc\ud558\uc600\uc2b5\ub2c8\ub2e4. \ub4f1\ub85d\ud560 \uc218  \uc5c6\uc2b5\ub2c8\ub2e4.'}
                err_message = raw_result["message"]
                if converted_raw_result["status"] == 5600 or converted_raw_result["status"] == "5600":
                    # {u'status': u'5600', u'message': u'\ucd9c\uae08\uc561\uc774 \ucd9c\uae08\uac00\ub2a5\uc561\uc744 \ucd08\uacfc\ud558\uc600\uc2b5\ub2c8\ub2e4. \ub4f1\ub85d\ud560 \uc218  \uc5c6\uc2b5\ub2c8\ub2e4.'}
                    #출금액이 출금가능액을 초과하였습니다. 등록할 수 없습니다. -> atraCycle 반영이 늦게 되서 안될수도 있으므로 retryable 가능하게 하고 모두 실패하면 fatal_error로 대응할것
                    print('err_message.find(u"\ucd9c\uae08\uc561\uc774 \ucd9c\uae08\uac00\ub2a5\uc561\uc744")', err_message.find(u"\ucd9c\uae08\uc561\uc774 \ucd9c\uae08\uac00\ub2a5\uc561\uc744"))
                    if err_message.find(u"\ucd9c\uae08\uc561\uc774 \ucd9c\uae08\uac00\ub2a5\uc561\uc744") >= 0:
                        # print(type(converted_raw_result["status"]), type(raw_result["status"]))
                        #{u'status': u'5600',u'message': u'\uc7a0\uc2dc \ud6c4 \uc774\uc6a9\ud574 \uc8fc\uc2ed\uc2dc\uc624.[9900]'} //->잠시후이용해 주세요
                        atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                      "raw_result": raw_result, "raw_message": err_message, "retryable": True, "is_fatal_error" : True}
                    else:
                        # 5600 내에서 확인되지 않은 에러임 후처리후 종료시킴
                        atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                      "raw_result": raw_result, "raw_message": err_message, "retryable": False, "is_fatal_error" : True}
                else:
                    #5600외는 모두 확인되지 않았으므로 후처리후 종료
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                  "raw_result": raw_result, "raw_message": err_message, "retryable": False, "is_fatal_error" : True}

        elif fromExchange == "bitfinex":
            converted_raw_result = converted_raw_result[0]
            if "status" in converted_raw_result.keys():
                print(converted_raw_result["status"])
                # bitfinex transfer 성공기준
                if converted_raw_result["status"] == "success":
                    atraStyle_result["raw_result"] = converted_raw_result
                    atraStyle_result["is_succeeded"] = True
                    atraStyle_result["decreased_amountOn_FROMEXCHANGE"] = willBe_decreased_amuntOn_FROMEXCHANGE
                    atraStyle_result["willBe_deposited_amountOn_TOEXCHANGE"] = willBe_deposited_amountOn_TOEXCHANGE
                #status에 다른 내용이 있을때
                else:
                    atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                        "raw_result": converted_raw_result, "raw_message": "", "retryable": True,
                                        "is_fatal_error": True}
            else:
                #status 자체도 오지 않을때
                atraStyle_result = {"is_succeeded": False, "failed_category": "EXCHANGE_FAIL",
                                    "raw_result": converted_raw_result, "raw_message": "", "retryable": False,
                                    "is_fatal_error": True}
            #todo :제대로 고칠것
        # 아래내용은 실패하던 성공하던 똑같이 들어감
        atraStyle_result["client_timestamp"] = time.time()
        atraStyle_result["fromExchange"] = fromExchange
        atraStyle_result["toExchange"] = toExchange
        atraStyle_result["coin_type"] = coin_type
        atraStyle_result["is_succeeded"] = False if atraStyle_result["is_succeeded"] != True else True
        return atraStyle_result

    @classmethod
    def threadController(cls,controll_name,thread_name=""):
        '''쓰레드에 대한 정보를 추출하거나 관리하는 메소드 더 나은것이 있다면 교체할것'''
        if controll_name == "find":
            if thread_name in [i.name for i in threading.enumerate()]:
                # print(str(threading.enumerate()) + " founded!!")
                return True
            else:
                # print(thread_name + " couldnt be founded!!")
                return False
        if controll_name == "show":
            return [i for i in threading.enumerate()]
    @classmethod
    def extractConvertibleSymbolInfo(cls, exchange, coin_symbol):
        '''atraCommon Symbol과  exchange 내부의 symbol이 다른경우  ex) bitfinex 내부 심볼은 DASH, atraCommon은 DSH임
        호출하면 commonSymbol과 exchange Symbol을 반환시킴'''
        atraCommon_coin_symbol = coin_symbol
        exchange_coin_symbol = coin_symbol
        convertible_symbol = SharedStorage.convertible_symbol_eachExchage

        if exchange in convertible_symbol:
            convertible_symbol_inExchange = convertible_symbol[exchange]

            #common 심볼이 오던  exchange심볼이 오던 ex) DASH 가 오던 DSH가 오던  convertible dict에 있기만 하면 둘 모두를 반환시킴
            for atraCommon_symbol, exchange_symbol in convertible_symbol_inExchange.iteritems():
                if coin_symbol == atraCommon_symbol or coin_symbol == exchange_symbol:
                    atraCommon_coin_symbol = atraCommon_symbol
                    exchange_coin_symbol = exchange_symbol
                    break

        # print(atraCommon_coin_symbol, exchange_coin_symbol)
        return atraCommon_coin_symbol, exchange_coin_symbol
    @classmethod
    def detectProperApiClientType(cls, exchange_name, role_name):
        exchange_apiClient_type =  SharedStorage.api_client_info[exchange_name]["type"]
        apiClient_type, api_client = None, None

        if len(exchange_apiClient_type) == 1:
            #api_client type이 한 종류 밖에 없으므로 그 한종류 반환
            apiClient_type = exchange_apiClient_type[0]
        else :
            #shared.json 에 role 항목 자체가 정의되지 않았거나 해당 role이 role항목 안에 정의 되지 않았다면 무조건 "restful" 을 반환
            if "role" not in SharedStorage.api_client_info[exchange_name] or role_name not in SharedStorage.api_client_info[exchange_name]["role"]:
                apiClient_type = "restful"
            else :
                #정의되어 있다면 각 role에 걸맞는 api_type 반환
                apiClient_type = SharedStorage.api_client_info[exchange_name]["role"][role_name]

        return apiClient_type, SharedStorage.client_api_eachExchange[exchange_name][apiClient_type]

    @classmethod
    def putAtraModuleData(self, destination, data):
        """
        :param destination: ex, "atraCycle_DB  same with module name
        :param data: dict or list to be putted in DataBase
        """
        atra_module_interactor = SharedStorage.atra_module_interactor
        # print("atra_module_interactor", atra_module_interactor,
        #       str([DB_client for DB_client in atra_module_interactor.atra_DB_client]))
        if atra_module_interactor != None and destination in [DB_client for DB_client in
                                                                 atra_module_interactor.atra_DB_client]:
            # print("trying to put queue")
            atraCycleData_for_dbPut = {"destination": destination, "data": data}
            atra_module_interactor.shared_queue.put(atraCycleData_for_dbPut)
        return atra_module_interactor



class AtraRepetitiveInformer:
    '''orderBook 이나 balance등을 지속적으로 호출시킴 atraStart에서 쓰레드 생성해서 호출시킴'''
    @classmethod
    def eachExchangeBalanceInfoBot(cls, exchange_name, repeat, delay=0):
        '''현재 각 코인의 계좌 잔액정보를 주기적으로 불러옴 thread처리를 할꺼면 무조건 repeat은 True'''
        # exchange_client_api = SharedStorage.client_api_eachExchange[exchange_name]
        exchange_clientApi_type, exchange_client_api = AtraUTIL.detectProperApiClientType(exchange_name,"balance")
        ## ------------------bithumb용 balance 함수------------------------------------------------##
        def balanceFrom_bithumb(client_api):

            rgParams = {
                "currency": "ALL"
            };
            balance_result = client_api.xcoinApiCall("/public/balance/", rgParams);
            if "data" in balance_result.keys():
                atra_style_balanceInfo = AtraUTIL.makeAtraStyleBalance(exchange_name, balance_result)
            else:
          #ALL로 안 뽑아지면 아래껄로 바꿀것
                #원래 위에처럼 ALL 로 뽑을수 있었으나  갑자기 안되서 {'data': {'available_btc': u'0.51075666',
              # 'available_dash': u'0.01412469', 'available_etc': u'0.21256927', 'available_eth': u'0.00000372',
              # 'available_ltc': u'0.08343243', 'available_xrp': u'0.00000000'}} 하나씩 뽑아서 예전 ALL이랑 비슷하게 만듬
                coinList = SharedStorage.coin_roleList_eachExchange[exchange_name]["KRW"]
                balance_result_sum = {"data" : {}}
                for coin in coinList:
                    rgParams = {
                        "currency": coin
                    };
                    balance_result = client_api.xcoinApiCall("/public/balance", rgParams);
                    # print(balance_result)
                    balance_result_sum["data"]["available_" + str.lower(coin)] = \
                        {key: value for key, value in balance_result["data"].iteritems() if key == "available_" + str.lower(coin)}["available_"+ str.lower(coin)]
                atra_style_balanceInfo = AtraUTIL.makeAtraStyleBalance(exchange_name, balance_result_sum)
            return atra_style_balanceInfo
            # SharedStorage.orderbook_eachExchange[exchange_name] = atra_style_orderBook
        ##------------------------------end----------------------------------------------------##

        ##------------------------------poloniex용 balance 함수----------------------------##
        def balanceFrom_poloniex(client_api):
            balance_result = client_api.api_query('returnCompleteBalances')
            atra_style_balanceInfo = AtraUTIL.makeAtraStyleBalance(exchange_name, balance_result)
            # print(atra_style_balanceInfo)
            # SharedStorage.orderbook_eachExchange[exchange_name] = atra_style_orderBook
            return atra_style_balanceInfo
        ##------------------------------end----------------------------------------------------##

        ##------------------------------bitfinex용 balance 함수----------------------------##
        def balanceFrom_bitfinex(client_api):
            balance_result = client_api.api_query("balances", {})
            atra_style_balanceInfo = AtraUTIL.makeAtraStyleBalance(exchange_name, balance_result)
            # print(atra_style_balanceInfo)
            # SharedStorage.orderbook_eachExchange[exchange_name] = atra_style_orderBook
            return atra_style_balanceInfo
        ##------------------------------end----------------------------------------------------##

        ##------------------------------bitfinex용 balance 함수----------------------------##
        def balanceFrom_bittrex(client_api):
            balance_result = client_api.query("getbalances")
            atra_style_balanceInfo = AtraUTIL.makeAtraStyleBalance(exchange_name, balance_result)
            # print(atra_style_balanceInfo)
            # SharedStorage.orderbook_eachExchange[exchange_name] = atra_style_orderBook
            return atra_style_balanceInfo
        ##------------------------------end----------------------------------------------------##

        if repeat:
            while True:
                try:
                    atra_style_orderBook = locals()["balanceFrom_" + exchange_name](exchange_client_api)
                    SharedStorage.setExchangeBalance(exchange_name, atra_style_orderBook)

                except (urllib2.HTTPError, ValueError) as  err:
                    logging_normal.warning("\n-----err on :" + exchange_name + "--------\n" + str(traceback.format_exc()))
                    logging_normal.info(exchange_name + " balance info retry...\n")

                    # urllib2(ex. poloniex) 의경우 timeout Exeption이
                    #  socket.error로 예외를 뱉어냄 pycurl 의 경우 pycurl.error  code 28로
                except (socket.error,  pycurl.error) as err:
                    logging_normal.warning("\n-----err on :" + exchange_name + "--------\n" + str(traceback.format_exc()))
                    logging_normal.info(exchange_name + " balance info  retry...\n")
                except KeyError as err:
                    #빗썸 api server에 문제가 생겨서 data가 오지 않을 경우 이므로 딜레이를 주고 제시작 시킴
                    logging_normal.warning(
                        "\n-----err on :" + exchange_name + "--------\n" + str(traceback.format_exc()))
                    logging_normal.info(exchange_name + " balance info  retry in 60sec...\n")
                    time.sleep(60)
                time.sleep(delay)
        else:
            return locals()["balanceFrom_" + exchange_name](exchange_client_api)

    @classmethod
    def eachExchangeOrderBookInfoBot(cls, exchange_name, repeat, delay=0):
        '''thread 처리를 할꺼면  무조건 repeat을 True로 보낼것 dealy는 초단위'''
        # exchange_client_api = SharedStorage.client_api_eachExchange[exchange_name]
        #websocket 인지 restful 인지 결정
        exchange_clientApi_type, exchange_client_api = AtraUTIL.detectProperApiClientType(exchange_name, "orderbook")

        if exchange_clientApi_type == "restful":

            ## ------------------빗썸용 orderbook 함수------------------------------------------------##
            # io인지 e 인지만 넣어서 호출하면 global에서 어떤 exchange인지 알아서 함수찾아 함수를 리턴하는식으로 만들것 다른 모든 로직을 이런식으로
            # orderbook 은 trade 가격 환산할때 greedy하게 해야 하므로 형식을 통일시키기 위해서 AtraStyleMaker거쳐서 저장되도록 할것
            def orderBookFrom_bithumb(client_api):
                rgParams = {
                };
                orderbook_all_result = client_api.xcoinApiCall("/public/orderbook/ALL", rgParams);
                atra_style_orderBook = AtraUTIL.makeAtraStyleOrderBook(exchange_name, orderbook_all_result)
                # SharedStorage.orderbook_eachExchange[exchange_name] = atra_style_orderBook
                # SharedStorage.setExchangeOrderBook(exchange_name,atra_style_orderBook)
                return atra_style_orderBook
            ##------------------------------end----------------------------------------------------##

            ##------------------------------poloniex용 orderBook 함수----------------------------##
            def orderBookFrom_poloniex(client_api):
                orderbook_all_result = client_api.api_query('returnOrderBook',{"currencyPair": "all","depth": 20})
                atra_style_orderBook = AtraUTIL.makeAtraStyleOrderBook(exchange_name, orderbook_all_result)

                # SharedStorage.orderbook_eachExchange[exchange_name] = atra_style_orderBook
                return atra_style_orderBook
                # SharedStorage.setExchangeOrderBook(exchange_name, atra_style_orderBook)

            ##------------------------------end----------------------------------------------------##

            ##------------------------------bitfinex용 orderBook 함수----------------------------##
            '''bitfinex는 orderBook을 ALL로 불러올 수 없기 때문에 coin별 thread 로 동시에 불러와서 
            합치는 식으로 감 thread 공유공간(orderbook_eachExchange_temp)을 미리 정의 했으며 atraStyle까지 여기서 다 만듬
            '''
            def orderBookFrom_bitfinex(client_api):
                #bitfinex나 bittrex처럼 orderBook을 coin별로 따로만 제공할 경우 atra 형식에 맞춰서 전체를 받은뒤 계산에 들어가야한다
                #개별 coin orderBook을  thread  이용해서 따로 받아올때 쓰일 공유공간 이 필요한데 orderbook_eachExchange_temp 가 그에 쓰인다  \
                #또한 dict는 immutable 하지 않기 때문에 thread 가 동시에 접근가능하다, 즉 lock 불필요
                if exchange_name not in SharedStorage.orderbook_eachExchange_temp.keys() :
                    SharedStorage.orderbook_eachExchange_temp[exchange_name] = {}

                def threadfy_eachCoin(rgParams, money_role, commodityRole_coin):
                    bitfinex_orderBook_temp = SharedStorage.orderbook_eachExchange_temp[exchange_name]
                    orderBookresult = client_api.api_query("book", rgParams)

                    # bitfinex는 수량 key name을 "amount" 라고 명명하므로 "quantity로 바꿔줌
                    for order_list in orderBookresult.itervalues():
                        for order in order_list:
                            order["quantity"] = order.pop("amount")

                    if money_role not in bitfinex_orderBook_temp.keys():
                        bitfinex_orderBook_temp[money_role] = {}
                    bitfinex_orderBook_temp[money_role][commodityRole_coin] = orderBookresult

                bitfinex_coinList = SharedStorage.coin_roleList_eachExchange[exchange_name]
                bitfinex_orderBook_temp = SharedStorage.orderbook_eachExchange_temp[exchange_name]
                # orderBook 형태의 빈 뼈대임 bitfinex_coinList와 같음
                bitfinex_all_orderBook_frame = {}
                eachCoin_thread_list = []
                timestamp = time.time()
                # time.sleep(3.5)
                for money_role, commodity_list in bitfinex_coinList.iteritems():
                    if money_role != "fiat":
                        bitfinex_all_orderBook_frame[money_role] = []

                        for coin in commodity_list:
                            # if coin == "DASH": coin = "DSH"  <- 필요없으면 삭제
                            atra_common_symbol, exchange_symbol = AtraUTIL.extractConvertibleSymbolInfo("bitfinex", coin)
                            symbol_pair = exchange_symbol + money_role
                            rgParams = {
                                "symbol": symbol_pair,
                                "limit_bids": "20",
                                "limit_asks": "20",
                                "group": 0
                            };
                            # if coin == "DSH": coin = "DASH" <- 필요없으면 삭제
                            bitfinex_all_orderBook_frame[money_role].append(atra_common_symbol)
                            eachCoin_thread = threading.Thread(name=str(timestamp) + symbol_pair, target=threadfy_eachCoin, args=(rgParams, money_role, atra_common_symbol,))
                            eachCoin_thread_list.append(eachCoin_thread)


                for eachCoin_thread in eachCoin_thread_list:
                    eachCoin_thread.start()
                    time.sleep(0.1)

                # 다 종료될 때까지 기다림 당연히 개별 쓰레드가 reponse 성공적으로 받은후 or Exception 발생했을때까지임
                for eachCoin_thread in eachCoin_thread_list:
                    eachCoin_thread.join()
                # print("join end time difference", timestamp - time.time())
                # print("threading :", str(threading.enumerate()), len(threading.enumerate()))
                # thread를 열어서 보낸 것들이 다 들어왔는지 검증
                # print("bitfinex_all_orderBook_frame", bitfinex_all_orderBook_frame)
                is_unchecked_founded = False
                for money_role_frame, commodity_list_frame in bitfinex_all_orderBook_frame.iteritems():
                    if money_role_frame != "fiat":
                        if money_role_frame not in bitfinex_orderBook_temp.keys():
                            is_unchecked_founded = True
                            break
                        else:
                            for commodity_coin_inFrame in commodity_list_frame:
                                if commodity_coin_inFrame not in bitfinex_orderBook_temp[money_role_frame].keys():
                                    is_unchecked_founded = True
                                    break
                            break
                # print("is_unchecked_founded : ", is_unchecked_founded)
                if is_unchecked_founded == True:
                    #하나라도 문제가 발생해서 못받아오면  exception  발생시킴(재시작됨)
                    print("orderbook concurrent failed, restarting it after 30 sec")
                    time.sleep(30)
                    raise ValueError("orderbook concurrent failed ")

                else:
                    timestamp = time.time()
                    # pprint.pprint(bitfinex_orderBook_temp)
                    bitfinex_all_orderBook = SharedStorage.orderbook_eachExchange_temp[exchange_name]
                    utf8_converted_orderBook = AtraUTIL.convert(bitfinex_all_orderBook)
                    #makeAtraStyle을 거치지 않으므로 바로 반환시킴
                    atra_style_orderBook = utf8_converted_orderBook
                    atra_style_orderBook["timestamp"] = timestamp
                    SharedStorage.orderbook_eachExchange_temp[exchange_name] = {}
                    # pprint(atra_style_orderBook)
                    return atra_style_orderBook
                ##------------------------------end----------------------------------------------------##

            ##------------------------------bittrex용 orderBook 함수----------------------------##
            '''bittrex는 orderBook을 ALL로 불러올 수 없기 때문에 coin별 thread 로 동시에 불러와서 
            합치는 식으로 감 thread 공유공간(orderbook_eachExchange_temp)을 미리 정의 했으며 atraStyle까지 여기서 다 만듬
            '''

            def orderBookFrom_bittrex(client_api):
                # bitfinex나 bittrex처럼 orderBook을 coin별로 따로 thread  이용해서 따로 받을때 쓰일 공유공간  \
                # dict는 immutable 하지 않기 때문에 thread 가 동시에 접근가능하다, 즉 lock 불필요
                if exchange_name not in SharedStorage.orderbook_eachExchange_temp.keys():
                    SharedStorage.orderbook_eachExchange_temp[exchange_name] = {}

                def threadfy_eachCoin(rgParams, money_role, commodityRole_coin):
                    bittrex_orderBook_temp = SharedStorage.orderbook_eachExchange_temp[exchange_name]
                    orderBookresult = client_api.query("getorderbook", rgParams)

                    if "buy" in orderBookresult.keys() and "sell" in orderBookresult.keys():
                        buy_list = orderBookresult["buy"]
                        sell_list = orderBookresult["sell"]
                        # 너무많으므로 잘라주고(거의 1000개이상씩 옴)
                        del buy_list[15:]
                        del sell_list[15:]
                        # orderBook 수량 이름이 Quantity이므로 바꿔줌
                        for i in range(len(buy_list)):
                            buy_list[i]["quantity"] = buy_list[i].pop("Quantity")
                            buy_list[i]["price"] = buy_list[i].pop("Rate")
                            sell_list[i]["quantity"] = sell_list[i].pop("Quantity")
                            sell_list[i]["price"] = sell_list[i].pop("Rate")

                        orderBookresult["bids"] = orderBookresult.pop("buy")
                        orderBookresult["asks"] = orderBookresult.pop("sell")

                        if money_role not in bittrex_orderBook_temp.keys():
                            bittrex_orderBook_temp[money_role] = {}
                        bittrex_orderBook_temp[money_role][commodityRole_coin] = orderBookresult

                bittrex_coinList = SharedStorage.coin_roleList_eachExchange[exchange_name]
                bittrex_orderBook_temp = SharedStorage.orderbook_eachExchange_temp[exchange_name]
                # orderBook 형태의 빈 뼈대임 bittrex_coinList와 같음
                bittrex_all_orderBook_frame = {}
                eachCoin_thread_list = []
                timestamp = time.time()
                #서버부하때문에 추가로 delay를 줌
                time.sleep(2)
                for money_role, commodity_list in bittrex_coinList.iteritems():
                    if money_role != "fiat":
                        bittrex_all_orderBook_frame[money_role] = []

                        for coin in commodity_list:
                            symbol_pair = money_role + "-" + coin
                            rgParams = {
                                "market": symbol_pair,
                                "type": "both"
                            };
                            bittrex_all_orderBook_frame[money_role].append(coin)
                            eachCoin_thread = threading.Thread(target=threadfy_eachCoin, args=(rgParams, money_role, coin,))
                            eachCoin_thread_list.append(eachCoin_thread)
                            eachCoin_thread.start()

                # 다 종료될 때까지 기다림 당연히 개별 쓰레드가 reponse 성공적으로 받은후 or Exception 발생했을때까지임
                for eachCoin_thread in eachCoin_thread_list:
                    eachCoin_thread.join()

                # print("join end time difference", timestamp - time.time())
                # print("threading :", str(threading.enumerate()), len(threading.enumerate()))
                # thread를 열어서 보낸 것들이 다 들어왔는지 검증
                # print("bittrex_all_orderBook_frame", bittrex_all_orderBook_frame)
                is_unchecked_founded = False
                for money_role_frame, commodity_list_frame in bittrex_all_orderBook_frame.iteritems():
                    if money_role_frame != "fiat":
                        if money_role_frame not in bittrex_orderBook_temp.keys():
                            is_unchecked_founded = True
                            break
                        else:
                            for commodity_coin_inFrame in commodity_list_frame:
                                if commodity_coin_inFrame not in bittrex_orderBook_temp[money_role_frame].keys():
                                    is_unchecked_founded = True
                                    break
                            break
                # print("is_unchecked_founded : ", is_unchecked_founded)
                if is_unchecked_founded == True:
                    # 하나라도 문제가 발생해서 못받아오면  exception  발생시킴(재시작됨)
                    print("orderbook concurrent failed, restarting it after 30 sec")
                    time.sleep(30)
                    raise ValueError("orderbook concurrent failed ")
                else:
                    timestamp = time.time()
                    # pprint.pprint(bittrex_orderBook_temp)
                    bittrex_all_orderBook = SharedStorage.orderbook_eachExchange_temp[exchange_name]
                    utf8_converted_orderBook = AtraUTIL.convert(bittrex_all_orderBook)
                    # makeAtraStyle을 거치지 않으므로 바로 반환시킴
                    atra_style_orderBook = utf8_converted_orderBook
                    atra_style_orderBook["timestamp"] = timestamp
                    SharedStorage.orderbook_eachExchange_temp[exchange_name] = {}
                    # pprint(atra_style_orderBook)
                    return atra_style_orderBook
                    ##------------------------------end----------------------------------------------------##
        #websocket으로 해당 role을 지정했을경우 websocket 으로 호출
        elif exchange_clientApi_type == "websocket":

            def orderBookFrom_bitfinex(client_api):
                wss = client_api
                bitfinex_coinList = SharedStorage.coin_roleList_eachExchange[exchange_name]
                oldest_timestamp_thisCall_inWss = None

                if exchange_name not in SharedStorage.orderbook_eachExchange_temp.keys():
                    SharedStorage.orderbook_eachExchange_temp[exchange_name] = {}
                # time.sleep(3.5)
                now_timestamp = time.time()

                # 아직 원인은 못 찾았지만 btfxwss 에서 10초이상 websocket data receive를 못받으면 계속 업데이트를 못시키므로 wss를 한번 restart를 해줌
                # btfxwss restart 사용하지 말것
                # if "timestamp" in SharedStorage.orderbook_eachExchange_temp[exchange_name]:
                #     if now_timestamp - SharedStorage.orderbook_eachExchange_temp[exchange_name]["timestamp"] > 15:
                #         logging_notify.info("btfxWss went wrong , restart it now")
                #         wss.stop()
                #         time.sleep(5)
                #         # 아예 새로 커넥션을 만들어 버림
                #         wss = SharedStorage._initExchangeClientApi(exchange_name, SharedStorage.keys, "websocket")
                #
                #         SharedStorage.orderbook_eachExchange_temp[exchange_name] = {}
                #
                #         time.sleep(5)
                for money_role, commodity_list in bitfinex_coinList.iteritems():
                    if money_role != "fiat":
                        # bitfinex_all_orderBook_frame[money_role] = []
                        # print(money_role, commodity_list)
                        for coin in commodity_list:
                            atra_common_symbol, exchange_symbol = AtraUTIL.extractConvertibleSymbolInfo("bitfinex", coin)
                            symbol_pair = exchange_symbol + money_role


                            # print([chan_val[1]["pair"] if chan_val[1]["channel"] else None for chan_id, chan_val in \
                            #        wss.channel_labels.iteritems()] )
                            # if not wss.books[symbol_pair].asks():  //-> 이걸로 하면 heartbeat초과댔을때 갱신을 못 시키므로 channel_labels여부로 바꿈
                            if len(wss.channel_labels) == 0 or \
                                            symbol_pair not in [chan_val[1]["pair"] if chan_val[1]["channel"] else None
                                                                for chan_id, chan_val in wss.channel_labels.iteritems()]:
                                # bitfinex_all_orderBook_frame[money_role].append(atra_common_symbol)
                                wss.order_book(symbol_pair)

                                # btfxwss 객체에 해당 symbol_pair가 성립되어 있는지 확인한뒤 넘어감
                                while not wss.books[symbol_pair].asks():
                                    logging_normal.debug("waiting subscribed websocket orderBook data " + str(
                                        symbol_pair) + " in " + exchange_name)
                                    time.sleep(1)
                            # print([key for key in wss.books])

                            orderBookresult = {"asks": wss.books[symbol_pair].asks(),
                                               "bids": wss.books[symbol_pair].bids()}

                            # pprint(orderBookresult)

                            bitfinex_orderBook_temp = SharedStorage.orderbook_eachExchange_temp[exchange_name]
                            if money_role not in bitfinex_orderBook_temp.keys():
                                bitfinex_orderBook_temp[money_role] = {}
                            #atraStyle 변환없이 websocket 내에서 바로 atraStyle로 변환 시켰음
                            bitfinex_orderBook_temp[money_role][coin] = orderBookresult
                            symbol_timestamp = wss.books[symbol_pair].latest_ts


                             #이번 호출에 들어가는 symbol별 timestamp 중 가장 늦은걸  orderBook 데이터의 timestamp로 넣어주기 위한 작업
                            oldest_timestamp_thisCall_inWss = symbol_timestamp if oldest_timestamp_thisCall_inWss == None or oldest_timestamp_thisCall_inWss > symbol_timestamp else oldest_timestamp_thisCall_inWss
                            # print(symbol_timestamp,oldest_timestamp_thisCall_inWss)
                            print(now_timestamp - oldest_timestamp_thisCall_inWss)
                            if now_timestamp - oldest_timestamp_thisCall_inWss > 13 :
                                logging_notify.info("btfxWss went wrong , restart it now")
                                wss.cmd_q.put("restart")
                                time.sleep(5)

                atra_style_orderBook = SharedStorage.orderbook_eachExchange_temp[exchange_name]
                atra_style_orderBook["timestamp"] = oldest_timestamp_thisCall_inWss
                # print("SharedStorage orderBook" ,atra_style_orderBook["USD"]["BTC"]["asks"][:3])
                # print(oldest_timestamp_thisCall_inWss)
                # pprint(atra_style_orderBook["USD"]["BTC"])
                return atra_style_orderBook
                # print(SharedStorage.orderbook_eachExchange_temp["bitfinex"]["USD"]["BTC"])




        if repeat:

            while True:
                try:
                    atra_style_orderBook = locals()["orderBookFrom_" + exchange_name](exchange_client_api)
                    SharedStorage.setExchangeOrderBook(exchange_name, atra_style_orderBook)
                except (urllib2.HTTPError, ValueError) as  err:
                    logging_notify.warning("-----err on :" + exchange_name + "--------1\n" + str(traceback.format_exc()))
                    logging_normal.info(exchange_name + " orderBook info retry...")
                    # urllib2(ex. poloniex) 의경우 timeout Exeption이
                    #  socket.error로 예외를 뱉어냄 pycurl 의 경우 pycurl.error  code 28로
                except (socket.error, pycurl.error) as err:
                    logging_notify.warning("-----err on :" + exchange_name + "--------1\n" + str(traceback.format_exc()))
                    logging_normal.info(exchange_name + " orderBook info  retry...")
                except KeyError as err:
                    # 빗썸 api server에 문제가 생겨서 data가 오지 않을 경우 이므로 딜레이를 주고 제시작 시킴
                    logging_normal.warning(
                        "\n-----err on :" + exchange_name + "--------3\n" + str(traceback.format_exc()))
                    logging_normal.info(exchange_name + " balance info  retry in 60sec...\n")
                    time.sleep(60)
                time.sleep(delay)

        else:
            return locals()["orderBookFrom_" + exchange_name](exchange_client_api)

    #pairing이 된후 base _coin과 counter coin들이  정해진뒤에 baseCoin ratio를 비교하는 목적이므로
    #pairing이 된후에 summarizeOrderBook 을 만드는 의미가 있다
    @staticmethod
    def summarizeOrderBook(pairingObj) :
        '''pairing된 두 거래소 각 counterCoin의 fiat 가격과 baseCoin price_rate 를 보여줌'''
        #현재 pairing 되어있는 각 exchangepairing을 하나씩 선택해서 돌리는데 이건 view에서 하는걸로

        summarizedOrderBook_inPairing = {"IOExchange" : {}, "EExchange" : {}}
        #각 pairing 내의 "ioexchange" 의 이름과  "EExchange" 의 exchange_name을 뽑아냄
        for key, exchange_name in pairingObj.pairing_exchangeName_dict.iteritems():

            #exchange이 IOExchange이냐 EExchange이냐에 따라서 arbitrage basecoin과 해당마켓의 moneyrol과
            #일치할수도 상이할수도 있음 ex. BTC를 매개로 각코인을 arbitrage할때 bithum의 moneyRole은(krw)
            #이지만 poloniex의 moneyrole은 BTC임  즉 poloniex 에선 moneyRole과 baseCoin이 일치하나 bithum은 아님
            money_role_inExchange = pairingObj.money_role_eachExchange[key]
            base_coin = pairingObj.money_role_eachExchange["EExchange"]
            fiat_inExchange = SharedStorage.coin_roleList_eachExchange[exchange_name]["fiat"]

            latest_orderBook_in_eachExchange = SharedStorage.orderbook_eachExchange[exchange_name]
            #coin by boin  거래가 가능한 exchange들
            if exchange_name == "poloniex" or exchange_name == "bitfinex" or exchange_name == "bittrex":
                # poloniex는 달러 가격은 ["USDT_COIN"]["last"] 으로 구하고  ratio는  ["{COIN}_{ALTCOIN{}"] 으로 구함
                timestamp = time.time()

                # 얘 안에 { ask_lowest_fiatPrice_perEachCoin : {BTC:~, ETH: ~, ..... }, exchange_basecoin_ratio:{BTC:~, ETH: ~, ..... }, } 이런식으로
                exchange_baseinfo = {}

                # 얘네들 안에 {BTC:~, ETH: ~, ..... } 각각의 값이 들어가서 baseinfo로 들어감
                ask_lowest_fiatPrice_perEachCoin = {}

                # poloniex 의 가격 체계는  coin By Coin 의 직접 거래가 가능하며, baseCoinAmount/counterCoinAmount
                # 로 counterCoin의 상대가치(poloniex에서의 화폐는 현재 BTC,ETH,XTR,USDT가 있음) 즉 가격을 나타낸다 이론참조
                ask_lowest_baseCoinRatioPrice_perEachCoin = {}  # ex 내가 baseCoin 으로 counterCoin을 살수있는 가격 중 가장 싼 order
                # 화폐를 기준으로 한 가격(상대가치) 은  krwPrice 로 했고 이건 baseCoin을 기반으로 한 가격이므로 baseCoinRatioPrice로 함
                bid_highest_baseCoinRatioPrice_perEachCoin = {}  # ex 내가 baseCoin을 받고 counterCoin을 팔수있는 counterCoint의 가장높은가격

                for commodity_role_coinType in pairingObj.paired_commodityRole_coin_sequence:
                    # doller 먼져
                    coin_list = []
                    if commodity_role_coinType in latest_orderBook_in_eachExchange[fiat_inExchange].keys():
                        ask_lowest_fiat = \
                            latest_orderBook_in_eachExchange[fiat_inExchange][commodity_role_coinType]["asks"][0]["price"]
                    else:
                        ask_lowest_fiat = None


                    # 실제 수익률을 구하기 위한 교환비(incoin과 outcoin이 정해졌을때구할수있는)
                    # 는 Profit 에서 orderBook 내용을 보고 구한다
                    if commodity_role_coinType in latest_orderBook_in_eachExchange[money_role_inExchange].keys():
                        ask_lowest_baseCoinRatioPrice = latest_orderBook_in_eachExchange[money_role_inExchange][commodity_role_coinType]["asks"][0]["price"]
                        bid_highest_baseCoinRatioPrice = latest_orderBook_in_eachExchange[money_role_inExchange][commodity_role_coinType]["bids"][0]["price"]
                        # 같은 coin 끼리는 당연히 비율이 1이므로 ex. BTC_BTC같은건 제공안함
                    elif commodity_role_coinType == money_role_inExchange:
                        ask_lowest_baseCoinRatioPrice = 1.0
                        bid_highest_baseCoinRatioPrice = 1.0
                        # 시장에 없는 코인일경우
                    else:
                        ask_lowest_baseCoinRatioPrice = None
                        bid_highest_baseCoinRatioPrice = None

                    ask_lowest_fiatPrice_perEachCoin[commodity_role_coinType] = ask_lowest_fiat

                    ask_lowest_baseCoinRatioPrice_perEachCoin[commodity_role_coinType] = ask_lowest_baseCoinRatioPrice
                    bid_highest_baseCoinRatioPrice_perEachCoin[commodity_role_coinType] = bid_highest_baseCoinRatioPrice

                # Coin By Coin 거래거 되는 poloniex같은 곳은 EExchange일때는 코인당 실물화폐 가치는 중요하지 않음
                exchange_baseinfo["exchangeName"] = exchange_name
                exchange_baseinfo["ask_lowest_fiatPrice_perEachCoin"] = ask_lowest_fiatPrice_perEachCoin
                exchange_baseinfo["ask_lowest_baseCoinRatioPrice_perEachCoin"] = ask_lowest_baseCoinRatioPrice_perEachCoin
                exchange_baseinfo["bid_highest_baseCoinRatioPrice_perEachCoin"] = bid_highest_baseCoinRatioPrice_perEachCoin

                #         여기서부터   [exchange_average: {BTC:~, ETH:~  .....}  , exchange_ratio : {BTC: ~, ETH : ~ } ]  이런식으로 뺄것
                #         bithumb도 역시 마찬가지임 나중에 poloniex_average같은것들이 index 가 되고 각 화폐약자가 column이 됨

            # coin by boin  거래가 불가능한 exchange들
            elif exchange_name == "bithumb":
                # 이상하게 timestamp 1000배 뿔려져서 옮 이런식으로 할것
                # date = float(orderbook_all_btmb["data"]["timestamp"]) / 1000.0;
                # date = time.strftime("%y%m%d %I:%M:%S", time.localtime(date))
                # print("bithumb sever  - " , str(date), "cnt = " + str(bit_server_cnt))
                timestamp = time.time()
                # print("bithumb client  - ", str(time.strftime("%y%m%d %H:%M:%S", time.localtime(timestamp))) + "cnt = " + str(bit_server_cnt))
                # print("bithumb client  - ", str(timestamp))
                print("-----------------")

                # 얘 안에 { exchange_prices : {BTC:~, ETH: ~, ..... }, exchange_basecoin_ratio:{BTC:~, ETH: ~, ..... }, } 이런식으로
                exchange_baseinfo = {}

                # 얘네들 안에 {BTC:~, ETH: ~, ..... } 각각의 값이 들어가서 baseinfo로 들어감
                ask_lowest_krwPrice_perEachCoin = {}
                bid_highest_krwPrice_perEachCoin = {}
                baseCoin_ratios_perEachCoin = {}

                # --counter coin과 나눠서 ratio를 계산하기 위해 쓰임 시장가격은 결국 살수 있는 가격이므로 ask lowest로 함
                # 이를 미리 구해둬야 나눌때 쓰임
                base_coin_price = latest_orderBook_in_eachExchange[money_role_inExchange][base_coin]["asks"][0]["price"]

                for commodity_role_coinType in pairingObj.paired_commodityRole_coin_sequence:

                    # baseCoin과 counterCoin과의 비율을 따질때  평균기준
                    # 사용자가 사는가격 임 view를 위해서는 이것 정도면 충분 정확한 수익률을 위해선 inCoin이냐 outCoin
                    #이냐에 따라서 calculate로직으로 통해 구하고 여기선 그냥 간단히 view를 위해서 구하는것임
                    ask_lowest_krwPrice = \
                    latest_orderBook_in_eachExchange[money_role_inExchange][commodity_role_coinType]["asks"][0]["price"]

                    # 원화는 float의미 없으므로 int로 바꾼다
                    ask_lowest_krwPrice_perEachCoin[commodity_role_coinType] = round(ask_lowest_krwPrice)

                    #base_coin 대비 각 commodity_role의 상대가치 물론 coinByCoin 거래가 가능한 exchange은 이렇게 구할필요없음
                    if commodity_role_coinType == base_coin:
                        baseCoin_ratios_perEachCoin[commodity_role_coinType] = 1
                    else:
                        # poloniex 에서처럼 직접 교환비를 제공하지 않으므로 이렇게 구하며 bithumb exchange에서 baseCoin은
                        # moneyrole이 아니므로 baseCoin ratio 변수명에  price를 넣지 않는다
                        # print(askBid_average, base_coin_price)
                        baseCoin_ratios_perEachCoin[commodity_role_coinType] = ask_lowest_krwPrice / base_coin_price

                exchange_baseinfo["exchangeName"] = exchange_name
                exchange_baseinfo["ask_lowest_krwPrice_perEachCoin"] = ask_lowest_krwPrice_perEachCoin
                exchange_baseinfo["bid_highest_krwPrice_perEachCoin"] = bid_highest_krwPrice_perEachCoin
                exchange_baseinfo["baseCoin_ratios_perEachCoin"] = baseCoin_ratios_perEachCoin
                # ---------------
            #io인지
            summarizedOrderBook_inPairing[key] = exchange_baseinfo

        return summarizedOrderBook_inPairing

class ExchangePairing:
    def __init__(self, base_coin, pairing_exchangeName_dict, money_role_eachExchange, atraCycle_setting_val):
        '''두 시장에서 moneyRole을 선택한뒤 결부시키는 클래스임 두 거래소의 관계에 관련된건 모두 이 객체에 의해서
            진행이 된다  base_coin과  money_role_eachExchange은 중복되는데 더블 체크를 위한것임
        '''
        #eExchange의
        #EExchange의 money role과 EExchange의 baseCoin 은 같은것을 가르키나 money role은 pairing neutral 한 의미이다 즉 하나의
        #거래소에서 거래의 도구인 화폐의 역할을 하고 있는것중 하나를 뜻한다.(krw usd BTC 실제화폐모두포함)  그러나 baseCoin은 paring이 성립된후
        #atra 과정에서 두 마켓을 오가는 coin 이며 하나의 exchange에서 화폐역할을 하고있는, 즉 counter coin과 반대되는 coin을 뜻함
        self.money_role_eachExchange = money_role_eachExchange
        if base_coin != money_role_eachExchange["EExchange"] : raise Exception("baseCoin and EExchange money role doesn't match")
        # 이걸 기준으로 모든 수익률과 in out 이 계산됨
        self.base_coin = base_coin

        #오직view를 위해서 간추린 최신 orderBook 중에 정보 몇개를 추려서 저장해 놓은것 view에서 호출 일단 jupyter pandas에서 이용
        self.summarized_orderBook_baseInfo_forView = {"IOExchange" : {}, "EExchange" : {}}

        #pairing으로 묶는 IOMarket EMarket별 거래소명
        self.pairing_exchangeName_dict = pairing_exchangeName_dict

        self.costAndProfit_basedOn_inOutCoin_latest = {"whenInCoin" : {}, "whenOutCoin" : {}}

        # 지금은 하나의 pair 당 하나의 atraCycle 점유를 허용하지만 추후에 투자금이 쌓이면commodityRole 코인 하나당
        # 거래를 허용하는 식  즉  {BTC : thread , ETH : thread, ETC : thread } 식으로  관리하는것도 생각해 볼것
        self.is_calculating_now = False
        self.is_atraCycle_now = False
        self.atraCycle_thread_event = None
        #trading Obj 에 얘네가 그대로 간다, THREASHOLD_PROFIT 은  threashold_rate는 넘겼는데 (atra cycle value 가 한 150000원 정도 인데
        #(spread가 너무 높아 한 150원 이득이 나와서 atra cycle이 진행되는 경우가 있다 이럴경우를 방지하기 위해 rate와 더불어 profit도 추가함 )
        self.atraCycle_setting_val = {"THREASHOLD_RATE" : 0,
                                      "THREASHOLD_PROFIT" : 0,
                                      "IS_ATRACYCLE_PERMITTED" : False,
                                      "PERMITTED_COIN": [],
                                      "COMMANDED_ATRA_CYCLE_VALUE" : 0,
                                      "TRANSFER_BAN" : []}
        #---------atraSettingVal --------------------
        #atraStart 에서 각 exchangePairing 객체 생성할때 인자로 보낸 atraCycle_setting_val을 pairing 객체의 전역 변수로 하나씩 집어 넣음
        for key, val in atraCycle_setting_val.iteritems() :
            # print(key, val)
            if key not in self.atraCycle_setting_val:
                raise Exception(key + "is not atraCycle_setting_val")
            self.atraCycle_setting_val[key] = val

        #여기서 부터는 transfer_ban에 대한것------
        trasfer_ban_list = self.atraCycle_setting_val["TRANSFER_BAN"]

        #모든 feeInfo중에 ioExchange랑 eExchange_obj 두곳만 뽑아내면 되지만 그냥 deepcopy로 pairingObj로 따로 뺀후 필요하면 조작
        self.feeInfo_atraPairing_specified = copy.deepcopy(SharedStorage.feeInfo_eachExchange)

        #이동금지 코인이 있다면 deepcopy로 복사한 feeInfo 에서 해당코인 transfer수수료를 0으로 만듬
        if len(trasfer_ban_list) > 0:
            for coin_name in trasfer_ban_list:
                # print(coin_name)
                self.feeInfo_atraPairing_specified[pairing_exchangeName_dict["IOExchange"]]["transferFee_coin"][coin_name] = 0
                self.feeInfo_atraPairing_specified[pairing_exchangeName_dict["EExchange"]]["transferFee_coin"][coin_name] = 0
        # ---------atraSettingVal --------------------
        self.paired_commodityRole_coin_sequence = self._init_pairing_coinRoleList()

        #만약 IOExchange와 EExchange의 MoneyRole이 상이 하다면 IOExchange에서 두번 거래되는 TRIANGULAR atra이고
        #같다면 NORMAL atra가됨(일반적인 arbitrage임)
        self.atra_type = "TRIANGULAR" if self.money_role_eachExchange["IOExchange"] != self.money_role_eachExchange["EExchange"] else "NORMAL"

        #ioExchangeMOneyrole_ioExchangeName__baseCoin__eExchangeName_eExchangeMoneyRole (ex. KRW_bithumb__BTC__poloniex_BTC) 이런식으로 만들어짐
        self.exchangePairing_name = self.money_role_eachExchange["IOExchange"] + "_" + self.pairing_exchangeName_dict["IOExchange"] + "__" + self.pairing_exchangeName_dict["EExchange"] + "_" + self.money_role_eachExchange["EExchange"]

        #exchange pairing이 하나 만들어 졌으므로 SharedStorage에 넣어줌
        SharedStorage.activated_pairingObj[self.exchangePairing_name] = self

        print(self.exchangePairing_name)
        pprint(self.atraCycle_setting_val)

    def _init_pairing_coinRoleList(self):
        '''pairing 된 exchange간의 commodiryRole List 정리가 __init__에서 하기에 너무 길어져서 따로뺌'''
        #SharedStorage에서 미리 정의된 exchange들의 coin_role 정 보로 paired된 공통 commodity Role을 뽑아낸다
        ioExchange_name = self.pairing_exchangeName_dict["IOExchange"]
        eExchange_name = self.pairing_exchangeName_dict["EExchange"]
        ioExchange_moneyRole = self.money_role_eachExchange["IOExchange"]
        eExchange_moneyRole = self.money_role_eachExchange["EExchange"]
        ioExchange_commodityRole_list = SharedStorage.coin_roleList_eachExchange[ioExchange_name][ioExchange_moneyRole]
        eExchange_commodityRole_list = SharedStorage.coin_roleList_eachExchange[eExchange_name][eExchange_moneyRole]

        common_commodityRole_coinList = []
        for i, ioExchange_commodityRole_coin in enumerate(ioExchange_commodityRole_list):
            for i, eExchange_commodityRole_coin in enumerate(eExchange_commodityRole_list):
                if ioExchange_commodityRole_coin == eExchange_commodityRole_coin :
                    common_commodityRole_coinList.append(ioExchange_commodityRole_coin)

        # # sequence에 base_coin 이 없더라도 coin _sequence에 넣어줌
        # #사실 이게 처음에 진짜 수익률 계산이 되는지 안되는지 의문이 있을때 한거라 현 시점에서 이게 필요하지 모르겠다
        # #없어도 되니까 필요없을때 되면 지울것   //-> 일단 실증적으로 검증했으니 필요없는 방향으로
        # if self.base_coin in common_commodityRole_coinList:
        #     baseCoin_position = common_commodityRole_coinList.index(self.base_coin)
        #     if baseCoin_position != 0:
        #         common_commodityRole_coinList.insert(0, common_commodityRole_coinList.pop(
        #             baseCoin_position))
        # else:
        #     common_commodityRole_coinList.insert(0, self.base_coin)

        print(self.base_coin, common_commodityRole_coinList)
        return common_commodityRole_coinList

    def _decidePriceRatioTurnedOrNot(self,inout_type, inCoin, outCoin,
                                     ioExchange_inTraded_result,
                                     ioExchange_outTraded_result,
                                     eExchangeTraded_result):
        '''시작하는 atraCycle Value 수준으로 theoritical 계산시에 in, out 방향에 따라 priceRatio는 한쪽Exchange가
        다른쪽 Exchange 에 비해서 작거나 크다 , greedy하게 atraCycle이 진행될경우 이러한 priceRatio(마지막 layer의 가격)
        가 역전될 수도 있는데 그렇다면 과도한 atraCycleValue가 투입된 것 이므로 이를 decreasing rate로 줄여서 다시 계산을 시킨다
        정확하진 않지만 어느정도 합리적인 조정로직으로 작동 시킬수 있을듯'''

        ioExchange_inTrade_orderPrice = ioExchange_inTraded_result["orderPrice_for_actualTrade"]
        usedLayerLength_for_ioExchange_inTrade = ioExchange_inTraded_result["used_layer_length"]
        ioExchange_outTrade_orderPrice = ioExchange_outTraded_result["orderPrice_for_actualTrade"]
        usedLayerLength_for_ioExchange_outTrade = ioExchange_outTraded_result["used_layer_length"]
        eExchange_trade_orderPriceRatio = eExchangeTraded_result["orderPrice_for_actualTrade"]
        usedLayerLength_for_eExchangeTrade = eExchangeTraded_result["used_layer_length"]

        ioExchange_name = self.pairing_exchangeName_dict["IOExchange"]
        eExchange_name = self.pairing_exchangeName_dict["EExchange"]

        latest_ioExchange_orderBook = SharedStorage.orderbook_eachExchange[ioExchange_name]
        latest_eExchange_orderBook = SharedStorage.orderbook_eachExchange[eExchange_name]
        ioExchange_moneyRole = self.money_role_eachExchange["IOExchange"]
        eExchange_moneyRole = self.money_role_eachExchange["EExchange"]

        # whenInCoin 일때는 baseCoin이, whenOutCoin일때는 counterCoin이 outCoin 이므로
        #before ioExchange_price Ratio를 구하기 위해 bid highest를 구한다
        outCoin_bid_highest_ioExchange_priceRatio = \
            latest_ioExchange_orderBook[ioExchange_moneyRole][outCoin]["bids"][0]["price"]
        #whenInCoin 일때는 counterCoin이 , whenOutCoin일때는 baseCoin이 inCoin이므로
        # ioExchange_price Ratio를 구하기 위해  ask lowest를 구한다
        inCoin_ask_lowest_ioExchange_priceRatio = \
            latest_ioExchange_orderBook[ioExchange_moneyRole][inCoin]["asks"][0]["price"]

        #최종적으로 이 before와 after를 비교하되 priceRatio는 counterCoin이 in 이냐 out이냐에 따라서 bid_highest인지 ask_lowest인지 나눠지며
        # atra_type 이 triangular 인지 normal인지에 따라서 ioExchange의 경우 baseCoin과 counterCoin의 ratio로구해진다
        before_ioExchange_priceRatio = ioExchange_firstLayer_price = 0
        before_eExchange_priceRatio = eExchange_firstLayer_price = 0

        #triangular일때 ioExchange의 priceRatio는 in과 out의 가격비율로 구해진다
        after_ioExchange_priceRatio = ioExchange_orderPriceRatio = 0
        after_eExchange_priceRatio = eExchange_trade_orderPriceRatio

        #whenInCoin 일때는 counterCoin 기준으로 eExchange가 커야함  symbol <
        profitable_symbol = ""

        if inout_type == "whenInCoin":
            profitable_symbol = "<"
            #whenInCoin이므로 counterCoin(inCoin)_ask_lowest_ioExchange_priceRatio / baseCoin(outCoin)_bid_highest_ioExchange_priceRatio
            #triangular 하에서 ioExchange에서는 FIAT를 매개로 BTC 마켓의 가격체계와 유사한 유사한 priceRatio(즉 baseCoin하나당 counterCoin하나의 교환비) 가 나온다
            before_ioExchange_priceRatio = ioExchange_firstLayer_price = \
                inCoin_ask_lowest_ioExchange_priceRatio / outCoin_bid_highest_ioExchange_priceRatio
            #eExchange에서는 counterCoin을 팔꺼니까 bids 필요
            before_eExchange_priceRatio = eExchange_firstLayer_price = \
                latest_eExchange_orderBook[eExchange_moneyRole][inCoin]["bids"][0]["price"]

            #in(counterCoin)과 out(baseCoin)의 theoretical한 orderPrice(usedLast_layer)의 비율로 ioExchange의 priceRatio를 구한다
            after_ioExchange_priceRatio = ioExchange_orderPrice = ioExchange_inTrade_orderPrice / ioExchange_outTrade_orderPrice


        elif inout_type == "whenOutCoin":
            profitable_symbol = ">"
            #whenOutCoin이므로 baseCoin(inCoin)_ask_lowest_ioExchange_priceRatio / counterCoin(outCoin)_bid_highest_ioExchange_priceRatio
            #triangular 하에서 ioExchange_obj 에서는  FIAT를 매개로 BTC 마켓의 가격체계와 유사한 priceRatio(즉 baseCoin하나당 counterCoin하나의 교환비) 가 나온다
            before_ioExchange_priceRatio = ioExchange_firstLayer_price = \
                outCoin_bid_highest_ioExchange_priceRatio / inCoin_ask_lowest_ioExchange_priceRatio
            #eExchange에서는 counterCoin을 살꺼니까 asks 필요
            before_eExchange_priceRatio = eExchange_firstLayer_price = \
                latest_eExchange_orderBook[eExchange_moneyRole][outCoin]["asks"][0]["price"]

            #in(counterCoin)과 out(baseCoin)의 theoretical한 orderPrice(usedLast_layer)의 비율로 ioExchange의 priceRatio를 구한다
            after_ioExchange_priceRatio = ioExchange_orderPrice = ioExchange_outTrade_orderPrice / ioExchange_inTrade_orderPrice
        before_symbol = "<" if before_ioExchange_priceRatio < before_eExchange_priceRatio else ">"
        after_symbol = "<" if after_ioExchange_priceRatio < after_eExchange_priceRatio else ">"

        is_priceRatio_turned = False
        #priceRatio가 역전됐는지 여부를 따진다
        if profitable_symbol == before_symbol:

            # print("\n#----------priceRatio tracking info------------------#")
            # print(self.exchangePairing_name, inCoin, outCoin, inout_type)
            # print(str(outCoin_bid_highest_ioExchange_priceRatio) + " -> " + str(ioExchange_outTrade_orderPrice),
            #       usedLayerLength_for_ioExchange_outTrade)
            # print(str(before_eExchange_priceRatio) + " -> " + str(after_eExchange_priceRatio),
            #       usedLayerLength_for_eExchangeTrade)
            # print(str(inCoin_ask_lowest_ioExchange_priceRatio) + " -> " + str(ioExchange_inTrade_orderPrice),
            #       usedLayerLength_for_ioExchange_inTrade)
            #
            # print("        #---beforePriceInfo---#")
            # print(inout_type + " " + "profitable_symbol", profitable_symbol)
            # print("ioExchange_obj : " + str(before_ioExchange_priceRatio) + "--------- " + before_symbol + " ---------" + \
            #       "eExchange_obj : " + str(before_eExchange_priceRatio))
            # print("        #---afterPriceInfo---#")
            # print("ioExchange_obj : " + str(after_ioExchange_priceRatio) + "--------- " + after_symbol + " ---------" + \
            #       "eExchange_obj : " + str(after_eExchange_priceRatio))
            # print("#----------------------------------------------------#")

            #이득이 될 가능성이 있는 atraCycle 하에서 atraCycle이후에 priceRatio가역전되었음 그러므로 priceRatio가 turned 됐음
            if before_symbol != after_symbol:
                print("profitable Cycle  and turned!!!")
                smaller_priceRatio = after_ioExchange_priceRatio if after_eExchange_priceRatio - after_ioExchange_priceRatio > 0 else after_eExchange_priceRatio
                bigger_priceRatio =  after_eExchange_priceRatio if smaller_priceRatio == after_ioExchange_priceRatio else after_ioExchange_priceRatio

                #이 turned_rate 대로 init value를 줄이는것은 아니고 그냥 30%씩 삭감한다
                turned_rate = (bigger_priceRatio - smaller_priceRatio) / bigger_priceRatio
                # print("turned_rate", turned_rate)
                return turned_rate
            #이득이 될 가능성이 있는 atraCycle 하에서 priceRatio가 역전되지 않았음 그러므로 priceRatio가 turned 되지 않았음
            else:
                # print("profitable Cycle  but not turned")
                return
        #애초에 해당 inOut_type 하에서 애초에 이득이 되는 거래가 아님 그러므로 그냥 return 시킴
        else:
            # print("not a profitable cycle")
            return





        # print(inout_type)
        # print(inout_type)
        # print(inout_type)
        # print(inout_type)
        # print(ioExchange_inTrade_orderPrice)
        # print(inout_type)
        # print(inout_type)
        # print(inout_type)
        # print(inout_type)


    def calculateCostAndProfit(self):
        atra_module_interactor = SharedStorage.atra_module_interactor
        self.is_calculating_now = True
        #여러 pairing이 log를 뿌려대므로 calculate 내부에서 단계적으로 뿌려대던걸 최종적으로 한번만 뿌리기 위해 모음
        log_set = {}
        log_set["atraExchangePairing_name"] = self.exchangePairing_name
        #계산중간에 짧은시간동안 atraCycle_setting_val이 바뀔 가능성이 있으므로초기에 확정시켜놓고 calculate끝까지 사용
        is_atraCycle_permitted = self.atraCycle_setting_val["IS_ATRACYCLE_PERMITTED"]

        # 양 시장간 비트코인 기준 각 코인의 가격차 , 수익률
        # 비용처리 후 수익률 수익금액 .
        # 내가 원하는 스타일로 짠 {exchange_average: {BTC:~, ETH:~  .....}  , exchange_ratio : {BTC: ~, ETH : ~ } }
        # 식의 dict 로 받는다
        ioExchange_name = self.pairing_exchangeName_dict["IOExchange"]
        eExchange_name = self.pairing_exchangeName_dict["EExchange"]

        ioExchange_feeInfo = self.feeInfo_atraPairing_specified[ioExchange_name]
        eExchange_feeInfo = self.feeInfo_atraPairing_specified[eExchange_name]

        ioExchange_money_role = self.money_role_eachExchange["IOExchange"]
        eExchange_money_role = self.money_role_eachExchange["EExchange"]

        latest_ioExchange_orderBook = SharedStorage.orderbook_eachExchange[ioExchange_name]
        latest_eExchange_orderBook = SharedStorage.orderbook_eachExchange[eExchange_name]
        latest_ioExchange_orderBook_timestamp = latest_ioExchange_orderBook["timestamp"]
        latest_eExchange_orderBook_timestamp = latest_eExchange_orderBook["timestamp"]

        latest_eExchange_balance = SharedStorage.balance_eachExchange[eExchange_name] \
            if eExchange_name in SharedStorage.balance_eachExchange.keys() else None
        latest_ioExchange_balance = SharedStorage.balance_eachExchange[ioExchange_name] \
            if ioExchange_name in SharedStorage.balance_eachExchange.keys() else None

        latest_ioExchange_balance_timestamp = latest_ioExchange_balance["timestamp"] \
            if latest_ioExchange_balance != None else None
        latest_eExchange_balance_timestamp = latest_eExchange_balance["timestamp"] \
            if latest_eExchange_balance != None else None
        if latest_eExchange_balance_timestamp != None:
            print("eExchange_obj timestamp : " + str(
            time.strftime("%y%m%d %H:%M:%S", time.localtime(latest_eExchange_balance_timestamp))))
         #whenInCoin whenOutCoin  각각의 baseinfo 안에 이 평균 timestamp가 들어감 기껏해야 위 두개라 0.0몇초 차이임
        average_orderBook_timestamp = (latest_eExchange_orderBook_timestamp  + latest_eExchange_orderBook_timestamp)/2
        log_set["calculate_start"] = "calculate Start : " + str(
            time.strftime("%y%m%d %H:%M:%S", time.localtime(average_orderBook_timestamp)))

        # theorical profit은 InCoin 이 무엇이냐에 따라 n(코인종류 * 시장수)개가 도출됨 물론 EExchange에서 BTC_BTC같은건 없지만 이론을 위해 있다고 가정
        inout_baseinfo = {"whenInCoin": {}, "whenOutCoin": {}}

        # 각 코인별 base코인과의 상대가격에 기반한 cost와 earning_rate , 기타 사항들은
        # exchange_neutral 이다 즉 ioExchange인지 eExchange인지에 상관없이 baseCoin과 각 코인의 관계에서
        # 해당코인이 inCoin일때 outCoin일때만을 따짐

        # 엄밀하게 따지자면 각 coin은  basecoin과의 관계에서 inCoin 일때는 그 코인의 매수가격을 buyMoney를 계산시 고려해야하며
        # outCoin일때는 그코인의 매도가격을 sellMoney를 계산시 고려해야 하기 때문 결국 가장 중요한것은 원화 얼마를
        # 투입해서 얼마를 건질수 있느냐다 따라서 inCoin 일때와 outCoin일때의 상황을 각각 고려해야함 당연하게 inCoin일때
        # 수익률이 + 이면 outCoin일때는  -  다 됨.  - 일때는 고려할 필요가 없지만 이론을 위해서 구함(당연히 둘 다 -일수 있음)
        # 수익률의 핵심은 사는돈대비 파는돈이며 정확하려면 inCoin의 사는가격과 outCoin의 파는 가격이 엄밀해야 함을 명심

        # 이론적으로 buyMoney 로 얼마의 sellMoney를 벌수 있느냐임
        # 1회 arbitrage의 inCoin을 사기위한 한번에 거래되는 투입fiat기준금액 test용
        commandedAtraCycleValue = self.atraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"]
        log_atraCycle_value = {"commanded"}

        #atraCycle
        # ioExchange_baseCoin_value_converted_ioExchange_moneyRole = \
        #     int(latest_ioExchange_orderBook[ioExchange_money_role][self.base_coin]["asks"][0]["price"] *
        #         latest_ioExchange_balance[base_coin]["available"])
        # commandedAtraCycleValue = ioExchange_baseCoin_value_converted_ioExchange_moneyRole / 2
        # print(commandedAtraCycleValue)

        # 이 for문 안의 내용이 baseinfo_whenInCoin whenOutCoin 안의 dict 구성물로 들어간다 필요하다 싶은건 싹다 넣고 view로직에서 조정
        for i, inout_type in enumerate(["whenInCoin", "whenOutCoin"]):
            theoretical_atraCycle_value_perEachCoin = {}
            theoretical_available_atraCycle_value_grouping_perEachCoin = {}
            theoretical_commanded_atraCycle_value_grouping_perEachCoin = {}
            theoretical_atraCycle_value_grouping_perEachCoin = {}
            inOut_coin_direction_perEachCoin = {}

            # 실제 atraCycle에서는 eExchange에서 거래가 이루어 지고 io의 in과 out이 무조건 시장가로 거래가 되는바
            # ioExchange에서는 시장가(market order) 매도 매수를 면 orderPrice가 필요없을수 있지만 시장가 매도 매수 지원이 안되는
            # 거래소의 경우 수량을 맞추려면 추출한 orderPrice를 매우크게 만들거나(매수시) 매우 작게(매도시) 하여 들어가야함
            theoretical_greedy_ioExchange_inTraded_result_perEachCoin = {}  #이론적 greedy 계산 데이터 들어있음
            theoretical_orderPrice_for_ioExchange_inTrade_perEachCoin = {}
            #최초 actual Trading은 무조건 이값으로 시작하게 된다. 아래 셋은 view가 아닌 실제 trading을 위한값임
            theoretical_inCoin_transferredAmount_greedy_perEachCoin = {}
            theoretical_greedy_eExchangeTraded_result_perEachCoin = {} #이론적 greedy 계산 데이터 들어있음
            theoretical_orderPrice_for_eExchangeTrade_perEachCoin = {}
            theoretical_outCoin_tradedAmount_greedy_perEachCoin = {}
            # 두개는 되도록 붙어 있을것
            theoretical_outCoin_transferredAmount_perEachCoin = {}
            theoretical_outCoin_transferredAmount_greedy_perEachCoin ={}
            theoretical_outCoin_amount_if_noArbitrage_perEachCoin = {}

            #실제 atraCycle에서는 eExchange에서 거래가 이루어 지고 io의 in과 out이 무조건 시장가로 거래가 되는바
            # ioExchange에서는 시장가 매도 매수를 하여 orderPrice가 필요없을수 있지만 시장가 매도 매수 지원이 안되는
            # 거래소의 경우 수량을 맞추려면 추출한 orderPrice를 매우크게 만들거나(매수시) 매우 작게(매도시) 하여 들어가야함
            theoretical_greedy_ioExchange_outTraded_result_perEachCoin = {} #이론적 greedy 계산 데이터 들어있음
            theoretical_orderPrice_for_ioExchange_outTrade_perEachCoin = {}
            # 수수료없이 EExchange에서 교환을 다 마친후 IOExchange에서 원화로 청산하는 금액 순수하게 이론적임
            theoretical_outCoin_sellMoney_perEachCoin = {}
            theoretical_outCoin_sellMoney_greedy_perEachCoin = {}
            # 1000단위로 끊는 , grouping 한건
            theoretical_outCoin_sellMoney_greedy_grouping_perEachCoin = {}

            theoretical_profit_byAtraCycleValue_perEachCoin = {}

            # 최초 inCoin의 가치대비 수익률임 atra 을 위해 IO와 EExchange에 분산해 놓은것의 총 합 대비가 아니라
            theoretical_earningRate_perEachCoin = {}
            theoretical_earningRate_greedy_perEachCoin = {}

            # 아래 내용이 위의 fees costs dict안에 {BTC:~, ETH:~} 각각 들어간다
            for commodityRole_coin in self.paired_commodityRole_coin_sequence:
                # python 스타일 삼황 연산자
                inCoin = commodityRole_coin if inout_type == "whenInCoin" else self.base_coin
                outCoin = self.base_coin if inout_type == "whenInCoin" else commodityRole_coin
                inOut_coin_direction = inCoin + "->" + outCoin

                # self.calculateAvailableAtraCycleValueByBalance(commodityRole_coin, inout_type)

                # if (eExchange_ask_lowest_baseCoinRatioPrice_perEachCoin[commodityRole_coin] == None):
                #BTC_BTC 같은건 없으므로 atraStyleOrderbook 에 없음
                theoretical_available_atraCycle_value_grouping = None
                theoretical_commanded_atraCycle_value_grouping = None
                theoretical_atraCycle_value = None
                theoretical_atraCycle_value_grouping=None
                theoretical_greedy_ioExchange_inTraded_result = None
                theoretical_orderPrice_for_ioExchange_inTrade = None
                theoretical_inCoin_amount_noCost = None
                theoretical_inCoin_amount_withCost = None

                theoretical_inCoin_transferredAmount_noCost = None
                theoretical_inCoin_transferredAmount_withCost = None
                theoretical_inCoin_transferredAmount_greedy = None
                theoretical_greedy_eExchangeTraded_result = None
                theoretical_orderPrice_for_eExchangeTrade = None
                theoretical_outCoin_tradedAmount_noCost = None
                theoretical_outCoin_tradedAmount_withCost = None
                theoretical_outCoin_tradedAmount_greedy = None

                theoretical_outCoin_transferredAmount_noCost = None
                theoretical_outCoin_transferredAmount_withCost = None
                theoretical_outCoin_transferredAmount_withCost_coinNotion = None
                theoretical_outCoin_amount_if_noArbitrage = None
                theoretical_greedy_ioExchange_outTraded_result = None
                theoretical_orderPrice_for_ioExchange_outTrade = None
                theoretical_outCoin_sellMoney_noCost = None
                theoretical_outCoin_sellMoney_withCost = None
                theoretical_outCoin_sellMoney_greedy_grouping = None

                theoretical_profit_byInitialBuyMoney = None
                theoretical_earningRate_withCost = None
                theoretical_outCoin_transferredAmount_greedy_coinNotion = \
                    theoretical_outCoin_sellMoney_greedy = theoretical_earningRate_greedy = None

                if (commodityRole_coin in latest_eExchange_orderBook[eExchange_money_role]):

                    #atraCycle value는 당연히 pared Exchanges 내의 balance에 의존 할 수밖에 없음
                    available_atraCycle_value_list = self.calculateAvailableAtraCycleValueByBalance(commodityRole_coin,inout_type)
                    #0으로 보내면 중간중간 골치아파 지므로 0일때는 그냥 1로 간주함 이는 무조건 fiat기준 이며 coin기준으로 한다면 조정을 해야 함
                    #혹시 모르니 98퍼센트 선에서 기준을 잡을 수 있도록
                    available_atraCycle_value = int(available_atraCycle_value_list["lowest_value"] * 0.99) if \
                        int(available_atraCycle_value_list["lowest_value"] * 0.99) != 0 else 1
                    # print(available_atraCycle_value_list["lowest_value"], available_atraCycle_value)
                    #trading 모드가 아닐때는 수익이 있는지 그냥 관찰하는 용도 이므로  available 상관없이 입력값 가치에 따라서 계산함
                    if is_atraCycle_permitted == False:
                       theoretical_atraCycle_value = commandedAtraCycleValue
                    #trading 모드 일때는 명시적 입력값과 available 중에 작은(가능한) 값으로로
                    else:
                       theoretical_atraCycle_value = commandedAtraCycleValue if available_atraCycle_value >=commandedAtraCycleValue \
                           else available_atraCycle_value

                    # print(commodityRole_coin, inout_type, available_atraCycle_value, commandedAtraCycleValue, theoretical_atraCycle_value)

                    #greedy한 계산에서 orderBook의 한계로 (ex. bithumb는 All로 orderbook을 호출하면 호가가 5layer씩밖에 안옴)
                    #계산에 error가 발생한다면 그만큼 줄여서 다시 계산시킴
                    while True:
                        '''theoretical한 부분을 다루는 calculate 부분 에서는 orderBook들이 calculte 처음부터끝까지 
                        고정되어 있으므로 무조건 ioExchange_obj inCoin 먼져 생각하지만(직관적으로 생각하기 편하다)
                        실제 trading은 coin-coin 거래가 가능한(inAmount와 outAmount를 확정할 수 있으므로) 
                        eExchange_obj(ex. poloniex, bitfinex) 먼져 거래를 시킨다,  
                        '''
                        #balance 기반의 AtraCycle 가능 금액  , 로 1000의 자리를 묶는다
                        theoretical_available_atraCycle_value_grouping = AtraUTIL.groupingFiatNumber(available_atraCycle_value)
                        #희망하는 Atra 가능금액(설정값기반)
                        theoretical_commanded_atraCycle_value_grouping = AtraUTIL.groupingFiatNumber(commandedAtraCycleValue)
                        # print("theoretical_atraCycle_value : " + str(theoretical_atraCycle_value))
                        # trading 모드인지 아닌지 여부, exchange balance 잔액, Atra입력값, orderBook 제한  모두 고려된값
                        theoretical_atraCycle_value_grouping = AtraUTIL.groupingFiatNumber(theoretical_atraCycle_value)
                        # 원화로 구매한 inCoin amount 최신 orderBook을 기반으로 구한다 실제 amount는 당연히 greedy에서 구함
                        theoretical_inCoin_amount_noCost = theoretical_atraCycle_value / (latest_ioExchange_orderBook[ioExchange_money_role][inCoin]["asks"][0]["price"])
                        theoretical_inCoin_amount_withCost = theoretical_inCoin_amount_noCost * (
                            1 - ioExchange_feeInfo["tradeFee_ratio"])
                        theoretical_greedy_ioExchange_inTraded_result = AtraUTIL.calculateTradingGreedily(ioExchange_name,ioExchange_money_role,"buy" ,inCoin,money_role_sum=theoretical_atraCycle_value)
                        # print("theoretical_greedy_ioExchange_inTraded_result : " + str(theoretical_greedy_ioExchange_inTraded_result))
                        # dict가 아니라 float이 왔다면 decreasing_rate(orderBook 부족 때문에 initial buyMoney를 다 계산 못 시킨므로 감소시키라는의미)
                        # 가 return 됐다는 이므로 #initial buymoney를  decreasing_rate 만큼 감소시켜서 while문 처음으로 돌아가 다시계산 시킨다
                        if type(theoretical_greedy_ioExchange_inTraded_result) == float :
                            decreasing_rate = theoretical_greedy_ioExchange_inTraded_result
                            logging_normal.info("theoretical_atraCycle_value decreased from " + str(
                                theoretical_atraCycle_value) + "to " + str(
                                theoretical_atraCycle_value * decreasing_rate * 0.95) + \
                                "\nreason : lack of " + inCoin + " orderbook information from " + ioExchange_name)

                            theoretical_atraCycle_value = theoretical_atraCycle_value * decreasing_rate
                            #1000단위만 끊어서
                            now_timestamp = time.time()
                            # 혹시 무한루프 걸려서 decreasing을 계속할 수 있으므로 5초이상 끌면 그냥 계산 취소시킴
                            if now_timestamp - average_orderBook_timestamp > 3: break
                            continue
                        theoretical_orderPrice_for_ioExchange_inTrade = theoretical_greedy_ioExchange_inTraded_result[
                            "orderPrice_for_actualTrade"]
                        theoretical_usedLayerLength_for_ioExchange_inTrade = theoretical_greedy_ioExchange_inTraded_result[
                            "used_layer_length"]
                        #greedy하게 orderBook을 탐색하며 계산했으므로 평균가격을 구할수 있음 view용
                        theoretical_inCoin_tradedAveragePrice_greedy = theoretical_greedy_ioExchange_inTraded_result["traded_price_average"]
                        #당연히 greedy는 noCost를 구할 이유가 없음 정확한 trading 결과를 예측하는 것이기 때문에 모든 greedy는 cost를 포함한 것으로 계산함
                        theoretical_inCoin_amount_greedy = theoretical_greedy_ioExchange_inTraded_result["traded_quantity_sum"]
                        theoretical_inCoin_amount_greedy = theoretical_inCoin_amount_greedy * (
                        1 - ioExchange_feeInfo["tradeFee_ratio"])

                        # Eexchange에 도착한 inCoin Amount
                        theoretical_inCoin_transferredAmount_noCost = theoretical_inCoin_amount_noCost
                        theoretical_inCoin_transferredAmount_withCost = theoretical_inCoin_amount_withCost - \
                                                                       ioExchange_feeInfo["transferFee_coin"][inCoin]
                        theoretical_inCoin_transferredAmount_greedy = theoretical_inCoin_amount_greedy - \
                                                                     ioExchange_feeInfo["transferFee_coin"][inCoin]
                        # print(inCoin,outCoin,theoretical_inCoin_transferredAmount_noCost,theoretical_inCoin_transferredAmount_withCost,theoretical_inCoin_transferredAmount_greedy)

                        #EExchange에서의 거래는 baseCoin이 money role(화폐)역할을 함 으로서 counterCoin이 inCoin인지 outCoin인지에 따라 분기한다
                        #사실 한번에 commodityRole coin으로 하면 한번에 할수 있으나 나중에 헷갈리니까 엄밀하게 분기함

                        # 이상황이라면 counterCoin(=commodityRole상품역할)을 sell해서 baseCoin(화폐역할)을 얻는것 이므로 baseCoin을 구하려면
                        # counterCoin 갯수에 baseCoin으로 환산된 상대가치를 곱해준다(greedy가 아니라면 bid 하는 사람중에 가장 높게 부르는 사람것을, greedy라면 계산을 통해 정확한 값을)
                        if inCoin == commodityRole_coin:
                            theoretical_outCoin_tradedAmount_noCost = theoretical_inCoin_transferredAmount_noCost * \
                                                                      latest_eExchange_orderBook[eExchange_money_role][commodityRole_coin]["bids"][0][
                                                                          "price"]
                            theoretical_outCoin_tradedAmount_withCost = (theoretical_inCoin_transferredAmount_withCost * \
                                                                         latest_eExchange_orderBook[eExchange_money_role][commodityRole_coin]["bids"][0][                                                                             "price"]) \
                                                                        * (1 - eExchange_feeInfo["tradeFee_ratio"])
                            theoretical_greedy_eExchangeTraded_result = \
                                AtraUTIL.calculateTradingGreedily(eExchange_name, eExchange_money_role, "sell", commodityRole_coin,
                                                                  commodity_role_coin_amount=theoretical_inCoin_transferredAmount_greedy)
                            if type(theoretical_greedy_eExchangeTraded_result) == float:
                                decreasing_rate = theoretical_greedy_eExchangeTraded_result
                                logging_normal.info("theoretical_atraCycle_value decreased from " + str(
                                    theoretical_atraCycle_value) + "to " + str(
                                    theoretical_atraCycle_value * decreasing_rate * 0.95) +
                                    "\nreason : lack of " + commodityRole_coin + " orderbook information from " + eExchange_name)
                                theoretical_atraCycle_value = theoretical_atraCycle_value * decreasing_rate
                                now_timestamp = time.time()
                                # 혹시 무한루프 걸려서 decreasing을 계속할 수 있으므로 5초이상 끌면 그냥 계산 취소시킴
                                if now_timestamp - average_orderBook_timestamp > 3: break
                                continue

                            #outCoin을 얻기위해 inCoin을 sell할때 actual 불러야 하는 price임 이걸 벗어나면  더이상 안부르는걸로
                            theoretical_orderPrice_for_eExchangeTrade = theoretical_greedy_eExchangeTraded_result[
                                "orderPrice_for_actualTrade"]
                            theoretical_usedLayerLength_for_eExchangeTrade = theoretical_greedy_eExchangeTraded_result[
                                "used_layer_length"]
                            theoretical_outCoin_traded_averagePrice_greedy = theoretical_greedy_eExchangeTraded_result["traded_price_average"]
                            theoretical_outCoin_tradedAmount_greedy = theoretical_greedy_eExchangeTraded_result["traded_sellMoney_sum"] * \
                                                                      (1 - eExchange_feeInfo["tradeFee_ratio"])
                        else:  # 이상황이라면 baseCoin(화폐역할) 으로 counterCoin(상품역할)을 buy 하는것 이므로 outCoin의 갯수를 구하려면
                            #  총화폐(baseCoin)을 counter코인의 가격으로 나눠준다
                            theoretical_outCoin_tradedAmount_noCost = theoretical_inCoin_transferredAmount_noCost / \
                                                                      latest_eExchange_orderBook[eExchange_money_role][commodityRole_coin]["asks"][0]["price"]
                            theoretical_outCoin_tradedAmount_withCost = (theoretical_inCoin_transferredAmount_withCost /
                                                                         latest_eExchange_orderBook[eExchange_money_role][commodityRole_coin]["asks"][0]["price"]) \
                                                                        * (1 - eExchange_feeInfo["tradeFee_ratio"])
                            #outcoin을 buy '사야' 되므로 calculateTradingGreedily 로직에 필요한 정보는 money role을 하는 BTC임
                            theoretical_greedy_eExchangeTraded_result = \
                                AtraUTIL.calculateTradingGreedily(eExchange_name, eExchange_money_role ,"buy", commodityRole_coin,money_role_sum=theoretical_inCoin_transferredAmount_greedy)
                            if type(theoretical_greedy_eExchangeTraded_result) == float:
                                decreasing_rate = theoretical_greedy_eExchangeTraded_result
                                logging_normal.info("theoretical_atraCycle_value decreased from " + str(
                                    theoretical_atraCycle_value) + "to " + str(
                                    theoretical_atraCycle_value * decreasing_rate * 0.95) +
                                    "\nreason : lack of " + commodityRole_coin + " orderbook information from " + eExchange_name)
                                theoretical_atraCycle_value = theoretical_atraCycle_value * decreasing_rate
                                # 1000단위만 끊어서
                                # theoretical_atraCycle_value = int(round(theoretical_atraCycle_value / 1000) * 1000) if \
                                #     theoretical_atraCycle_value > 1000 else theoretical_atraCycle_value
                                now_timestamp = time.time()
                                # 혹시 무한루프 걸려서 decreasing을 계속할 수 있으므로 5초이상 끌면 그냥 계산 취소시킴
                                if now_timestamp - average_orderBook_timestamp > 3: break
                                continue
                            # greedy하게 orderBook을 탐색하며 계산했으므로 정확한 거래를 구할수 있음
                            # outCoin을 inCoin(base_coin) 으로 buy할때 actual 불러야 하는 price임 이걸 벗어나면  더이상 안부르는걸로
                            theoretical_orderPrice_for_eExchangeTrade = theoretical_greedy_eExchangeTraded_result[
                                "orderPrice_for_actualTrade"]
                            theoretical_usedLayerLength_for_eExchangeTrade = theoretical_greedy_eExchangeTraded_result[
                                "used_layer_length"]
                            #greedy하게 outCoin을 구매한 평균가치 view 용으로 쓰임
                            theoretical_outCoin_traded_averagePrice_greedy = theoretical_greedy_eExchangeTraded_result["traded_price_average"]
                            theoretical_outCoin_tradedAmount_greedy = theoretical_greedy_eExchangeTraded_result["traded_quantity_sum"] * \
                                                                      (1 - eExchange_feeInfo["tradeFee_ratio"])

                        # EExchange에서 IOExchange로  도착한 outCoin Amount
                        theoretical_outCoin_transferredAmount_noCost = theoretical_outCoin_tradedAmount_noCost
                        theoretical_outCoin_transferredAmount_withCost = theoretical_outCoin_tradedAmount_withCost \
                                                                        - eExchange_feeInfo["transferFee_coin"][outCoin]
                        theoretical_outCoin_transferredAmount_greedy = theoretical_outCoin_tradedAmount_greedy \
                                                                        - eExchange_feeInfo["transferFee_coin"][outCoin]

                        #만약 spread가 너무 심하게 벌어져서 eExchange의 (baseCoin-counterCoin)ratio와  ioExchange_obj 의 ratio가 극단적으로 차이가 날 경우 inCoin 으로부터 교환된
                        #outCoin의 수량(동시에 ioExchange에서 거래될 outCoin수량)이 너무 커져서 ioExchange의 outCoin이 이를 소화 못하게 될 경우가 생긴다
                        #atra 원리에 따라 transfer되는건 나중에 받는다고 생각하고 동시에 ioExchange에서 동일수량을 동시에 거래하기 때문에
                        #이를 위해 theoretical 선에서 이를 미리 감지하고  theoretical_atraCycle_value를 미리 재 조정해서 다시 계산 시켜야함
                        #그러나 trading이 허용되지 않은 view 모드에서는 orderBook을 기반으로 모든 balance가 갖춰져 있다고 가정한후 거래를 하기 때문에
                        #이는 trading 허용시 에서만 적용함  또한 임의로 outCoin 부분을 비워놨을경우는 어차피 필요없으므로 0.5 이살일때만
                        latest_ioExchange_outCoin_balance = latest_ioExchange_balance[outCoin]["available"]
                        if latest_ioExchange_outCoin_balance - theoretical_outCoin_transferredAmount_greedy < 0 and \
                                        is_atraCycle_permitted == True :
                            print(
                                "inOutType :" + inout_type, "latest outcoin :" + outCoin,
                                latest_ioExchange_outCoin_balance,
                                theoretical_outCoin_transferredAmount_greedy,
                                latest_ioExchange_outCoin_balance - theoretical_outCoin_transferredAmount_greedy,
                                latest_ioExchange_outCoin_balance / theoretical_outCoin_transferredAmount_greedy)
                            decreasing_rate = latest_ioExchange_outCoin_balance / theoretical_outCoin_transferredAmount_greedy
                            logging_normal.info("theoretical_atraCycle_value decreased from " + str(theoretical_atraCycle_value) + "to "
                                                +str(theoretical_atraCycle_value * decreasing_rate * 0.80) +
                                                "\nreason : lack of " + outCoin + " outTradeAmount in" + ioExchange_name)
                            #calculateTradingGreedily 에서 orderBook Layrer때문에 축소하는 것과는 다르게 실제 outCoin_transferredAmount 가 어떻게 될줄 모르므로 5%더 줄임
                            #또한 0이 되버리면 안되므로 0이라면 1로 바꿔줌
                            theoretical_atraCycle_value = theoretical_atraCycle_value * decreasing_rate * 0.80 if theoretical_atraCycle_value * decreasing_rate * 0.80 != 0 else 1
                            # theoretical_atraCycle_value = int(round(theoretical_atraCycle_value / 1000) * 1000) if \
                            #    theoretical_atraCycle_value > 1000 else theoretical_atraCycle_value
                            now_timestamp = time.time()
                            # 혹시 무한루프 걸려서 decreasing을 계속할 수 있으므로 5초이상 끌면 그냥 계산 취소시킴
                            if now_timestamp - average_orderBook_timestamp > 3: break
                            continue

                        # view에 표시될때 outCoin이 무엇인지 표시하기 위해 coin 종류를 붙여줌
                        theoretical_outCoin_transferredAmount_withCost_coinNotion = \
                            str(theoretical_outCoin_transferredAmount_withCost) + outCoin
                        theoretical_outCoin_transferredAmount_greedy_coinNotion = \
                            str(theoretical_outCoin_transferredAmount_greedy) + outCoin

                        #최초 동일 buyMoney로 바로 inCoint이 아니라 outCoin을 샀다고 가정했을때,view에서 theoretical_outCoin_transferredAmount_withCost 와 비교하기위해쓰임
                        #greedy는 이걸 만들지 않는다
                        theoretical_outCoin_amount_if_noArbitrage = str(
                            theoretical_atraCycle_value / latest_ioExchange_orderBook[ioExchange_money_role][outCoin]["asks"][0]["price"] *
                            (1 - ioExchange_feeInfo["tradeFee_ratio"])) + outCoin
                        # 최초 동일 buyMoney로 바로 inCoint이 아니라 outCoin을 샀다고 가정했을때,view에서 theoretical_outCoin_transferredAmount_greedy 와 비교하기위해쓰임

                        # IOExchange에서 outCoin을 팔고 받은 원화
                        theoretical_outCoin_sellMoney_noCost = theoretical_outCoin_transferredAmount_noCost * \
                                                               latest_ioExchange_orderBook[ioExchange_money_role][outCoin]["bids"][0]["price"]
                        # 수수료 고려해서 EExchange에서 교환을 다 마친후 IOExchange에서 원화로 청산하는 금액 이론적을 dummy buyMoney와 비교하면 비용 고려한 수익률이 나옴
                        theoretical_outCoin_sellMoney_withCost = (theoretical_outCoin_transferredAmount_withCost *
                                                                  latest_ioExchange_orderBook[ioExchange_money_role][outCoin]["bids"][0]["price"]) * (
                                                                 1 - ioExchange_feeInfo["tradeFee_ratio"])
                        theoretical_greedy_ioExchange_outTraded_result = \
                            AtraUTIL.calculateTradingGreedily(ioExchange_name, ioExchange_money_role, "sell",outCoin,commodity_role_coin_amount=theoretical_outCoin_transferredAmount_greedy)
                        if type(theoretical_greedy_ioExchange_outTraded_result ) == float:
                            decreasing_rate = theoretical_greedy_ioExchange_outTraded_result
                            logging_normal.info("theoretical_atraCycle_value decreased from " + str(
                                theoretical_atraCycle_value) + "to " + str(
                                theoretical_atraCycle_value * decreasing_rate * 0.95) +
                                "\nreason : lack of " + outCoin + " orderbook information from " + ioExchange_name)
                            theoretical_atraCycle_value = theoretical_atraCycle_value * decreasing_rate
                            # 1000단위만 끊어서
                            # theoretical_atraCycle_value = int(round(theoretical_atraCycle_value / 1000) * 1000) if \
                            #     theoretical_atraCycle_value > 1000 else theoretical_atraCycle_value
                            now_timestamp = time.time()
                            #혹시 무한루프 걸려서 decreasing을 계속할 수 있으므로 5초이상 끌면 그냥 계산 취소시킴
                            if now_timestamp - average_orderBook_timestamp >3 : break
                            continue
                        theoretical_orderPrice_for_ioExchange_outTrade = theoretical_greedy_ioExchange_outTraded_result [
                            "orderPrice_for_actualTrade"]
                        theoretical_usedLayerLength_for_ioExchange_outTrade = theoretical_greedy_ioExchange_outTraded_result[
                            "used_layer_length"]
                        #todo:
                        # '''이시점에서  만약 earning_EExchange 의 price_rate와 [ inCoin 마지막 layer의 가격(Atra cycle이  그 가격까지 시장가격을 바꿀것 이므로
                        # outCoin의 마지막 layer가격(Atra cycle이  그 가격까지 시장가격을 바꿨으므로) 이 만들어내는 rate가] 원래 spread를 역전 시킬 정도가 된다면 이를다시 decreasing_Rate를
                        # 추려내서 atraCycle value 를 조정 시켜야함'''
                        decreasing_rate =  self._decidePriceRatioTurnedOrNot(inout_type, inCoin, outCoin,
                                                             theoretical_greedy_ioExchange_inTraded_result,
                                                          theoretical_greedy_ioExchange_outTraded_result,
                                                          theoretical_greedy_eExchangeTraded_result)

                        if type(decreasing_rate ) == float and theoretical_atraCycle_value *0.7 > 2500000:
                            logging_normal.info("theoretical_atraCycle_value decreased from " + str(
                                theoretical_atraCycle_value) + "to " + str(
                                theoretical_atraCycle_value * 0.7) +
                                                "\nreason : turned priceRatio")
                            theoretical_atraCycle_value = theoretical_atraCycle_value * 0.7
                            continue

                        # print(theoretical_greedy_ioExchange_outTraded_result )
                        # greedy하게 orderBook을 탐색하며 계산한 이론상 정확한 sellMoney 이며 역시 dummy buyMoney 와 비교한다
                        theoretical_outCoin_sellMoney_greedy = theoretical_greedy_ioExchange_outTraded_result ["traded_sellMoney_sum"]
                        theoretical_outCoin_sellMoney_greedy = theoretical_outCoin_sellMoney_greedy * (
                                                                 1 - ioExchange_feeInfo["tradeFee_ratio"])
                        theoretical_outCoin_sellMoney_greedy_grouping = AtraUTIL.groupingFiatNumber(int(theoretical_outCoin_sellMoney_greedy))

                        # 수수료를 포함한 이론적인 수익률 (어디까지나 한번에 이동하는 가치를 기준으로 한거지 하나의 arbitrage를 동시에
                        #체결하기위해 투입해놓은 전체 돈을 기준으로 한 수익률이 아님
                        theoretical_earningRate_withCost = (theoretical_outCoin_sellMoney_withCost - theoretical_atraCycle_value) / theoretical_atraCycle_value
                        theoretical_earningRate_greedy = round((theoretical_outCoin_sellMoney_greedy - theoretical_atraCycle_value) / theoretical_atraCycle_value , 6)

                        #cycle 이후 최종적으로 손에 쥐는 profit임
                        theoretical_profit_byInitialBuyMoney = int((theoretical_outCoin_sellMoney_greedy - theoretical_atraCycle_value))
                        #여기까지 왔으면 greedy calculating에 문제가 없으므로 while문 더이상 안돌리고 빠져나옴

                        # # 만약 earningRate가 0보다 크다면 priceRatio가 역전되는지 여부를 확인 해야함
                        # if theoretical_earningRte_greedy > 0:
                        #
                        #
                        #     before_ioExchange_priceRatio = 0
                        #     after_ioExchange_priceRatio = 0
                        #     before_eExchange_priceRatio = 0
                        #     after_eExchange_priceRatio = 0
                        #
                        #
                        #     if inout_type == "whenInCoin":
                        #         bid_highest_ioExchange_priceRatio = \
                        #             latest_ioExchange_orderBook[ioExchange_money_role][inCoin]["bids"][0]["price"]
                        #
                        #         bid_highest_ioExchange_priceRatio = \
                        #         latest_ioExchange_orderBook[ioExchange_money_role][inCoin]["bids"][0]["price"]
                        #         ask_lowest_ioExchange_priceRatio = \
                        #         latest_ioExchange_orderBook[ioExchange_money_role][inCoin]["asks"][0]["price"]
                        #
                        #         #eExchange에서는 counterCoin을 sell해야 하므로
                        break

                # 두개는 되도록 붙어 있을것
                # 일단 어떠한 계산을 할 것이 아니라 view에서 쓸것이기 때문에 coin 종류 notion이 들어간 것을 쓴다

                theoretical_available_atraCycle_value_grouping_perEachCoin[commodityRole_coin] = theoretical_available_atraCycle_value_grouping
                theoretical_commanded_atraCycle_value_grouping_perEachCoin[commodityRole_coin] = theoretical_commanded_atraCycle_value_grouping
                theoretical_atraCycle_value_perEachCoin[commodityRole_coin] = theoretical_atraCycle_value
                theoretical_atraCycle_value_grouping_perEachCoin[commodityRole_coin] = theoretical_atraCycle_value_grouping
                inOut_coin_direction_perEachCoin[commodityRole_coin] = inOut_coin_direction

                theoretical_greedy_ioExchange_inTraded_result_perEachCoin[commodityRole_coin] = theoretical_greedy_ioExchange_inTraded_result
                # ioExchange에서 inCoin 지정가 계산하기 위해 쓰임
                theoretical_orderPrice_for_ioExchange_inTrade_perEachCoin[commodityRole_coin] = theoretical_orderPrice_for_ioExchange_inTrade
                #아래 셋은 실제 trading을 위해 쓰임!
                theoretical_inCoin_transferredAmount_greedy_perEachCoin[
                    commodityRole_coin] = theoretical_inCoin_transferredAmount_greedy

                theoretical_greedy_eExchangeTraded_result_perEachCoin[commodityRole_coin] = \
                    theoretical_greedy_eExchangeTraded_result
                theoretical_orderPrice_for_eExchangeTrade_perEachCoin[
                    commodityRole_coin] = theoretical_orderPrice_for_eExchangeTrade
                theoretical_outCoin_tradedAmount_greedy_perEachCoin[
                    commodityRole_coin] = theoretical_outCoin_tradedAmount_greedy

                theoretical_outCoin_transferredAmount_perEachCoin[
                    commodityRole_coin] = theoretical_outCoin_transferredAmount_withCost_coinNotion
                theoretical_outCoin_transferredAmount_greedy_perEachCoin[
                    commodityRole_coin] = theoretical_outCoin_transferredAmount_greedy_coinNotion

                theoretical_greedy_ioExchange_outTraded_result_perEachCoin[commodityRole_coin] = \
                    theoretical_greedy_ioExchange_outTraded_result
                theoretical_orderPrice_for_ioExchange_outTrade_perEachCoin[commodityRole_coin] = \
                    theoretical_orderPrice_for_ioExchange_outTrade

                theoretical_outCoin_amount_if_noArbitrage_perEachCoin[commodityRole_coin] = theoretical_outCoin_amount_if_noArbitrage

                #값들 계산하는데 위에서 다 써먹었으므로  sellMoney들은 모두 자릿수 표시한 str로 바꿔서 빼줌
                theoretical_outCoin_sellMoney_perEachCoin[commodityRole_coin] = theoretical_outCoin_sellMoney_withCost
                theoretical_outCoin_sellMoney_greedy_perEachCoin[commodityRole_coin] = theoretical_outCoin_sellMoney_greedy
                theoretical_outCoin_sellMoney_greedy_grouping_perEachCoin[commodityRole_coin] = \
                    theoretical_outCoin_sellMoney_greedy_grouping

                theoretical_earningRate_perEachCoin[commodityRole_coin] = theoretical_earningRate_withCost
                theoretical_profit_byAtraCycleValue_perEachCoin[commodityRole_coin] = theoretical_profit_byInitialBuyMoney
                theoretical_earningRate_greedy_perEachCoin[commodityRole_coin] = theoretical_earningRate_greedy

            inout_baseinfo[inout_type][
                "theoretical_available_atraCycle_value_grouping_perEachCoin"] = theoretical_available_atraCycle_value_grouping_perEachCoin
            inout_baseinfo[inout_type][
                "theoretical_commanded_atraCycle_value_grouping_perEachCoin"] = theoretical_commanded_atraCycle_value_grouping_perEachCoin

            inout_baseinfo[inout_type]["theoretical_atraCycle_value_perEachCoin"] = theoretical_atraCycle_value_perEachCoin
            inout_baseinfo[inout_type]["theoretical_atraCycle_value_grouping_perEachCoin"] = theoretical_atraCycle_value_grouping_perEachCoin
            inout_baseinfo[inout_type]["inOut_coin_direction"] = inOut_coin_direction_perEachCoin
            inout_baseinfo[inout_type]["theoretical_orderPrice_for_ioExchange_inTrade_perEachCoin"] = \
                theoretical_orderPrice_for_ioExchange_inTrade_perEachCoin

            inout_baseinfo[inout_type]["theoretical_inCoin_transferredAmount_greedy_perEachCoin"] = \
                theoretical_inCoin_transferredAmount_greedy_perEachCoin
            inout_baseinfo[inout_type]["theoretical_greedy_eExchangeTraded_result_perEachCoin"] = \
                theoretical_greedy_eExchangeTraded_result_perEachCoin
            inout_baseinfo[inout_type]["theoretical_orderPrice_for_eExchangeTrade_perEachCoin"] = \
                theoretical_orderPrice_for_eExchangeTrade_perEachCoin
            inout_baseinfo[inout_type]["theoretical_outCoin_tradedAmount_greedy_perEachCoin"] = \
                theoretical_outCoin_tradedAmount_greedy_perEachCoin

            inout_baseinfo[inout_type]["theoretical_outCoin_transferredAmount_perEachCoin"] = \
                theoretical_outCoin_transferredAmount_perEachCoin
            inout_baseinfo[inout_type]["theoretical_outCoin_transferredAmount_greedy_perEachCoin"] = \
                theoretical_outCoin_transferredAmount_greedy_perEachCoin
            inout_baseinfo[inout_type]["theoretical_outCoin_amount_if_noArbitrage_perEachCoin"] = \
                theoretical_outCoin_amount_if_noArbitrage_perEachCoin
            inout_baseinfo[inout_type]["theoretical_greedy_ioExchange_outTraded_result_perEachCoin"] = \
                theoretical_greedy_ioExchange_outTraded_result_perEachCoin
            inout_baseinfo[inout_type]["theoretical_orderPrice_for_ioExchange_outTrade_perEachCoin"] = \
                theoretical_orderPrice_for_ioExchange_outTrade_perEachCoin
            inout_baseinfo[inout_type]["theoretical_outCoin_sellMoney_perEachCoin"] = \
                theoretical_outCoin_sellMoney_perEachCoin
            inout_baseinfo[inout_type]["theoretical_earningRate_perEachCoin"] = \
                theoretical_earningRate_perEachCoin
            inout_baseinfo[inout_type]["theoretical_outCoin_sellMoney_greedy_perEachCoin"] = \
                theoretical_outCoin_sellMoney_greedy_perEachCoin
            inout_baseinfo[inout_type]["theoretical_outCoin_sellMoney_greedy_grouping_perEachCoin"] = \
                theoretical_outCoin_sellMoney_greedy_grouping_perEachCoin
            inout_baseinfo[inout_type]["theoretical_earningRate_greedy_perEachCoin"] = \
                theoretical_earningRate_greedy_perEachCoin
            inout_baseinfo[inout_type]["theoretical_profit_byAtraCycleValue_perEachCoin"] = \
                theoretical_profit_byAtraCycleValue_perEachCoin
            inout_baseinfo[inout_type]["timestamp"] = average_orderBook_timestamp

        #earning_rate가  0보다 큰 후보를 list로 만듬
        #commodityRole_coin은 엄밀히 말하자면 eExchange의 commodityRole_coin 즉 base_coin과 반대되는 counter_coin임
        possible_trading_info_list = []
        for inout_type, data in inout_baseinfo.iteritems():
            for commodityRole_coin in data["theoretical_earningRate_greedy_perEachCoin"].keys():
                #지금 이 commodityRole_coin(counter_coin)에 맞는 theoretical profit도 꺼내어줌
                earningRate = data["theoretical_earningRate_greedy_perEachCoin"][commodityRole_coin]
                profit = data["theoretical_profit_byAtraCycleValue_perEachCoin"][commodityRole_coin]
                #decreasing_rate를 거친후 최종적인 atraCycle initial Value 임
                final_atraCycle_initial_value = data["theoretical_atraCycle_value_perEachCoin"][commodityRole_coin]
                # 양의 수익률을 갖고 있는 모든 결과는 모두 list에 들어가도록 한다 거래를 할지 말지는 아래 에서 결정
                possible_trading_info = {"inout_type": "", "commodityRole_coin": "", "earning_rate": 0}
                if earningRate > 0 :
                    possible_trading_info["inout_type"] = inout_type
                    possible_trading_info["commodityRole_coin"] = commodityRole_coin
                    possible_trading_info["earning_rate"] = earningRate
                    possible_trading_info["profit"] = profit
                    possible_trading_info["final_atraCycle_initial_value"] = final_atraCycle_initial_value

                    #만약 0이상의 거래 기회가 있다면 earning_rate가 큰순서대로 list 를 만들어줌
                    #만약 permitted_coin에 없다면 list에 안넣음
                    if commodityRole_coin not in self.atraCycle_setting_val["PERMITTED_COIN"]:
                        continue
                    elif len(possible_trading_info_list) == 0:
                        # print(earningRate, profit)
                        possible_trading_info_list.append(possible_trading_info)
                    elif len(possible_trading_info_list) > 0:
                        last_index = len(possible_trading_info_list) -1
                        newInfo_location_index = 0
                        #이미 있는 possible_trading_info 의 earning_rate와 지금 earning_rate
                        #를 비교해서 순서대로 배열 될 수 있게 해준다
                        for i, past_possible_trading_info in enumerate(possible_trading_info_list):
                            # print(earningRate, possible_trading_info_list[i]["earning_rate"])
                            # if earningRate >= possible_trading_info_list[i]["earning_rate"] :
                            if profit >= possible_trading_info_list[i]["profit"]:
                                newInfo_location_index = i
                                break
                            else:
                                if i ==  last_index:
                                    newInfo_location_index = i+1
                                    break
                        possible_trading_info_list.insert(newInfo_location_index, possible_trading_info)

        max_earningRate_info = {"inout_type": "", "commodityRole_coin": "", "earning_rate": 0, "profit" : 0}
        if len(possible_trading_info_list) > 0:
            max_earningRate_info = possible_trading_info_list[0]

        atraCycle_thread_name = "atraCycle"
        # print("atraCycle ThreadName" ,AtraUTIL.threadController("find", atraCycle_thread_name))
        if not AtraUTIL.threadController("find",thread_name=atraCycle_thread_name) and is_atraCycle_permitted:
            print("trading thread start.............")

            #threashold_rate보다 높으면 무조건 thread에 신호를 보내서 작동 시킨다 그러기 위해서 pairObj에서
            #접근 가능해야함 event객체를 pairingObj에 넣어줘서 다음부터 계속 접근 가능하게 만듬
            # atraCycle_thread_event = threading.Event()
            # self.atraCycle_thread_event = atraCycle_thread_event
            # atraCycleObj = AtraCycle(name=atraCycle_thread_name, args=(self, atraCycle_thread_event))
            # atraCycleObj.start()
            SharedStorage._initAtraCycleThread()

        # 각 pairingObj에 저장되며  뒤에서 return으로 반환도 시킴 이위치에 있어야 atraCycle 쓰레드에서 사용함
        #self.costAndProfit_basedOn_inOutCoin_latest = inout_baseinfo 이렇게 바꾸면 assigning 하는 reference 주소값이 아예 바뀌어 버려서 기존에  costAndProfit_basedOn_inOutCoin_latest
        #를 할당했던 변수들은 새로운 값과 아무 관련 없어짐  만약  val["whenInCoin"] ={new incoin dict}  val["whenOutCoin"] ={new outcoin dict}
        #이런식으로 한다면 계속 동일한 레퍼런스를 유지할 수 있음음
        self.costAndProfit_basedOn_inOutCoin_latest = inout_baseinfo
        self.costAndProfit_basedOn_inOutCoin_latest["max_earningRate_info"] = max_earningRate_info
        # 지금은 하나의 pair 당 하나의 trading 점유를 허용하지만 추후에 투자금이 쌓이면commodityRole 코인 하나당
        # 거래를 허용하는 식  즉  {BTC : thread , ETH : thread, ETC : thread } 식으로  관리하는것도 생각해 볼것

        #trading 안하는 이유를 명시하게 하고 안되는 이유가 없을때만 trading 시작
        no_trading_reason = []
        if max_earningRate_info["earning_rate"] < self.atraCycle_setting_val["THREASHOLD_RATE"] :
            no_trading_reason.append("low_earningRate")
        if max_earningRate_info["profit"] < self.atraCycle_setting_val["THREASHOLD_PROFIT"] :
            no_trading_reason.append("low_profit")
        if not is_atraCycle_permitted:
            no_trading_reason.append("trading is not Permitted")
        if latest_ioExchange_balance_timestamp == None or latest_eExchange_balance_timestamp == None:
            no_trading_reason.append("empty_balance_info")
        if (abs(average_orderBook_timestamp - latest_ioExchange_balance_timestamp)) > 5 and \
                                 abs((average_orderBook_timestamp - latest_eExchange_balance_timestamp)) > 5:
            print("sec difference reference orderBook and ioExchange_obj balance :" + str(abs(average_orderBook_timestamp - latest_ioExchange_balance_timestamp)))
            print("sec difference reference orderBook and eExchange_obj balance  :" + str(average_orderBook_timestamp - latest_eExchange_balance_timestamp))
            no_trading_reason.append("balance_info_is_tooOld")

        if SharedStorage.pairingObj_connectedWith_atraCycle == None:
            if len(possible_trading_info_list) > 0:
                # 모든 + 의 profit 이 가능한 atraCycle 계산결과들
                log_set["possible_trading_info_list"] = possible_trading_info_list
                # 그중에서 가장 높은 profit을 가진 것
                log_set["max_earningRate_info"] = max_earningRate_info
                log_set["no_trading_reason"] = str(no_trading_reason)
            log_set["atraCycle_settingVal"] = ("THR : > " + str(self.atraCycle_setting_val["THREASHOLD_RATE"]), \
                                              "THP : > " + str(self.atraCycle_setting_val["THREASHOLD_PROFIT"]) , "IS_ATRACYCLE_PERMITTED :" + str(is_atraCycle_permitted))
            logging_normal.info(pformat(log_set, indent=3))
        # 이미 자기 자신의 이전계산 결과에 의한 atraCycle이 이미 진행 중 이거나 다른 exchangePairing이 진행중이면 atraCycle진행하지 않음
        else :
            no_trading_reason.append("already other exchangePairing is on atraCycle")

        #문제될게 없으면 trading 시작
        if len(no_trading_reason) ==  0 :
            #이제 모든 pairing의 calculate thread가  atraCycle을 요청할 수 있으므로 공통함수로 뺐음
            SharedStorage.requestAtraCycle(self)

        # print(self.costAndProfit_basedOn_inOutCoin_latest)

        #이걸해줘야 다음 orderBook 이 왔을때 계산을 불필요하게 안함, 근데 거의 그런일은 없을듯
        self.is_calculating_now = False

        #atraCnp DB저장 모듈을 활성화 했고 초기화 테스트가 완료  됐을경우 module queue에 집어 넣음
        if atra_module_interactor != None and "atraCnp_DB" in [DB_client for DB_client in atra_module_interactor.atra_DB_client]:
            atraCnpData_for_dbPut = {"destination" : "atraCnp_DB","data" : {"baseCnpinfo" : inout_baseinfo, "paring_name" : self.exchangePairing_name}}
            atra_module_interactor.shared_queue.put(atraCnpData_for_dbPut)

        # 반환도 시킴 알아서 사용할수있게(개발초기 단일 호출로 test로 사용해 먹을때)
        return inout_baseinfo["whenInCoin"], inout_baseinfo["whenOutCoin"]

    def calculateAvailableAtraCycleValueByBalance(self, counter_coin, inout_type):
        '''IOExchange의 counter_coin , base_coin  EExchange의 counter_coin,base_coin 을 모두 ioExchange의 moneyRole(fiat krw)의
        가치로 환산한다. 하나의 Atra  Cycle이 가능하려면 이 네개의 가치중 가장 낮은  value 기준으로 돌아야 되기 때문
        but 하나의 방향만을 고려한 AtraCycle일 경우 네곳이 아니라 두곳에만 있어도 가능함 (ex.ltc whenInCoin일때 eExchange의 ltc와  ioExchange_obj 의 base_coin
        '''
        available_atraCycle_value_list = {}
        latest_eExchange_balance = SharedStorage.balance_eachExchange[self.pairing_exchangeName_dict["EExchange"]]
        latest_ioExchange_balance = SharedStorage.balance_eachExchange[self.pairing_exchangeName_dict["IOExchange"]]
        latest_ioExchange_orderBook = SharedStorage.orderbook_eachExchange[self.pairing_exchangeName_dict["IOExchange"]]
        latest_eExchange_orderBook = SharedStorage.orderbook_eachExchange[self.pairing_exchangeName_dict["EExchange"]]

        base_coin = self.base_coin

        # ioExchange_obj 의 moneyRole로 파악된 currency임( earning Fiat 모드 라면 fiat(ex.krw or usd)가 되고
        # earning coin 모드라면 코인 갯수를 늘어나야 하므로 ioExchange의 moneyRole임(ex.BTC)
        ioExchange_moneyRole= self.money_role_eachExchange["IOExchange"]

        eExchange_counterCoin_value_converted_ioExchange_moneyRole = \
            int(latest_ioExchange_orderBook[ioExchange_moneyRole][counter_coin]["bids"][0]["price"] *
                latest_eExchange_balance[counter_coin]["available"])
        eExchange_baseCoin_value_converted_ioExchange_moneyRole = \
            int(latest_ioExchange_orderBook[ioExchange_moneyRole][base_coin]["bids"][0]["price"] *
                latest_eExchange_balance[base_coin]["available"])
        ioExchange_counterCoin_value_converted_ioExchange_moneyRole = \
            int(latest_ioExchange_orderBook[ioExchange_moneyRole][counter_coin]["bids"][0]["price"] *
                latest_ioExchange_balance[counter_coin]["available"])
        ioExchange_baseCoin_value_converted_ioExchange_moneyRole = \
            int(latest_ioExchange_orderBook[ioExchange_moneyRole][base_coin]["bids"][0]["price"] *
                latest_ioExchange_balance[base_coin]["available"])

        #whenInCoin 으로 counterCoin이 들어갈때는 상대적으로 ioExchange의 counterCoin 가격이 쌀 때 이므로
        #eExchange_obj 에서는  counterCoin만 있으면 되며 ioExchange에서는 baseCoin만 있으면 AtraCycle 가능
        if inout_type == "whenInCoin":
            available_atraCycle_value_list["eExchange_counterCoin_value_converted_ioExchange_moneyRole"] = \
                eExchange_counterCoin_value_converted_ioExchange_moneyRole
            available_atraCycle_value_list["ioExchange_baseCoin_value_converted_ioExchange_moneyRole"] = \
                ioExchange_baseCoin_value_converted_ioExchange_moneyRole
        #whenInCoin 으로 counterCoin이 들어갈때는 상대적으로 ioExchange의 counterCoin 가격이 쌀 때 이므로
        #eExchange_obj 에서는  counterCoin만 있으면 되며 ioExchange에서는 baseCoin만 있으면 AtraCycle 가능
        elif inout_type == "whenOutCoin":
            available_atraCycle_value_list["eExchange_baseCoin_value_converted_ioExchange_moneyRole"] = \
                eExchange_baseCoin_value_converted_ioExchange_moneyRole
            available_atraCycle_value_list["ioExchange_counterCoin_value_converted_ioExchange_moneyRole"] = \
                ioExchange_counterCoin_value_converted_ioExchange_moneyRole

        available_atraCycle_value_list["lowest_value"] = \
            min([value for key,value in available_atraCycle_value_list.iteritems()])
        #
        # print(counter_coin, inout_type)
        # print(available_atraCycle_value_list)

        return available_atraCycle_value_list

class AtraCycle(threading.Thread):
    ATRACYCLE_SUCCEEDED = 0
    FROM_IOEXCHANGE_TRANSFERRING = 5
    FROM_EEXCHANGE_TRANSFERRING = 4
    IOEXCHANGE_IN_TRADING = 3
    IOEXCHANGE_OUT_TRADING = 2
    EEXCHANGE_TRADING =1
    def __init__(self, target = None, name=None, args=None, kwargs=None):
        #쓰레드 레벨에서 부모(Thread클래스) 생성자에 다음과 같이 넘기면
        #생성된 쓰레드 객체 레벨에서 name args kwargs 등등을 접근할수 있다
        #만약 부모 쓰레드를 생성자로 생성하기만 하고 넘기지 않으면  함수 내부에서 self로
        #접근할수 있지만 외부에서 쓰레드 객체로 접근할숭 없을듯 하다  일든 그냥 대충 해보자
        threading.Thread.__init__(self, target=target, name=name)
        # self.pairingObj = args[0]
        self.atraCycle_thread_event = args[0]
        #pairingObj에서도 접근할 수 있도록 빼준다
        # self.pairingObj.nowTradingObj = self
        # #pairing Obj에서 바로 공통적으로 도출되는 것들은 생성자에서 thread 객체 전역변수로 빼줌
        # self.ioExchange_name = self.pairingObj.pairing_exchangeName_dict["IOExchange"]
        # self.eExchange_name = self.pairingObj.pairing_exchangeName_dict["EExchange"]
        # self.ioExchange_moneyRole = self.pairingObj.money_role_eachExchange["IOExchange"]
        # self.eExchange_moneyRole = self.pairingObj.money_role_eachExchange["EExchange"]
        # self.base_coin = self.pairingObj.base_coin
        # self.paired_commodityRole_coin_sequence = self.pairingObj.paired_commodityRole_coin_sequence
    #run 함수가 중앙에서 나머지를 통제하여 거래를 완료 시킨다

    def _initPairingObjInfo(self):
        #pairingObj와 atraCycle 진행중인 Obj 를 상호 참조 시킨다.
        self.pairingObj.nowTradingObj = self
        # pairing Obj에서 바로 공통적으로 도출되는 것들은 생성자에서 thread 객체 전역변수로 빼줌
        self.ioExchange_name = self.pairingObj.pairing_exchangeName_dict["IOExchange"]
        self.eExchange_name = self.pairingObj.pairing_exchangeName_dict["EExchange"]
        self.ioExchange_moneyRole = self.pairingObj.money_role_eachExchange["IOExchange"]
        self.eExchange_moneyRole = self.pairingObj.money_role_eachExchange["EExchange"]
        self.base_coin = self.pairingObj.base_coin
        self.paired_commodityRole_coin_sequence = self.pairingObj.paired_commodityRole_coin_sequence

    def run(self):
        print("SharedStorage.pairingObj_connectedWith_atraCycle", SharedStorage.pairingObj_connectedWith_atraCycle)
        print("//-> before while")
        while True :
            #pairing Obj의 calculate 에서 eventSet할때까지 일단 기다림
            print("//-> before wait")
            is_event_setted = self.atraCycle_thread_event.wait()
            # logging_atraCycle.info(
            print("//-> after wait")
            logging_atraCycle.info("-------------------------------------AtraCycleStart-------------------------------------------------------#"
                            "\n----------------------------------------------------------------------------------------------------------#")
            AtraUTIL.notifyInfoUsingBeep("normal")
            #이미 SharedStorage.requestAtraCycle() 에서 pairingObj_connectedWith_atraCycle 안에 pairingObj를 넣었으므로
            self.pairingObj = SharedStorage.pairingObj_connectedWith_atraCycle
            self.pairingObj.is_atraCycle_now = True  #필요없으니 지울것
            #이제 여러 종류의 pairingObj들과 붙어 먹을것 이기 때문에 connected 될때마다 해당 pairing Obj의 내용을가져온다
            self._initPairingObjInfo()
            #한번만 호출하고 끝내기위해서 잠깐 해놓음
            # self.pairingObj.atraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = False
            #pairingObj의 costAndProfit_basedOn_inOutCoin_latest 은 새로운 calculateCostAndProfit 의 계산을 거치면
            #  통째로 바뀌기 때문에 (val =  { ... })  당연히 여기서 지정한  값은 원래의 레퍼런스를 유지하며
            # 변하지 않음 ( hardcopy나 shallow copy 때문에 안 변하는게 아님)
            self.baseCNP = self.pairingObj.costAndProfit_basedOn_inOutCoin_latest
            self.baseCNP_timestamp = self.pairingObj.costAndProfit_basedOn_inOutCoin_latest["whenInCoin"]["timestamp"]

            #얕은 복사를 해놔야 calculateCostAndProfit에서 max_earningRate_info  dict 의 부분 value를 바꿀때 같이 바뀌지 않음
            self.atraCycle_info = {}

            self.atraCycle_info["atraCycle_initial_info"] = self.pairingObj.costAndProfit_basedOn_inOutCoin_latest["max_earningRate_info"].copy()

            self.atraCycle_info["atraCycle_initial_info"]["exchangePairing_name"] = self.pairingObj.exchangePairing_name
            self.atraCycle_info["atraCycle_initial_info"]["ioExchange_moneyRole"] = self.ioExchange_moneyRole
            self.atraCycle_info["atraCycle_initial_info"]["eExchange_moneyRole"] = self.eExchange_moneyRole
            self.atraCycle_info["atraCycle_initial_info"]["atra_type"] = self.pairingObj.atra_type
            self.atraCycle_info["atraCycle_initial_info"]["client_name"] = SharedStorage.client_name

            logging_atraCycle.info(
                "atraCycle_initial " + self.pairingObj.exchangePairing_name + "\n" + pformat(
                    self.atraCycle_info["atraCycle_initial_info"], indent=3) + "\n")

            self.atraCycle_info["trade_process_info"] = {"eExchange_trade" : {}, "ioExchange_inTrade" : {}, "ioExchange_outTrade" : {}}
            self.atraCycle_info["transfer_process_info"] =  {"from_eExchange" : {}, "from_ioExchange" : {}}

            #actual_profit 같은경우  transfer가 시작하기 전에 trading 결과 만으로도 도출될 수 있으나 finalized_info에 넣는다
            self.atraCycle_info["atraCycle_finalized_info"] = {"actual_profit" : None, "actual_earningRate" : None}

            eExchange_trade_process_info = self.atraCycle_info["trade_process_info"]["eExchange_trade"]\
                = { "predictive" : {}, "actual" : {}, "is_succeeded" : None}
            ioExchange_inTrade_process_info = self.atraCycle_info["trade_process_info"]["ioExchange_inTrade"]\
                = { "predictive" : {}, "actual" : {}, "is_succeeded" : None}
            ioExchange_outTrade_process_info = self.atraCycle_info["trade_process_info"]["ioExchange_outTrade"]\
                = { "predictive" : {}, "actual" : {}, "is_succeeded" : None}
            from_eExchange_transfer_process_info = self.atraCycle_info["transfer_process_info"]["from_eExchange"] \
                = {"is_succeeded": None}
            from_ioExchange_transfer_process_info = self.atraCycle_info["transfer_process_info"]["from_ioExchange"] \
                = {"is_succeeded": None}

            commodityRole_coin = self.atraCycle_info["atraCycle_initial_info"]["commodityRole_coin"]
            inout_type = self.atraCycle_info["atraCycle_initial_info"]["inout_type"]
            #commodityRoleCoin이 outCoin이라면 EExchange 에서는 inCoin인 baseCoin으로 counterCoin을 buy를 하게 된다
            eExchange_trade_type = eExchange_trade_process_info["predictive"]["trade_type"] = \
                eExchange_trade_process_info["actual"]["trade_type"] = \
                "buy" if inout_type == "whenOutCoin" else "sell"

            # calculateCostAndProfit에서 만들어지는 모든 perEachCoin 데이터는 sell상황이던 buy 상황이던 commodityRole_Coin이 무엇이냐에
            # 따른 기준으로 만들어진 것이다 따라서 EExchange 에서 거래를 buy든 sell이든 상관없이 actualTrade는 commodityRoleCoin 기준으로 저장되어있음
            #calculateCostAndProfit 에서는 commodityRoleCoin이 inCoin이냐 outCoin이냐에 따라서 분기했음
            inCoin = self.atraCycle_info["atraCycle_initial_info"]["inCoin"] = commodityRole_coin if inout_type == "whenInCoin" else self.base_coin
            outCoin = self.atraCycle_info["atraCycle_initial_info"]["outCoin"] = commodityRole_coin if inout_type == "whenOutCoin" else self.base_coin

            #거래소api 가 시장가 주문을 지원하지 않을경우 (ex . poloniex) + cycle의 첫번째 일 경우 CNP에서 예상된 orderPrice를 뽑아내서 주문한다
            #주로 atraCycle의 첫번째 trading인(현재까진)  eExchange은 Fill or Kill 로만 주문하게 된다. 가격이 급변할 리스크를 줄이기 위해
            predictive_orderPrice_for_eExchangeTrade = eExchange_trade_process_info["predictive"]["predictive_orderPrice_for_eExchangeTrade"] = \
                self.baseCNP[inout_type]["theoretical_orderPrice_for_eExchangeTrade_perEachCoin"][commodityRole_coin]

            #역시 inCoin이냐 outCoin이냐에 따라서 데이터가 저장된 것이 아니라 commodityRole_coin 이 inCoin이냐 outCoin 이냐에 따라무엇이냐에 따라
            #저장되어 있다 헷갈리지 말것
            # predictive_inCoin_transferredAmount = predictive_outCoin_orderAmount_EExchangeTrade(굳이 만들진 않았음 나중에 이 변수명이 더 적합하다고 생각하면 바꿀것)
            predictive_inCoin_transferredAmount = eExchange_trade_process_info["predictive"]["predictive_inCoin_transferredAmount"] = \
                self.baseCNP[inout_type]["theoretical_inCoin_transferredAmount_greedy_perEachCoin"][commodityRole_coin]
            predictive_outCoin_tradedAmount_withFee = eExchange_trade_process_info["predictive"]["predictive_outCoin_tradedAmount_withFee"] = \
                self.baseCNP[inout_type]["theoretical_outCoin_tradedAmount_greedy_perEachCoin"][commodityRole_coin]

            #trading 수량 test시 축소하려고 놔둔것임
            predictive_inCoin_transferredAmount = predictive_inCoin_transferredAmount
            predictive_outCoin_tradedAmount_withFee = predictive_outCoin_tradedAmount_withFee

            logging_atraCycle.info("---------------------------------tradingStart-------------------------------------#")

            logging_atraCycle.info(str(inout_type) + "," + str(inCoin) + ", " + str(outCoin))

            #coin - coin 직접거래가 가능한(e.g poloniex) 거래소에서 먼져 거래를 시작한다 actual 한 inAmount와 outAmount을 동시에 확정할 수 있으므로
            #따라서 theoretical 가격으로 amount를 다 거래할 수 없으면  거래 취소 될 수 있는  fillOrKill 이 있으면사용할 것(e.g  poloniex) cycle 중간에 실패해도 손해 보지 않으므로
            #baseCoin으로 commodityRole_coin을 buy할때 이므로 계산된 매수주문 요청수량 predictive_outCoin_tradedAmount_withFee

            #todo need_stop이 필요한지 아침에 생각해 볼것
            decision_process_info = {"retry" : False, "need_stop" : False, "retry_count" : 0,"failed_log" :[]}
            while True:  #cycle 중간중간에 실패햘경우 retry를 해야 한다. do while 문이 없으므로 그냥 while문으로
                if inCoin == self.base_coin and inCoin == self.eExchange_moneyRole:
                    eExchange_trade_result = self.buy(self.eExchange_name, self.eExchange_moneyRole,commodityRole_coin ,
                                                      predictive_orderPrice_for_eExchangeTrade, predictive_outCoin_tradedAmount_withFee)
                    #buy restful output 값중에 total은 무조건 (eExchange_obj moneyRole==baseCoin) 의 결과값임 buy이므로 commodityRoleCoin을 사는데에
                    # 들어간 비용이다 즉 실제 inCoin transferredAmount가 됨

                #commodityRole_coin을 sell 해서 outCoin인 (baseCoin = eExchange_moneyRole)을 구할때 이므로 계산된 매도 수량인
                #predictive_inCoin_transferredAmount 가 들어간다
                elif outCoin == self.base_coin:
                    eExchange_trade_result = self.sell(self.eExchange_name, self.eExchange_moneyRole,
                                                       commodityRole_coin, predictive_orderPrice_for_eExchangeTrade, predictive_inCoin_transferredAmount)

                logging_atraCycle.info("EExchange_trade_result from "+ self.eExchange_name +"\n" + pformat(eExchange_trade_result, indent=3) + "\n")

                #성공시 계속 반복문에서 빠져나와 cycle 계속 진행
                if eExchange_trade_result["is_succeeded"] == True :
                    eExchange_trade_process_info["is_succeeded"] = True
                    break

                # 거래가 실패했다면 판단기에서 결정을 받아옴
                elif eExchange_trade_result["is_succeeded"] == False:
                    AtraUTIL.notifyInfoUsingBeep("warning")
                    #failed 됐다면 result는 failed dict가 옴
                    failed_result = eExchange_trade_result
                    logging_atraCycle.warning("trade failed , now asking decision processor .. \n" 
                                            "old_decision_process_info before decision Processing : \n" +
                                            pformat(decision_process_info,indent=3)+ "\n")
                    decision_process_info = self._decideProcess(failed_result, decision_process_info)
                    logging_atraCycle.warning("new_decision_process_info after decision Processing : \n"
                                            + pformat(decision_process_info, indent=3) + "\n")

                    if decision_process_info["retry"] == True:
                        logging_atraCycle.warning("failed EExchange trading from " + self.eExchange_name + " is retryable, retrying it Now \n" +
                                                "retry_count : " + str(decision_process_info["retry_count"]))
                        #재시도 횟수(sec) 만큼 쉰뒤 retry 한다
                        time.sleep(decision_process_info["retry_count"])
                        continue

                    else :
                        #roll_back 시키고 계속 atra를 진행시킨다
                        #todo: 추후에 failed_result의 is_fatal_error 체크에서 rollback 후에 아예 종료 시킬지 프로그램을 지속시킬지 결정
                        logging_atraCycle.warning(
                            "failed trading from " + self.eExchange_name + "is NOT retryable, need rollBack")
                        self._finalizeAtraCycle(eExchange_trade_result["is_fatal_error"])
                        break
                        # os._exit(1)

            #retry 최종적으로 실패했으면 cycling 종료후 돌아가서 다시 thread event 기다림
            if eExchange_trade_result["is_succeeded"] == False:
                break

            eExchange_overall_trade_result = eExchange_trade_result["overall_result"]

            # print("eExchange_overall_trade_result :" + str(eExchange_overall_trade_result))
            #todo :ioExchange_outTrade_process_info 안에 들어갈 내용들 집어 넣을것
            #buy일때 sell 일때에 따라 각각 실제로 eExchange에서의 거래로 내가 얻게 되는 종류가 다르다
            #당연히 base_Coin이 inCoin일때는 buy고 result에서는 코인을 얻는양인 "amount" 항목임
            actual_outCoinAmount_afterEExchangeTrade_withoutFee = \
                eExchange_trade_process_info["actual"]["actual_outCoinAmount_afterEExchangeTrade_withoutFee"] = \
                eExchange_overall_trade_result["total"] if eExchange_trade_type == "sell" else eExchange_overall_trade_result["amount"]
            actual_outCoinAmount_afterEExchangeTrade_withFee = \
                eExchange_trade_process_info["actual"]["actual_outCoinAmount_afterEExchangeTrade_withFee"] = \
                actual_outCoinAmount_afterEExchangeTrade_withoutFee * (1 - self.pairingObj.feeInfo_atraPairing_specified[self.eExchange_name]["tradeFee_ratio"])
            #predictive_inCoin_transferredAmount는 위에서 예상치로 정의되어 있음 이건  실제로 거래된 결과를 기반으로  ioExchange에서 inCoin
            #구매를 역산하기 위해서 필요한 것임 만약 eExchange에서 만약 예를 들어 poloniex라면 immediateOrCancel
            #로 주문을 넣을때 일부만 체결되고 일부는 취소될 경우  predictive 한 값보다 적은양을 buy하거나 sell 하게 될 수있으므로 이 값을 추가 시킴
            #만약 시장가 거래나 fillOrKill 로 거래 된다면 predictive값과 동일하게 됨
            actual_inCoinAmount_forEExchangeTrade = eExchange_trade_process_info["actual"]["actual_inCoinAmount_forEExchangeTrade"]  = \
                eExchange_overall_trade_result["total"] if eExchange_trade_type == "buy" else eExchange_overall_trade_result["amount"]
            #거래 체결된 결과로 나온 average Price   greedy 하게 거래를 했다면 한 가격으로 거래되진 않았으므로 평균매수가격이 필요함
            actual_averagePrice_afterEExchangeTrade = eExchange_trade_process_info["actual"]["actual_averagePrice_afterEExchangeTrade"] = \
                eExchange_overall_trade_result["price_rate"]

            #ioMarket에서 구매할 inCoin 수량을 구함 counter_coin 일수도 있고 base_Coin일수도 있음
            predictive_inCoin_orderAmount_IOExchangeTrade = ioExchange_inTrade_process_info["predictive"]["predictive_inCoin_orderAmount_IOExchangeTrade"] = \
                self._calculateInverseCoinAmount(self.ioExchange_name, inCoin, actual_inCoinAmount_forEExchangeTrade)
            # ioMarket에서 sell할 outCoin 수량을 구함 counter_coin 일수도 있고 base_Coin일수도 있음  당연히 actual_outCoinAmount_afterEExchangeTrade_withFee 에서 출금수수료를 제외한 수량이다
            predictive_outCoin_orderAmount_IOExchangeTrade = ioExchange_outTrade_process_info["predictive"]["predictive_outCoin_orderAmount_IOExchangeTrade"] = \
                actual_outCoinAmount_afterEExchangeTrade_withFee -  self.pairingObj.feeInfo_atraPairing_specified[self.eExchange_name]["transferFee_coin"][outCoin]
            print("\n" + "actual_inCoinAmount_forEExchangeTrade:" + str(actual_inCoinAmount_forEExchangeTrade))
            print("actual_outCoinAmount_afterEExchangeTrade_withoutFee:" + str(actual_outCoinAmount_afterEExchangeTrade_withoutFee))
            print("actual_outCoinAmount_afterEExchangeTrade_withFee:" + str(actual_outCoinAmount_afterEExchangeTrade_withFee))
            print("actual_averagePrice_afterEExchangeTrade:" +str(actual_averagePrice_afterEExchangeTrade))
            print("predictive_inCoin_orderAmount_IOExchangeTrade:"+str(predictive_inCoin_orderAmount_IOExchangeTrade))
            print("predictive_outCoin_orderAmount_IOExchangeTrade:" +str(predictive_outCoin_orderAmount_IOExchangeTrade) + "\n")

            decision_process_info = {"retry": False, "need_stop": False, "retry_count": 0, "failed_log": []}
            while True:  #cycle 중간중간에 실패햘경우 retry를 해야 한다. do while 문이 없으므로 그냥 while문으로

                ioExchange_outTrade_result = self.sell(self.ioExchange_name, self.ioExchange_moneyRole,
                                                       outCoin, 0, predictive_outCoin_orderAmount_IOExchangeTrade)

                logging_atraCycle.info("IOExchange_outTrade_result from" + self.ioExchange_name +"\n" + pformat(ioExchange_outTrade_result, indent=3) + "\n")

                # 성공시 계속 반복문에서 빠져나와 cycle 계속 진행
                if ioExchange_outTrade_result["is_succeeded"] == True:
                    ioExchange_outTrade_process_info["is_succeeded"] = True
                    break

                # 거래가 실패했다면 판단기에서 결정을 받아옴
                elif ioExchange_outTrade_result["is_succeeded"] == False:
                    AtraUTIL.notifyInfoUsingBeep("warning")
                    # failed 됐다면 result는 failed dict가 옴
                    failed_result = ioExchange_outTrade_result

                    logging_atraCycle.warning("trade failed , now asking decision processor .. \n"
                                            "old_decision_process_info before decision Processing : \n" +
                                            pformat(decision_process_info, indent=3) + "\n")
                    decision_process_info = self._decideProcess(failed_result, decision_process_info)
                    logging_atraCycle.warning("new_decision_process_info after decision Processing : \n"
                                            + pformat(decision_process_info, indent=3) + "\n")

                    if decision_process_info["retry"] == True:

                        logging_atraCycle.warning(
                            "failed ioExchange_obj OUT trading from  " + self.ioExchange_name + " is retryable, retrying it Now \n" +
                            "retry_count : " + str(decision_process_info["retry_count"]))

                        # 재시도 횟수(sec) 만큼 쉰뒤 retry 한다
                        time.sleep(decision_process_info["retry_count"] )
                        continue

                    else:
                        # roll_back 시키고 계속 atra를 진행시키거나 종료시킨다
                        # todo: 추후에 failed_result의 is_fatal_error 체크에서 rollback 후에 아예 종료 시킬지 프로그램을 지속시킬지 결정
                        logging_atraCycle.warning(
                            "failed ioExchange_obj OUT trading from " + self.ioExchange_name + " is NOT retryable, need rollBack")
                        self._finalizeAtraCycle(ioExchange_outTrade_result["is_fatal_error"])
                        os._exit(1)

            ioExchange_overall_outTrade_result = ioExchange_outTrade_result["overall_result"]

            #IOExchange의 outCoinAmount는 inCoin이 되었든 outCoin이 되었든 무조건 coin이므로 result의 amount
            actual_outCoinAmount_IOExchangeTrade = \
                ioExchange_outTrade_process_info["actual"]["actual_outCoinAmount_IOExchangeTrade"] = \
                ioExchange_overall_outTrade_result["amount"]

           #triangular 이므로 ioExchange의 moneyRole 로 sell이 이루어짐 그걸 moneyRoleSellTotal  명명
            actual_moneyRoleSellTotal_IOExchangeTrade = \
                ioExchange_outTrade_process_info["actual"]["actual_moneyRoleSellTotal_IOExchangeTrade"] = \
                ioExchange_overall_outTrade_result["total"]

            #일단 outTrade로 확보된 fiat로 inTrade를 진행하기 때문에 너무빠르게 주문을 하면 IOExchange fiat(krw) 가 부족 하다고 뜰수있음 fiat를 보유하고 있으면 상관없음
            #좆같은 bithumb같은경우 추가 order를 하려면 10초를 기다려야 하기 때문에  9초 기다림
            #2018년 3월 27일 현재 없어진 것으로 보임
            time.sleep(0.5)

            decision_process_info = {"retry": False, "need_stop": False, "retry_count": 0, "failed_log": []}
            while True:  #cycle 중간중간에 실패햘경우 retry를 해야 한다. do while 문이 없으므로 그냥 while문으로

                ioExchange_inTrade_result = self.buy(self.ioExchange_name, self.ioExchange_moneyRole, inCoin,
                                                     0, predictive_inCoin_orderAmount_IOExchangeTrade)

                logging_atraCycle.info(
                    "IOExchange_IN Trade_result from" + self.ioExchange_name +"\n" + pformat(ioExchange_inTrade_result, indent=3) + "\n")

                # 성공시 계속 반복문에서 빠져나와 cycle 계속 진행
                if ioExchange_inTrade_result["is_succeeded"] == True:
                    ioExchange_inTrade_process_info["is_succeeded"] = True
                    break

                # 거래가 실패했다면 판단기에서 결정을 받아옴
                elif ioExchange_inTrade_result["is_succeeded"] == False:
                    AtraUTIL.notifyInfoUsingBeep("warning")
                    failed_result = ioExchange_inTrade_result
                    logging_atraCycle.warning("trade failed , now asking decision processor .. \n"
                                            "old_decision_process_info before decision Processing : \n" +
                                            pformat(decision_process_info, indent=3) + "\n")
                    decision_process_info = self._decideProcess(failed_result, decision_process_info)
                    logging_atraCycle.info("new_decision_process_info after decision Processing : \n"
                                            + pformat(decision_process_info, indent=3) + "\n")

                    if decision_process_info["retry"] == True:
                        logging_atraCycle.warning(
                            "failed ioExchange_obj IN trading from " + self.ioExchange_name + " is retryable, retrying it Now \n" +
                            "retry_count : " + str(decision_process_info["retry_count"]))
                        # 재시도 횟수(sec) 만큼 쉰뒤 retry 한다
                        time.sleep(decision_process_info["retry_count"])
                        continue

                    else:
                        # 더이상 재시도 하지 않으니 roll_back 시키고 계속 atra를 진행시키거나 종료시킨다
                        # todo: 추후에 failed_result의 is_fatal_error 체크에서 rollback 후에 아예 종료 시킬지 프로그램을 지속시킬지 결정
                        logging_atraCycle.warning(
                            "failed ioExchange_obj IN trading from " + self.ioExchange_name + " is NOT retryable, need rollBack")
                        self._finalizeAtraCycle(ioExchange_inTrade_result["is_fatal_error"])
                        os._exit(1)

            ioExchange_overall_inTrade_result = ioExchange_inTrade_result["overall_result"]

            # IOExchange의 outCoinAmount는 inCoin이 되었든 outCoin이 되었든 무조건 coin이므로 result의 amount
            actual_outCoinAmount_IOExchangeTrade = \
                ioExchange_inTrade_process_info["actual"]["actual_inCoinAmount_IOExchangeTrade"] = \
                ioExchange_overall_inTrade_result["amount"]

            # triangular 이므로 ioExchange의 fiat(moneyRole)로 sell이 이루어짐 그걸 outSellFiat로 명명
            actual_moneyRoleBuyTotal_IOExchangeTrade = \
                ioExchange_inTrade_process_info["actual"]["actual_moneyRoleBuyTotal_IOExchangeTrade"] = \
                ioExchange_overall_inTrade_result["total"]

            logging_atraCycle.info("---------------------------------tradingEnd-------------------------------------#\n")
            time.sleep(5)
            self._calculateActualProfit()

            # logging_atraCycle.info("---------------------------------transfer start----------------------------------#")
            # #from exchange 기준으로 변수명 만들었음(eExchange_obj == fromExchange 일때)
            # #eExchange는 cycle 이후 코인의 증감이 없어야 하므로 eExchange이후에 증가된 outCoin을 모두 소진시켜야 한다 (ioE에 도착할 outCoin amount + transferFee)
            # eExchange_transfer_result = self.transfer(self.eExchange_name, self.ioExchange_name, outCoin, actual_outCoinAmount_afterEExchangeTrade_withFee)
            #
            # logging_atraCycle.info("ioExchange_obj transfer trying :" + str(actual_inCoinAmount_forEExchangeTrade))
            # #(ioExchange_obj == fromExchange일때)
            # ioExchange_transfer_result = self.transfer(self.ioExchange_name, self.eExchange_name, inCoin,actual_inCoinAmount_forEExchangeTrade)
            #
            # logging_atraCycle.info("eExchange_obj transfer result" + str(eExchange_transfer_result))
            # logging_atraCycle.info("ioExchange_obj transfer result" + str(ioExchange_transfer_result))
            # logging_atraCycle.info("---------------------------------transfer End-------------------------------------#")

            logging_atraCycle.info("---------------------------------transfer start----------------------------------#")
            #여기서는 will_be_deposited_amout 즉 (옮기고자 하는 양 == 도착하기를 희망하는 양)을 측정해서   transfer 함수를 호출하고
            #transfer 함수 에서는 willBe_deposited_amount(순수한 코인 이동양) 을 받으면 다른 판단은 하지 않음
            # 그 순수 이동량을 기반으로 api 특성에 따라서(수수료를 고려해서 transfer order를 내려야 되는지 여부)
            #order_amount를 만들어 낸다  여기서는 그것을 위해 willBe_deposited_amount를 계산한다

            # from exchange 기준으로 변수명 만들었음(eExchange_obj == fromExchange 일때)
            # eExchange는 cycle 이후 코인의 증감이 없어야 하므로 eExchange이후에 증가된 outCoin을 모두 소진시켜야 한다 (ioE에 도착할 outCoin amount + transferFee)
            #_calculateWillBeDepositedAmount에 보내서 그 소진액을 고려한 ioExchange가 받게되는 양(willBe_deposited_amount)을 계산 받아옴
            willBe_deposited_amount = self._calculateWillBeDepositedAmount(self.eExchange_name, self.ioExchange_name, outCoin,
                                                      actual_outCoinAmount_afterEExchangeTrade_withFee)
            decision_process_info = {"retry": False, "need_stop": False, "retry_count": 0, "failed_log": []}
            skipped = False
            while True:
                if outCoin in self.pairingObj.atraCycle_setting_val["TRANSFER_BAN"]:
                    skipped = True
                    print("BTC skip Mode so BTC Transfer skipped")
                    eExchange_transfer_result = {"is_succeeded" : True, "skipped" : True}
                    break
                eExchange_transfer_result = self.transfer(self.eExchange_name, self.ioExchange_name, outCoin,
                                                          willBe_deposited_amount)
                logging_atraCycle.info(
                    "eExchange_transfer_result from " + self.eExchange_name + "\n" + pformat(eExchange_transfer_result,indent=3) + "\n")

                # 성공시 계속 반복문에서 빠져나와 cycle 계속 진행
                if eExchange_transfer_result["is_succeeded"] == True:
                    from_eExchange_transfer_process_info["is_succeeded"] = True
                    break

                # 거래가 실패했다면 판단기에서 결정을 받아옴
                elif eExchange_transfer_result["is_succeeded"] == False:
                    AtraUTIL.notifyInfoUsingBeep("warning")
                    # failed 됐다면 result는 failed dict가 옴
                    failed_result = eExchange_transfer_result

                    logging_atraCycle.warning("transfer failed , now asking decision processor .. \n"
                                            "old_decision_process_info before decision Processing : \n" +
                                            pformat(decision_process_info, indent=3) + "\n")
                    decision_process_info = self._decideProcess(failed_result, decision_process_info)
                    logging_atraCycle.warning("new_decision_process_info after decision Processing : \n"
                                            + pformat(decision_process_info, indent=3) + "\n")

                    if decision_process_info["retry"] == True:

                        logging_atraCycle.warning(
                            "failed transfering from  " + self.eExchange_name + " is retryable, retrying it Now \n" +
                            "retry_count : " + str(decision_process_info["retry_count"]))

                        # 재시도 횟수(sec) 만큼 쉰뒤 retry 한다
                        time.sleep(decision_process_info["retry_count"])
                        continue

                    else:
                        # roll_back 시키고 계속 atra를 진행시키거나 종료시킨다
                        # todo: 추후에 failed_result의 is_fatal_error 체크에서 rollback 후에 아예 종료 시킬지 프로그램을 지속시킬지 결정
                        logging_atraCycle.warning(
                            "failed transfering from  " + self.eExchange_name + " is NOT retryable, need rollBack")
                        # 현재(171202)는 rollBack 기능이 없으므로 무조건 fatalerror로 종료시킴
                        self._finalizeAtraCycle(True)
                        os._exit(1)

            #그냥 atraStyleTransferResult를  통째로 DB에 넣는게 편할듯

            self.atraCycle_info["transfer_process_info"]["from_eExchange"] = eExchange_transfer_result
            skipped =False
            decision_process_info = {"retry": False, "need_stop": False, "retry_count": 0, "failed_log": []}
            while True:
                # if self.eExchange_name ==  "bitfinex" and inCoin  == "BTC" and False:
                print("transfer_ban", self.pairingObj.atraCycle_setting_val["TRANSFER_BAN"])
                if inCoin in self.pairingObj.atraCycle_setting_val["TRANSFER_BAN"]:
                    skipped = True
                    print("BTC skip Mode so BTC Transfer skipped")
                    ioExchange_transfer_result = {"is_succeeded": True, "skipped": True}
                    break
                # (ioExchange_obj == fromExchange일때)
                # ioExchange의 inTrade수량은 결국 eExchange_obj 의 inCoin 수량을 맞춰주기 위한것 이므로
                # actual_inCoinAmount_forEExchangeTrade == willBe_deposited_amount 가된다 따라서 따로 계산 필요없음
                ioExchange_transfer_result = self.transfer(self.ioExchange_name, self.eExchange_name, inCoin,
                                                           actual_inCoinAmount_forEExchangeTrade)
                logging_atraCycle.info(
                    "ioExchange_transfer_result from " + self.ioExchange_name + "\n" + pformat(ioExchange_transfer_result,
                                                                                             indent=3) + "\n")

                # 성공시 계속 반복문에서 빠져나와 cycle 계속 진행
                if ioExchange_transfer_result["is_succeeded"] == True:
                    from_ioExchange_transfer_process_info["is_succeeded"] = True
                    break

                # 거래가 실패했다면 판단기에서 결정을 받아옴
                elif ioExchange_transfer_result["is_succeeded"] == False:
                    AtraUTIL.notifyInfoUsingBeep("warning")
                    # failed 됐다면 result는 failed dict가 옴
                    failed_result = ioExchange_transfer_result

                    logging_atraCycle.warning("transfer failed , now asking decision processor .. \n"
                                              "old_decision_process_info before decision Processing : \n" +
                                              pformat(decision_process_info, indent=3) + "\n")
                    decision_process_info = self._decideProcess(failed_result, decision_process_info)
                    logging_atraCycle.warning("new_decision_process_info after decision Processing : \n"
                                              + pformat(decision_process_info, indent=3) + "\n")

                    if decision_process_info["retry"] == True:

                        logging_atraCycle.warning(
                            "failed transfering from " + self.ioExchange_name + " is retryable, retrying it Now \n" +
                            "retry_count : " + str(decision_process_info["retry_count"]))

                        # 재시도 횟수(sec) 만큼 쉰뒤 retry 한다
                        time.sleep(decision_process_info["retry_count"])
                        continue

                    else:
                        # roll_back 시키고 계속 atra를 진행시키거나 종료시킨다
                        # todo: 추후에 failed_result의 is_fatal_error 체크에서 rollback 후에 아예 종료 시킬지 프로그램을 지속시킬지 결정
                        logging_atraCycle.warning(
                            "failed transfering from  " + self.ioExchange_name + " is NOT retryable, need rollBack")
                        #현재(171202)는 rollBack 기능이 없으므로 무조건 fatalerror로 종료시킴
                        self._finalizeAtraCycle(True)
                        os._exit(1)

            # 그냥 통째로 DB에 넣는게 편할듯
            self.atraCycle_info["transfer_process_info"]["from_ioExchange"] = ioExchange_transfer_result
            # logging_atraCycle.info("eExchange_obj transfer result" + str(eExchange_transfer_result))
            # logging_atraCycle.info("ioExchange_obj transfer trying :" + str(actual_inCoinAmount_forEExchangeTrade))
            # logging_atraCycle.info("ioExchange_obj transfer result" + str(ioExchange_transfer_result))
            logging_atraCycle.info("---------------------------------transfer End-------------------------------------#")
            # winsound.Beep(400, 10000)
            # print("ioExchange_inTrade_result : ")
            # logging.debug(ioExchange_inTrade_result)
            # print("ioExchange_outTrade_result : ")
            # logging.debug(ioExchange_outTrade_result)
            self._finalizeAtraCycle(False)
            time.sleep(2)
            AtraUTIL.notifyInfoUsingBeep("normal")
            self.pairingObj.is_atraCycle_now = False
            SharedStorage.pairingObj_connectedWith_atraCycle = None
            self.pairingObj.atraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
            logging_atraCycle.info("----------------------------------------------------------------------------------------------------##\n"
                                 + "-------------------------------------AtraCycleEnd---------------------------------------------------##\n\n\n\n")

    def _calculateActualProfit(self):
        '''triangular인지  normal인지 구분하여 profit과 어떤종류의 currency 로 이득을 봤는지 계산한다'''
        profit_currencyType = None
        practical_profit = None
        accounting_profit = None
        practical_earningRate = None
        accounting_earningRate = None
        print("is it triangular!!!!!!!!!!!!"  + str(self.pairingObj.atra_type == "TRIANGULAR"))
        if self.pairingObj.atra_type == "TRIANGULAR":
            #triangular 이므로 IOExchange에서 두번 거래됨
            ioExchange_inTrade_process_info = self.atraCycle_info["trade_process_info"]["ioExchange_inTrade"]
            ioExchange_outTrade_process_info = self.atraCycle_info["trade_process_info"]["ioExchange_outTrade"]
            #triangular 에서는 무조건 IOExchange의 moneyRole로 수익을 보게됨 normal 역시도 마찬가지 일 것으로 보임
            profit_currencyType =  self.ioExchange_moneyRole

            print("actual_moneyRoleSellTotal_IOExchangeTrade in actual", "actual_moneyRoleSellTotal_IOExchangeTrade" in ioExchange_outTrade_process_info["actual"] )
            print("actual_moneyRoleBuyTotal_IOExchangeTrade in actual",  "actual_moneyRoleBuyTotal_IOExchangeTrade" in ioExchange_inTrade_process_info["actual"])
            if "actual_moneyRoleSellTotal_IOExchangeTrade" in ioExchange_outTrade_process_info["actual"]  and \
                    "actual_moneyRoleBuyTotal_IOExchangeTrade" in ioExchange_inTrade_process_info["actual"] :

                #bithumb 같이 prepaid 쿠폰을 지원하는 곳을 IOExchange로 두고 TRIANGULAR를 진행한경우  moneyRoleSellTotal,moneyRoleBuyTotal
                #등은 coupon 때문에 수수료가 0 인것처럼 거래가 되므로 눈으로 보이는 액수를 practical로 두고 수수료를 고려한것을  actual로 두어야함
                #prepaid coupon이 없다면 필요 없음(즉 practical과 actual이 동일함)
                practical_moneyRoleSellTotal_IOExchangeTrade = ioExchange_outTrade_process_info["actual"]["actual_moneyRoleSellTotal_IOExchangeTrade"]
                practical_moneyRoleBuyTotal_IOExchangeTrade = ioExchange_inTrade_process_info["actual"]["actual_moneyRoleBuyTotal_IOExchangeTrade"]

                accounting_moneyRoleSellTotal_IOExchangeTrade = 0
                accounting_moneyRoleBuyTotal_IOExchangeTrade = 0

                #prePaid coupon을 사용하는 IOExchange라면 실제 수익을 계산할때 조정해 줘야함
                if self.pairingObj.feeInfo_atraPairing_specified[self.ioExchange_name]["prePaid_coupon"] == True:
                    inCoin = self.atraCycle_info["atraCycle_initial_info"]["inCoin"]
                    outCoin = self.atraCycle_info["atraCycle_initial_info"]["outCoin"]
                    ioExchange_tradeFee = self.pairingObj.feeInfo_atraPairing_specified[self.ioExchange_name]["tradeFee_ratio"]

                    #prePaid 쿠폰으로 인해 0으로 적용된 ioExchange_obj 거래 수수료를 지불했다고 가정한 accounting_moneyRoleSellTotal과 accounting_moneyRoleBuyTotal
                    #을 만들어야함

                    #sellMoneyTotal은 outCoin의 판매액중에서 수수료를 제한 금액이지만 prePaid쿠폰인곳은 이 수수료가 0이라고 계산해서 받은 액수 이므로
                    #수수료를 제외 해야 실제 회계적인 sellMoneyTotal이 됨
                    accounting_moneyRoleSellTotal_IOExchangeTrade = practical_moneyRoleSellTotal_IOExchangeTrade * (1-float(ioExchange_tradeFee))

                    #buyMoneyTotal 은 atraCycle을 위해 inCoin을 사기위한 액수이나 prePaid쿠폰을 적용했을경우 수수료없이
                    #inCoin이 마련되므로 practical_moneyRoleBuyTotal 보다 더 많은 액수가 회계적인  buyTotal이라고 생각해야함
                    accounting_moneyRoleBuyTotal_IOExchangeTrade = practical_moneyRoleBuyTotal_IOExchangeTrade / ((1-float(ioExchange_tradeFee)))

                else :
                    #prePaid 쿠폰이 없는 거래소의 경우 구입하거나 판매한 practical한 액수가 실제 accounting 액수와 같음
                    accounting_moneyRoleSellTotal_IOExchangeTrade = practical_moneyRoleSellTotal_IOExchangeTrade
                    accounting_moneyRoleBuyTotal_IOExchangeTrade = practical_moneyRoleBuyTotal_IOExchangeTrade

                practical_profit = round(practical_moneyRoleSellTotal_IOExchangeTrade - practical_moneyRoleBuyTotal_IOExchangeTrade, 4)
                accounting_profit = round(accounting_moneyRoleSellTotal_IOExchangeTrade - accounting_moneyRoleBuyTotal_IOExchangeTrade, 4)
                print("practical_moneyRoleSellTotal_IOExchangeTrade", practical_moneyRoleSellTotal_IOExchangeTrade)
                print("practical_moneyRoleBuyTotal_IOExchangeTradec", practical_moneyRoleBuyTotal_IOExchangeTrade)
                print("accounting_moneyRoleSellTotal_IOExchangeTrade", accounting_moneyRoleSellTotal_IOExchangeTrade)
                print("accounting_moneyRoleBuyTotal_IOExchangeTrade", accounting_moneyRoleBuyTotal_IOExchangeTrade)

                print("practical_profit", practical_profit)
                print("accounting_profit", accounting_profit)

                #atraCycle에서의 earning Rate는 한Cycle내에서 한번에 움직인 대략적인 가치가 되므로
                #triangular에서는 sellMoneyTotal로 측정한다(final_atraCycle_initialValue 는 bitfinex처럼 fillOrKill로 주문해도 partial로
                #체결이 될수 있으므로 적합하지 않음)
                accounting_earningRate = round(float(accounting_profit) / float(accounting_moneyRoleBuyTotal_IOExchangeTrade) , 5)
                practical_earningRate = round(float(practical_profit) / float(accounting_moneyRoleBuyTotal_IOExchangeTrade), 5)
                print("accounting_earningRate_Rounded", accounting_earningRate)
                print("practical_earningRate_Rounded", practical_earningRate)
                #눈으로 보이는 수익이 아니라 실제 coupon 사용액수를 고려한 회계적(accounting) 수익을 actual_profit으로 넣는다
        self.atraCycle_info["atraCycle_finalized_info"]["actual_profit"] = accounting_profit
        self.atraCycle_info["atraCycle_finalized_info"]["actual_earningRate"] = accounting_earningRate

    def _calculateWillBeDepositedAmount(self, fromExchange, toExchange, coin_type, coin_amount):
        '''어떠한 의도한 가격을 받아서 atraCycle특성을 사용해 transfer 후 실제 받게 되는 가격을 산출한다'''
        fromExchange_feeInfo = self.pairingObj.feeInfo_atraPairing_specified[fromExchange]
        willBe_deposited_amount = 0
        # eExchange에서 outCoin의 증가분 을 io로 옮길때 eExchange의 trade 결과로 생긴것을 싹다 줘야 한다 eExchange는 코인의 증감이 없어야 한다
        if fromExchange == self.eExchange_name:
            #fromExchange가 eExchange_obj 이므로  증가된 outCoin 만큼만을 eExchange_obj 나의 계좌에서 없애는게 목적임
            #이경우 이 함수로 넘어온 coin_amount는 actual_outCoinAmount_afterEExchangeTrade_withFee 가 됨
            actual_outCoinAmount_afterEExchangeTrade_withFee = coin_amount

            #당연히 여기서 fee를 제외한 것이 ioMarket으로 옮겨짐(willBe_deposited_amount)
            willBe_deposited_amount = actual_outCoinAmount_afterEExchangeTrade_withFee - fromExchange_feeInfo["transferFee_coin"][coin_type]
        #ioExchange에서 inCoin의 증가분을 EExchange로 옮길때 eExchange의 trade에서 쓰인 inCoin양 만큼 옮겨야 한다
        elif fromExchange == self.ioExchange_name:
            #받는곳이 eExchange_obj 이기 때문에 거의 필요가 없을듯 하다
            pass

        return willBe_deposited_amount

    #TRIANGULAR에서만 사용가능하게 되어있음
    def _calculateInverseCoinAmount(self, exchange_name, coin_type, coin_amount_forInverseCalculating):
        '''EExchange에서의 inCoin 소모량을 다시 채워 넣을수 있는 ioExchange의 incoin orderAmount를 역계산한다'''
        #ioExchange_obj 거래 끝나고 보내서 eExchange에 도착하는 수량(순수하게 옮겨지는 양임)
        inCoin_transferAmount = coin_amount_forInverseCalculating + self.pairingObj.feeInfo_atraPairing_specified[exchange_name]["transferFee_coin"][coin_type]

        # 특정 exchange(ex.bithumb)의 경우 tradeFee_coupon 을제공해서 일정액의 쿠폰값을 미리 치루면 거래액이 특정액(4000만)이
        # 될때까지 거래수수료가 0이 된다.
        # 즉, 이러한 exchange가 포함된 Atra진행시 수수료가 0이 되어서 이익이 난 상황에서도 사실상 쿠폰값을 제한다면 이득이라고 볼 수 없
        # 을때가 존재 한다(물론 calculating CostAndprofit에서 이를 반영해놓고 통과한 것만 거래를 하긴한다 )
        # 수익률이 특정 % 이하라면 쿠폰이 적용되는 거래액을 사용하였으므로 사실상은 -인 경우가 존재
        # 따라서 이러한 exchange를 이용할 때에는 buyMoney(구매비용보다 더 많이계산) 또는 sellMoney(판매금액보다 덜계산)
        # 를 조정한 수치인 actual_buymoney_prePaidCoupon_notApplied 와 actual_sellMoney_prePaidCoupon_notApplied 를 만들어야함
        # 즉 주문에 쓰기위한 inCoin order Amount를  계산할때는 수수료가 0으로 계산을 하고 -> practical order측면
        # 나중에 회계적으로 실제 손해냐 이득이냐를 계산할때는 3만원/4000만원(쿠폰이 적용되는 거래한도) 까지 수수료가 0 0.025% 로 계산한다.

        #eExchange에서 실제로 사용된 inCoin_orderAmount를 채워넣을 양으로서 이렇게 계산 했으나 만약 쿠폰때문에
        #tradeFee가 명목상 0이라면 실제 채워넣을양보다 더 많은 inCoin을 구매하게 된다

        if self.pairingObj.feeInfo_atraPairing_specified[exchange_name]["prePaid_coupon"] == True:
            inCoin_orderAmount_forPractical = inCoin_transferAmount
            # prepay 쿠폰으로 tradefee가 0이지만 나중에 수익률 계산 할때는 tradeFee가 0이라고 생각하면 안됨
            inCoin_orderAmount_forAccount = inCoin_transferAmount / (1 - self.pairingObj.feeInfo_atraPairing_specified[exchange_name]["tradeFee_ratio"])
        else:
            inCoin_orderAmount_forAccount = inCoin_orderAmount_forPractical = \
                inCoin_transferAmount / (1 - self.pairingObj.feeInfo_atraPairing_specified[exchange_name]["tradeFee_ratio"])

        return inCoin_orderAmount_forPractical


    def buy(self,exchange_name,exchange_money_role , commodity_role_coin_type,
            price, buy_amount):

        #todo: api role을 좀더 세련되게 접근해서 들어갈 수 있게
        # client_api = SharedStorage.client_api_eachExchange[exchange_name]
        exchange_clientApi_type, client_api = AtraUTIL.detectProperApiClientType(exchange_name, "buy")
        print(exchange_name,exchange_money_role , commodity_role_coin_type,
            price, buy_amount)
        #atra Common symbol 과 exchange symbol이 다른경우 ex) bitfinex 의 DASH : DSH 관계 여기서 필요한건 exchange symbol임
        atraCommon_coin_symbol, exchange_coin_symbol = AtraUTIL.extractConvertibleSymbolInfo(exchange_name,commodity_role_coin_type)
        commodity_role_coin_type = exchange_coin_symbol
        ##------------------------------poloniex용 buy함수----------------------------##
        def buy_on_poloniex(client_api):
            poloniex_api = client_api
            currencyPair = exchange_money_role + "_" + commodity_role_coin_type
            rate = price
            amount = buy_amount
            buy_result = poloniex_api.api_query('buy', {"currencyPair": currencyPair, "rate": rate,
                                                            "amount": amount, "fillOrKill": 1})
            return buy_result
        ##------------------------------end----------------------------------------------------##

        ##------------------------------bithumb용 buy함수----------------------------##
        def buy_on_bithumb(client_api):
            bithumb_api = client_api
            rgParams = {
                "currency" : commodity_role_coin_type,
                #//->bithumb는 네자리밖에 order를  못함
                "units" : round(buy_amount,4)
            };
            buy_result = bithumb_api.xcoinApiCall("/info/market_buy", rgParams);
            return buy_result
        ##------------------------------buthumb buy end----------------------------------------------------##

        ##------------------------------bitfinex용 buy----------------------------##
        def buy_on_bitfinex(client_api):
            bitfinex_orderPrice = price
            bitfinex_api = client_api

            #xrp의 경우 이해할수 없지만 price를 조정하고  type을 이렇게 해야함 일단 되니까 나중에 원인  찾을것 유효숫자관련같음
            if commodity_role_coin_type == "XRP":
                type = "exchange market"
                bitfinex_orderPrice = 1
            else :
                type ="exchange fill-or-kill"
            rgParams = {  # 꼭 amount와 price는 str로
                "symbol": commodity_role_coin_type + exchange_money_role,
                "amount": str(buy_amount),
                "price": str(bitfinex_orderPrice),
                "side": "buy",  # trade_type임
                "type": type,
                "ocoorder": False
            };
            buy_result = bitfinex_api.api_query("order/new", rgParams);
            return buy_result
        ##------------------------------bitfinex용 buy end----------------------------------------------------##


        try:
            buy_result = locals()["buy_on_" + exchange_name](client_api)
            #network 에러는 여기서 잡아서 result 만든뒤 반환시킴
        except (urllib2.HTTPError, ValueError, socket.error, pycurl.error) as  err:
            logging_notify.warning("-----buy trade NETWORK_failed on :" + exchange_name + "--------\n" + str(err))
            buy_result = {"is_succeeded": False, "failed_category": "NETWORK_FAIL", "raw_result": str(err),
                          "raw_message": str(err), "retryable": True, "is_fatal_error": False}
            return buy_result
        except Exception as err:
            # 보지못한 에러는 후처리후 종료시켜야함
            logging_notify.warning("-----buy FATAL NETWORK_failed on :" + exchange_name + "--------\n" + str(err))
            buy_result = {"is_succeeded": False, "failed_category": "NETWORK_FAIL",
                              "raw_result": str(traceback.format_exc()), "raw_message": str(err), "retryable": False,
                              "is_fatal_error": True}
            return buy_result

        # pprint( exchange_name  + " buy func raw buy_result : ", buy_result)
        atraStyle_buy_result = AtraUTIL.makeAtraStyleTradeResult(exchange_name,buy_result, price , buy_amount, exchange_money_role, commodity_role_coin_type, "buy" )
        # logging_atraCycle.info(exchange_name  + "buy func atraStyle buy_result : \n" + (pformat(atraStyle_buy_result,indent=3)))
        return atraStyle_buy_result

    def sell(self, exchange_name, exchange_money_role , commodity_role_coin_type,
            price, sell_amount):
        print(exchange_name, exchange_money_role , commodity_role_coin_type,
            price, sell_amount)
        # client_api = SharedStorage.client_api_eachExchange[exchange_name]
        exchange_clientApi_type, client_api = AtraUTIL.detectProperApiClientType(exchange_name, "sell")
        sell_result = None

        # atra Common symbol 과 exchange symbol이 다른경우 ex) bitfinex 의 DASH : DSH 관계 여기서 필요한건 exchange symbol임
        atraCommon_coin_symbol, exchange_coin_symbol = AtraUTIL.extractConvertibleSymbolInfo(exchange_name,
                                                                                             commodity_role_coin_type)
        commodity_role_coin_type = exchange_coin_symbol
        ##------------------------------poloniex용 sell함수----------------------------##
        def sell_on_poloniex(client_api):
            poloniex_api = client_api
            currencyPair = exchange_money_role + "_" + commodity_role_coin_type
            rate = price
            amount = sell_amount

            sell_result = poloniex_api.api_query('sell',{"currencyPair":currencyPair,"rate":rate,"amount":amount,"fillOrKill" : 1 })
            return sell_result
        ##------------------------------poloniex sell end----------------------------------------------------##

        ##------------------------------bithumb용 sell----------------------------##
        def sell_on_bithumb(client_api):
            bithumb_api = client_api
            rgParams = {
                "currency": commodity_role_coin_type,
                # //->bithumb는 네자리밖에 order를  못함
                "units": round(sell_amount,4)
            };
            sell_result = bithumb_api.xcoinApiCall("/info/market_sell", rgParams);
            return sell_result
        ##------------------------------buthumb sell end----------------------------------------------------##

        ##------------------------------bitfinex용 sell----------------------------##
        def sell_on_bitfinex(client_api):
            bitfinex_orderPrice = price
            bitfinex_api = client_api

            # xrp의 경우 이해할수 없지만 price를 조정하고  type을 이렇게 해야함 일단 되니까 나중에 원인  찾을것 유효숫자관련같음
            if commodity_role_coin_type == "XRP":
                type = "exchange market"
                bitfinex_orderPrice =1
            else :
                type ="exchange fill-or-kill"
            print(type)
            rgParams = {  #꼭 amount와 price는 str로
            	"symbol" : commodity_role_coin_type+ exchange_money_role,
                "amount" :  str(sell_amount),
                "price" :  str(bitfinex_orderPrice),
                "side" : "sell",   #trade_type임
                "type": type,
                "ocoorder" : False
            };
            sell_result = bitfinex_api.api_query("order/new", rgParams);
            return sell_result
        ##------------------------------bitfinex용 sell end----------------------------------------------------##

        try:
            sell_result = locals()["sell_on_" + exchange_name](client_api)
            # network 에러는 여기서 잡아서 result 만든뒤 반환시킴
        except (urllib2.HTTPError, ValueError, socket.error, pycurl.error) as  err:
            logging_notify.warning("-----sell trade NETWORK_failed on :" + exchange_name + "--------\n" + str(err))
            sell_result = {"is_succeeded": False, "failed_category": "NETWORK_FAIL", "raw_result": str(err),
                           "raw_message": str(err), "retryable": True, "is_fatal_error": False}
            return sell_result
        except Exception as err:
            # 보지못한 에러는 후처리후 종료시켜야함
            logging_notify.warning("-----sell FATAL NETWORK_failed on :" + exchange_name + "--------\n" + str(err))
            sell_result = {"is_succeeded": False, "failed_category": "NETWORK_FAIL",
                              "raw_result": str(traceback.format_exc()), "raw_message": str(err), "retryable": True,
                              "is_fatal_error": True}
            return sell_result

        # print(exchange_name + "sell func raw sell_result : ", sell_result)
        atraStyle_sell_result = AtraUTIL.makeAtraStyleTradeResult(exchange_name, sell_result, price, sell_amount, exchange_money_role, commodity_role_coin_type, "sell")
        # logging_atraCycle.info(exchange_name + " sell func atraStyle sell_result : \n" + (pformat(atraStyle_sell_result, indent=3)))
        return atraStyle_sell_result


    def transfer(self, fromExchange, toExchange, coin_type, willBe_deposited_amountOn_TOEXCHANGE):
        '''willBe_deposited_amount(순수한 코인 이동양) 을 받아서 exchange마다 상이한 fee 고려사항을 체크한뒤
        실제 willBe_deposited_amount 를 움직이고 받기위한 orderAmount를 산출한뒤 각 api로 order 시킴'''
        toExchange_address = SharedStorage.coin_address_eachExchange[toExchange][coin_type]
        toExchange_tag = None

        if type(toExchange_address) == dict:
            # 리플의 경우 태그 변수에 집어넣고  address를 다시 dict 변수에 넣음
            toExchange_tag = toExchange_address["tag"]
            toExchange_address = toExchange_address["address"]

        fromExchange_feeInfo = self.pairingObj.feeInfo_atraPairing_specified[fromExchange]
        #보내주는 쪽(fromExchange client_api가 필요)
        # client_api = SharedStorage.client_api_eachExchange[fromExchange]
        exchange_clientApi_type, client_api = AtraUTIL.detectProperApiClientType(fromExchange, "transfer")

        #willBe_deposited_amount을 움직이기 위해서는  fee를 추가해서 주문을 해야하는지 여부를 consideringFee_when_transferOrder 를 확인해서
        #실제 움직이는 willBe_deposited_amount 에다 fee를 추가 시킨다 그게 아니라면 willBe_deposited_amount 그대로 주문을 넣으면 알아서 fee가 추가됨
        order_amount_for_clientApi = willBe_deposited_amountOn_TOEXCHANGE + fromExchange_feeInfo["transferFee_coin"][coin_type] \
            if fromExchange_feeInfo["consideringFee_when_transferOrder"] == True \
            else willBe_deposited_amountOn_TOEXCHANGE

        if fromExchange_feeInfo["consideringFee_when_transferOrder"] == True:
            order_amount_for_clientApi = willBe_deposited_amountOn_TOEXCHANGE + \
                                         fromExchange_feeInfo["transferFee_coin"][coin_type]
        else:
            order_amount_for_clientApi = willBe_deposited_amountOn_TOEXCHANGE
            # 개좆같은 빗썸은 BTC와 다른 코인의 consideringFee_when_transferOrder 적용이 다르다 씨발 따라서 BTC만 fee추가해서 order넣음
            #20171228부로 다른코인과 같은기준이 적용됨
            # if fromExchange == "bithumb" and coin_type == "BTC" :
            #     order_amount_for_clientApi = willBe_deposited_amountOn_TOEXCHANGE + \
            #                                  fromExchange_feeInfo["transferFee_coin"][coin_type]

        #움직이는 실제 coin amount + transfer fee
        willBe_decreased_amountOn_FROMEXCHANGE = willBe_deposited_amountOn_TOEXCHANGE + fromExchange_feeInfo["transferFee_coin"][coin_type]

        ##------------------------------bithumb용 transfer함수----------------------------##
        def transfer_from_bithumb(client_api):
            bithumb_api = client_api
            rgParams = {
                "currency" : coin_type,
                "units" :  order_amount_for_clientApi,
                "address" : toExchange_address,
            };
            if toExchange_tag != None:
                rgParams["destination"] = toExchange_tag
            tranfer_result = bithumb_api.xcoinApiCall("/trade/btc_withdrawal", rgParams);
            return tranfer_result

        ##------------------------------poloniex용 transfer함수----------------------------##
        def transfer_from_poloniex(client_api):
            poloniex_api = client_api
            params = {"currency": coin_type,
                      "amount": order_amount_for_clientApi,
                      "address": toExchange_address}
            if toExchange_tag != None:
                params["paymentId"] = toExchange_tag
            tranfer_result = poloniex_api.api_query("withdraw", params)
            return tranfer_result

        ##------------------------------bitfinex용 transfer함수----------------------------##
        def transfer_from_bitfinex(client_api):
            print(fromExchange, toExchange, coin_type, willBe_deposited_amountOn_TOEXCHANGE,order_amount_for_clientApi,toExchange_address)
            fullName_dict = {"BTC" : "bitcoin", "ETH" : "ethereum", "ETC" : "ethereumc", "LTC" :"litecoin", "DASH" : "dash", "XRP" :"ripple","QTUM" : "qtum"}
            bitfinex_api = client_api
            rgParams = {
                "withdraw_type": fullName_dict[coin_type],
                "walletselected": "exchange",  # 다은건 margin wallet에 옮겨논것임
                "amount": str(order_amount_for_clientApi),  #str로 안하면 400 에러뜸
                "address": toExchange_address,
            };
            if toExchange_tag != None:
                rgParams["payment_id"] = toExchange_tag
            tranfer_result = bitfinex_api.api_query("withdraw", rgParams)
            return tranfer_result

        try:
            tranfer_result = locals()["transfer_from_" + fromExchange](client_api)
        except (urllib2.HTTPError, ValueError, socket.error, pycurl.error) as  err:
            logging_notify.warning("-----transfer NETWORK_failed on :" + fromExchange + "--------\n" + str(err))
            tranfer_result = {"is_succeeded": False, "failed_category": "NETWORK_FAIL",
                              "raw_result": str(err), "raw_message": str(err), "retryable": True, "is_fatal_error" : False}
            return tranfer_result

        except Exception as err:
            # 보지못한 에러는 후처리후 종료시켜야함
            logging_notify.warning("-----transfer FATAL NETWORK_failed on :" + fromExchange + "--------\n" + str(err))
            tranfer_result = {"is_succeeded": False, "failed_category": "NETWORK_FAIL",
                              "raw_result": str(traceback.format_exc()), "raw_message": str(err), "retryable": True,
                              "is_fatal_error": True}
            return tranfer_result

        print(fromExchange, order_amount_for_clientApi, willBe_deposited_amountOn_TOEXCHANGE)
        atraStyle_transfer_result = AtraUTIL.makeAtraStyleTransferResult(tranfer_result, fromExchange, toExchange,
                                                                         coin_type, willBe_decreased_amountOn_FROMEXCHANGE, willBe_deposited_amountOn_TOEXCHANGE)
        #실패시에 DB데이터를 보고 manual로 사용자가 address 를 직접 확인 할 수 있도록 하기 위해
        atraStyle_transfer_result["toExchange_address"] = toExchange_address

        return atraStyle_transfer_result

    def _decideProcess(self, failed_result, old_decision_process_info):
        '''AtraStyle로 만들어낸 실패 처리 dict를 보고 재시도 할것인지, 재시도 안한다면 프로그램을 종료시킬것이지
         아닌지를 결정해서 return 시킨다'''
        atraCycle_state = self._checkAtraCycleState()

        #buy sell 등에서 받아온 atraStyle_fail dict
        # NETWORK 인지 EXCHANGE_FAIL인지, 같은 category가 3번 연속되면 retryable 이어도 retry시키지 않음
        failed_category =  failed_result["failed_category"]
        #message관련
        raw_result =  failed_result["raw_result"]
        raw_message = failed_result["raw_message"]
        #failed한  trading 이 retry  가능한 failed 인지 결정
        retryable = failed_result["retryable"]
        #retryable 하던 하지 않던(99% 하지 않을듯) 후처리(cycle rollBack) 한뒤 프로그램을 종료시킬지 결정
        is_fatal_error = failed_result["is_fatal_error"]

        #atraCycle 중 실패한 하나의 trading process 과정을 결정할 개별항목들임, 위에 변수들은 실패에서 결정한 일반적인 것들이고
        #아래 decision_info 들은 이러한것을 decideProcess 에서 조건에 따라 판단한 것들임 실제로 process를 진행시킬지 말지는
        #여기서 판단한다
        #failed_result retryable 에 의해서 결정되대 특정조건이 만족되면 retryble이 true 이어도 False 반환시킨다
        decision_info_retry = old_decision_process_info["retry"]
        #todo need_stop 이건 나중에 필요할지 어떨지 모르므로 일단 놔둠  is_fatal_error로 가능하나 혹시 모르니
        #atraStyle_failed dict는
        decision_info_need_stop = old_decision_process_info["need_stop"]
        decision_info_retry_count = old_decision_process_info["retry_count"]
        #일단 failed_category를 집어넣어 놓음
        decision_info_failed_log = old_decision_process_info["failed_log"]

        decision_info_failed_log.append(failed_category)

        # todo 일단 아래서 공통으로 적용하고   나중에 분기 할것
        if atraCycle_state == self.EEXCHANGE_TRADING:
            multiplying_val = 0.5
        elif atraCycle_state == self.IOEXCHANGE_OUT_TRADING or atraCycle_state == self.IOEXCHANGE_IN_TRADING:
            multiplying_val = 4.5
        elif atraCycle_state == self.FROM_EEXCHANGE_TRANSFERRING or atraCycle_state == self.FROM_IOEXCHANGE_TRANSFERRING:
            multiplying_val = 6
        else :
            #혹시 모르니 3으로 조정
            multiplying_val = 3

        #상황에 따라서 decision _info의 retry 를 조정함
        if retryable == False:
            decision_info_retry = False
        elif retryable ==True:
            decision_info_retry = True
            decision_info_retry_count += 1
            #retryable이 True라 할 지라도 다음과 같은 조건들이 충족되면 False로 바뀜
            #추후에 cycle 단계에 따라 5는 조정할것 즉 한cycle내의 처음 거래(ex EExchange)는 2,3번 실패하면 그냥 다 취소시켜도
            #cycle의 2번째 부터는 rollback을 해야 하므로 첫 거래 보다는 retry를 더 허용한다
            #우선 5회가 넘어가면 뭔가 이상이 있으므로 묻지도 따지지도 말고 retry False로 함
            if decision_info_retry_count > 4 * multiplying_val:
                decision_info_retry = False
            #5회가 아니더라도 같은 category로 3번 연속 failed가 나면 retry False 함
            elif len(decision_info_failed_log) > 3 * multiplying_val :
                last_three_failed_category = decision_info_failed_log[-3:]
                if all([x == last_three_failed_category[0] for x in last_three_failed_category]):
                    decision_info_retry = False

        old_decision_process_info["retry"] = decision_info_retry
        old_decision_process_info["retry_count"] = decision_info_retry_count

        new_decision_process_info = old_decision_process_info
        #_decideProcess 를 거친 decision_info를 반환
        return new_decision_process_info

    def _checkAtraCycleState(self):
        '''atraCycle 중에 현재 어느 상황에 있는지 확인해서 return 시킴'''
        eExchange_trade_process_info = self.atraCycle_info["trade_process_info"]["eExchange_trade"]
        #만약 ioExchange와 eExchange_obj 모두 coin-coin이 가능하다면  ioExchange도 processInfo가 하나 일것임
        ioExchange_inTrade_process_info = self.atraCycle_info["trade_process_info"]["ioExchange_inTrade"]
        ioExchange_outTrade_process_info = self.atraCycle_info["trade_process_info"]["ioExchange_outTrade"]

        from_eExchange_transfer_process_info = self.atraCycle_info["transfer_process_info"]["from_eExchange"]
        from_ioExchange_transfer_process_info = self.atraCycle_info["transfer_process_info"]["from_ioExchange"]


        state = self.ATRACYCLE_SUCCEEDED
        #atraCycle 과정 후반부 부터 해당 단계가 성공했는지 질의하여 어느과정중에  이곳으로  state를 check하러 왔는지 확인한다

        if from_ioExchange_transfer_process_info["is_succeeded"] == None:
            state = self.FROM_IOEXCHANGE_TRANSFERRING
            if from_eExchange_transfer_process_info["is_succeeded"] == None:
                state = self.FROM_EEXCHANGE_TRANSFERRING
                if ioExchange_inTrade_process_info["is_succeeded"] == None:
                    state = self.IOEXCHANGE_IN_TRADING
                    if ioExchange_outTrade_process_info["is_succeeded"] == None:
                        state = self.IOEXCHANGE_OUT_TRADING
                        if eExchange_trade_process_info["is_succeeded"] == None :
                            state = self.EEXCHANGE_TRADING


        logging_normal.info("cycle State : " + str(state))
        return state

    def _finalizeAtraCycle(self, is_fatal_error):
        '''atra Cycle이 정상 종료 했을경우 또는 decision_process에서 최종적으로 fatal error로 결정됐을경우
        모든 종료나 continue 상황모두 _finalize  호출 할 수 있도록 한다. 또한 모든 atraCycle 작업의 종료에서
         DB모듈을 체크하여 DB를 작동 시켜야 한다 atraCycle의 최초 작업(ex.eExchange_obj trading) 일경우 rollback  시킬 작업이 없'''
        atraCycleState = self._checkAtraCycleState()

        # atraCycle DB저장 모듈을 활성화 했고 초기화 테스트가 완료  됐을경우 module queue에 집어 넣음
        atra_module_interactor = AtraUTIL.putAtraModuleData("atraCycle_DB",self.atraCycle_info)
        logging_atraCycle.info(pformat(self.atraCycle_info, indent=3) + "\n")
        logging_atraCycle.info("atraCycle ended in " + str(atraCycleState) + " finalizing it now")
        #성공했으면 바로 return 시키고 Cycle 끝냄
        if atraCycleState == self.ATRACYCLE_SUCCEEDED:
            return

        logging_atraCycle.warning("and this failed is " + "(fatal_error : " + str(is_fatal_error))
        #eExchange_obj 에서 실패했다면  딱히 rollBack 할게 없으므로 그냥  return시킴
        if atraCycleState == self.EEXCHANGE_TRADING:
            if is_fatal_error == False:
                SharedStorage.pairingObj_connectedWith_atraCycle = None
                self.pairingObj.atraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
                # thread.exit()
                logging_atraCycle.info("atra continue")
                return
            else :
            #eExchange_obj 이외에서 실패하였다면 rollBack 을 시도해야하나 구현하지 않았으므로 아직까진 그냥 종료로(171130)
                logging_atraCycle.info("closing it now")
                os._exit(1)
        else:
            #현재 rollBack 시킬수 있는 수단이 존재하지 않으므로 일단 무조건 종료시킴
            #DB모듈이 작동되고 있을때는 queue 처리할게 있는지 질의한뒤에 종료 시킬것
            if atra_module_interactor != None and "atraCycle_DB" in [DB_client for DB_client in
                                                                     atra_module_interactor.atra_DB_client]:
                while not atra_module_interactor.shared_queue.empty():
                    print("atra module shared queue is not empty, so wait a moment")
                    time.sleep(1)
                    continue
            logging_atraCycle.info("closing it now")
            os._exit(1)


