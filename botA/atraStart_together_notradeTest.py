#-*- coding:utf-8 -*- 
import pandas
from atraOuter import *
from atraBot import *
import pandas as pd
import threading, argparse, configparser
from atraModule import atraInteractor

def parseAtraArg():
    argp = argparse.ArgumentParser(
        description='manage atra')
    argp.add_argument('--config-keys',
                      default="./setting/keys.ini",
                      help="Use different config //->keys setting file (default: %(default)s)")
    argp.add_argument('--config-sharedJson',
                      default="./setting/shared.json",
                      help="Use different config for shared json //->atra shared Json setting file (default: %(default)s)")
    argp.add_argument('--add-secret', action="store_true",
                      help="prompt for API secret for each Exchanges, encrypt them and then exit")
    argp.add_argument('--get-secret', action="store_true",
                      help="show specified exchange saved decrypted_secret_key using password")
    argp.add_argument('--password', action="store", default=None,
                      help="password for decryption of stored key. This is a dangerous option" +
                      "~/.bash_history 에 남을수 있으니 생각하고 사용할것 ")
    argp.add_argument('--module', action="append", default=None,
                      help="select atra Module")
    argp.add_argument('-f', action="store", default=None,
                      help="for jupyter , jupyter에서 사용하려면 꼭 필요함 ")


    args = argp.parse_args()
    config_keys_filename = args.config_keys
    config_sharedJson_filename = args.config_sharedJson
    atra_module_list = args.module
    print("atra_module_list : " + str(atra_module_list))
    print("config_keys_file : " + config_keys_filename)
    print("config_sharedJson_file : " + config_sharedJson_filename)

    #//->그냥 configparser를 상속받아서 만든 ini파일 관리 class임 이거 자체가 decrypting encrypting 하는 클래스는 아님
    config = AtraConfig(config_keys_filename)
    # config.read(config_keys_filename)
    #decrypting encrypting 하는 클래스
    k_manager = KeyManager(config)
    k_manager.password_from_commandline_option = args.password
    # --add-secret 모드 일때
    if args.add_secret:
        k_manager.prompt_exchanges()
        os._exit(1)
    elif args.get_secret:
        k_manager.prompt_decrypt(display = True)
        os._exit(1)
    else:
        atra_module_interactor = None
        if atra_module_list != None:
            with open(config_sharedJson_filename, 'r') as f:
                shared_config = json.load(f)
            atra_module_interactor = atraInteractor.Interactor(atra_module_list,shared_config["atra_module"])


        #비번을 제대로 치면 시작 할 수 있도록
        decrypted_result = k_manager.prompt_decrypt(display = False)
        print("decrypted_result", decrypted_result)
        if decrypted_result == k_manager.S_OK:
            return k_manager, config_sharedJson_filename, atra_module_interactor
        #todo 각 mode view 모드 virtual-trading 모드 cycle 모드 등등을 어떻게 할지 지금은 curses를 안쓰니 크게 고려하지 않아도 될수있을듯듯
        else:
            pass


def atraStart():
    #keys ini filename은  key_manager 객체 안에 있음
    key_manager, config_sharedJson_filename, atra_module_list = parseAtraArg()
    SharedStorage.__init__(key_manager, config_sharedJson_filename, atra_module_list)

    # bithumb - poloniex
    pairing_exchangeName_dict = {"IOExchange": "bithumb", "EExchange": "poloniex"}
    money_role_eachExchange = {"IOExchange": "KRW", "EExchange": "BTC"}
    b = ExchangePairing("BTC", pairing_exchangeName_dict, money_role_eachExchange)
    b.atraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = False
    b.atraCycle_setting_val["THREASHOLD_RATE"] = 0.005
    b.atraCycle_setting_val["THREASHOLD_PROFIT"] = 5000
    b.atraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = 3000000
    b.atraCycle_setting_val["PERMITTED_COIN"] = ["ETC", "DASH", "LTC","XRP"]

    # bithumb - bitfinex
    pairing_exchangeName_dict = {"IOExchange": "bithumb", "EExchange": "bitfinex"}
    money_role_eachExchange = {"IOExchange": "KRW", "EExchange": "BTC"}
    a = ExchangePairing("BTC", pairing_exchangeName_dict, money_role_eachExchange)
    # io가 어디고 EExchange이 어디고는 생성자 생성할때만 딱 넣고 나머지는 다 exchange Type하나로만 구분하여 호출
    a.atraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = False
    a.atraCycle_setting_val["THREASHOLD_RATE"] = 0.0025
    a.atraCycle_setting_val["THREASHOLD_PROFIT"] = 4000
    a.atraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = 2500000
    a.atraCycle_setting_val["PERMITTED_COIN"] = ["ETC", "DASH","LTC","XRP"]


    #각 exchange의 balance를 주기적으로 받아오는 쓰레드 시작
    balance_autoBot_poloniex = threading.Thread(name="poloniex_balance",
                                                target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot,
                                                args=("poloniex", True), kwargs={"delay": 1})
    balance_autoBot_poloniex.daemon = True
    balance_autoBot_poloniex.start()

    balance_autoBot_bithumb = threading.Thread(name="bithumb_balance",
                                               target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot,
                                               args=("bithumb", True), kwargs={"delay": 2})
    balance_autoBot_bithumb.daemon = True
    balance_autoBot_bithumb.start()

    balance_autoBot_bitfinex = threading.Thread(name="bitfinex_balance",
                                                target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot,
                                                args=("bitfinex", True), kwargs={"delay": 3})
    balance_autoBot_bitfinex.daemon = True
    balance_autoBot_bitfinex.start()


    # 각 마켓의 orderbook을 주기적으로 받아 오는  쓰레드 시작
    orderBook_autoBot_poloniex = threading.Thread(name="poloniex_orderBook",
                                                  target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot,
                                                  args=("poloniex", True), kwargs={"delay": 2})
    orderBook_autoBot_poloniex.daemon = True
    orderBook_autoBot_poloniex.start()

    orderBook_autoBot_bithumb = threading.Thread(name="bithumb_orderBook",
                                                 target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot,
                                                 args=("bithumb", True))
    orderBook_autoBot_bithumb.daemon = True
    orderBook_autoBot_bithumb.start()

    orderBook_autoBot_bitfinex = threading.Thread(name="bitfinex_orderBook",
                                                  target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot,
                                                  args=("bitfinex", True), kwargs={"delay": 4.5})
    orderBook_autoBot_bitfinex.daemon = True
    orderBook_autoBot_bitfinex.start()
    while True:
    #     print("is poloniex_orderBook_Exist" + str(AtraUTIL.threadController('find','poloniex_orderBook')))
    #     print("is bithumb_orderBook_Exist" + str(AtraUTIL.threadController('find','bithumb_orderBook')))
    #     clear_output()
        #pairingObj 객체의 costAndProfit_basedOn_inOutCoin_latest 에 값이 들어가 있다는것은 시작 가능하다는 의미 이므로
        if  a.costAndProfit_basedOn_inOutCoin_latest["whenInCoin"] :
            # whenInCoin whenOutCoin 둘 모두 동일한 timestamp 가 들어있음
            timestamp = a.costAndProfit_basedOn_inOutCoin_latest["whenOutCoin"]["timestamp"]
            earning_rate = a.costAndProfit_basedOn_inOutCoin_latest["max_earningRate_info"]["earning_rate"]
    #         if earning_rate >=a.atraCycle_setting_val["THREASHOLD_RATE"]:
    #             time.sleep(100)
    #             break
    #         break
        time.sleep(3)

if __name__ == "__main__":
    try:
        atraStart()
    except KeyboardInterrupt as e:
        print(e)
        exit()    