#-*- coding:utf-8 -*-
import threading
import time

working = None

def work():
    i = 0
    while i < 10:
        print ('I am working..')
        time.sleep(0.5)
        i += 1


def sleep():
    working.join()
    i = 0
    while i < 10:
        print ('I am sleeping..')
        time.sleep(0.3)
        i += 1

working= threading.Thread(target=work)
working.setDaemon(True)
working.start()

sleeping = threading.Thread(target=sleep)
sleeping.setDaemon(True)
sleeping.start()

# blocking됨
# working.join()
# sleeping.join()

print ('main thread finished')
# daemon으로 동작하게 했을 때와 그렇지 않을 때의 비교!