import threading

# class MyThread(threading.Thread):
#
#     def run(self):
#         print("this is a thread")
#         return
#
# for i in range(6):
#     t = MyThread()
#     t.start()
#

import threading
import time


def work(e):
    print("wait for the event")
    event_set = e.wait()
    print("event set : {}".format(event_set))


e = threading.Event()
t1 = threading.Thread(name="", target=work, args=(e,))

print('main thread before calling Event.set()')
time.sleep(5)

e.set()

print('event is just set')