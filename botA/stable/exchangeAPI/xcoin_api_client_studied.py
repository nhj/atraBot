#-*- coding:utf-8 -*-
#
# @brief XCoin API-call related functions (for Python 2.x, 3.x)
#
# @author btckorea
# @date 2017-04-14
#

import sys
import time
import math
import base64
import hmac, hashlib
from StringIO import StringIO

PY3 = sys.version_info[0] > 2;
if PY3:
	import urllib.parse
else:
	import urllib

import pycurl
import json

#//-> api connect key  KeyManager Key 는 특정 api 활성화 할때마다 지속적으로 바뀜
class XCoinAPI:
    api_url = "https://api.bithumb.com";
    api_key = "";
    api_secret = "";

    def __init__(self, api_key, api_secret):
        self.api_key = api_key;
        self.api_secret = api_secret;

    def http_body_callback(self, buf):
        print("buf raw")
        print(buf)
        self.contents = buf;
        print("self.contents")
        print(self.contents)

    def microtime(self, get_as_float = False):
        if get_as_float:
            return time.time();
        else:
            return '%f %d' % math.modf(time.time());

    def microsectime(self) :
        mt = self.microtime(False);
        mt_array = mt.split(" ")[:2];
        return mt_array[1] + mt_array[0][2:5];

    #//->endpoint -> restful 경로
    #//->rgParams는 "order_currency" : "BTC", "payment_currency" : "KRW"
    def xcoinApiCall(self, endpoint, rgParams):
        # 1. Api-Sign and Api-Nonce information generation.
        # 2. Request related information from the Bithumb API server.
        #
        # - nonce: it is an arbitrary number that may only be used once. (Microseconds)
        # - api_sign: API signature information created in various combinations values.

        endpoint_item_array = {
            "endpoint" : endpoint
        };
        #--> 하나의 dictionary로 합침
        uri_array = dict(endpoint_item_array, **rgParams); # Concatenate the two arrays.
        if PY3:
            e_uri_data = urllib.parse.urlencode(uri_array);
        else:
            #//-> restful 주소로 쓸 수 있게 직렬화
            e_uri_data = urllib.urlencode(uri_array);

        # Api-Nonce information generation.
        nonce = self.microsectime();

        # Api-Sign information generation.
        #//-> bithumb api object  생성할때 생성자에 넣은 개인키
        hmac_key = self.api_secret;
        utf8_hmac_key = hmac_key.encode('utf-8');
        
        #//-> 무결성 인증을 위해  uri endpoint 와 직렬화한 e_uridata를 합침
        #//-> e_uri_data 안에는 중간에 또 endpoint가 들어있음
        hmac_data = endpoint + chr(0) + e_uri_data + chr(0) + nonce;
        utf8_hmac_data = hmac_data.encode('utf-8');
        
        #//-> utf-8 엔코딩한 hmac_data를 sha512 해쉬한뒤 이를 개인키로 암호화
        #//-> 그후 기타 엔코딩을 거쳐서 전자서명완성 
        hmh = hmac.new(bytes(utf8_hmac_key), utf8_hmac_data, hashlib.sha512);
        hmac_hash_hex_output = hmh.hexdigest();
        utf8_hmac_hash_hex_output = hmac_hash_hex_output.encode('utf-8');
        utf8_hmac_hash = base64.b64encode(utf8_hmac_hash_hex_output);
        
        api_sign = utf8_hmac_hash;
        #전자서명을 utf-8로 디코드
        utf8_api_sign = api_sign.decode('utf-8');

        # Connects to Bithumb API server and returns JSON result value.
        curl_handle = pycurl.Curl();
        curl_handle.setopt(pycurl.POST, 1);
        # curl_handle.setopt(pycurl.VERBOSE, 1); # vervose mode :: 1 => True, 0 => False
        curl_handle.setopt(pycurl.POSTFIELDS, e_uri_data);

        #//-> 10초 이상되면 무조건 exception  날릴수 있돌록 함  pycurl.error 에서 잡힘
        curl_handle.setopt(pycurl.TIMEOUT, 20);
        curl_handle.setopt(pycurl.CONNECTTIMEOUT, 20);
        #//-> 이거 설정 안하면 여러 thread가 pycurl을 사용중인 상황에서 정상 thread에게 까지 timeout을 날려버림  그러면 segmentatioin fault 로 전부 멈춰버림
        curl_handle.setopt(pycurl.NOSIGNAL,1);

        url = self.api_url + endpoint;
        curl_handle.setopt(curl_handle.URL, url);
        curl_handle.setopt(curl_handle.HTTPHEADER, ['Api-Key: ' + str(self.api_key), 'Api-Sign: ' + str(utf8_api_sign), 'Api-Nonce: ' + str(nonce)]);

        #지정된 콜백으로 받음 콜백함수의 첫번째(self말고) 인자가 respone 값임
        #//->원래 이렇게 했는데 response를 분할해서 받으면 데이터가 한번에 안오고 분할되서 옮 때문에 따로 합쳐지는 작업을 해줘야되버림 따라서 아래의 StringIO로 바꿨음
        # curl_handle.setopt(curl_handle.WRITEFUNCTION, self.http_body_callback);
        body = StringIO()
        curl_handle.setopt(curl_handle.WRITEFUNCTION, body.write);
        #//-> header관련 두줄은 그냥 내가 추가 했음 필요없음 지울것
        # headers = StringIO()
        # curl_handle.setopt(curl_handle.HEADERFUNCTION, headers.write)

        # start = time.time()
        # print("start" + str(start))
        #perform을 수행하고 server에서 값을 받아온뒤 callback이 호출 될 때까지 blocking이 있음 평규 0.2초~0.3초 정도임 따라서 그냥
        #자동으로 delay가 있기 때문에 이 윗단에선 sleep없이 while문 돌려도 될듯함
        curl_handle.perform();

        # end = time.time()
        # print("end" + str(end) ,  (end - start))
        #pycurl object가 perform을 수행하면  pycurl.RESPONSE_CODE static 영역에 code가 써지는 것으로 추정
        #response_code = curl_handle.getinfo(pycurl.RESPONSE_CODE); # Get http response status code.
        curl_handle.close();

        self.contents = body.getvalue()
        # print(self.contents)
        #-> json()
        #json파싱해서 return 시킴
        return (json.loads(self.contents));