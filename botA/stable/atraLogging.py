#-*- coding:utf-8 -*-
import logging
import logging.config

# logging.basicConfig(level=logging.DEBUG, format='(%(threadName)-9s) %(message)s',)
def configureAtraLogger(name):
    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'default': {'format':'\n%(threadName)s)%(asctime)s - %(levelname)s - \n%(message)s\n\n', 'datefmt': '%y%m%d %H:%M:%S'},
            'atraCycle': {'format': '%(asctime)s - %(levelname)s - \n%(message)s ', 'datefmt': '%y%m%d %H:%M:%S'}
        },
        'handlers': {
            'consoleNormal': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'default',
                # 'stream': 'ext://sys.stdout'
            },
            'console_atraCycle': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'atraCycle',
                # 'stream': 'ext://sys.stdout'
            },
            'file_commonError': {   #//-> file 안 할꺼면 이거 없어야함 즉 정의해 놓으면 안됨
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'default',
                'filename': 'error_log.log',
                'maxBytes': 1024 * 1024 * 10,    #10MB
                'backupCount': 3  #3개까지
            },
            'file_atraCycle': {  # //-> file 안 할꺼면 이거 없어야함 즉 정의해 놓으면 안됨
                'level': 'INFO',
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'atraCycle',
                'filename': 'atraCycle.log',
                'maxBytes': 1024 * 1024 * 10,  # 10MB
                'backupCount': 3  # 3개까지
            }
        },
        'loggers': {
            'normal': {
                'level': 'DEBUG',
                'handlers': ['consoleNormal']
            },
            #pycurl urllib2  등등에서 나오는 network 관련 error
            'notify_error': {
                'level': 'DEBUG',
                'handlers': ['consoleNormal','file_commonError']
            },
            'atraCycle': {
                'level': 'DEBUG',
                'handlers': ['console_atraCycle', 'file_atraCycle']
            }

        },
        'disable_existing_loggers': False
    })
    return logging.getLogger(name)

# logging_normal = configureAtraLogger('normal')
# logging_normal.debug('debug message!')
# logging_normal.info('info message!')
# logging_normal.error('error message')
# logging_normal.critical('critical message')
# logging_normal.warning('warning message')
