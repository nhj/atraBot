#-*- coding:utf-8 -*-
# Import Built-Ins
import datetime
import hashlib
import hmac
import json
import logging
import os, sys
import shutil
import threading
import time
import pprint
from collections import defaultdict
from itertools import islice
from threading import Thread
#//->python 3.4기반 api 이므로 원래 queue였는데 Queue로 교체
if sys.version_info < (3, 5, 0):
    import Queue
else:
    import queue as Queue


from btfxExceptions import FaultyPayloadError

# Import Homebrew
# import Server-side Exceptions
from btfxExceptions import InvalidBookLengthError, GenericSubscriptionError
from btfxExceptions import InvalidEventError, InvalidBookPrecisionError
from btfxExceptions import InvalidPairError, InvalidChannelError
from btfxExceptions import NotRegisteredError, UnknownChannelError
from btfxExceptions import NotSubscribedError, AlreadySubscribedError
# import Client-side Exceptions
from btfxExceptions import UnknownEventError, UnknownWSSError
from btfxExceptions import UnknownWSSInfo, AlreadyRegisteredError
from websocket import WebSocketConnectionClosedException
# Import Third-Party
from websocket import create_connection, WebSocketTimeoutException,WebSocket

# Init Logging Facilities

#//->
# log = logging.getLogger(__name__)
# fh = logging.FileHandler('test.log')
# sh = logging.StreamHandler(sys.stdout)

# log.setLevel(logging.DEBUG)
# log.addHandler(sh)
# log.addHandler(fh)


#//->위의 logger는 git page 공식 예시고 일단 error메세지와 exception 정보만 사후적으로 취할 것 이기 때문에 filehandler만 만듬
btfx_log = logging.getLogger("btfx_log")

#//->python 3.6 기준이기 때문에 임시로 builtin Error 만듬
if sys.version_info < (3, 5, 0):
    class TimeoutError(OSError):
        """ Timeout expired. """
        def __init__(self, *args, **kwargs):  # real signature unknown
            pass


    class ConnectionError(OSError):
        """ Connection error. """

        def __init__(self, *args, **kwargs):  # real signature unknown
            pass

    class ConnectionResetError(ConnectionError):
        """ Connection reset. """

        def __init__(self, *args, **kwargs):  # real signature unknown
            pass


class Orders:
    """self.books["symbol"].asks  또는 self.book["symbol"].bids  안에 들어갈 내용들임"""
    def __init__(self, reverse=False):
        self._orders = {}
        self._reverse = reverse

    def __call__(self):
        """instance를  call 할수 있는 magic method이며 현재시점에서 해당 symbol orderBook 전체를 호출"""
        return [self._orders[i] for i in sorted(self._orders.keys(),
                                                  reverse=self._reverse)]

    def __repr__(self):
        return str(self.__call__())

    def __setitem__(self, key, value):
        """obj["key"]=something 식으로 obj를 dict화 할수 있는 magic method
        btfxwssObj._handle_book 에서 삽입되며
        key = str(price), value = (price, amount, count, ts)  임
        """
        self._orders[key] = value

    def __getitem__(self, key):
        """obj["key"] 식으로 호출될때 사용하는 magic method"""
        keys = sorted(self._orders.keys(), reverse=self._reverse)

        if isinstance(key, int):
            # an index was passed
            # str(price) 로 key가 저장되어 있는데 index로도 뽑아낼 수 있도록 설계됨
            key, = islice(keys, key, key + 1)
            return self._orders[key]
        elif isinstance(key, str) or isinstance(key, float):
            return self._orders[key]
        elif not isinstance(key, slice):
            raise TypeError()

        #slice 형식 [::4] [1:-1:2] 등등이 들어왔을때 라고 봄
        return [self._orders[key] for key in
                islice(keys, key.start, key.stop,
                       key.step)]

    def pop(self, key):
        return self._orders.pop(key)


class Orderbook:
    """ self.books["symbol"] 안에 들어가는 내용들임 defaultdict 이기 때문에 어떤 symbol key로 dict요소를 만들면
     이 객체로 초기화 됨"""
    # subscribed 되어 최초 모양이 갖추어진 snapshot 이후에 지속적인 update가 오는데 이를 정리해서 모양을갖춤
    def __init__(self):
        self.bids = Orders(reverse=True)
        self.asks = Orders()
        #//-> atraBot에서 쓰기위해 update 될때마다 latest_ts를 갱신한다
        self.latest_ts = 0


class BtfxWss:
    """
    Client Class to connect to Bitfinex Websocket API. Data is stored in attributes.
    Features error handling and logging, as well as reconnection automation if
    the Server issues a connection reset.
    """

    def __init__(self, key=None, secret=None, addr=None):
        """
        Initializes BtfxWss Instance.
        :param key: Api Key as string
        :param secret: Api secret as string
        :param addr: Websocket API Address
        """
        btfx_log.error("btfxWss started")
        self.key = key if key else ''
        self.secret = secret if secret else ''
        self.conn = None
        self.addr = addr if addr else 'wss://api.bitfinex.com/ws/2'

        # Set up variables for receiver and main loop threads
        self.running = False
        self._receiver_lock = threading.Lock()
        self._processor_lock = threading.Lock()
        self.q = Queue.Queue()
        self.receiver_thread = None
        self.processing_thread = None
        self.controller_thread = None
        self.cmd_q = Queue.Queue()

        #def ping에서 ping을 server로 날린시간을 기록해둠
        self.ping_timer = None
        self.timeout = 5
        self._heartbeats = {}
        self._late_heartbeats = {}

        # Set up book-keeping variables & configurations
        self.api_version = None
        #134: <bound method BtfxWss._handle_book of <exchangeAPI.btfxwss.classes_1.BtfxWss instance at 0x3cb4d40>> 식의 함수 포인터값 저장
        #self.channels[chanId] = self._data_handlers[channel_key] chanId로 함수를 바로 호출할 수 있음
        self.channels = {}  # Dict for matching channel ids with handlers
        # {134: (u'book', {u'channel': u'book', u'freq': u'F0', u'pair': u'BTCUSD', u'prec': u'P0', u'symbol': u'tBTCUSD'})같은 데이터가 저장됨
        self.channel_labels = {}  # Dict for matching channel ids with names
        self.channel_states = {}  # Dict for matching channel ids with status of each channel (alive/dead)
        self.channel_configs = {}  # Variables, as set by subscribe command
        self.wss_config = {}  # Config as passed by 'config' command

        #//-> defaultdict를 넣었으므로 임의의 symbol 명을 가진 key의 default 내부값에 바로바로 접근
        self.tickers = defaultdict(list)
        self.books = defaultdict(Orderbook)
        self.raw_books = defaultdict(Orderbook)
        self._trades = defaultdict(list)
        self.candles = defaultdict(list)
        self.account = defaultdict(list)

        #//->예외발생시 원인 분석하기위해 임의로 내가 만든것


        #channel subscribed 성립이나 해제  에러 등등의 data message (snapshot, update) 를 제외한 response message가 왔을때 처리하는 핸들러
        self._event_handlers = {'error': self._raise_error,
                                'unsubscribed': self._handle_unsubscribed,
                                'subscribed': self._handle_subscribed,
                                'auth': self._handle_subscribed,
                                'unauth': self._handle_unsubscribed,
                                'info': self._handle_info,
                                'pong': self._handle_pong,
                                'conf': self._handle_conf}
        #data message (snapshot, update)가 올때(ex. orderBook 일때 [chan_id, [price, amount]]) 각 data message 를 처리하는 핸들러
        self._data_handlers = {'ticker': self._handle_ticker,
                               'book': self._handle_book,
                               'raw_book': self._handle_raw_book,
                               'candles': self._handle_candles,
                               'trades': self._handle_trades,
                               'auth': self._handle_auth}

        # 1XXXX == Error Code -> raise, 2XXXX == Info Code -> call
        def restart_client():
            self.cmd_q.put('restart')

        self._code_handlers = {'20051': restart_client,
                               '20060': self.pause,
                               '20061': self.unpause,
                               '10000': InvalidEventError,
                               '10001': InvalidPairError,
                               '10300': GenericSubscriptionError,
                               '10301': AlreadySubscribedError,
                               '10302': InvalidChannelError,
                               '10400': GenericSubscriptionError,
                               '10401': NotSubscribedError,
                               '10011': InvalidBookPrecisionError,
                               '10012': InvalidBookLengthError}

    def _get_current_channelInfo(self):
        """//-> 중간에 계속 register 안된게 뜨므로 그것을 확인하기 위해 내가 만들었음"""
        pass
        # current_channelInfo = pprint.pformat(
        #     {"channel": self.channels, "chan_lables": self.channel_labels, "chan_states": self.channel_states, "heartbeats" : self._heartbeats, "late_heartbeats" : self._late_heartbeats})
        # return current_channelInfo

    def _controller(self):
        """
        Thread func to allow restarting / stopping of threads, for example
        when receiving a connection reset info message from the wss server.
        :return:
        """
        while self.running:
            try:
                cmd = self.cmd_q.get(timeout=1)
            except TimeoutError:
                continue
            except Queue.Empty:
                continue
            if cmd == 'restart':
                self.restart(soft=True)
            elif cmd == 'stop':
                self.stop()

    def _check_heartbeats(self, ts, *args, **kwargs):
        """
        'hb' response를 체크하는 로직이 아님!
        각 chan_id 별로 recv가 올때마다 self._heartbeats[chan_id] 를 갱신 시킨다 왜냐하면 얼마만에 갱신이 되는지 체크하기위해
        만약 해당 channel에서 아무런 변동사항이 없어서 update할 내용이 없다면   5초에 한번 'hb' 응답을 준다. 즉 update내용이 오건 hb recv가 오던가 한다
        그럼에도 불구하고 오랫동안(여기선 10초기준) self._heartbeats[chan_id] 에 갱신이 안된다는 소리는 뭔가 문제가 생긴것이라고 판단하며
        이 로직은 이를 체크하는 로직임
        Checks if the heartbeats are on-time. If not, the channel id is escalated
        to self._late_heartbeats and a warning is issued; once a hb is received
        again from this channel, it'll be removed from this dict, and an Info
        message logged.
        :param ts: timestamp, declares when data was received by the client
        :return:
        """
        #모든 열려있는 channel을 검사(열려있는 channel은 모두 _heartbeats에 chan_id 별로 마지막 갱신값을 넣어두었음)
        for chan_id in self._heartbeats:
            #마지막 heartbeats 갱신값이 현재 check 할때 ts 보다 10초이상 차이난다면 _late_heartbeats에 넣음
            if ts - self._heartbeats[chan_id] >= 10:
                if chan_id not in self._late_heartbeats:
                    try:
                        # This is newly late; escalate
                        btfx_log.warning("BtfxWss.heartbeats: Channel %s hasn't sent a "
                                         "heartbeat in %s seconds!",
                                         self.channel_labels[chan_id],
                                         ts - self._heartbeats[chan_id])
                        self._late_heartbeats[chan_id] = ts
                        # btfx_log.error(self._get_current_channelInfo())
                    except KeyError:
                        # This channel ID Is not known to us - log and raise
                        btfx_log.error("BtfxWss.heartbeats: Channel %s is not "
                                       "registered in the client's registry! "
                                       "Restarting client to avoid errors..", chan_id)
                        btfx_log.exception(self._get_current_channelInfo())
                        raise UnknownChannelError
                else:
                    # We know of this already
                    # self._late_heartbeats 관찰대상에 들어가 있는 channel 이라면 서버를 체크해서 살았는지 확인
                    self.ping()
                    continue
            else:
                # its not late
                try:
                    self._late_heartbeats.pop(chan_id)
                except KeyError:
                    # wasn't late before, check next channel
                    continue
                btfx_log.info("BtfxWss.heartbeats: Channel %s has sent a "
                              "heartbeat again!", self.channel_labels[chan_id])

    def _check_ping(self):
        """
        Checks if the ping command timed out and raises TimeoutError if so.
        :return:
        """
        if time.time() - self.ping_timer > self.timeout:   #//-> timeout은 5초로 설정
            btfx_log.error("ping Command timed out!\n" + self._get_current_channelInfo())
            raise TimeoutError("Ping Command timed out!")


    def pause(self):
        """
        Pauses the client
        :return:
        """
        self._receiver_lock.acquire()
        btfx_log.info("BtfxWss.pause(): Pausing client..")

    def unpause(self):
        """
        Unpauses the client
        :return:
        """
        self._receiver_lock.release()
        btfx_log.info("BtfxWss.pause(): Unpausing client..")

    def start(self):
        """
        Start the websocket client threads
        :return:
        """
        self.running = True

        # Start controller thread //-> start, restart만
        self.controller_thread = Thread(target=self._controller, name='Controller Thread')
        self.controller_thread.daemon = True #//-> atra 때문에 추가했음
        self.controller_thread.start()

        btfx_log.info("BtfxWss.start(): Initializing Websocket connection..")
        while self.conn is None:
            try:
                import ssl
                # ws = WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
                # self.conn = ws.connect(self.addr)
                self.conn = create_connection(self.addr, timeout=3,sslopt={"cert_reqs": ssl.CERT_NONE})
            except WebSocketTimeoutException:
                self.conn = None
                print("Couldn't create websocket connection - retrying!")

        btfx_log.info("BtfxWss.start(): Initializing receiver thread..")
        if not self.receiver_thread:
            self.receiver_thread = Thread(target=self.receive, name='Receiver Thread')
            self.receiver_thread.daemon = True #//-> atra 때문에 추가했음
            self.receiver_thread.start()
        else:
            btfx_log.info("BtfxWss.start(): Thread not started! "
                          "self.receiver_thread is populated!")

        btfx_log.info("BtfxWss.start(): Initializing processing thread..")
        if not self.processing_thread:
            self.processing_thread = Thread(target=self.process, name='Processing Thread')
            self.processing_thread.daemon = True #//-> atra 때문에 추가했음
            self.processing_thread.start()
        else:
            btfx_log.info("BtfxWss.start(): Thread not started! "
                          "self.processing_thread is populated!")

    def stop(self):
        """
        Stop all threads and modules of the client.
        //-> self.running =False 를 해 놓으면  receiver_thread, processing_thread 가 모두 정지됨
        :return:
        """

        btfx_log.info("BtfxWss.stop(): Stopping client..")
        self.running = False

        btfx_log.info("BtfxWss.stop(): Joining receiver thread..")
        try:
            self.receiver_thread.join()
            if self.receiver_thread.is_alive():
                time.time(1)
        except AttributeError:
            # btfx_log.debug("BtfxWss.stop(): Receiver thread was not running!")
            btfx_log.info("BtfxWss.stop(): Receiver thread was not running!") #내가바꿈

        btfx_log.info("BtfxWss.stop(): Joining processing thread..")
        try:
            self.processing_thread.join()
            if self.processing_thread.is_alive():
                time.time(1)
        except AttributeError:
            # btfx_log.debug("BtfxWss.stop(): Processing thread was not running!")
            btfx_log.info("BtfxWss.stop(): Processing thread was not running!") #내가 바꾸

        btfx_log.info("BtfxWss.stop(): Closing websocket conection..")
        try:
            self.conn.close()
        except WebSocketConnectionClosedException:
            pass
        except AttributeError:
            # Connection is None
            pass

        self.conn = None
        self.processing_thread = None
        self.receiver_thread = None

        btfx_log.info("BtfxWss.stop(): Done!")


    def restart(self, soft=False):
        """
        Restarts client. If soft is True, the client attempts to re-subscribe
        to all channels which it was previously subscribed to.
        :return:
        """
        # //->내가 추가 모든 orderBook unsubscribe시킴
        if soft == False:
            all_chan_unsubscribed = False
            while  len([chan_id for chan_id, chan_info in self.channel_labels.iteritems()]) != 0:
                for chan_id, chan_info in self.channel_labels.iteritems():
                    btfx_log.info(chan_id + "founded unsubscribe now")
                    self._unsubscribe(chan_id)
                    time.time(1)
                    btfx_log.info(pprint.pformat(self.channel_labels))

        btfx_log.error("BtfxWss.restart(): Restarting client..")  #//->원래 info 였는데 error로 바꿈
        self.stop()
        self.start()
        # cache channel labels temporarily
        channel_labels = [self.channel_labels[k] for k in self.channel_labels] if soft else None

        # clear previous channel caches
        self.channels = {}
        self.channel_labels = {}
        self.channel_states = {}

        # //-> 이둘 내가 추가함
        # 타이밍이 안 맞아져서 self.check_heartbeat 를 해버릴때 오류가 날 수 있으니 얘네 둘도 초기화 해줌
        self._heartbeats = {}
        self._late_heartbeats = {}

        if channel_labels:
            # re-subscribe to channels
            for channel_name, kwargs in channel_labels:
                self._subscribe(channel_name, **kwargs)
        # //-> 내가 추가  books 도 초기화 시킴
        else :
            self.books = defaultdict(Orderbook)
    def receive(self):
        """
        Receives incoming websocket messages, and puts them on the Client Queue
        for processing.
        :return:
        """
        #//->pause 상태일때 이 쓰레드 작동을 못하게 하려고 lock 을 만들었음
        while self.running:
            if self._receiver_lock.acquire():
                try:
                    raw = self.conn.recv()
                except WebSocketTimeoutException:
                    continue
                except WebSocketConnectionClosedException:
                    # this needs to restart the client, while keeping track
                    # of the currently subscribed channels!
                    self.conn = None
                    self.cmd_q.put('restart')
                except AttributeError:
                    # self.conn is None, idle loop until shutdown of thread
                    continue
                msg = time.time(), json.loads(raw)
                #//-> (ts,recvData)
                # data Recv 만 debug 레벨로 두고 나머지는  모두  info  이상으로
                btfx_log.debug("receiver Thread: Data Received: %s", msg)
                self.q.put(msg)
                self._receiver_lock.release()
            else:
                # The receiver_lock was locked, idling until available
                time.sleep(0.5)

    def process(self):
        """ def receive 에서  queue에 넣은걸 받아서 처리 ,여기서 바로 세부 handler를 호출하지는 않고 handle_response, handle_data 를 먼져 호출시킨뒤
        각각 적합한 세부 handler를 호출 시킨다
        Processes the Client Queue, and passes the data to the respective
        methods.
        :return:
        """

        while self.running:
            # //->def pause 기능을 작동시키면 lock 이 작동되어 process를 하지 않도록 설계했음
            if self._processor_lock.acquire():
                #bitfinex server에 ping을 보내어 server가 죽었는지 살았는지 체크하고 있는상태라면 process 작업하지말고
                #pong이 오는지 부터 체크  //-> 내가 삭제
                # if self.ping_timer:
                #     try:
                #         self._check_ping()
                #     except TimeoutError:
                #         btfx_log.exception("BtfxWss.ping(): TimedOut! (%ss)" %
                #                            self.ping_timer)
                #     except (WebSocketConnectionClosedException, WebSocketTimeoutException,
                #             ConnectionResetError):
                #         btfx_log.exception("BtfxWss.ping(): Connection Error!")
                #         self.conn = None
                # if not self.conn:
                #     # The connection was killed - initiate restart
                #     self.cmd_q.put('restart')

                skip_processing = False

                try:
                    ts, data = self.q.get(timeout=0.1)
                except Queue.Empty:
                    skip_processing = True
                    ts = time.time()

                if not skip_processing:
                    #//-> subscribed 이후에 server 응답이 오고 실제 오는 data message 값  ex.  (1519660977.5863173, [179, [10329, 67.31852068, 10330, 128.20877931, 930.5, 0.0991, 10323, 40792.7176887, 10352, 9280.4]])
                    if isinstance(data, list):
                        try:
                            self.handle_data(ts, data)
                        except FaultyPayloadError as e:
                            # Data had unexpected format, log and continue
                            btfx_log.exception(e)
                    # //->data가 아닌 response message info, subscribed 직후 나오는 모든 server 응답 무슨 event인지 무슨 info인지 있음  ex. {'event': 'subscribed', 'channel': 'ticker', 'chanId': 2, 'symbol': 'tBTCUSD', 'pair': 'BTCUSD'}
                    else:  # Not a list, hence it could be a response
                        try:
                            self.handle_response(ts, data)
                        except UnknownEventError:

                            # We don't know what event this is- restart & log data!
                            btfx_log.exception("main() - UnknownEventError: %s",
                                               data)
                            btfx_log.info("main() - Shutting Down due to "
                                          "Unknown Error!")
                            self.cmd_q.put('restart')
                        except ConnectionResetError:
                            btfx_log.info("processor Thread: Connection Was reset, "
                                          "initiating restart")
                            self.cmd_q.put('restart')

                try:   #//-> 내가 없앰 윗단에서 아예 늦는거 보이면 restart 시키는 걸로
                    #이번 process 처리한뒤 무조건  모든 channel에 대해서 마지막 갱신시간(beats) 체크
                    self._check_heartbeats(ts)
                except (WebSocketConnectionClosedException, ConnectionResetError,
                        UnknownChannelError):
                    btfx_log.exception()

                self._processor_lock.release()
            else:
                time.sleep(0.5)

    ##
    # Response Message Handlers
    ##

    def handle_response(self, ts, resp):
        """ def process 에서 받음  response message(data message가 아닌 receive) 받은뒤 핸들러 호출
        Passes a response message to the corresponding event handler, and also
        takes care of handling errors raised by the _raise_error handler.
        :param ts: timestamp, declares when data was received by the client
        :param resp: dict, containing info or error keys, among others
        :return:
        """
        btfx_log.info("handle_response: Handling response %s", resp)
        self._event_handlers
        event = resp['event']
        print("in handle_response", "event", event, "resp", resp)
        try:
            self._event_handlers[event](ts, **resp)
        # Handle Non-Critical Errors
        except (InvalidChannelError, InvalidPairError, InvalidBookLengthError,
                InvalidBookPrecisionError) as e:
            btfx_log.exception(e)
            print(e)
        except (NotSubscribedError, AlreadySubscribedError) as e:
            btfx_log.exception(e)
            print(e)
        except GenericSubscriptionError as e:
            btfx_log.exception(e)
            print(e)

        # Handle Critical Errors
        except InvalidEventError as e:
            btfx_log.critical("handle_response(): %s; %s", e, resp)
            btfx_log.exception(e)
            raise SystemError(e)
        except KeyError:
            # unsupported event!
            btfx_log.exception("Unknown exception")
            raise UnknownEventError("handle_response(): %s" % resp)

    def _handle_subscribed(self,  *args,   **kwargs):
        #data가 아닌 response(ex event response)handle_response에서 self._eventhandlers[event]로 호출됨
        # 만약 이 함수를 self._event_handlers[event](ts, **resp) 식으로 호출하는대 resp에 chanId가 있을
        # handle_subscribed(self, chanId=None, channel=None, *args, **kwargs): 에서
        #chanId=None과  kwarhs["chanId"] 중복으로 할당되므로 TypeError: _handle_subscribed() got multiple values for keyword argument 'chanId' 발생
        #따라서 chanId=None 을 지워준다
        """
        Handles responses to subscribe() commands - registers a channel id with
        the client and assigns a data handler to it.
        :param chanId: int, represent channel id as assigned by server
        :param channel: str, represents channel name
        """
        if "chanId" in kwargs :
            chanId = kwargs["chanId"]

        if "channel" in kwargs:
            channel = kwargs["channel"]
        else :
            #authentication 때문에 넣었는데  v2는 지원이 잘 안되서 보류 하는걸로 함
            channel = kwargs["event"]


        # btfx_log.debug("_handle_subscribed: %s - %s - %s", chanId, channel, kwargs)
        btfx_log.info("_handle_subscribed: %s - %s - %s", chanId, channel, kwargs) #//->내가바꿈
        # btfxwss 객체에 이미 chanId가 정의되어 있을경우 custom error 발생
        if chanId in self.channels:
            btfx_log.error("alreay this %s, %s was subscribed\n %s", chanId, channel, self._get_current_channelInfo())  #//-> 내가 추가

            raise AlreadyRegisteredError()

        self._heartbeats[chanId] = time.time()
        # //-> orderBook 인지 RawOrderBook 인지
        try:
            #subscribed 된 채널의 str 명임(당연히 동일 채널명이어도 symbol이 다른 여러개가 있을수 있음)
            channel_key = ('raw_'+channel
                           if kwargs['prec'].startswith('R') and channel == 'book'
                           else channel)
        except KeyError:
            channel_key = channel

        #개별 chanId (ex, 1234 , 11) 이 어떤 이름인지,
        # 즉 어떤 pair(ex LTCBTC)인지에 따라 여러개의 orderBook channel을 subscribe할수 있으므로
        #response 말고 channel에 걸맞는 data가 왔을때 나중에 바로 적합한 data_handler를 호출할 수 있도록
        # 각 핸들러 함수 주소값을  self.channels chanId별로 저장시킴
        try:
            self.channels[chanId] = self._data_handlers[channel_key]
        except KeyError:
            btfx_log.error("%s, %s \n %s", chanId, channel,
                           self._get_current_channelInfo())  # //-> 내가 추가
            raise UnknownChannelError()

        # prep kwargs to be used as secondary value in dict key
        try:
            kwargs.pop('event')
        except KeyError:
            pass

        try:
            kwargs.pop('len')
        except KeyError:
            pass

        try:
            kwargs.pop('chanId')
        except KeyError:
            pass
        #ex. kwargs = {'event': 'subscribed', 'channel': 'ticker', 'chanId': 3, 'symbol': 'tBTCUSD', 'pair': 'BTCUSD'}
        #대충 위와같은 reponse가 오므로 여기서 chanId event len  같은거 제외하고 channel_label에 집어넣음
        #이를 추후에  trades 등등에서 symbol 불러낼때 사용
        self.channel_labels[chanId] = (channel_key, kwargs)

    def _handle_unsubscribed(self, *args, **kwargs):
        """
        Handles responses to unsubscribe() commands - removes a channel id from
        the client.
        :param chanId: int, represent channel id as assigned by server
        """

        if "chanId" in kwargs:
            chanId = kwargs["chanId"]

        if "channel" in kwargs:
            channel = kwargs["channel"]

        # btfx_log.debug("_handle_unsubscribed: %s - %s", chanId, kwargs)
        btfx_log.info("_handle_unsubscribed: %s - %s", chanId, kwargs)
        try:
            self.channels.pop(chanId)
        except KeyError:
            raise NotRegisteredError()

        try:
            self._heartbeats.pop(chanId)
        except KeyError:
            pass

        try:
            self._late_heartbeats.pop(chanId)
        except KeyError:
            pass

    def _raise_error(self, *args, **kwargs):
        """
        Raises the proper exception for passed error code. These must then be
        handled by the layer calling _raise_error()
        """
        btfx_log.error("_raise_error(): %s" % kwargs)
        try:
            error_code = str(kwargs['code'])
        except KeyError as e:
            btfx_log.exception() #..-> 내가추가
            raise FaultyPayloadError('_raise_error(): %s' % kwargs)

        try:
            raise self._code_handlers[error_code]()
        except KeyError:
            btfx_log.exception()  # ..-> 내가추가
            raise UnknownWSSError()

    def _handle_info(self, *args, **kwargs):
        """
        Handles info messages and executed corresponding code
        """
        if 'version' in kwargs:
            # set api version number and exit
            self.api_version = kwargs['version']
            print("Initialized API with version %s" % self.api_version)
            return
        try:
            info_code = str(kwargs['code'])
        except KeyError:
            raise FaultyPayloadError("_handle_info: %s" % kwargs)

        if not info_code.startswith('2'):
            raise ValueError("Info Code must start with 2! %s", kwargs)

        output_msg = "_handle_info(): %s" % kwargs
        btfx_log.info(output_msg)

        try:
            self._code_handlers[info_code]()
        except KeyError:
            raise UnknownWSSInfo(output_msg)

    def _handle_pong(self, *args, **kwargs):
        """
        1.def wss.ping() 으로  conn.send(json.dumps({'event': 'ping'}))호출하면 (1521869673.221, {u'event': u'pong', u'ts': 1521869674121L}) 식으로 오는데 처리하고
        메세지 날려줌
        Handles pong messages; resets the self.ping_timer variable and logs
        info message.
        :param ts: timestamp, declares when data was received by the client
        :return:
        """
        if "ts" in kwargs:
            ts = kwargs["ts"]
        btfx_log.info("BtfxWss.ping(): Pong received! (%ss)",
                      ts - self.ping_timer)
        # self.ping_timer = None

    def _handle_conf(self, ts, *args, **kwargs):
        pass

    ##
    # Data Message Handlers  ㄷㅈ\\\\\\\
    ##

    def handle_data(self, ts, msg):
        """
        Passes msg to responding data handler, determined by its channel id,
        which is expected at index 0.
        :param ts: timestamp, declares when data was received by the client
        :param msg: list or dict of websocket data
        :return:
        """
        try:
            #//-> python 3 이상에서  a, *b = [1,3,4,5] 일경우  일경우 a = 1, b = [2,3,4,5]
            #변수 할당에서 starred expression이(*) 오면 무조건  비어있는 []를 생각할것, list나 tuple 인것을 재 할당할때 나머지를 전부 *data에 할당
            # chan_id, *data = msg  //-> 2.7에서 starred expression 할당 안됨
            chan_id, data = msg[0], msg[1:]


        except ValueError as e:
            # Too many or too few values
            btfx_log.exception("Too many or too few values")
            raise FaultyPayloadError("handle_data(): %s - %s" % (msg, e))
        # //-> self._heartbeats[chan_id] = ts 는 subscribed 나  data가 올때 계속 갱신시키는데
        # bitfinex websocket에서 'hb' recv는  해당 channel에서 움직임이 5초이상없을때(서버가 멈춘건 아님 ex .orderBook 에 사람들이 거래를 안해서 변화가 없을때 ) 오는것 으로서
        # 아직 살아 있음을 알려주기 위해 보내는것임 또한 hb recv가 아니라도 어떠한 응답이라도 왔다면 또한 살아 있다는 것 이므로 heartbeat를 갱신시킨다
        self._heartbeats[chan_id] = ts
        #서버에서 응답이 없을때 hb를 보낸다
        if data[0] == 'hb':
            self._handle_heartbeat(ts, chan_id)
            return
            # hb는 server 응답으로 hb를 갱신시키고 실제 데이터가 정상적으로 와도 hb를 갱신시킨다
        try:
            #이미 _event_handlers 인 handle_subscribed 에서 chan_id 에따라 적절히 처리할 수 있게
            #data_handler함수 주소값을 저장시켜놨음 따라서 self.data_handler 에서 channelKey로
            #호출하는게 아닌 저장된 self.channels 에서 chanId로 바로 호출 가능
            # (self.data_handler 에 정의된 함수 호출임)
            self.channels[chan_id](ts, chan_id, data)
        except KeyError:
            btfx_log.exception("handle_data: %s not registered - "
                               "Payload: %s \n $s", chan_id, msg, self._get_current_channelInfo())
            raise NotRegisteredError("handle_data: %s not registered - "
                                     "Payload: %s" % (chan_id, msg))

        except ValueError as e:
            btfx_log.exception("handle_data: %s not registered - "
                               "Payload: %s \n $s", chan_id, msg, self._get_current_channelInfo())
            raise FaultyPayloadError("handle_data(): %s - %s" % (msg, e))

    @staticmethod
    def _handle_heartbeat(*args, **kwargs):
        """
        By default, does nothing.
        :param args:
        :param kwargs:
        :return:
        """
        # print("hb received")

    def _handle_ticker(self, ts, chan_id, data):
        """
        Adds received ticker data to self.tickers dict, filed under its channel
        id.
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: tuple or list of data received via wss
        :return:
        """
        label = self.channel_labels[chan_id][1]['pair']
        # entry = (*data, ts,)  //-> python 2.7에서 이걸 지원하지 않으므로 list로 만든뒤 변환

        entry = list()
        if len(data) == 0:
            entry = (ts,)
        else:
            if isinstance(data, tuple):
                data = list(data)

            data.insert(len(data), ts)
        entry = tuple(data)
        self.tickers[label].append(entry)

    def _handle_book(self, ts, chan_id, data):
        """
        Updates the order book stored in self.books[chan_id]
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: dict, tuple or list of data received via wss
        :return:
        """
        # handle_subscribed 에 의해서 subscribed 성립된 이후
        #  event response dict중에 해당 chan_id 별로 pair 정보를 self.channel_labels에 저장해 뒀음
        label = self.channel_labels[chan_id][1]['pair']
        #subscribed이후에 최초 snapshot데이터일때
        if isinstance(data[0][0], list):
            data = data[0]  # //-> 내가 추가한것
            for order in data:
                price, count, amount = order
                side = (self.books[label].bids if amount > 0
                        else self.books[label].asks)
                # side[str(price)] = (price, amount, count, ts)  //-> 원래 소스
                # atraStyle로 만들기 위해 내가 바꿈  asks quantity는 음수로 오기 때문에 양수로 바꿔줌
                side[str(price)] = {"price" : price, "quantity" : amount if amount > 0 else -1 * amount, "count" : count}

        else:
            # update  //-> 갱신 데이터
            data = data[0]  # //-> 내가 추가한것
            price, count, amount = data
            side = (self.books[label].bids if amount > 0
                    else self.books[label].asks)
            #bitfinex websocket orderBook api규칙임  count가 0으로 오면 사라진것으로 보고 현재까지 갱신된 내용에서 제거
            if count == 0:
                # remove from book
                try:
                    side.pop(str(price))
                except KeyError:
                    # didn't exist, move along
                    pass
            else:
                # update in book
                # side[str(price)] = (price, amount, count, ts)  //-> 원래소스
                # atraStyle로 만들기 위해 내가 바꿈  asks quantity는 음수로 오기 때문에 양수로 바꿔줌
                side[str(price)] = {"price": price, "quantity": amount if amount > 0 else -1 * amount, "count": count}

        self.books[label].latest_ts = self._heartbeats[chan_id]    #//-> atra용으로 내가 추가
    def _handle_raw_book(self, ts, chan_id, data):
        """
        Updates the raw order books stored in self.raw_books[chan_id]
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: dict, tuple or list of data received via wss
        :return:
        """
        data = data[0]  # peel off blob remainder
        if isinstance(data[0], list):
            # snapshot
            for order in data:
                order_id, price, amount = order
                side = (self.raw_books[chan_id].bids if amount > 0
                        else self.raw_books[chan_id].asks)
                side[str(order_id)] = (order_id, price, amount, ts)
        else:
            # update in book
            order_id, price, amount = data
            side = (self.raw_books[chan_id].bids if amount > 0
                    else self.raw_books[chan_id].asks)
            if price == 0:
                # remove from book
                try:
                    side.pop(str(order_id))
                except KeyError:
                    # didn't exist, move along
                    pass
            else:
                side[str(order_id)] = (order_id, price, amount, ts)

    def _handle_trades(self, ts, chan_id, data):
        """
        Files trades in self._trades[chan_id]
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: list of data received via wss
        :return:
        """
        label = self.channel_labels[chan_id][1]['pair']
        if isinstance(data[0], list):
            # snapshot
            for trade in data:
                order_id, mts, amount, price = trade
                self._trades[label][order_id] = (order_id, mts, amount, price)
        else:
            # single data
            _type, trade = data
            self._trades[label][trade[0]] = trade

    def _handle_candles(self, ts, chan_id, data):
        """
        Stores OHLC data received via wss in self.candles[chan_id]
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: list of data received via wss
        :return:
        """
        label = self.channel_labels[chan_id][1]['pair']
        if isinstance(data[0][0], list):
            # snapshot
            for candle in data:
                self.candles[label].append(candle)
        else:
            # update
            self.candles[label].append(data)

    def _handle_auth(self, ts, chan_id, data):
        keys = {'hts': self._handle_auth_trades,
                'te': self._handle_auth_trades, 'tu': self._handle_auth_trades,
                'ps': self._handle_auth_positions,
                'pn': self._handle_auth_positions,
                'pu': self._handle_auth_positions,
                'pc': self._handle_auth_positions,
                'os': self._handle_auth_orders, 'on': self._handle_auth_orders,
                'ou': self._handle_auth_orders, 'oc': self._handle_auth_orders,
                'hos': self._handle_auth_orders, 'ws': self._handle_auth_wallet,
                'wu': self._handle_auth_wallet, 'bs': self._handle_auth_balance,
                'bu': self._handle_auth_balance,
                'mis': self._handle_auth_margin_info,
                'miu': self._handle_auth_margin_info,
                'fis': self._handle_auth_funding_info,
                'fiu': self._handle_auth_funding_info,
                'fos': self._handle_auth_offers, 'fon': self._handle_auth_offers,
                'fou': self._handle_auth_offers, 'foc': self._handle_auth_offers,
                'hfos': self._handle_auth_offers,
                'fcs': self._handle_auth_credits,
                'fcn': self._handle_auth_credits,
                'fcu': self._handle_auth_credits,
                'fcc': self._handle_auth_credits,
                'hfcs': self._handle_auth_credits,
                'fls': self._handle_auth_loans, 'fln': self._handle_auth_loans,
                'flu': self._handle_auth_loans, 'flc': self._handle_auth_loans,
                'hfls': self._handle_auth_loans,
                'hfts': self._handle_auth_funding_trades,
                'fte': self._handle_auth_funding_trades,
                'ftu': self._handle_auth_funding_trades}

        # event, *data = data   //-> 2.7 에서 starred expression 할당 안됨
        event, data = data[0], data[1:]
        try:
            keys[event](event, data)
        except KeyError:
            btfx_log.exception('%s; %s', chan_id, data)
            raise UnknownEventError('The Passed event in data[0] is not '
                                    'associated with any data handler!')
        except Exception:
            btfx_log.exception("_handle_auth: %s - %s, %s", chan_id, event, data)
            raise

    def _handle_auth_trades(self, event, data):
        self.account['trades'].append((event, data))

    def _handle_auth_positions(self, event, data):
        self.account['positions'].append((event, data))

    def _handle_auth_orders(self, event, data):
        self.account['orders'].append((event, data))

    def _handle_auth_wallet(self, event, data):
        self.account['wallet'].append((event, data))

    def _handle_auth_balance(self, event, data):
        self.account['balance'].append((event, data))

    def _handle_auth_margin_info(self, event, data):
        self.account['margin_info'].append((event, data))

    def _handle_auth_funding_info(self, event, data):
        self.account['funding_info'].append((event, data))

    def _handle_auth_offers(self, event, data):
        self.account['offers'].append((event, data))

    def _handle_auth_credits(self, event, data):
        self.account['credits'].append((event, data))

    def _handle_auth_loans(self, event, data):
        self.account['loans'].append((event, data))

    def _handle_auth_funding_trades(self, event, data):
        self.account['funding_trades'].append((event, data))

    ##
    # Commands
    ##

    def ping(self):
        """
        1. 그냥 상위 application에서 바로 ping을 호출 하며 쓰일수도 있고
        2. def process 에서 queue처리 한뒤에 self._check_heartbeats(ts) 를 호출하고 heartbeat가 날라온 channel중
        서버가 멈춘건지 그냥 받을 데이터에 변동이 없는것인지 확인할때 ping을 날리게 되는데 그때사용한다

        이 외에는 btfxwss에서 사용처 없음
        Pings Websocket server to check if it's still alive.
        :return:
        """
        self.ping_timer = time.time()
        self.conn.send(json.dumps({'event': 'ping'}))

    def config(self, decimals_as_strings=True, ts_as_dates=False,
               sequencing=False, **kwargs):
        """
        Send configuration to websocket server
        :param decimals_as_strings: bool, turn on/off decimals as strings
        :param ts_as_dates: bool, decide to request timestamps as dates instead
        :param sequencing: bool, turn on sequencing
        :param kwargs:
        :return:
        """
        flags = 0
        if decimals_as_strings:
            flags += 8
        if ts_as_dates:
            flags += 32
        if sequencing:
            flags += 65536
        q = {'event': 'conf', 'flags': flags}
        q.update(kwargs)
        self.conn.send(json.dumps(q))

    def _subscribe(self, channel_name, **kwargs):
        if not self.conn:
            btfx_log.error("_subscribe(): Cannot subscribe to channel,"
                           "since the client has not been started!")
            return
        q = {'event': 'subscribe', 'channel': channel_name}
        q.update(**kwargs)
        # btfx_log.debug("_subscribe: %s", q)
        btfx_log.info("_subscribe: %s", q) #//-> 내가바꿈
        self.conn.send(json.dumps(q))

    # def _unsubscribe(self, channel_name, **kwargs):
    #원소스가 잘못됐음  unsubscribe 할때 필요한건 chan_id 인데 원소스는 channel_name을 가져와서 교체함
    def _unsubscribe(self, chan_id, **kwargs):
        """ 만약 orderBook 에서 호출 한다면 channel_name은  'book' 가 되고 symbol은 따로 BTCUSD같은게 온다 """
        if not self.conn:
            btfx_log.error("be(): Cannot unsubscribe from channel,"
                           "since the client has not been started!")
            return
        print(self._get_current_channelInfo())
        try:
            pass
            # chan_id = self.channels.pop(chan_id)
            # # self.channels.pop(chan_id)
        except KeyError:
            raise NotRegisteredError("be(): %s" % chan_id)
        q = {'event': 'unsubscribe', 'chanId': chan_id}
        self.conn.send(json.dumps(q))


    def ticker(self, pair, unsubsribe=False, **kwargs):
        """
        Subscribe to the passed pair's ticker channel.
        :param pair: str, Pair to request data for.
        :param kwargs:
        :return:
        """
        if unsubsribe:
            self._unsubscribe('ticker', symbol=pair, **kwargs)
        else:
            self._subscribe('ticker', symbol=pair, **kwargs)

    def order_book(self, pair, unsubscribe=False, **kwargs):
        """
        Subscribe to the passed pair's order book channel.
        :param pair: str, Pair to request data for.
        :param kwargs:
        :return:
        """

        if unsubscribe:
            proper_chanId = None
            for chan_id, chan_info in self.channel_labels.iteritems():
                channel_key = chan_info[0]
                channel_pair = chan_info[1]['pair']
                # print(channel_key, channel_pair, pair)
                if channel_key == 'book' and channel_pair == pair:
                    proper_chanId = chan_id
            self._unsubscribe(proper_chanId, symbol=pair, **kwargs)
        else:
            self._subscribe('book', symbol=pair, **kwargs)

    def raw_order_book(self, pair, prec=None, unsubscribe=False, **kwargs):
        """
        Subscribe to the passed pair's raw order book channel.
        :param pair: str, Pair to request data for.
        :param kwargs:
        :return:
        """
        prec = 'R0' if prec is None else prec
        if unsubscribe:
            self._unsubscribe('book', pair=pair, prec=prec, **kwargs)
        else:
            self._subscribe('book', pair=pair, prec=prec, **kwargs)

    def trades(self, pair, unsubscribe=False, **kwargs):
        """
        Subscribe to the passed pair's trades channel.
        :param pair: str, Pair to request data for.
        :param kwargs:
        :return:
        """
        if unsubscribe:
            self._unsubscribe('trades', symbol=pair, **kwargs)
        else:
            self._subscribe('trades', symbol=pair, **kwargs)

    def ohlc(self, pair, timeframe=None, unsubcribe=False, **kwargs):
        """
        Subscribe to the passed pair's OHLC data channel.
        :param pair: str, Pair to request data for.
        :param timeframe: str, {1m, 5m, 15m, 30m, 1h, 3h, 6h, 12h,
                                1D, 7D, 14D, 1M}
        :param kwargs:
        :return:
        """
        valid_tfs = ['1m', '5m', '15m', '30m', '1h', '3h', '6h', '12h', '1D',
                     '7D', '14D', '1M']
        if timeframe:
            if timeframe not in valid_tfs:
                raise ValueError("timeframe must be any of %s" % valid_tfs)
        else:
            timeframe = '1m'
        pair = 't' + pair if not pair.startswith('t') else pair
        key = 'trade:' + timeframe + ':' + pair
        if unsubcribe:
            self._unsubscribe('candles', key=key, **kwargs)
        else:
            self._subscribe('candles', key=key, **kwargs)

    ##
    # Private Endpoints
    ##

    def authenticate(self, *filters):
        """
        //-> key와 secret을 삽입하면 되긴 되는데  channel에서  받아지지를 않음 일단 auth는 안 쓰는걸로....
        v1.0은 작동하는데 v2.0은 의미없는 recv 만 오고 안됨 심지어 bifinex  api 페이지 websocket playground에서도..
        Authenticate with API; this automatically subscribe to all private
        channels available.
        :return:
        """
        print(filters)
        nonce = str(int(time.time() * 1000000000))
        payload = 'AUTH' + nonce
        h = hmac.new(self.secret, payload, hashlib.sha384)
        signature = h.hexdigest()

        data = {'event': 'auth', 'apiKey': self.key, 'authPayload': payload,
                'authNonce': nonce, 'authSig': signature, 'filter': filters}
        print(json.dumps(data))
        return
        self.conn.send(json.dumps(data))

    def unauth(self):
        js = {'event': 'unauth', 'chanId': 0}
        self.conn.send(json.dumps(js))

class BtfxWssRaw(BtfxWss):
    """
    Bitfinex Websocket API Client. Inherits from BtfxWss, but stores data in raw
    format on disk instead of in-memory. Rotates files every 24 hours by default.
    """

    def __init__(self, key=None, secret=None, addr=None, output_dir=None,
                 rotate_after=None, rotate_at=None, rotate_to=None):
        """
        Initializes BtfxWssRaw Instance.
        :param key: Api Key as string.
        :param secret: Api secret as string.
        :param addr: Websocket API Address.
        :param output_dir: Directory to create file descriptors in.
        :param rotate_after: time in seconds, after which descriptor ought to be
                             rotated. Default 3600*24s.
        :param rotate_at: optional time, date str in format 'HH:MM' (24h clock),
                          at which descriptors should be rotated. Mutually
                          exclusive with rotate_after.
        :param rotate_to: path as str, target folder to copy data to.
        """
        super(BtfxWssRaw, self).__init__(key=key, secret=secret, addr=addr)

        self.tar_dir = output_dir if output_dir else '/tmp/'

        if not rotate_after and not rotate_at:
            self.rotate_after = rotate_after if rotate_after else 3600 * 24
            self.rotate_at = None
        elif rotate_after and rotate_at:
            raise ValueError("Can only specify either rotate_at or rotate_after! Not both!")
        else:
            self.rotate_after = rotate_after
            self.rotate_at = rotate_at

        self.rotate_to = rotate_to if rotate_to else '/var/tmp/data/'

        # File Desciptor Variables
        self.tickers = None
        self.books = None
        self.raw_books = None
        self._trades = None
        self.candles = None
        self.account = None

        self.rotator_thread = None

    def _init_file_descriptors(self, path=None):
        """
        Assign the file descriptors to their respective attributes.
        :param path: str, defaults to self.tar_dir
        :return:
        """
        path = path if path else self.tar_dir
        self.tickers = open(path + 'btfx_tickers.csv', 'a', encoding='UTF-8')
        self.books = open(path + 'btfx_books.csv', 'a', encoding='UTF-8')
        self.raw_books = open(path + 'btfx_rawbooks.csv', 'a', encoding='UTF-8')
        self._trades = open(path + 'btfx_trades.csv', 'a', encoding='UTF-8')
        self.candles = open(path + 'btfx_candles.csv', 'a', encoding='UTF-8')
        if self.key and self.secret:
            self.account = open(path + 'btfx_account.csv', 'a', encoding='UTF-8')

    def _close_file_descriptors(self):
        """
        Closes all file descriptors
        :return:
        """
        self.tickers.close()
        self.books.close()
        self.raw_books.close()
        self._trades.close()
        self.candles.close()
        if self.account:
            self.account.close()

    def _rotate(self):
        """
        Function for the rotator thread, which calls the rotate_descriptors()
        method after the set interval stated in self.rotate_after
        :return:
        """
        t = time.time()
        rotated = False
        while self.running:

            time_to_rotate = ((time.time() - t >= self.rotate_after)
                              if self.rotate_after
                              else (datetime.datetime.now().strftime('%H:%M') ==
                                    self.rotate_at))

            if time_to_rotate and not rotated:
                self._rotate_descriptors(self.rotate_to)
                rotated = True
                t = time.time()
            else:
                rotated = False
                time.sleep(1)

    def _rotate_descriptors(self, target_dir):
        """
        Acquires the processor lock and cCloses file descriptors, renames the
        files and moves them to target_dir.
        Afterwards, opens new batch of file descriptors and releases the lock.
        :param target_dir: str, path.
        :return:
        """
        with self._processor_lock:

            # close file descriptors
            self._close_file_descriptors()

            # Move old files to a new location
            fnames = ['btfx_tickers.csv', 'btfx_books.csv', 'btfx_rawbooks.csv',
                      'btfx_candles.csv', 'btfx_trades.csv']
            if self.account:
                fnames.append('btfx_account.csv')
            date = time.strftime('%Y-%m-%d_%H:%M:%S')
            for fname in fnames:
                ex_name, dtype = fname.split('_')
                new_name = ex_name + '_' + date + '_' + dtype
                src = self.tar_dir+'/'+fname
                tar = target_dir + '/' + new_name
                shutil.copy(src, tar)
                os.remove(src)

            # re-open file descriptors
            self._init_file_descriptors()

    def start(self):
        """
        Start the websocket client threads
        :return:
        """
        self._init_file_descriptors()

        super(BtfxWssRaw, self).start()

        btfx_log.info("BtfxWss.start(): Initializing rotator thread..")
        if not self.rotator_thread:
            self.rotator_thread = Thread(target=self._rotate, name='Rotator Thread')
            self.rotator_thread.daemon = True #//-> atra 때문에 추가했음
            self.rotator_thread.start()
        else:
            btfx_log.info("BtfxWss.start(): Thread not started! "
                     "self.rotator_thread is populated!")

    def stop(self):
        """
        Stop all threads and modules of the client.
        :return:
        """
        super(BtfxWssRaw, self).stop()

        btfx_log.info("BtfxWssRaw.stop(): Joining rotator thread..")
        try:
            self.rotator_thread.join()
            if self.rotator_thread.is_alive():
                time.time(1)
        except AttributeError:
            btfx_log.debug("BtfxWss.stop(): Rotator thread was not running!")

        self.rotator_thread = None

        btfx_log.info("BtfxWssRaw.stop(): Done!")

    ##
    # Data Message Handlers
    ##

    def _handle_ticker(self, ts, chan_id, data):
        """
        Adds received ticker data to self.tickers dict, filed under its channel
        id.
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: tuple or list of data received via wss
        :return:
        """
        js = json.dumps((ts, self.channel_labels[chan_id], data))
        self.tickers.write(js + '\n')

    def _handle_book(self, ts, chan_id, data):
        """
        Updates the order book stored in self.books[chan_id]
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: dict, tuple or list of data received via wss
        :return:
        """
        js = json.dumps((ts, self.channel_labels[chan_id], data))
        self.books.write(js + '\n')

    def _handle_raw_book(self, ts, chan_id, data):
        """
        Updates the raw order books stored in self.raw_books[chan_id]
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: dict, tuple or list of data received via wss
        :return:
        """

        js = json.dumps((ts, self.channel_labels[chan_id], data))
        self.raw_books.write(js + '\n')

    def _handle_trades(self, ts, chan_id, data):
        """
        Files trades in self._trades[chan_id]
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: list of data received via wss
        :return:
        """
        js = json.dumps((ts, self.channel_labels[chan_id], data))
        self._trades.write(js + '\n')

    def _handle_candles(self, ts, chan_id, data):
        """
        Stores OHLC data received via wss in self.candles as json
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: list of data received via wss
        :return:
        """
        js = json.dumps((ts, self.channel_labels[chan_id], data))
        self.candles.write(js + '\n')

    def _handle_auth(self, ts, chan_id, data):
        """
        Store Account specific data received by authenticating to the API in
        self.account, as json.
        :param ts: timestamp, declares when data was received by the client
        :param chan_id: int, channel id
        :param data: list of data received via wss
        :return:
        """
        js = json.dumps((ts, self.channel_labels[chan_id], data))
        self.account.write(js + '\n')