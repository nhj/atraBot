#-*- coding:utf-8 -*-
import time, sys
from btfxwss import BtfxWss

sys.path.append("../")
from atraBot import AtraUTIL

wss = BtfxWss()
wss.start()

client_api = wss

if client_api == None:
    raise Exception("you should create wss Obj")

print(wss)


def extractConvertibleSymbolInfo(exchange, coin_symbol):
    print("asdf")
    '''atraCommon Symbol과  exchange 내부의 symbol이 다른경우  ex) bitfinex 내부 심볼은 DASH, atraCommon은 DSH임
    호출하면 commonSymbol과 exchange Symbol을 반환시킴'''
    atraCommon_coin_symbol = coin_symbol
    exchange_coin_symbol = coin_symbol
    convertible_symbol = {"bitfinex": {"DASH": "DSH", "QTUM": "QTM"},
                           "_comment": "atraCommonSymbol : exchangeSymbol"}
    #     convertible_symbol_eachExchane = SharedStorage.convertible_symbol_eachExchane

    if exchange in convertible_symbol:
        print(exchange)
        convertible_symbol_inExchange = convertible_symbol[exchange]

        # common 심볼이 오던  exchange심볼이 오던 ex) DASH 가 오던 DSH가 오던  convertible dict에 있기만 하면 둘 모두를 반환시킴
        for atraCommon_symbol, exchange_symbol in convertible_symbol_inExchange.iteritems():
            if coin_symbol == atraCommon_symbol or coin_symbol == exchange_symbol:
                atraCommon_coin_symbol = atraCommon_symbol
                exchange_coin_symbol = exchange_symbol
                break


####shared 에 있는걸여기 스타일로 바꿨음
bitfinex_coinList = {"fiat": "USD", "BTC": ["ETC", "LTC", "DASH", "XRP"], "USD": ["BTC"]}

####shared 에 있는걸 여기 스타일로 바꿨음

bitfinex_subscribed_orderBookSymbol = {}

bitfinex_all_orderBook_frame = {}
eachCoin_thread_list = []
timestamp = time.time()
# time.sleep(3.5)
for money_role, commodity_list in bitfinex_coinList.iteritems():
    if money_role != "fiat":
        bitfinex_all_orderBook_frame[money_role] = []
        print(money_role, commodity_list)
        for coin in commodity_list:
            atra_common_symbol, exchange_symbol = extractConvertibleSymbolInfo("bitfinex", coin)
            symbol_pair = exchange_symbol + money_role

            bitfinex_all_orderBook_frame[money_role].append(atra_common_symbol)
            wss.order_book(symbol_pair)

time.sleep(3)

print(wss.books.iteritems())

# #해당 symbol의 orderbook subscribed가 열려있는지 확인
# is_unchecked_founded = False

# for money_role_frame, commodity_list_frame in bitfinex_all_orderBook_frame.iteritems():
#     if money_role_frame != "fiat":
#         if money_role_frame not in wss.books.iterkeys():
#             is_unchecked_founded = True
#             break
#         else:
#             for commodity_coin_inFrame in commodity_list_frame:
#                 if commodity_coin_inFrame not in bitfinex_orderBook_temp[money_role_frame].keys():
#                     is_unchecked_founded = True
#                     break
#             break
