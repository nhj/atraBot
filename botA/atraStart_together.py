#-*- coding:utf-8 -*- 
import pandas
from atraOuter import *
from atraBot import *
import pandas as pd
import threading, argparse, configparser
from atraModule import atraInteractor

# def parseAtraArg():  --> atraOuter로 이동 201804


def atraStart():
    #keys ini filename은  key_manager 객체 안에 있음
    key_manager, config_sharedJson_filename, atra_module_list = parseAtraArg()
    SharedStorage.__init__(key_manager, config_sharedJson_filename, atra_module_list)



    # bithumb - poloniex
    #exchange Obj setting 값
    money_role_eachExchange = {"IOExchange": "KRW", "EExchange": "BTC"}
    #pairing Obj settring 값
    pairing_exchangeName_dict = {"IOExchange": "bithumb", "EExchange": "poloniex"}
    BatraCycle_setting_val = {}
    BatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    BatraCycle_setting_val["THREASHOLD_RATE"] = 0.002
    BatraCycle_setting_val["THREASHOLD_PROFIT"] = 5000
    BatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = 5000000
    BatraCycle_setting_val["PERMITTED_COIN"] = ["ETC", "DASH", "LTC", "XRP","ETH","QTUM"]
    BatraCycle_setting_val["TRANSFER_BAN"] = ["BTC"]
    b = ExchangePairing("BTC", pairing_exchangeName_dict, money_role_eachExchange, BatraCycle_setting_val)

    # bithumb - bitfinex
    pairing_exchangeName_dict = {"IOExchange": "bithumb", "EExchange": "bitfinex"}
    money_role_eachExchange = {"IOExchange": "KRW", "EExchange": "BTC"}

    # io가 어디고 EExchange이 어디고는 생성자 생성할때만 딱 넣고 나머지는 다 exchange Type하나로만 구분하여 호출
    AatraCycle_setting_val = {}
    AatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    AatraCycle_setting_val["THREASHOLD_RATE"] = 0.002
    AatraCycle_setting_val["THREASHOLD_PROFIT"] = 5000
    AatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = 5000000
    AatraCycle_setting_val["PERMITTED_COIN"] = ["ETC", "DASH", "XRP", "LTC","ETH","QTUM"]
    AatraCycle_setting_val["TRANSFER_BAN"] = ["BTC"]
    # AatraCycle_setting_val["PERMITTED_COIN"] = ["ETC", "DASH","XRP"]
    a = ExchangePairing("BTC", pairing_exchangeName_dict, money_role_eachExchange, AatraCycle_setting_val)


    #각 exchange의 balance를 주기적으로 받아오는 쓰레드 시작
    balance_autoBot_poloniex = threading.Thread(name="poloniex_balance",
                                                target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot,
                                                args=("poloniex", True), kwargs={"delay": 5})
    balance_autoBot_poloniex.daemon = True
    balance_autoBot_poloniex.start()

    balance_autoBot_bithumb = threading.Thread(name="bithumb_balance",
                                               target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot,
                                               args=("bithumb", True), kwargs={"delay": 5})
    balance_autoBot_bithumb.daemon = True
    balance_autoBot_bithumb.start()

    balance_autoBot_bitfinex = threading.Thread(name="bitfinex_balance",
                                                target=AtraRepetitiveInformer.eachExchangeBalanceInfoBot,
                                                args=("bitfinex", True), kwargs={"delay": 9})
    balance_autoBot_bitfinex.daemon = True
    balance_autoBot_bitfinex.start()


    # 각 마켓의 orderbook을 주기적으로 받아 오는  쓰레드 시작
    orderBook_autoBot_poloniex = threading.Thread(name="poloniex_orderBook",
                                                  target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot,
                                                  args=("poloniex", True), kwargs={"delay": 0})
    orderBook_autoBot_poloniex.daemon = True
    orderBook_autoBot_poloniex.start()

    orderBook_autoBot_bithumb = threading.Thread(name="bithumb_orderBook",
                                                 target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot,
                                                 args=("bithumb", True))
    orderBook_autoBot_bithumb.daemon = True
    orderBook_autoBot_bithumb.start()

    orderBook_autoBot_bitfinex = threading.Thread(name="bitfinex_orderBook",
                                                  target=AtraRepetitiveInformer.eachExchangeOrderBookInfoBot,
                                                  args=("bitfinex", True), kwargs={"delay": 1})
    orderBook_autoBot_bitfinex.daemon = True
    orderBook_autoBot_bitfinex.start()


    while True:
    #     print("is poloniex_orderBook_Exist" + str(AtraUTIL.threadController('find','poloniex_orderBook')))
    #     print("is bithumb_orderBook_Exist" + str(AtraUTIL.threadController('find','bithumb_orderBook')))
    #     clear_output()
        #pairingObj 객체의 costAndProfit_basedOn_inOutCoin_latest 에 값이 들어가 있다는것은 시작 가능하다는 의미 이므로

        if  a.costAndProfit_basedOn_inOutCoin_latest["whenInCoin"] :
            # whenInCoin whenOutCoin 둘 모두 동일한 timestamp 가 들어있음
            timestamp = a.costAndProfit_basedOn_inOutCoin_latest["whenOutCoin"]["timestamp"]
            earning_rate = a.costAndProfit_basedOn_inOutCoin_latest["max_earningRate_info"]["earning_rate"]
    #         if earning_rate >=AatraCycle_setting_val["THREASHOLD_RATE"]:
    #             time.sleep(100)
    #             break
    #         break
        time.sleep(3)


if __name__ == "__main__":
    try:
        atraStart()
    except KeyboardInterrupt as e:
        print(e)
        sys.exit()