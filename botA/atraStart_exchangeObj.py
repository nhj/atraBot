#-*- coding:utf-8 -*- 
import sys
from atraOuter import *
from atraBot_exchangeObj import *
# import pandas as pd
# import threading, argparse, configparser
from atraModule import atraInteractor

# def parseAtraArg():  --> atraOuter로 이동 201804

#atraServer에서 호출할경우 kwargs를 포함해서 config_key 파일 위치와 shared.json 파일 위치를 조정해서 호출하게됨
def atraStart():
    #keys ini filename은  key_manager 객체 안에 있음
    # print("in atraStart loded from " + str(inspect.stack()[1][1]), kwargs)
    key_manager, config_sharedJson_filename, atra_module_list = parseAtraArg()
    SharedStorage.__init__(key_manager, config_sharedJson_filename, atra_module_list)

    bithumb_KRW = Exchange("bithumb", "KRW")

    poloniex_BTC = Exchange("poloniex", "BTC")

    bitfinex_BTC = Exchange("bitfinex", "BTC")

    bitfinex_USD = Exchange("bitfinex", "USD")

    poloniex_USDT = Exchange("poloniex", "USDT")

    #ETH기반 exchangePairing
    poloniex_ETH = Exchange("poloniex", "ETH")

    bitfinex_ETH = Exchange("bitfinex", "ETH")

    #대표로 exchange Obj 아무거나로 생성
    repr_bithumb = RepresentitiveExchange(bithumb_KRW)
    repr_poloniex = RepresentitiveExchange(poloniex_BTC)
    repr_bitfinex = RepresentitiveExchange(bitfinex_BTC)


    #INNEREXCHANGE 기반
    # poloniex_USD -poloniex_BTC
    AatraCycle_setting_val = {}
    AatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    AatraCycle_setting_val["THREASHOLD_RATE"] = 0.002
    AatraCycle_setting_val["THREASHOLD_PROFIT"] = (10, "USDT")
    AatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = (5000, "USDT")
    AatraCycle_setting_val["LIST_BAN"] = ["DASH"]
    AatraCycle_setting_val["TRANSFER_BAN"] = ["BTC"]
    USD_poloniex__poloniex_BTC = ExchangePairing("BTC", poloniex_USDT, poloniex_BTC, AatraCycle_setting_val)
    #
    ## bitfinex_USD -bitfinex_BTC    websocker orderBook 중복성 검사 로직 완료하고 실행할것
    # AatraCycle_setting_val = {}
    # AatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    # AatraCycle_setting_val["THREASHOLD_RATE"] = 0.002
    # AatraCycle_setting_val["THREASHOLD_PROFIT"] = (10, "USD")
    # AatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = (5000, "USD")
    # AatraCycle_setting_val["LIST_BAN"] = ["ETC", "DASH",  "XRP", "ETH", "QTUM"]
    # AatraCycle_setting_val["TRANSFER_BAN"] = ["BTC"]
    # USD_bitfinex__bitfinex_BTC = ExchangePairing("BTC", bitfinex_USD, bitfinex_BTC, AatraCycle_setting_val)

    ##TRIANGULAR KRW BTC 기반
    #bithumb_KRW - poloniex_BTC
    BatraCycle_setting_val = {}
    BatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    BatraCycle_setting_val["THREASHOLD_RATE"] = 0.002
    BatraCycle_setting_val["THREASHOLD_PROFIT"] = (5000, "KRW")
    BatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = (5000000, "KRW")
    BatraCycle_setting_val["LIST_BAN"] = ["DASH"]
    BatraCycle_setting_val["TRANSFER_BAN"] = ["BTC"]
    KRW_bithumb__poloniex_BTC = ExchangePairing("BTC", bithumb_KRW, poloniex_BTC, BatraCycle_setting_val)

    # bithumb_KRW - bitfinex_BTC
    AatraCycle_setting_val = {}
    AatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    AatraCycle_setting_val["THREASHOLD_RATE"] = 0.002
    AatraCycle_setting_val["THREASHOLD_PROFIT"] = (5000, "KRW")
    AatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = (5000000, "KRW")
    AatraCycle_setting_val["LIST_BAN"] = ["DASH"]
    AatraCycle_setting_val["TRANSFER_BAN"] = ["BTC"]
    KRW_bithumb__bitfinex_BTC = ExchangePairing("BTC", bithumb_KRW, bitfinex_BTC, AatraCycle_setting_val)

    ##TRIANGULAR KRW ETH 기반
    # bithumb_KRW - poloniex_ETH
    BatraCycle_setting_val = {}
    BatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    BatraCycle_setting_val["THREASHOLD_RATE"] = 0.001
    BatraCycle_setting_val["THREASHOLD_PROFIT"] = (5000, "KRW")
    BatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = (5000000, "KRW")
    BatraCycle_setting_val["LIST_BAN"] = ["DASH"]
    BatraCycle_setting_val["TRANSFER_BAN"] = []
    KRW_bithumb__poloniex_ETH = ExchangePairing("ETH", bithumb_KRW, poloniex_ETH, BatraCycle_setting_val)

    # bithumb_KRW - bitfinex_ETH
    AatraCycle_setting_val = {}
    AatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    AatraCycle_setting_val["THREASHOLD_RATE"] = 0.001
    AatraCycle_setting_val["THREASHOLD_PROFIT"] = (5000, "KRW")
    AatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = (5000000, "KRW")
    AatraCycle_setting_val["LIST_BAN"] = ["DASH"]
    AatraCycle_setting_val["TRANSFER_BAN"] = []
    KRW_bithumb__bitfinex_ETH = ExchangePairing("ETH", bithumb_KRW, bitfinex_ETH, AatraCycle_setting_val)


    ## USDT - BTC 기반
    ## poloniex_BTc - bitfinex_BTC
    AatraCycle_setting_val = {}
    AatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = True
    AatraCycle_setting_val["THREASHOLD_RATE"] = 0.002
    AatraCycle_setting_val["THREASHOLD_PROFIT"] = (0.002, "BTC")
    AatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = (1, "BTC")
    AatraCycle_setting_val["LIST_BAN"] = ["DASH"]
    AatraCycle_setting_val["TRANSFER_BAN"] = []
    USDT_poloniex__bitfinex_BTC = ExchangePairing("BTC", poloniex_BTC, bitfinex_BTC, AatraCycle_setting_val)

    #각 exchange의 balance를 주기적으로 받아오는 쓰레드 시작  SharedStorage의 start_mode가 STARTWATCHER 일 경우  balance는 받지 않는다
    repr_bithumb.startBalanceInfoThread(delay=5)
    repr_bitfinex.startBalanceInfoThread(delay=9)
    repr_poloniex.startBalanceInfoThread(delay=5)

    # # 각 마켓의 orderbook을 주기적으로 받아 오는  쓰레드 시작

    repr_bithumb.startOrderBookInfoThread(delay=1)
    repr_poloniex.startOrderBookInfoThread(delay=0)
    repr_bitfinex.startOrderBookInfoThread(delay=1)

    while True:
        # print("is poloniex_orderBook_Exist" + str(AtraUTIL.threadController('find','poloniex_orderBook')))
        # print("is bithumb_orderBook_Exist" + str(AtraUTIL.threadController('find','bithumb_orderBook')))
        # # clear_output()
        # # pairingObj 객체의 costAndProfit_basedOn_inOutCoin_latest 에 값이 들어가 있다는것은 시작 가능하다는 의미 이므로
        #
        # if  a.costAndProfit_basedOn_inOutCoin_latest["whenInCoin"] :
        #     # whenInCoin whenOutCoin 둘 모두 동일한 timestamp 가 들어있음
        #     timestamp = a.costAndProfit_basedOn_inOutCoin_latest["whenOutCoin"]["timestamp"]
        #     earning_rate = a.costAndProfit_basedOn_inOutCoin_latest["max_earningRate_info"]["earning_rate"]
        #     if earning_rate >=AatraCycle_setting_val["THREASHOLD_RATE"]:
        #         time.sleep(100)
        #         break
        #     break
        # {"atraBot_clientName" ,"start_mode" , "exchangeObj" , "repr_exchangeObj" , "paringObj" } 를 보고한다
        #normal과 atraWatcher모드 각각 따로 저장시킴
        SharedStorage.reportAtraStatus()
        time.sleep(10)


if __name__ == "__main__":
    try:
        atraStart()
    except KeyboardInterrupt as e:
        print(e)
        sys.exit()