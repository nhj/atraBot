#-*- coding:utf-8 -*-
import threading, argparse, configparser, getpass, configparser, hashlib, base64, time,pprint, os
#이거 못 불러오면 sudo yum install python-devel 설치할것
from Crypto.Cipher import AES

import json
from atraModule import atraInteractor
'''주로 atraBot과 분리되어 설정파일등을 다루는 메소드들임'''

class AtraConfig(configparser.ConfigParser):
    """return a config parser object with default values. If you need to run
    more Api() objects at the same time you will also need to give each of them
    them a separate ApiConfig() object. For this reason it takes a filename
    in its constructor for the ini file, you can have separate configurations
    for separate Api() instances"""

    def __init__(self, filename):
        self.filename = filename
        configparser.ConfigParser.__init__(self)
        self.load()

    def init_defaults(self, defaults):
        """add the missing default values, default is a list of defaults"""
        for (sect, opt, default) in defaults:
            self._default(sect, opt, default)

    def save(self):
        """save the config to the .ini file"""
        with open(self.filename, 'wb') as configfile:
            self.write(configfile)

    def load(self):
        """(re)load the onfig from the .ini file"""
        self.read(self.filename)

    def get_safe(self, sect, opt):
        """get value without throwing exception."""
        try:
            return self.get(sect, opt)

        except:
            for (dsect, dopt, default) in self._DEFAULTS:
                if dsect == sect and dopt == opt:
                    self._default(sect, opt, default)
                    return default
            return ""

    def get_bool(self, sect, opt):
        """get boolean value from config"""
        return self.get_safe(sect, opt) == "True"

    def get_string(self, sect, opt):
        """get string value from config"""
        return self.get_safe(sect, opt)

    def get_int(self, sect, opt):
        """get int value from config"""
        vstr = self.get_safe(sect, opt)
        try:
            return int(vstr)
        except ValueError:
            return 0

    def get_float(self, sect, opt):
        """get int value from config"""
        vstr = self.get_safe(sect, opt)
        try:
            return float(vstr)
        except ValueError:
            return 0.0

    def _default(self, section, option, default):
        """create a default option if it does not yet exist"""
        if not self.has_section(section):
            self.add_section(section)
        if not self.has_option(section, option):
            self.set(section, option, default)
            self.save()


class KeyManager:
    '''pytrader의 api.py의 Secret임 내가 좀 추가 시킴'''
    """Manage the API secret. This class has methods to decrypt the
    entries in the ini file and it also provides a method to create these
    entries. The methods encrypt() and decrypt() will block and ask
    questions on the command line, they are called outside the curses
    environment (yes, its a quick and dirty hack but it works for now)."""

    S_OK = 0
    S_FAIL = 1
    S_NO_SECRET = 2
    S_FAIL_FATAL = 3

    def __init__(self, config):
        """initialize the instance"""
        self.config = config
        self.api_key = ""
        self.secret_key = ""
        self.password_from_commandline_option = None
        #여기에 담아서 keyManager를 SharedStorage 로 보냄
        self.keys_eachExchange = {}

    def decrypt(self, exchange_name, password):
        """decrypt "secret_key" from the ini file with the given password.
        This will return false if decryption did not seem to be successful.
        After this menthod succeeded the application can access the secret
        //->일단 모두 동일한 password로 encoding이 되어 있음을 가정"""

        api_key = self.config.get_string(exchange_name, "api_key")
        encrypted_secretKey = self.config.get_string(exchange_name, "secret_key")
        #진짜 key의 원래 length값
        secret_key_length = self.config.get_string(exchange_name, "secret_key_length")
        if encrypted_secretKey == "" or api_key == "":
            return self.S_NO_SECRET

        hashed_pass = hashlib.sha512(password.encode("utf-8")).digest()
        crypt_key = hashed_pass[:32]
        crypt_ini = hashed_pass[-16:]
        aes = AES.new(crypt_key, AES.MODE_OFB, crypt_ini)
        try:
            encrypted_secret = base64.b64decode(encrypted_secretKey.strip().encode("ascii"))
            self.api_key = api_key.strip()
            self.secret_key = aes.decrypt(encrypted_secret).strip()
            self.keys_eachExchange[exchange_name] ={"api_key" : self.api_key, "secret_key" : self.secret_key}
        except ValueError:
            return self.S_FAIL

        # now test if we now have something plausible
        try:
            print("testing secret... ")
            # is it plain ascii? (if not this will raise exception)
            #//->secret 키는 모두 base64로 나타내어진다(post로 전송하기 때문에 근데 결과물이 ascii로 decoding이 안된다면 잘못된 것임)
            # dummy = self.secret.decode("ascii")
            # can it be decoded? correct size afterwards?
            # print(self.secret_key)
            # print(len(self.secret_key)), int(secret_key_length)
            #원래의 길이와 비교한다
            if len(self.secret_key) != int(secret_key_length):
                raise Exception("Decrypted secret has wrong size")
            if not self.secret_key:
                raise Exception("Unable to decrypt secret")

            print("testing api_key...")
            # api_key must be only hex digits and have the right size
            # hex_key = self.api_key.replace("-", "").encode("ascii")
            # if len(binascii.unhexlify(hex_key)) != 16:
            #     raise Exception("api_key has wrong size")
            if not self.api_key:
                raise Exception("Unable to decrypt api_key")

            print(exchange_name + " OK")
            return self.S_OK

        except Exception as exc:
            # this api_key and secret do not work
            self.secret_key = ""
            self.api_key = ""
            print("### Error occurred while testing the decrypted secret:" + exchange_name)
            print("    '%s'" % exc)
            print("    This does not seem to be a valid API secret")
            return self.S_FAIL

    def prompt_decrypt(self,display = False):
        """ask the user for password on the command line
        and then try to decrypt the secret."""
        # if self.know_secret():
        #     return self.S_OK

        result = None

        if self.password_from_commandline_option:
            password = self.password_from_commandline_option
        else:
            password = getpass.getpass("enter passphrase for secret: ")

        #get password 모드이므로 보여주고 종료
        if display == True:
            exchange_name = raw_input("      exchange_name: ").strip()
            self.decrypt(exchange_name, password)
            pprint.pprint(self.keys_eachExchange)
            os._exit(1)

        #exchange가 하나라도 있으면 deCrypt 들어감
        if len(self.config.sections()) == 0:
            return self.S_NO_SECRET
        else :
            for exchange_name in self.config.sections():
                key = self.config.get_string(exchange_name, "api_key")
                sec = self.config.get_string(exchange_name, "secret_key")
                key_length = self.config.get_string(exchange_name, "secret_key_length")
                #하나라도 빈게 있으면 NO_SECRET 반환
                if sec == "" or key == "":
                    print(exchange_name  + " has empty key so S_NO_SECRET result returning")
                    return self.S_NO_SECRET
                else:
                    #password 받았을경우 decrypt 들어감
                    result = self.decrypt(exchange_name, password)

        if result != self.S_OK:
            print("")
            print("secret could not be decrypted")
            answer = raw_input("press any key to continue anyways "
                           + "(trading disabled) or 'q' to quit: ")
            if answer == "q":
                result = self.S_FAIL_FATAL
            else:
                result = self.S_NO_SECRET
        return result

    def prompt_exchanges(self):
        '''일단 무조건 exchange 이름 받아서 기계적으로 key랑 secret 추가 시키는 걸로 할 것'''
        while True:
            exchange_name = ""
            print("saved exchange : " + str(self.config.sections()))
            exchange_name = raw_input("      exchange_name: ").strip()
            self.prompt_encrypt(exchange_name)


    def prompt_encrypt(self,exchange_name):
        """ask for api_key, secret_key and password on the command line,
        then encrypt the secret_key and store it in the ini file. 이걸 eachExchange로 할 수 있게 변환"""
        print("Please copy/paste keys and secrets from each exchanges and")
        print("then provide a password to encrypt them.")
        print("")

        api_key = raw_input("             api_key: ").strip()
        secret_key = raw_input("          secret_key: ").strip()
        while True:
            password1 = getpass.getpass("        password: ").strip()
            if password1 == "":
                print("aborting")
                return
            password2 = getpass.getpass("password (again): ").strip()
            if password1 != password2:
                print("you had a typo in the password. try again...")
            else:
                break
        #d0f4c14c48ad4837905ea7520cc4af700f6433ce0985e6bb87b6b4617cb944abf814bd53964ddbf55b41e5812b3afe90890c0a4db75cb04367e139fd62eab2e1
        #16진수로된 128자리  hashed_pass.digest_seze 찍어보면 64로 뜸(64byte이므로)
        hashed_pass = hashlib.sha512(password1.encode("utf-8")).digest()
        #처음부터 32바이트 까지  aes 로 암호화
        crypt_key = hashed_pass[:32]
        #끝에 16byte는 initialization vector 로서 evernote 참조 python -> AES
        crypt_ini = hashed_pass[-16:]

        aes = AES.new(crypt_key, AES.MODE_OFB, crypt_ini)

        # since the secret_key is a base64 string we can just pad it with
        # spaces which can easily be stripped again after decryping
        secret_key_length = len(secret_key)
        print(secret_key_length)
        #AES의 encrypt type인 MODE_OFB는 16byte 로 나누어 떨어져야 하므로(block_size로 접근하면 나옴)\
        # secret_key key를 16 byte의 배수가 될 수 있게 끊어주고 나머지는  " "  패딩해줌(AES encoding 규칙)
        secret_key += " " * (16 - (len(secret_key) % 16))
        # print(len(secret_key))
        # encrypted = aes.encrypt(secret_key)
        # print(encrypted)
        # encrypted_b64 = base64.b64encode(aes.encrypt(secret_key))
        # print(encrypted_b64)
        # ascii_decoded = encrypted_b64.decode("ascii")
        # print(ascii_decoded)

        # print(aes.encrypt(secret_key), type(aes.encrypt(secret_key)))
        # # ascii_encoded = byteString.encode("ascii")
        # # print(ascii_encoded, type(ascii_encoded))
        #
        # base64_encoded = base64.b64encode(aes.encrypt(secret_key))
        # print(base64_encoded, type(base64_encoded))
        #
        # ascii_decoded = base64_encoded.decode("ascii")
        # print(ascii_decoded, type(ascii_decoded))

        encrypted_secretKey = base64.b64encode(aes.encrypt(secret_key)).decode("ascii")
        if exchange_name not in self.config.sections():
            self.config.add_section(exchange_name)
        self.config.set(exchange_name, "api_key", api_key)
        self.config.set(exchange_name, "secret_key", encrypted_secretKey)
        self.config.set(exchange_name, "secret_key_length", str(secret_key_length))
        self.config.set(exchange_name, "saved_date", time.strftime("%y%m%d %I:%M:%S", time.localtime(time.time())))
        self.config.save()

        print("encrypted secret_key has been saved in %s" % self.config.filename)

    def know_secret(self):
        """do we know the secret key? The application must be able to work
        without secret and then just don't do any account related stuff"""
        return (self.secret_key != "") and (self.api_key != "")

#config_keys는 jupyter나 atraServer에서 쓸 수 있기 때문에 파라미터화 함 jupyter에서 호출할때는 ../setting/keys.ini 로 호출해서
#사용함
def parseAtraArg(config_keys_path = "./setting/keys.ini"):
    argp = argparse.ArgumentParser(
        description='manage atra')
    argp.add_argument('--config-keys',
                      default=config_keys_path,
                      help="Use different config //->keys setting file (default: %(default)s)")
    argp.add_argument('--config-sharedJson',
                      default="./setting/shared.json",
                      help="Use different config for shared json //->atra shared Json setting file (default: %(default)s)")
    argp.add_argument('--atraWatcher', action="store_true", default=False,
                      help="atraWatcher mode makes atraCnp module automatically started and all exchangeParing will not be permitted")
    argp.add_argument('--add-secret', action="store_true",
                      help="prompt for API secret for each Exchanges, encrypt them and then exit")
    argp.add_argument('--get-secret', action="store_true",
                      help="show specified exchange saved decrypted_secret_key using password")
    argp.add_argument('--password', action="store", default=None,
                      help="password for decryption of stored key. This is a dangerous option" +
                      "~/.bash_history 에 남을수 있으니 생각하고 사용할것 ")
    argp.add_argument('--module', action="append", default=None,
                      help="select atra Module")
    argp.add_argument('-f', action="store", default=None,
                      help="for jupyter , jupyter에서 사용하려면 꼭 필요함 ")


    args = argp.parse_args()
    config_keys_filename = args.config_keys
    config_sharedJson_filename = args.config_sharedJson
    atra_module_list = args.module
    atraWatcher_mode = args.atraWatcher
    print("atraWatcher_mode", atraWatcher_mode)
    print("atra_module_list : " + str(atra_module_list))
    if atraWatcher_mode == True and atra_module_list == None :
        raise Exception("you should put atraCnp_DB module when atraWatcher mode")
    elif atraWatcher_mode == True and "atraCnp_DB" not in atra_module_list:
        raise Exception("you should put atraCnp_DB module when atraWatcher mode")

    print("config_keys_file : " + config_keys_filename)
    print("config_sharedJson_file : " + config_sharedJson_filename)

    #//->그냥 configparser를 상속받아서 만든 ini파일 관리 class임 이거 자체가 decrypting encrypting 하는 클래스는 아님
    config = AtraConfig(config_keys_filename)
    # config.read(config_keys_filename)
    #decrypting encrypting 하는 클래스
    k_manager = KeyManager(config)
    k_manager.password_from_commandline_option = args.password
    # --add-secret 모드 일때
    if args.add_secret:
        k_manager.prompt_exchanges()
        os._exit(1)
    elif args.get_secret:
        k_manager.prompt_decrypt(display = True)
        os._exit(1)
    else:
        atra_module_interactor = None
        if atra_module_list != None:
            with open(config_sharedJson_filename, 'r') as f:
                shared_config = json.load(f)
                atra_module_interactor = atraInteractor.Interactor(atra_module_list, shared_config["atra_module"])
        if atraWatcher_mode == False:
            #비번을 제대로 치면 시작 할 수 있도록
            decrypted_result = k_manager.prompt_decrypt(display = False)
            # print("decrypted_result", decrypted_result)
            if decrypted_result == k_manager.S_OK:
                return k_manager, config_sharedJson_filename, atra_module_interactor

        #atraWatcher 모드일때는 orderBook 정보만 필요하므로 secret_key 인증이 필요 없음
        #만약 특정 exchange api가 orderBook 만 이용하는대도 secret_key 정보를 요구한다면 그냥 위와 합치고
        #atraWatcher mode임을 명시하는 특정 값을 SharedStorage에 넘겨 줄것
        if atraWatcher_mode == True:

            return None, config_sharedJson_filename, atra_module_interactor
        #todo 각 mode view 모드 virtual-trading 모드 cycle 모드 등등을 어떻게 할지 지금은 curses를 안쓰니 크게 고려하지 않아도 될수있을듯듯
        else:
            pass