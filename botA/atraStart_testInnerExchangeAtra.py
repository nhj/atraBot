#-*- coding:utf-8 -*- 
import sys
from atraOuter import *
from atraBot_exchangeObj import *
import pandas as pd
import threading, argparse, configparser
from atraModule import atraInteractor

# def parseAtraArg():  --> atraOuter로 이동 201804


def atraStart():
    #keys ini filename은  key_manager 객체 안에 있음
    key_manager, config_sharedJson_filename, atra_module_list = parseAtraArg()
    SharedStorage.__init__(key_manager, config_sharedJson_filename, atra_module_list)

    bithumb_KRW = Exchange("bithumb", "KRW")

    poloniex_BTC = Exchange("poloniex", "BTC")

    bitfinex_BTC = Exchange("bitfinex", "BTC")

    bitfinex_USD = Exchange("bitfinex", "USD")

    poloniex_USDT = Exchange("poloniex", "USDT")

    #ETH기반 exchangePairing
    poloniex_ETH = Exchange("poloniex", "ETH")

    bitfinex_ETH = Exchange("bitfinex", "ETH")

    #대표로 exchange Obj 아무거나로 생성
    repr_bithumb = RepresentitiveExchange(bithumb_KRW)
    repr_poloniex = RepresentitiveExchange(poloniex_BTC)
    repr_bitfinex = RepresentitiveExchange(bitfinex_BTC)



    ## innerExchange기반

    ## poloniex_USD -poloniex_BTC
    # AatraCycle_setting_val = {}
    # AatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = False
    # AatraCycle_setting_val["THREASHOLD_RATE"] = 0.0001
    # AatraCycle_setting_val["THREASHOLD_PROFIT"] = (10, "USDT")
    # AatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = (8810, "USDT")
    # AatraCycle_setting_val["PERMITTED_COIN"] = ["ETC", "DASH", "LTC", "XRP", "ETH","QTUM"]
    # AatraCycle_setting_val["TRANSFER_BAN"] = ["BTC"]
    # USD_poloniex__poloniex_BTC = ExchangePairing("BTC", poloniex_USDT,poloniex_BTC, AatraCycle_setting_val)
    
    ## bitfinex_USD -bitfinex_BTC
    AatraCycle_setting_val = {}
    AatraCycle_setting_val["IS_ATRACYCLE_PERMITTED"] = False
    AatraCycle_setting_val["THREASHOLD_RATE"] = 0.00001
    AatraCycle_setting_val["THREASHOLD_PROFIT"] = (0, "USD")
    AatraCycle_setting_val["COMMANDED_ATRA_CYCLE_VALUE"] = (20000, "USD")
    AatraCycle_setting_val["PERMITTED_COIN"] = ["ETC", "DASH", "LTC", "XRP", "ETH","QTUM"]
    AatraCycle_setting_val["TRANSFER_BAN"] = ["BTC"]
    USD_bitfinex__bitfinex_BTC = ExchangePairing("BTC", bitfinex_USD,bitfinex_BTC, AatraCycle_setting_val)




    #각 exchange의 balance를 주기적으로 받아오는 쓰레드 시작
    repr_bithumb.startBalanceInfoThread(delay=5)
    repr_bitfinex.startBalanceInfoThread(delay=9)
    repr_poloniex.startBalanceInfoThread(delay=5)

    # # 각 마켓의 orderbook을 주기적으로 받아 오는  쓰레드 시작

    repr_bithumb.startOrderBookInfoThread(delay=0.1)
    repr_poloniex.startOrderBookInfoThread(delay=0)
    repr_bitfinex.startOrderBookInfoThread(delay=1)


    while True:
        # print("is poloniex_orderBook_Exist" + str(AtraUTIL.threadController('find','poloniex_orderBook')))
        # print("is bithumb_orderBook_Exist" + str(AtraUTIL.threadController('find','bithumb_orderBook')))
        # # clear_output()
        # # pairingObj 객체의 costAndProfit_basedOn_inOutCoin_latest 에 값이 들어가 있다는것은 시작 가능하다는 의미 이므로
        #
        # if  a.costAndProfit_basedOn_inOutCoin_latest["whenInCoin"] :
        #     # whenInCoin whenOutCoin 둘 모두 동일한 timestamp 가 들어있음
        #     timestamp = a.costAndProfit_basedOn_inOutCoin_latest["whenOutCoin"]["timestamp"]
        #     earning_rate = a.costAndProfit_basedOn_inOutCoin_latest["max_earningRate_info"]["earning_rate"]
        #     if earning_rate >=AatraCycle_setting_val["THREASHOLD_RATE"]:
        #         time.sleep(100)
        #         break
        #     break
        time.sleep(3)


if __name__ == "__main__":
    try:
        atraStart()
    except KeyboardInterrupt as e:
        print(e)
        sys.exit()