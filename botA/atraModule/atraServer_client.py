# -*- coding:utf-8 -*-
import urllib, os
import urllib2, httplib, ssl
import json
import time
from bson import ObjectId
import hmac, hashlib
import base64


def troubleshootEncode(obj):
    """object 나 function은  json.dumps 호출시 typeError 를 뱉는데 이럴경우 str로 바꿔줌"""
    try:
        json.dumps(obj)
    except TypeError:
        return str(obj)
    return obj


def createTimeStamp(datestr, format="%Y-%m-%d %H:%M:%S"):
    return time.mktime(time.strptime(datestr, format))

# class HTTPSClientAuthHandler(urllib2.HTTPSHandler):
#     def __init__(self, key, cert):
#         urllib2.HTTPSHandler.__init__(self)
#         self.key = key
#         self.cert = cert
#
#     def https_open(self, req):
#         # Rather than pass in a reference to a connection class, we pass in
#         # a reference to a function which, for all intents and purposes,
#         # will behave as a constructor
#         return self.do_open(self.getConnection, req)
#
#     def getConnection(self, host, timeout=300):
#         return httplib.HTTPSConnection(host, key_file=self.key, cert_file=self.cert)


class AtraServerClient:
    def __init__(self, url="https://150.95.179.9:5000", public_key="public.pem", cert="server.crt"):
        self.url = url
        self.public_key = public_key
        self.cert = cert
        #self sighned certificate 일 경우 SSL 오류가 날 경우가 있음(인증서 만들때 domainname 같은데 호출 주소랑 다를경우
        # 등등 그럴때는 ssl 을 unverified_context로 해야함  but MITM 공격의 대상이 될 수도 있음
        self.ssl_context = ssl._create_unverified_context()
    def api_query(self, endpoint, data=None):

        # # else 문 이전은 모두 public API
        #
        # atraCycle/data?check 같이 DB 작동여부 체크하는 endpoint
        if (endpoint.find("?check") > 0 ):
            print(self.url, endpoint)
            response = urllib2.urlopen(urllib2.Request(self.url + endpoint), context=self.ssl_context)

            return response.read()
        #atraCycle이나 balance 데이터 넣을때 data를 담아서 오면 urllib2.Request 에서 data를 넣으면 자동으로 POST로 보내짐
        elif data != None :
            post_data = data
            # post_data = urllib.urlencode(data)
            post_data = json.dumps(data, default=troubleshootEncode)
            #content-type 헤더 지정을 하면 json.dumps로 시리얼라이즈 된(str) 을 바로 flask에서 request.get_json()으로
            #뽑을 수 있게 만들어줌 header지정안하고 보내면  flask에서 그냥 request.values로 받아냄
            header = {'Content-Type': 'application/json'}
            # print(post_data)

            # opener = urllib2.build_opener(HTTPSClientAuthHandler(self.public_key, self.cert))
            response = urllib2.urlopen(urllib2.Request(self.url + endpoint, post_data, header), timeout=30, context=self.ssl_context)
            # response = opener(urllib2.Request(self.url + endpoint, post_data, header), timeout=30)
            # print(response, type(response))
            return response.read()
            # start = time.time()
            # # print("start",start)
            # jsonRet = json.loads(response.read())
            # # print("start - end",start - time.time())
            # return jsonRet

        # #URI에 request 명이 끝이고 추가 정보 필요없을때
        # elif endpoint.find("symbol") >= 0:
        #     start = time.time()
        #     response = urllib2.urlopen(
        #         urllib2.Request('https://api.bitfinex.com/v1/' + endpoint))
        #     end = time.time()
        #     # print("end" + str(end) ,  (end - start))
        #     return json.loads(response.read())
        # #orderBook 일때때
        # elif (endpoint == "book"):
        #     start = time.time()
        #     # print('https://api.bitfinex.com/v1/' + endpoint + "/" + str(request_params["symbol"]))
        #     response = urllib2.urlopen(
        #         urllib2.Request('https://api.bitfinex.com/v1/' + endpoint + "/" + str(request_params["symbol"] +
        #             '?limit_bids=' + str(request_params['limit_bids'] + '&limit_asks=' + str(request_params['limit_asks'] + '&group=' + str(request_params['group']))))),timeout=10)
        #     end = time.time()
        #     # print("end" + str(end) ,  (end - start))
        #     return json.loads(response.read())
        #
        # # //->여기서 부터는 private API 임
        # else:
        # request_params['request'] = endpoint
        # request_params['nonce'] = str(time.time())
        # #json직렬화와 같은것 같음
        # # post_data = urllib.urlencode(request_params)
        # post_data = json.dumps(request_params)
        # payload = base64.b64encode(post_data)
        #
        #
        # #poloniex는 sha512고 bitfinex는 sha384로 해쉬
        # sign = hmac.new(self.Secret, payload, hashlib.sha384).hexdigest()
        # # print(self.APIKey, self.Secret ,request_params, payload,str(sign))
        # headers = {
        #     'X-BFX-APIKEY': self.APIKey,
        #     'X-BFX-PAYLOAD': payload,
        #     'X-BFX-SIGNATURE': sign
        # }
        # # private은 무조건 post로 보내고 헤더에 전자서명 담아서 보냄
        # # private(trading, withdraw등) 요청은 timeout을 30초 정도로 한다
        # response = urllib2.urlopen(urllib2.Request('https://api.bitfinex.com/v1/' + endpoint, post_data, headers), timeout=30)
        # start = time.time()
        # # print("start",start)
        # jsonRet = json.loads(response.read())
        # # print("start - end",start - time.time())
        # return jsonRet

         # return self._post("/v1/positions", return_json=True)