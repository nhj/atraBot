#-*- coding:utf-8 -*-
from pymongo import MongoClient
from Queue import Queue
from threading import Thread
import pprint
from atraServer_client import AtraServerClient
import logging

logging_notify = logging.getLogger("notify_error")

class Network_worker(Thread):
    '''네트워크 작업을 하는 que 로서 다른 쓰레드가  queue에 집어 넣은 내용을 destination 목적지에 전송시킨다(ex, DB)'''
    def __init__(self, args = None,kargs = None ):
        Thread.__init__(self,name="network_worker")
        self.interactor = args[0]
        self.atra_module_setting_val = self.interactor.atra_module_setting_val



    def run(self):
        #atraInteractor 객체를 집어넣어서 접근함
        shared_queue = self.interactor.shared_queue
        atraServer_client = self.interactor.atraServer_client
        while True:
            called_data = shared_queue.get()
            #sharedStorage에 정의된 이름이 온다 ex. atraCycle_DB
            destination = called_data["destination"]
            data = called_data["data"]
            # print("i found " + str(destination))
            #atra초기 실행때 기재하는 module name = 각 사용 위치에서 호출하는 destination임

            if destination.find("DB") > -1:
                # DB_name = destination.split("_DB")[0]
                DB_name = self.atra_module_setting_val[destination]["DB_name"]

                collection_name  = self.atra_module_setting_val[destination]["collection"]
                # result = collection_obj.insert_one(called_data["data"]).inserted_id
                try:
                    returned_data = atraServer_client.api_query("/" + collection_name + "/data", data)
                    print(returned_data,type(returned_data))
                except Exception as err:
                    logging_notify.exception("server Error Occured")
            if destination.find("REPORT") > -1:
                try:
                    # pprint.pprint(data)
                    returned_data = atraServer_client.api_query("/report", data)
                    # print(returned_data,type(returned_data))
                except Exception as err:
                    logging_notify.exception("server Error Occured")

class Interactor:

    def __init__(self, atra_module_list,atra_module_setting_val):
        self.atra_module_setting_val = atra_module_setting_val
        #DB일 경우 atraDB라는것만 module로서 성립하게 두고
        self.atra_module_list = atra_module_list
        # self.atra_DB_client = {}
        self.atraServer_client = None
        self.shared_queue = None
        self.network_worker = None
        self._initAtraModule()

    def _initAtraModule(self):
        #db관련한건 대부분 대문자로 DB로 한다
        '''실행시에 언급한 atraModule setting 파일들이 모두 setting에 있는지 있다면 connector 객체를 생성해서 저장해 둘것'''
        atraServer_host = self.atra_module_setting_val["atraServer"]["host"]
        atraServer_port = self.atra_module_setting_val["atraServer"]["port"]
        atraServer_url = atraServer_host + ":" + atraServer_port
        self.atraServer_client = atraServer_client = AtraServerClient(url=atraServer_url)
        # print(self.atra_module_setting_val.keys())
        for intended_module in self.atra_module_list:
            if intended_module not in [module_name for module_name in self.atra_module_setting_val] :
                raise Exception("you didn't setted "+ intended_module + " setting val on setting file")
                #setting값이 있을경우 connector / client 객체를 생성함
            else:
                print(intended_module, intended_module.find("DB"))
                if intended_module.find("DB") > 0:
                    collection_name = self.atra_module_setting_val[intended_module]["collection"]
                    #ex. /atraCycle/data?check
                    returned_data = atraServer_client.api_query("/" + collection_name + "/data?check")

                    if returned_data == "failed":
                        raise Exception("module " + returned_data + " loaded Failed !!!!")
                    else :
                        # test_document = DB["test"].find_one() //->원래 db랑 직접 소통했음
                        # 각 database의 test 컬렉션에  DB_name(DB명과 동일함) 을 불러와서 setting 값과 비교함
                        print("module " + returned_data + " loaded .... ok")

                #module_DB 가 된다는 소리는 atraServer가 작동이 잘 된다는 의미 이기 때문에 작동여부를 확인할 필요는 없음
                elif intended_module == "atraServer" :
                    pass
                elif intended_module.find("REPORT") >= 0:
                    print("module " + intended_module + " loaded .... ok")
        if self.shared_queue == None:
            self.shared_queue = Queue()
        if self.network_worker == None or self.network_worker.is_alive != True :
            self.network_worker = Network_worker(args=(self,))
            self.network_worker.daemon = True
            self.network_worker.start()



    def set_DB_data(self,database,collection,data):
        pass






