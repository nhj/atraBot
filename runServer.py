#-*- coding:utf-8 -*-
import os, sys, time
from atraServer import atraApp
from botA import atraStart_exchangeObj
from threading import Thread

#패키지 형식으로 flask 를 구성하기 위해서 runServer파일만 외부로 뺐음 자동으로 atra(container)가 sys.path에
#포함 됨으로써 내부에 있는 submodule에서 다른 submodule 호출 가능

# def atraServerConfigInit():
#     key_manager, config_sharedJson_filename, atra_module_list = parseAtraArg()
#     SharedStorage.__init__(key_manager, config_sharedJson_filename, atra_module_list)
#
#     print(key_manager,config_sharedJson_filename,atra_module_list)
#
# atraServerConfigInit()


# atraStart_exchangeObj.atraStart()
# print("start")

# if sys.argv > 1 :
#     atraBot_thread = Thread(target=atraStart_exchangeObj.atraStart)
#     atraBot_thread.daemon = True
#     atraBot_thread.start()

cert_file_name = "server.crt"
privateKey_file_name = "private.pem"
try:
    with file(cert_file_name, "r") as f:
        print(f.name)
    with file(privateKey_file_name, "r") as f:
        print(f.name)
except IOError as err:
    print(err)
    print("starting without HTTPS mode")
    atraApp.secret_key = os.urandom(8)
    #use_reloader=False 옵션이 없으면  * Restarting with stat 메시지와 함께 이 파일을 한번 더 읽어 버림
    atraApp.run(debug=True, host="0.0.0.0", use_reloader=False)
    # atraApp_thread = Thread(target=atraApp.run, kwargs={"debug" : True, "host" : "0.0.0.0", "use_reloader" : False})

else:
    atraApp.secret_key = os.urandom(8)
    print("starting with HTTPS mode")
    # use_reloader=False 옵션이 없으면  * Restarting with stat 메시지와 함께 이 파일을 한번 더 읽어 버림
    #threaded 처리를 안하면 특정 요청에서 걸려버리고 장기간 대기할 수 있음
    atraApp.run(debug=True, host="0.0.0.0", ssl_context=(cert_file_name, privateKey_file_name),
                use_reloader=False, threaded=True)
    # atraApp_thread = Thread(target=atraApp.run, kwargs={"debug": True, "host": "0.0.0.0", "use_reloader": False,
    #                                                     "ssl_context" : (cert_file_name, privateKey_file_name)})






