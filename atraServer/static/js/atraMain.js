"use strict";
var atraBalanceTable = null;

// 여기에 exchangeList 갯수대로 데이터를 불러온다
var exchangeList = ["bithumb", "bitfinex","poloniex"];
var fiatList = ["KRW","USD","USDT"]
var dataTableObj = {}

var coinSumList = []
var amountSumList = []

var returnedExchangesBlances = null;

var atraMainEtcMethod = {
    //exchangeList 숫자에 맞는 table element 생성
    makeBalanceDataTable : function(){
        for(var i = 0 ; exchangeList.length > i ; i++) {
            $("#atraBalance").append('<h3>'+exchangeList[i]+'</h3><table id="balanceTable_' + exchangeList[i] +'"' +'class="balanceDisplay"  style="margin-left :0 ;width:70%">' +
                '<thead><tr></tr></thead></table>  <br>')
        }
        //sub 테이블 추가
        $("#atraBalance").append('<h2>SUM</h2><table id="balanceTable_sum"' +'class="balanceDisplay"  style="margin-left :0 ;width:70%">' +
                '<thead><tr></tr></thead></table>  <br>')
    },

    callAtraBalanceAjax : function callAtraBalanceAjax() {
        for (var i in exchangeList) {
            // console.log("this in call AtraBalanceA", this)
            var exchange = exchangeList[i]
            // console.log(exchange)
            $.ajax({
                url: "/atraBalance/data?" + exchange,
                dataType: "json",
                exchangeName : exchange, //->여기에 exchangeName을 선언해야 callback 함수에서 사용가능
                success: this.afterBalanceDataSuccess,

            })
        }
    },


    afterBalanceDataSuccess : function afterBalanceDataSuccess(response) {
                //this는 원래 $.ajax안의 url이 포함된 object가 되버림
                var exchange = this.exchangeName;
                // console.log(response, typeof(response) )
                // console.log(exchange)
                var innerData = response[exchange];
                // console.log(innerData)

                var tableHeadersList = [];
                var tableHeaders = '';
                var availableAmountList = [];
                var atraBalanceDate = new Date(parseInt(innerData["atra_balances_timestamp"] * 1000));
                var atraBalanceDateFormat = (atraBalanceDate.getMonth() + 1) + '/' + atraBalanceDate.getDate() + '/' + atraBalanceDate.getFullYear().toString().slice(-2) + " " +
                    ("0" + atraBalanceDate.getHours().toString()).slice(-2) + ":" + ("0" + atraBalanceDate.getMinutes()).slice(-2)  + ":" + ("0" + atraBalanceDate.getSeconds()).slice(-2)

                //내가 원하는 의도대로 배열  timestamp - fiat - alt... - BTC
                for (var key in innerData){
                    var val = innerData[key];

                    // console.log(key,val)
                    if(key === "atra_balances_timestamp") {
                        tableHeadersList.splice(0,0,key)
                        availableAmountList.splice(0,0,atraBalanceDateFormat)

                    }else if (fiatList.includes(key)){
                        // console.log("i found " + key)
                        if (tableHeadersList[0] == "atra_balances_timestamp"){
                            tableHeadersList.splice(1,0,key);
                            availableAmountList.splice(1,0,val.available);
                        }else{
                            tableHeadersList.splice(0,0,key)
                            availableAmountList.splice(0,0,val.available)
                        }

                    }else if (key == "BTC") {
                        if (tableHeadersList.length != 0){
                            tableHeadersList[tableHeadersList.length] = key
                            availableAmountList[availableAmountList.length] = val.available
                        }
                    }else {
                        if (tableHeadersList.length != 0) {
                            tableHeadersList.splice(-1, 0, key)
                            availableAmountList.splice(-1, 0, val.available)
                        } else {
                            tableHeadersList.push(key)
                            availableAmountList.push(val.available)
                        }

                    }

                }

                //각 exchangeBalanceTable에 있는 모든 것들을 중복없이 coinAllList에 넣고 나중에 dataTables Sum 에서 사용
                for (i in tableHeadersList){
                    if( tableHeadersList[i] != "atra_balances_timestamp") {
                        if (coinSumList.includes(tableHeadersList[i])) {

                        } else {
                            coinSumList.push(tableHeadersList[i])
                            amountSumList.push(0)
                        }
                    }
                }

                //각 exchangeBalanceTable에 있는 amount들을 coinSumList 순서에 맞춰서 넣고 나중에 dataTables Sum에서 사용
                for (key in innerData){
                    if( key != "atra_balances_timestamp") {
                        if (coinSumList.includes(key)) {
                            console.log(key, coinSumList.indexOf(key))
                            amountSumList[coinSumList.indexOf(key)] += innerData[key].available

                        }
                    }
                }

                console.log(coinSumList,amountSumList)
                for (var i = 0; tableHeadersList.length > i; i++){
                    tableHeaders += ("<th>" + tableHeadersList[i] + "</th>")
                }

                // console.log(tableHeaders)
                // console.log(availableAmountList)
                //DataTable에 들어가려면 list 구조가 되야 하니까 한번더 array 로 감쌈
                availableAmountList = [availableAmountList]
                $("table#balanceTable_" + exchange).find("tr").append(tableHeaders)
                setTimeout(function(){
                    $("table#balanceTable_" + exchange).DataTable({
                        data : availableAmountList,
                        paging : false,
                        searching: false,
                        ordering:  false,
                        buttons: ['copy'],
                        "drawCallback" : function (settings) {

                        }
                    })
                },1)

                var sumKeyList = []

            },

    drawSumBalanceTable : function (evt) {

        console.log(evt.target)
        var allExchangeTableCompleted = 0
        //table 다 됐는지 체크
        while (!(allExchangeTableCompleted == exchangeList.length)){

            for(var exchange in exchangeList){
                if ($("#balanceTable_" + exchangeList[exchange])[0] != undefined){
                    allExchangeTableCompleted += 1
                }

            }
        }
        console.log("all exchange Table completed")

        var sumtableHeaders = ''
        for (var i = 0; coinSumList.length > i; i++){
                    sumtableHeaders += ("<th>" + coinSumList[i] + "</th>")
                }

        console.log(sumtableHeaders)
        console.log(amountSumList)
        //DataTable에 들어가려면 list 구조가 되야 하니까 한번더 array 로 감쌈
        amountSumList = [amountSumList]
        $("table#balanceTable_sum").find("tr").append(sumtableHeaders)
        setTimeout(function(){
            $("table#balanceTable_sum").DataTable({
                data : amountSumList,
                paging : false,
                searching: false,
                ordering:  false,
                "drawCallback" : function (settings) {

                }
            })
        },50)


    }
}
$(document).ready(function() {
    atraMainEtcMethod.makeBalanceDataTable()

    atraMainEtcMethod.callAtraBalanceAjax()


    // eventhandlers
    $("input#reset").on("click", function (evt) {
        console.log(exchangeList)
        //table로서 존재하는 게 true가 아니라면 다시 만듬

        var $balanceDiv = $("#atraBalance")
        $balanceDiv.empty()
        amountSumList = []
        coinSumList = []
        atraMainEtcMethod.makeBalanceDataTable()
        atraMainEtcMethod.callAtraBalanceAjax()
    })

    console.log("table#balanceTable_" + exchangeList[exchangeList.length - 1])

    // $("#atraBalance").on("draw.dt","#balanceTable_poloniex",function (evt) {
    //     console.log(evt.target)
    // })

    //exchageList대로 모든 데이터 테이블들이 draw 되면 gross 계산해서 만듬 delegate 되게 하기 위해 일단 항상 존재하는 #atraBalance
    //Div에다 이벤트를 걸어두고  추후에 생길  exchange별 테이블을 두번째 parameter에 넣어둠
    $("#atraBalance").on("draw.dt","#balanceTable_"+exchangeList[exchangeList.length - 1],atraMainEtcMethod.drawSumBalanceTable)


})