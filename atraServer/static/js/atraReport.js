//atraWatcher 페이지와 atraMain에서 공통적으로 호출한뒤 title 명으로 각각 다른 atra_REPORT를 호출한다
var atraReportData = null

var atraReportEtcMethod = {
    callAtraBalanceAjax : function callAtraBalanceAjax(){

    },
}
$(document).ready(function() {
    var $reportDataDiv = $("span#atraReportData");
    var titleVal = $("title").text()
    var reportMode = titleVal == "atraMain" ? "atraMain" : "atraWatcher";

    $.ajax({
        url: "/report?reportMode=" + reportMode,
        dataType: "json",
        success: function(response){
            // strResponse = $.json.stringify(response)
            // console.log(response)
            atraReportData = response
            // console.log(atraReportData)
            var additionalText = ""
            additionalText += "atra_mode : " + atraReportData["start_mode"]
            additionalText += "  atraBot_clientName : " + atraReportData["atraBot_clientName"]
            $reportDataDiv.append(additionalText + "<br>")
            for (dataName in atraReportData){
                if(dataName != "start_mode" && dataName != "atraBot_clientName") {
                    // console.log(dataName)
                    eachReportObj = atraReportData[dataName]
                    $reportDataDiv.append("<span id='" + dataName + "' ></span>")
                    $("span#" + dataName).JSONView(eachReportObj)
                    $("span#" + dataName).prepend(dataName)
                    $("span#" + dataName).JSONView("toggle")
                }
            }
            // $("div.jsonview").css("display","inline")

            // $reportDataDiv.JSONView(atraReportData,{"collapsed" : true})

        }
    })

})