"use strict";
var atraCycleTable = null;
var atraCycleEtcMethod = {
    outputPreperformed  : function output(inp) {
    },
}

$(document).ready(function(){
    // alert("asdfasdf");
    var atraCycleTable = $("#atraCycleTable").DataTable({
        serverSide: true,
        processing : true,
        ajax : {url : "/atraCycle/data", type : "POST"},
        columns: [
            //render 가 없다면 data 가 모든 역할을 하게되고(display, sort, export etc) render 가 있다면
            //각각(display, sort, etc)이 data Source에서 무엇인지 명시하거나  function으로 분기 시킨다

            //child 버튼  //->추가적으로 png 파일을 다운받고 css 추가해서 + 아이콘 넣는 작업 하면 아이콘을 추가할 수 있음
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            //시간을  mm/dd/yy hh:mm 으로 바꿈  나머지는 추후에 결정
            { "data": "_id.$oid" , "render": function (data, type, row) {
                //->  만약 테이블에서 해당컬럼을 정렬시킨다면 type이 "sort"로 callback 함수를 호출시킨다
                //->일반상황에서는 type이 "display"로 넘어온다
                // console.log(data, type);
                var oid = row._id.$oid;
                var timestamp = oid.toString().substring(0,8);
                //여러곳에서 쓰면 etcMethod에서 분기 시킬것
                var atraCycleDate = new Date( parseInt( timestamp, 16 ) * 1000 );
                var atraCycleDateFormat = (atraCycleDate.getMonth() + 1) + '/' + atraCycleDate.getDate() + '/' +  atraCycleDate.getFullYear().toString().slice(-2) +" " +
                    ("0" + atraCycleDate.getHours().toString()).slice(-2) +":" + ("0" + atraCycleDate.getMinutes()).slice(-2)
                // atraCycleDateFormat = atraCycleDate.toLocaleString('en-GB')
                // console.log(timestamp, atraCycleDateFormat);
                return atraCycleDateFormat
            } },
            { "data": "atraCycle_initial_info.exchangePairing_name" },
            //->earningRate와 profit을  actual/pred 로 표시
            { "data": "" , "render": function (data, type, row) {
                //기본적으로 atraCycle Document가 생겼다는말은 initial_info가 다 있다는 의미임
                var ioExchangeMoneyRole = row.atraCycle_initial_info.ioExchange_moneyRole;
                var predictive_earningRate = row.atraCycle_initial_info.earning_rate;
                var predictive_profit= row.atraCycle_initial_info.profit;

                var predictiveSentence = predictive_profit.toString() + ioExchangeMoneyRole + "(" + predictive_earningRate.toString() + ")";

                //atraCycle_finalized_info 가 없는 document가 있을수 있으므로
                if (row.hasOwnProperty("atraCycle_finalized_info")){
                    var actual_earningRate = row.atraCycle_finalized_info.actual_earningRate == null ? "null" : row.atraCycle_finalized_info.actual_earningRate;
                    var actual_profit= row.atraCycle_finalized_info.actual_profit == null ? "null" : row.atraCycle_finalized_info.actual_profit.toFixed(4);
                }else{
                    var actual_earningRate = "undefined";
                    var actual_profit= "undefined";
                }
                var actualSentence = actual_profit.toString() + ioExchangeMoneyRole + "(" + actual_earningRate.toString() + ")"
                return actualSentence + "/<br>" + predictiveSentence
            }},
            { "data": "atraCycle_initial_info.inCoin" },
            { "data": "atraCycle_initial_info.outCoin" },
            //eExchange_obj trading 결과 표시
            { "data": "trade_process_info.eExchange_trade.is_succeeded","render": function (data, type, row) {
                // eExchange의 trading 교환비를 actual / predictive로 나누어서 보여준다
                // console.log(row, data)
                var is_trading_succeeded = data
                var inCoin = row.atraCycle_initial_info.inCoin
                var outCoin = row.atraCycle_initial_info.outCoin
                var predictiveInCoinTransferredAmount= row.trade_process_info.eExchange_trade.predictive.predictive_inCoin_transferredAmount;
                var predictiveOutCoinTradedAmount = row.trade_process_info.eExchange_trade.predictive.predictive_outCoin_tradedAmount_withFee;
                //예전이름 바뀐게 있어서 actual_inCoinAmount_forEExchangeTrade  도 남겨둠
                var actualInCoinAmount = row.trade_process_info.eExchange_trade.actual.actual_inCoinAmount_forEExchangeTrade ||
                    row.trade_process_info.eExchange_trade.actual.actual_usedInCoinAmount_afterEExchangeTrade;
                var actualOutCoinAmount = row.trade_process_info.eExchange_trade.actual.actual_outCoinAmount_afterEExchangeTrade_withFee;
                // console.log(predictiveInCoinTransferredAmount, predictiveOutCoinTradedAmount, actualInCoinAmount, actualOutCoinAmount)
                // console.log(typeof(predictiveInCoinTransferredAmount),typeof(predictiveOutCoinTradedAmount),typeof(actualInCoinAmount),typeof(actualOutCoinAmount)  )
                predictiveInCoinTransferredAmount = typeof(predictiveInCoinTransferredAmount) == "number" ? predictiveInCoinTransferredAmount.toFixed(4) : predictiveInCoinTransferredAmount;
                predictiveOutCoinTradedAmount = typeof(predictiveOutCoinTradedAmount) == "number" ? predictiveOutCoinTradedAmount.toFixed(4) : predictiveOutCoinTradedAmount;
                actualInCoinAmount = typeof(actualInCoinAmount) == "number" ? actualInCoinAmount.toFixed(4) : actualInCoinAmount;
                actualOutCoinAmount = typeof(actualOutCoinAmount) == "number" ? actualOutCoinAmount.toFixed(4) : actualOutCoinAmount;

                var predictiveSentence = "" + predictiveOutCoinTradedAmount +" : " + predictiveInCoinTransferredAmount
                var actualSentence = "" + actualOutCoinAmount+ outCoin + " : " + actualInCoinAmount + inCoin

                return actualSentence + "/<br>" + predictiveSentence
            }
            },
            //->ioExchange의 결과  TRIANGULAR일때와 DIRECT일때를 구분할것  TRIANGULAR일때는 ---------- 필요
            { "data": null,"render": function (data, type, row) {
                // console.log(data,type,row);
                var ioExchangeMoneyRole = row.atraCycle_initial_info.ioExchange_moneyRole
                var inCoin = row.atraCycle_initial_info.inCoin
                var outCoin = row.atraCycle_initial_info.outCoin
                //in out 이 아닌 ioExchange_trade가 있을경우 TRIANGULAR 가 아니라  DIRECT atra임
                if ("ioExchange_trade" in row.trade_process_info){
                    return
                }else{
                    //TRIANGULAR인 상황임
                    var ioInTradeInfo= row.trade_process_info.ioExchange_inTrade;
                    var ioOutTradeInfo= row.trade_process_info.ioExchange_outTrade;
                    var isIOExchangeOutTradeSucceeded = ioOutTradeInfo.is_succeeded;
                    var isIOExchangeInTradeSucceeded = ioInTradeInfo.is_succeeded;

                    //eExchange의 predictive의 경우 미리 atraCnp 에서 계산된 값(eExchange_obj 내에서 in out 모두)을 따지나
                    //ioExchange의경우  predictive outcoin양과  inCoin의 양만을 따진다다(ioExchange의 moneyRole Total은 actual만 따진다)
                    var predictiveOutCoinAmount = ioOutTradeInfo.predictive.predictive_outCoin_orderAmount_ioExchangeTrade;
                    var actualOutCoinAmount = ioOutTradeInfo.actual.actual_outCoinAmount_IOExchangeTrade || ioOutTradeInfo.actual.actual_usedOutCoinAmount_afterIOExchangeTrade;
                    var actualMoneyRoleSellTotal = ioOutTradeInfo.actual.actual_moneyRoleSellTotal_ioExchangeTrade;

                    var predictiveOutTradeSentence  = predictiveOutCoinAmount;
                    var actualOutTradeSentence = "" + actualMoneyRoleSellTotal + ioExchangeMoneyRole + " : " + actualOutCoinAmount + outCoin;
                    var outTradeSentence = actualOutTradeSentence + "/<br>" + predictiveOutTradeSentence;


                    var predictiveInCoinAmount = ioInTradeInfo.predictive.predictive_inCoin_orderAmount_ioExchangeTrade;
                    var actualInCoinAmount = ioInTradeInfo.actual.actual_inCoinAmount_IOExchangeTrade || ioInTradeInfo.actual.actual_inCoinAmount_afterIOExchangeTrade_withFee;
                    var actualMoneyRoleBuyTotal = ioInTradeInfo.actual.actual_moneyRoleBuyTotal_ioExchangeTrade;

                    var predictiveInTradeSentence  = predictiveInCoinAmount;
                    var actualInTradeSentence = "" + actualMoneyRoleBuyTotal+ ioExchangeMoneyRole + " : " + actualInCoinAmount + inCoin;
                    var inTradeSentence = actualInTradeSentence + "/<br>" + predictiveInTradeSentence;

                    return outTradeSentence + "<br>-----------<br>" + inTradeSentence;
                }

            }
            },
            //fromEExchangeTransfer
            { "data": "transfer_process_info.from_eExchange" , "render": function (data, type, row) {
                var fromEExchangeTransferInfo = row.transfer_process_info.from_eExchange;
                var isTransferSucceded = fromEExchangeTransferInfo.is_succeeded;
                var fromEExchangeSentence = "";
                var willBeReceivedAmount = "";
                //예전 포맷 버전의 경우 is_succeed = false 가 없고(성공못했으면 python의 None값임 == javascrip의 null ) toExchange toExchangeAddress 등이 없다.
                if (isTransferSucceded == null || isTransferSucceded == false) {
                    fromEExchangeSentence += isTransferSucceded ;

                } else {
                //성공했을 경우
                    if (fromEExchangeTransferInfo.skipped) {
                        var willBeReceivedAmount = "skipped";
                    } else {
                        var willBeReceivedAmount = fromEExchangeTransferInfo.willBe_deposited_amountOn_TOEXCHANGE;
                    }

                }
                fromEExchangeSentence = isTransferSucceded + '<br>' + willBeReceivedAmount
                return fromEExchangeSentence
            } },
            //fromIOExchangeTransfer
            { "data": "transfer_process_info.from_ioExchange" , "render": function (data, type, row) {
                var fromIOExchangeTransferInfo = row.transfer_process_info.from_ioExchange;
                var isTransferSucceded = fromIOExchangeTransferInfo.is_succeeded;
                var fromIOExchangeSentence = "";
                //예전 포맷 버전의 경우 is_succeed = false 가 없고(성공못했으면 python의 None값임 == javascrip의 null ) toExchange toExchangeAddress 등이 없다.
                if (isTransferSucceded == null || isTransferSucceded == false){
                    fromIOExchangeSentence += isTransferSucceded;
                }else{
                    //성공했을 경우
                    if (fromIOExchangeTransferInfo.skipped){
                        var willBeReceivedAmount = "skipped"
                    }else{
                        var willBeReceivedAmount = fromIOExchangeTransferInfo.willBe_deposited_amountOn_TOEXCHANGE;
                    }
                    fromIOExchangeSentence = isTransferSucceded + '<br>' + willBeReceivedAmount;
                }
                return fromIOExchangeSentence
            } },
        ],
         "order" : [[1,'desc']]
    })

    //child row 열고 닫기 컨트롤
    $("#atraCycleTable").on("click","tbody td.details-control",function(evt){
        // callback 내부에서 바인딩된 this는 evt.target의 element 객체임
        var target =evt.target;
        //child row 호출한 버튼의 tr node 불러옴
        var tr = $(target).closest("tr");
        //해당 tr이 들어간 dataTables 객체
        var row = atraCycleTable.row(tr)

        // console.log(row)

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            //beautify 개념임 dataTables가 json으로 받아와서 object로 row객에에 저장된 것을 string으로 변형시키면서 beautify식으로 변형
            var stringifiedRowData = JSON.stringify(row.data(),undefined,4);
            var preFied = '<pre>' + stringifiedRowData + '</pre>';

            // Open this row
            row.child(preFied).show();
            tr.addClass('shown');
        }
    });
});
