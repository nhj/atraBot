var atraWatcherTable = null;

var displayLength  = 1000;

//->amChart를 그릴때 사용하는 데이터를 atraPairingObj의 key명으로 저장시킨다
var parsedResponsesOriginal = {};
var parsedResponsesFlattened = {};

var amChartsEtcFunc = {
    //amCharts가 nested 구조의 데이터에 접근하지 못하므로(cnp_data[whenInCoin][commodiry_role] 여러depth안에 원하는 값이 들어가 있음)
    // 개별 데이터 depth를 1로 만듬
    flattenObject : function(ob) {
        var toReturn = {};
        for (var i in ob) {
            if (!ob.hasOwnProperty(i)) continue;
            if ((typeof ob[i]) == 'object') {
                var flatObject = this.flattenObject(ob[i]);
                for (var x in flatObject) {
                    if (!flatObject.hasOwnProperty(x)) continue;
                    toReturn[i + '.' + x] = flatObject[x];
                }
            } else {
                toReturn[i] = ob[i];
            }
        }
        return toReturn;
    },

    makeCommodityRoleCoinsByDirection : function(pairingCnpData){
        //-> parsedResponse 중 가장 최근 Obj로 commodityRole array를 만들어냄
        //형식은 {"whenInCoin" : [cois~], "whenOutCoin" : [cois~] } 가 됨
        var mostRecentData = pairingCnpData[pairingCnpData.length - 1]
        var commodityRoleCoinsByDirection = {"whenInCoin" :[], "whenOutCoin" : []}

        //whenInCoin outCoin으로 나눠서 찾아냄
        for (var direction in mostRecentData.cnp_data){
            console.log(direction)
            for (var coin in mostRecentData.cnp_data[direction]){
                if (commodityRoleCoinsByDirection[direction].includes(coin) == false){
                    commodityRoleCoinsByDirection[direction].push(coin)
                }
            }
        }
//                console.log(commodityRoleCoinsByDirection)
        return commodityRoleCoinsByDirection
    },

    makeAmChartsGraphObjArrayByDirection : function(pairingObjName, commodityCoins){
        var amChartsGraphObjArrayWhenInCoin = [];
        var amChartsGraphObjArrayWhenOutCoin = [];
        var amChartsGraphObjArrayByDirection = {"whenInCoin" : [], "whenOutCoin" : []};
        var baseCoinName = parsedResponsesOriginal[pairingObjName][0].base_coin

        // console.log(commodityCoins);

        for (var direction in commodityCoins) {

            for (var i in commodityCoins[direction]) {
                var commodityCoinName = commodityCoins[direction][i];
                var amChartsGraphObj = {
                    "id": "g" + i,
                    "balloonText": "<b><span style='font-size:4px;'>"+commodityCoinName +": [[value]]</span></b>",
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "hideBulletsCount": 50, //-> zoom out이 어느정도 되면 bullet이 사라짐
                    "title": commodityCoinName + baseCoinName,
                    "valueField": "cnp_data."+ direction +"." + commodityCoinName + ".earning_rate",
                    "useLineColorForBulletBorder": true
                }
                amChartsGraphObjArrayByDirection[direction].push(amChartsGraphObj)
            }
        }

        return amChartsGraphObjArrayByDirection;
    },

    getAtraWatcherChartsDataAndDraw : function(evt){
        //-> form submit event로 호출 되었으므로 페이지 전환 막음
        evt.preventDefault()
        var form = evt.target
        var displayLength = form.elements["displayLength"].value
        var pairingObjName = form.elements["pairingObjName"].value

        console.log(displayLength,pairingObjName)


        // var pairingObjName = evt.target.;

         $.ajax({
            dataType : "json",
            url: "/amchartsTest/data?pairingObjName=" +  pairingObjName + "&displayLength=" + displayLength,
            exchangeName : pairingObjName, //->여기에 paringObjName을 선언해야 callback 함수에서 사용가능
            beforeSend : function(evt){
               //loader 이미지 불러옴
               var pairingObjName = this.exchangeName;
               $("span#loaderImg_"+pairingObjName).show()
            } ,
            success: function(response){

                var pairingObjName = this.exchangeName;
    //                    var parsedResponse = JSON.parse(response);
                //loader이미지 없앰
                $("span#loaderImg_"+pairingObjName).hide()
                var parsedResponse = response


                if ((pairingObjName in parsedResponsesOriginal) == false){
                    parsedResponsesOriginal[pairingObjName] = []
                    parsedResponsesFlattened[pairingObjName] = []
                }
                //flatten 하기 전 original을 만들어 둠
                parsedResponsesOriginal[pairingObjName] = $.extend([],parsedResponse)
                console.log(parsedResponsesOriginal)

                for (var i= 0;i < parsedResponse.length; i++){
                    //amcharts에서 object nested구조에 접근을 못하므로 모두 첫단계로 오게 만들어 줌.
                    parsedResponsesFlattened[pairingObjName][i] = amChartsEtcFunc.flattenObject(parsedResponse[i]);
                    // parsedResponse[i]["atraCnp_timestamp"] = new Date(parseInt(parsedResponse[i]["atraCnp_timestamp"]) * 1000)
                    parsedResponsesFlattened[pairingObjName][i]["atraCnp_timestamp"] = parseInt(parsedResponse[i]["atraCnp_timestamp"]) * 1000

                }
                console.log(parsedResponsesFlattened);

                //최신 obj를 보내줘서 그 기준으로 coinRoleList를 만들게 한다
                var commodiryRoleCoinsByDirection= amChartsEtcFunc.makeCommodityRoleCoinsByDirection(parsedResponsesOriginal[pairingObjName])

                var graphObjArrayByDirection = amChartsEtcFunc.makeAmChartsGraphObjArrayByDirection(pairingObjName, commodiryRoleCoinsByDirection)


                // zoomChart();
                // // this method is called when chart is first inited as we listen for "rendered" event
                // function zoomChart() {
                //     // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
                //     chart.zoomToIndexes(parsedResponse.length - 40, parsedResponse.length - 1);
                //
                // }
                for (var direction in graphObjArrayByDirection){
                    console.log(direction, graphObjArrayByDirection)
                    var chart = AmCharts.makeChart("chartdiv_" + direction + "_" + pairingObjName , {
                        "titles" : [
                            {"text" : direction}
                        ],
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": parsedResponsesFlattened[pairingObjName],
                        "mouseWheelZoomEnabled": true,
                        "pathToImages" : "https://www.amcharts.com/lib/3/images/",

                        "graphs" : graphObjArrayByDirection[direction],
                        "chartScrollbar": {
                            "autoGridCount": true,
                            "graph": "g1",
                            "scrollbarHeight": 40
                        },
                        "chartCursor": {
                            "cursorPosition": "mouse"
                        },
                        //->x축 설정
                        "categoryField": "atraCnp_timestamp",
                        "categoryAxis": {
                            "parseDates": true,
                            "minPeriod" : "ss",
                            "axisColor": "#DADADA",
                            "dashLength": 1,
                            "minorGridEnabled": true,
                            "labelRotation" : 45
                        },
                        //-> chart Object로 만들때는 chart.valueAxis = {} 로 접근해서 만들지만 JSON으로 만들때는
                        //valueAxes [{},{}] 식으로 만듬
                        "valueAxes" : [{
                            "autoGridCount" : true,
                            // "minimum" : -0.02,
                            // "maximum" : 0.01,
                            "inside" : true,
                            "axisAlpha" : 0

                        }],

                        "legend": {
                            //->graph에 설정해 둔 title이  legend명으로 들어간다.
                            "useGraphSettings": true
                        }

                    });
                }

            }

        })
    }
}

$(document).ready(function(){
    // alert("asdfasdf");
    atraWatcherTable = $("#atraWatcherTable").DataTable({
        ajax : {url : "/atraWatcher/overallData",
                type : "GET",
                // dataSrc : function(d){  //-> ajax success callback을 override 하지말고 이걸로 해야함
                //     console.log(d)
                // }
                dataSrc : ""  //=> [{}{}{}] 이런식으로 오면 dataTables가 온 그대로 dataTables로 사용하도록 "" 지정해줌
        },
        columns: [
            //render 가 없다면 data 가 모든 역할을 하게되고(display, sort, export etc) render 가 있다면
            //각각(display, sort, etc)이 data Source에서 무엇인지 명시하거나  function으로 분기 시킨다
            //child 버튼  //->추가적으로 png 파일을 다운받고 css 추가해서 + 아이콘 넣는 작업 하면 아이콘을 추가할 수 있음
            {
                "className": 'details-control',  //->이 내용대로 해당 html element에 class명이 정해짐
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "pairingObj_name" },
            //시간을  mm/dd/yy hh:mm 으로 바꿈  나머지는 추후에 결정
            { "data": "whole_period" , "render": function (data, type, row) {
                //->  만약 테이블에서 해당컬럼을 정렬시킨다면 type이 "sort"로 callback 함수를 호출시킨다
                //->일반상황에서는 type이 "display"로 넘어온다  (type은 sort나 display 등등이 있다)
                // console.log(data, type);
                var oldestTimestamp = data.oldestDate * 1000;
                var newestTimestamp = data.newestDate * 1000;

                var oldestDate = new Date(oldestTimestamp);
                var newestDate = new Date(newestTimestamp );

                var oldestDateFormat = (oldestDate.getMonth() + 1) + '/' + oldestDate.getDate() + '/' +  oldestDate.getFullYear().toString().slice(-2) +" " +
                    ("0" + oldestDate.getHours().toString()).slice(-2) +":" + ("0" + oldestDate.getMinutes()).slice(-2)
                var newestDateFormat = (newestDate.getMonth() + 1) + '/' + newestDate.getDate() + '/' +  newestDate.getFullYear().toString().slice(-2) +" " +
                    ("0" + newestDate.getHours().toString()).slice(-2) +":" + ("0" + newestDate.getMinutes()).slice(-2)
                // // atraCycleDateFormat = atraCycleDate.toLocaleString('en-GB')
                // // console.log(timestamp, atraCycleDateFormat);
                var wholePeriodFormat  = oldestDateFormat + " - " + newestDateFormat
                return wholePeriodFormat
            } },
            {"data": "mostRecentObj_commodityRoleCoins"}
        ],
         "order" : [[1,'desc']]
    })

    atraWatcherTable.on( 'draw', function () {
        atraWatcherTable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            console.log(rowIdx,tableLoop, rowLoop);
            //-> 부모row의 pairingObj_name을 class명으로 div를 만든다
            var pairingObjName = atraWatcherTable.row(rowIdx).data().pairingObj_name
            var atraType = atraWatcherTable.row(rowIdx).data().atra_type
            console.log(pairingObjName)


            this.child(
                    $(
                        '<h3 style="vertical-align: middle">' +pairingObjName +' ('+ atraType +') Charts</h3>' +
                        // '<div>displayLength<input type="text" value="1000">&nbsp&nbsp<button class="chartRequest">'+pairingObjName+'</button></form> ' +
                        '<form id="requestChartForm">' +
                            '<label for="displayLength">displayLength</label> <input type="text" name="displayLength" value="1000">&nbsp&nbsp' +
                            '<input type="hidden" name="pairingObjName" value="'+pairingObjName +'">&nbsp&nbsp' +
                            '<input type="submit" name="submit" class="chartRequest"> <span id="loaderImg_'+pairingObjName +'" class="loaderImg" style="display: none;"><img src="/static/js/ajax-loader.gif"></span>'+
                        '</form>' +
                        '<div id="chartdiv_whenInCoin_'+pairingObjName +'" style="width: 80%;height: 550px"></div>' +
                        '<div id="chartdiv_whenOutCoin_'+pairingObjName +'"style="width: 80%;height: 550px"></div>'
                    )
                ).show();


        });
            //child.show() 한 상태에서 해야함
        $("form#requestChartForm").submit(amChartsEtcFunc.getAtraWatcherChartsDataAndDraw)

        atraWatcherTable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        //child()를  호출하면 child(내용).show() 로 만든 내용이 모두 사라져 버림 따라서 child.hide() 로 닫아줌
           this.child.hide()
        });



    });

    console.log("child buttonEvent starts")
    //child row 열고 닫기 컨트롤
    $("#atraWatcherTable").on("click","tbody td.details-control",function(evt){
        console.log(evt.target)
        // callback 내부에서 바인딩된 this는 evt.target의 element 객체임
        var target =evt.target;
        //child row 호출한 버튼의 tr node element 불러옴
        var tr = $(target).closest("tr");
        //해당 tr이 들어간 dataTables 객체
        var row = atraWatcherTable.row(tr)
        console.log(row.child.isShown());
        // console.log(row)

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child.show()
            tr.addClass('shown');
        }
    });

});
