#-*- coding:utf-8 -*-
from flask_pymongo import PyMongo
import pymongo, time
from atraServer import atraApp
from processDataTablesServerSide import ProcessDataTablesServerSide
from bson import json_util, ObjectId
import json


atraModel = None
db_val = {"atraCycle": {"host": "150.95.179.9", "port": "27017","DB_name" : "atra","collection" : "atraCycle"},
            "atraCnp": {"host": "150.95.179.9", "port": "27017","DB_name" : "atraCnp", "collection" : ""},
            "atraBalance": {"host": "150.95.179.9", "port" : "27017","DB_name" : "atra" ,"collection" : "atraBalance"},
            "atraUser": {"host": "150.95.179.9", "port" : "27017","DB_name" : "atra" ,"collection" : "atraUser"}\
            }

class AtraModel:

    def __init__(self, host="192.168.0.1", port=27017):
        self.db_client_dict = {}
        self.host = host
        self.port = port

    def checkDB(self, collection_name, db_name):
        #mongo를 사용하는 것들중 DB사용여부를 check 하는 요청을 model에서 공통으로 묶어 관리 mongodb 작동이 안되면 fail
        db_client = self.getDBClient(db_name)

        DB = db_client.db
        #atraCnp는 collection을 요청에 따라 onlyExchangeName_pairing으로 지정하므로 따로 검증
        if db_name == "atraCnp":
            if DB != None:
                return "atraCnp"
            else:
                return "failure"
        else :

            collection_count = DB[collection_name].count()
            collection_fullName = DB[collection_name].full_name
            print(type(collection_count), collection_fullName)
            if type(collection_count) == int and collection_fullName == db_name + "." + collection_name:
                return collection_fullName
            else:
                return "failure"

    def getDBClient(self, db_name):
        if not self.db_client_dict.has_key(db_name):
            db_client = self.setDBClient(db_name)
        else:
            db_client = self.db_client_dict[db_name]
        return db_client

    def setDBClient(self,db_name):
        '''flask_pymongo에서는 flask app 객체의 config 설정값으로  mongodb client를 생성할 수 있으므로
               mongoDB client 객체를 생성해서 controller를 사용한다
               db_name별로  다른 client 를 만들어서 db_client_dict에 저장시킴'''
        if self.db_client_dict.has_key(db_name):
            return
        configuable_hostName = db_name + "_HOST"
        configuable_portName = db_name + "_PORT"
        configuable_dbName = db_name + "_DBNAME"

        # flask app.cofing 에 설정해두면 Flask와 연결된 pyMongo 가 각각 prefix 이름에 따라 각각 따로 DB client
        # 를 생서할 수 있음 DB name으로 각각 다른 DB client 를 관리
        atraApp.config[configuable_hostName] = self.host
        atraApp.config[configuable_portName] = self.port
        atraApp.config[configuable_dbName] = db_name
        client = PyMongo(atraApp, config_prefix=db_name)
        # 두번째부터 dict 로 접근할 수 있게
        self.db_client_dict[db_name] = client
        DB = client.db
        # todo 나중에 암호처리 할 것
        DB.authenticate("atraDB", "zenchy0123")

        return client

def readAtraCycle(request):
    db_name = db_val["atraCycle"]["DB_name"]
    collection_name = db_val["atraCycle"]["collection"]

    if request.method == "GET" and "check" in request.args:
        checked_data = atraModel.checkDB(collection_name,db_name)
        return checked_data

    db_client = atraModel.getDBClient(db_name)

    #client측 DataTables 에서 자동으로 넘어오는 requset 변수들을 받아서 넘겨줌

    #만약 server단에서 db내용의 필드값을 선별해서 보내어 준다면
    columns = ['client_name', 'baseCycleinfo', 'column_3', 'column_4']
    index_column = "_id"
    result = ProcessDataTablesServerSide(request, columns, index_column, collection_name,db_client).output_result()

    #mongo document마다 있는  _id 는 pymongo 객체로 뽑아내면 ObjectId라는 객체로 인식되기 때문에
    #이를 json으로 바꾸려면 처리를 해줘야 함 일반 json.dumps 로 하면 안되고 json_util.dumps로 해 줘야함
    sanitized_result = json_util.dumps(result)
    return sanitized_result

def writeAtraCycle(data):
    db_name = db_val["atraCycle"]["DB_name"]
    collection_name = db_val["atraCycle"]["collection"]
    # print(type(data), data)
    #atraBalance일띠는 아닌데
    db_client = atraModel.getDBClient(db_name)
    collection_obj = db_client.db[collection_name]

    result = collection_obj.insert_one(data).inserted_id
    # inserted_id가 Object id 객체로 받아지기 때문에 str화 함
    result = str(result)

    return result



def readAtraBalance(request, exchange_name):
    """
    :param request:
    :param exchange_name: latest balance Data will be selected by this exchange name
    :return:
    """
    print("model balanceData")
    db_name = db_val["atraBalance"]["DB_name"]
    collection_name = db_val["atraBalance"]["collection"]

    if request.method == "GET" and "check" in request.args:
        checked_data = atraModel.checkDB(collection_name,db_name)
        return checked_data

    db_client = atraModel.getDBClient(db_name)

    collection_obj = db_client.db[collection_name]

    _filter = {}
    _filter[exchange_name] = {"$exists": True, "$ne": "null"}
    raw_result_data = collection_obj.find(limit=1, filter=_filter).sort("_id", pymongo.DESCENDING)
    result_data = list(raw_result_data)
    # print(result_data,type(result_data))
    # print(result_data[0])
    del result_data[0]["_id"]

    return result_data[0]

def writeAtraBalance(data):
    db_name = db_val["atraBalance"]["DB_name"]
    collection_name = db_val["atraBalance"]["collection"]
    db_client = atraModel.getDBClient(db_name)
    collection_obj = db_client.db[collection_name]

    result = collection_obj.insert_one(data).inserted_id
    #inserted_id가 Object id 객체로 받아지기 때문에 str화 함
    result = str(result)
    return result


def readAtraCnp(request):
    """
    :param request:
    :param exchange_name: latest cnp Data will be selected by this exchange name
    :return:
    """
    print("model cnpData")
    db_name = db_val["atraCnp"]["DB_name"]
    collection_name = db_val["atraCnp"]["collection"]

    if request.method == "GET" and "check" in request.args:
        checked_data = atraModel.checkDB(collection_name,db_name)
        return checked_data

    db_client = atraModel.getDBClient(db_name)

    collection_obj = db_client.db[collection_name]

    raw_result_data = collection_obj.find(limit=1, filter=_filter).sort("_id", pymongo.DESCENDING)
    result_data = list(raw_result_data)
    # print(result_data,type(result_data))
    # print(result_data[0])
    del result_data[0]["_id"]

    return result_data[0]

def writeAtraCnp(data):
    db_name = db_val["atraCnp"]["DB_name"]
    # collection_name = db_val["atraCnp"]["colection"]
    #atraCnp 는 atraCnp  DB안에 onlyExchangeName_pairing(ex. bithumb__bifinex) collection을 두고 그 안에 atraCnp 내용을저장
    collection_name = onlyExchangeName_pairing = data["exchangePairing_name"]
    db_client = atraModel.getDBClient(db_name)
    collection_obj = db_client.db[collection_name]
    # print(onlyExchangeName_pairing)
    result = collection_obj.insert_one(data).inserted_id
    #inserted_id가 Object id 객체로 받아지기 때문에 str화 함
    result = str(result)
    return result



def readUserData(user_name):
    db_name = db_val["atraUser"]["DB_name"]
    collection_name = db_val["atraUser"]["collection"]
    db_client = atraModel.getDBClient(db_name)
    collection_obj = db_client.db[collection_name]

    existing_user = collection_obj.find_one({'userId': user_name})
    print("existing", existing_user)
    return existing_user


def writeUserData(user_name,hashed_pass):
    db_name = db_val["atraUser"]["DB_name"]
    collection_name = db_val["atraUser"]["collection"]
    db_client = atraModel.getDBClient(db_name)
    collection_obj = db_client.db[collection_name]
    collection_obj.insert({'userId': user_name, 'password': hashed_pass})

    return collection_obj

#atraWatcher page관련로직
def readCollectionList(db_name):
    """retrieve all collection names as a list in specific DB"""
    db_client = atraModel.getDBClient(db_name)
    collectionList = [collection for collection in db_client.db.collection_names(include_system_collections=False)]
    return collectionList

def readOldestNewestDateAndCoinList(pairingObj_name):
    db_client = atraModel.getDBClient("atraCnp")
    # atraCnp는 데이터 수가 많으므로 query 시간을 단축하기 위해 pairingObjName이 곧 collection임
    collection_name = pairingObj_name
    collection_obj = db_client.db[collection_name]
    # timestamp를 오늘차순으로 정렬해서 가장 위에꺼(oldest)하나\  //->데이터가 많으면 느림 $natural로 변경
    # oldestObj = list(collection_obj.find().sort([("atraCnp_timestamp", 1)]).limit(1))[0]
    oldestObj = list(collection_obj.find().limit(1).sort("$natural", 1))[0]

    # timestamp를 내림차순으로 정렬해서 가장 위에꺼(newest)하나 //->데이터가 많으면 느림 $natural로 변경
    newestObj = list(collection_obj.find().limit(1).sort("$natural", -1))[0]
    # newestObj = list(collection_obj.find().sort([("atraCnp_timestamp", -1)]).limit(1))[0]
    oldestDate = oldestObj["atraCnp_timestamp"]
    newestDate = newestObj["atraCnp_timestamp"]
    atra_type = newestObj["atra_type"]
    commodityRoleCoin_list = [coin for coin in newestObj["cnp_data"]["whenInCoin"]]
    print(commodityRoleCoin_list)

    return {"oldestDate" : oldestDate, "newestDate" : newestDate}, commodityRoleCoin_list, atra_type
#atraDB import 될때 자동으로 객체 생성
atraModel = AtraModel()

