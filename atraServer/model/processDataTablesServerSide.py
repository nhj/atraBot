#-*- coding:utf-8 -*-
from collections import namedtuple
import pymongo

class ProcessDataTablesServerSide:
    '''client에서 사용하는 DataTables 를 serverSide로 작동시켰을경우 서버에서 다루는 로직임'''
    def __init__(self,request, columns, index, collection,db_client):

         self.columns = columns
         self.index = index
         self.collection = collection

         self.request_values = request.values

         self.db_client = db_client

         #db에서 받은것들이 여기로 들어감
         self.result_data = None

         #total in the table after filtering
         self.cardinality_filtered = 0

         #total in the table unfiltered
         self.cardinality = 0

         self.run_queries()

    def run_queries(self):

        #flask-pymongo로 db_client객체를 만들었으므로 db명 필요없이 그냥  nameSpace'db'로 접근
        db = self.db_client.db

        #namedTuple로  'start'와 'length'를 반환받는다
        pages = self.paging()
        print(pages.start,pages.length)
        #만약 해당 요청이 client search  textbox에서 무언가를 검색했을때 넘어온 것이라면
        _filter = self.filtering()

        # 일단 sorting 기능은 사용 하지 않는다
        # the document field you chose to sort
        # sorting = self.sorting()

        #skip(optional): the number of documents to omit(from the start of the
        #result set) when returning  the results

        #mongo의 find_one() 이 아닌 find는 iterable 로 반환되며  for문으로 하나씩 뽑아내거나  list()로 묶어 내야함 또한 기본적으로 ASCENDING이므로 최신순으로 보려면 DESCENDING으로 할 것
        self.result_data = list(db[self.collection].find(skip = pages.start,
                                                         limit = pages.length,
                                                         filter = _filter).sort("_id", pymongo.DESCENDING))
        #sort준비되면 sort=sorting 추가 인데 현재 pymongo  버전에서는 sort()함수를 추가로 호출해야 하는것 같음
        #예전 버전에서는 filter kwargs key 가 spec 임(예시에선)

        # length of all results you wish to display in the datatable, unfiltered
        #//->해당 collection의 모든 데이터의 '갯수'
        self.cardinality = db[self.collection].find().count()

        # length of filtered set
        #해당 collection의 filter만 걸친 '갯수' 그러나 1~10  같은 갯수 제한없는 갯수임
        self.cardinality_filtered = db[self.collection].find(filter=_filter).count()


    def paging(self):
        #한번 로드되면  client로 반환시까지 불변하므로 튜플로 넣되 nameSpace로 접근할 수 있도록 namedTuple사용
        #namedTuple은 tuple과 동일하나  쩜(.)연산자로 내용에 접근 가능
        pages = namedtuple("pages", ["start", "length"])

        if self.request_values["start"] != "" and self.request_values["length"] != "":
            #1.19버전에서는 start = iDisplayStart, length = iDisplayLength 임
            pages.start = int(self.request_values["start"])
            pages.length = int(self.request_values["length"])
            # print(pages.start, pages.length)
        return pages

    def filtering(self):
        '''request value에 전체 search[value], search[redex] 가 오며 각 column의 갯수대로
        column[i][search][value], column[i][search][redex]가 온다  '''

        _filter = {}
        if self.request_values.has_key("search") and self.request_values['search'] != "":
            #모든 column 명에서 filter에 적은 내용을 찾기위해 만든다 mongo query인 $or 로 검색됨
            or_filter_on_all_columns = []

            #개별column filter 적용
            for i in range(len(self.columns)):
                column_filter = {}
                #$regex $option 모두 mongo query 연산자 pymongo에서는 karg로 들어간다
                column_filter[self.columns[i]] = {'$regex': self.request_values['sSearch'], '$options': 'i'}
                or_filter_on_all_columns.append(column_filter)

            _filter['$or'] = or_filter_on_all_columns

            # individual column filtering - uncomment if needed
            # and_filter_individual_columns = []
            # for i in range(len(columns)):
            #    if (request_values.has_key('sSearch_%d' % i) and request_values['sSearch_%d' % i] != ''):
            #        individual_column_filter = {}
            #        individual_column_filter[columns[i]] = {'$regex': request_values['sSearch_%d' % i], '$options': 'i'}
            #        and_filter_individual_columns.append(individual_column_filter)

            # if and_filter_individual_columns:
            #    _filter['$and'] = and_filter_individual_columns

            return _filter

    def sorting(self):
        '''pymongo의 find 함수 kwarg 에 쓸 수 있도록 client에서 넘어오는 request 변수를 조작한다
        1.9 버전 내용을 적절히 조작할 것  '''
        #todo 일단 사용하지 않는다
        order = []
        # mongo translation for sorting order
        if (self.request_values['iSortCol_0'] != "") and (self.request_values['iSortingCols'] > 0):

            for i in range(int(self.request_values['iSortingCols'])):
                # column number
                column_number = int(self.request_values['iSortCol_' + str(i)])
                # sort direction
                sort_direction = self.request_values['sSortDir_' + str(i)]

                # order.append((self.columns[column_number], order_dict[sort_direction]))

        return order

    def output_result(self):
        output = {}
        #xss방지용 client로부터 1로온 값이면 1로 반환한다.정확한 내용확인이 더 필요
        output['draw'] = str(int(self.request_values['draw']))
        output['recordsTotal'] = str(self.cardinality)
        output['recordsFiltered'] = str(self.cardinality_filtered)

        #//-> db단에서 필드를 선별할 경우 필요함(1.9버전내용임) 일단atraCycle의 경우 선별없이 모두 response 시키고 client에서 선별하기 때문에 필요없음
        # aaData_rows = []
        #
        # for row in self.result_data:
        #     aData_row = []
        #     for i in range(len(self.columns)):
        #         aData_row.append(row[self.columns[i]].replace('"', '\\"'))
        #
        #     # add additional rows here that are not represented in the database
        #     # aData_row.append(('''<input id='%s' type='checkbox'></input>''' % (str(row[ self.index ]))).replace('\\', ''))
        #
        #     aaData_rows.append(aData_row)

        output['data'] = self.result_data


        return output