#-*- coding:utf-8 -*-
from flask import Flask, render_template, g
import inspect

# print("in atraServer\__init__.py loded from " + str(inspect.stack()[1][1]))
atraApp = Flask(__name__.split('.')[0])
# print(__name__, __name__.split('.')[0])


#개발자 입장에서는 subForder로 controller를 둬서 관리하지만 python flask 는 모든 route를 atraServer/__init__.py 컨텍스트로 불러와서 route 시킴
#따라서 app 객체를 먼져 만들고  나머지 controller를 이곳으로 불러와야함
from controller.atraCycle import *
from controller.atraMain import *
from controller.atraWatcher import *

#해당 폴더 한단계 외각 ../ 에서 atraApp을 run 시키므로 필요없어짐
# if __name__ == '__main__':
#     atraApp.run()
