#-*- coding:utf-8 -*-
#controller 모듈들이 atraServer/__init__.py로 불려들어가서 사용됨 아래것은 순환참조임
import time
from atraServer import atraApp
from atraServer.model.atraDB import *
from werkzeug.security import generate_password_hash, check_password_hash
from atraServer.model.atraDB import *
from atraServerUtil import *
from flask import render_template, request, redirect ,url_for, session, current_app
from bson import json_util, ObjectId
from pprint import pprint
import json

#atraWatcher route
@atraApp.route('/atraWatcher', methods=['GET','POST'])
@atraLoginRequired
def atraWatcher():
    # print("atraMain",session,session.get("userId"))
    # if request.method == 'POST':
    #     return 'ok'
    return render_template("atraWatcher.html")

@atraApp.route('/atraWatcher/overallData', methods =['GET'])
def getAtraWatcherOverallData():
    atraCnpCollectionList = list(readCollectionList("atraCnp"))

    overallDataList = []
    for pairingObj_name in atraCnpCollectionList :
        overallData ={"pairingObj_name" : pairingObj_name, "whole_period" : {}, "mostRecentObj_commodityRoleCoins" : [], "atra_type" : ""}
        overallData["whole_period"], overallData["mostRecentObj_commodityRoleCoins"], overallData["atra_type"] = readOldestNewestDateAndCoinList(pairingObj_name)
        overallDataList.append(overallData)
    pprint(overallDataList)
    return json_util.dumps(overallDataList)
#chartistTest route
@atraApp.route('/amchartsTest', methods=['GET'])
def chartistTest():
    return render_template("amChartsTest.html")

@atraApp.route('/amchartsTest/data', methods=['GET'])
def chartistTestData():

    pairingObjName = request.args.get("pairingObjName")
    display_length = int(request.args.get("displayLength"))
    #일단 테스트로 여기서 DB 호출함.
    from atraServer import atraModel
    print(pairingObjName, display_length)
    db_client = atraModel.getDBClient("atraCnp")
    #atraCnp는 데이터 수가 많으므로 query 시간을 단축하기 위해 pairingObjName이 곧 collection임
    collection_name = pairingObjName
    collection_obj = db_client.db[collection_name]

    # find_config = {"exchage_pairingName" : pairingObjName}
    # raw_resultData = collection_obj.find(find_config).skip(collection_obj.find(find_config).count() -1000)
    aTime = time.time()
    # print("db access Start Time", aTime)
    skip_length = 0 if collection_obj.find().count() < display_length else collection_obj.find().count() - display_length
    raw_resultData = collection_obj.find().skip(skip_length)
    # print(pairingObjName + "Count", raw_resultData.count(), "skip_length", skip_length)
    bTime = time.time() ; print("db access end Time", bTime ,  "difference", bTime - aTime)
    # sort([("_id", -1), ("atraCnp_timestamp", 1)]).limit(30)
    #find로 반환한 Cursor 객체
    sanitized_result = json_util.dumps(raw_resultData)
    # pprint(sanitized_result)
    return sanitized_result
