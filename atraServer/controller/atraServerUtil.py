#-*- coding:utf-8 -*-
from flask import request, session, url_for, redirect, abort
from functools import wraps
from atraServer import atraApp

#login이 필요한 document get 호출에 사용되며 login 안되면 404 에러로 보내버림
def atraLoginRequired(func):
    @wraps(func)   #//-> wraps 안쓰면 다른곳에서 해당함수를 url_for 호출 못함
    def loginCheck(*args, **kwargs):
        print(request.method, session)
        if request.method == "GET" and "userId" not in session :
            return abort(404)
        return func(*args, **kwargs)
    return loginCheck
