#-*- coding:utf-8 -*-
#controller 모듈들이 atraServer/__init__.py로 불려들어가서 사용됨 아래것은 순환참조임
from atraServer import atraApp
from atraServer.model.atraDB import *
from atraServerUtil import *
from flask import render_template, request, redirect
from bson import json_util, ObjectId
import json


@atraApp.route('/atraCycle')
@atraLoginRequired
def atraCycleindex():
    print("atraCycle")
    return render_template("atraCycle.html")

@atraApp.route('/atraCycle/data', methods=["POST","GET"])
def getAtraCycleData():
    """이건 Datatables 누가 만든걸 그대로 쓰느라 business 로직을 controller에 넣었지만 다른것들은 모두 구분하는걸로"""
    if request.method == "GET":
        #botA 에서 atraCycle DB가 작동하는지 확인하려고 endpoint에 ?check 붙여서 올때나
        #atraWeb에서 DataTables serverSide로 데이터 받아오는 쿼리문 올때 GET으로 설정하면 여기로 옴
        data = readAtraCycle(request)
        return data
    elif request.method == "POST":
        # client 에서  {'Content-Type': 'application/json'} 헤더 지정안하면 request.values로 받음
        # pprint(request.values)
        # client 에서  {'Content-Type': 'application/json'} 헤더 지정하면 get_json으로 받아서 바로 씀
        if request.get_json() == None:
            # atraWeb에서 DataTables serverSide로 데이터 받아오는 쿼리문 올때 POST로 설정하면 requestValues로 옴
            result = readAtraCycle(request)
        else:
            #botA에서 atraCycle 이 POST로 오면 writeAtraCycle이 호출됨
            data = request.get_json()
            result = writeAtraCycle(data)
            print(result, type(result))
        return result
    # return render_template("atraCycle.html")