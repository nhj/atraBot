#-*- coding:utf-8 -*-
#controller 모듈들이 atraServer/__init__.py로 불려들어가서 사용됨 아래것은 순환참조임
from atraServer import atraApp
from werkzeug.security import generate_password_hash, check_password_hash
from atraServer.model.atraDB import *
from atraServerUtil import *
from flask import render_template, request, redirect ,url_for, session, current_app
from bson import json_util, ObjectId
from pprint import pprint
import json

#어플리케이션 레벨에서 공유시킬 수 있다
with atraApp.app_context(): #with문으로 감싸야 현행 appContext의 current_app  변수를 사용하고 내용을 저장가능
    current_app.visitor = 0
    current_app.atra_reported_normal = None
    current_app.atra_reported_watcher = None

@atraApp.route('/', methods=['GET','POST'])
@atraLoginRequired
def atraMain():
    # print("atraMain",session,session.get("userId"))
    # if request.method == 'POST':
    #     return 'ok'
    return render_template("atraMain.html", visitor = current_app.visitor)

#atra_balance_route
@atraApp.route('/atraBalance/data',methods=['GET', 'POST'])
def getAtraBalanceData():
    if request.method == "GET":
        print("atraBalanceData Get")
        exchange = list(request.args)
        balance_dict = readAtraBalance(request, exchange[0])
        # print(balance_dict)
        return json_util.dumps(balance_dict)

    if request.method == "POST":
        print("atraBalanceData POST")
        # client 에서  {'Content-Type': 'application/json'} 헤더 지정안하면 request.values로 받음
        # pprint(request.values)
        # client 에서  {'Content-Type': 'application/json'} 헤더 지정하면 get_json으로 받아서 바로 씀
        data = request.get_json()
        result = writeAtraBalance(data)
        # print("atraBalancePostResult", result)
        return result

# atra_cnp_route
@atraApp.route('/atraCnp/data', methods=['GET', 'POST'])
def getAtraCnpData():
    #atraBot에서 /data?check 요청할때와  view  client에서 cnp data 불러올때 쓰인다
    if request.method == "GET":
        print("atraCnpData Get")
        cnp_dict = readAtraCnp(request)
        # print(cnp_dict)
        return json_util.dumps(cnp_dict)

    if request.method == "POST":
        print("atraCnpData POST")
        # client 에서  {'Content-Type': 'application/json'} 헤더 지정안하면 request.values로 받음
        # pprint(request.values)
        # client 에서  {'Content-Type': 'application/json'} 헤더 지정하면 get_json으로 받아서 바로 씀
        data = request.get_json()
        # pprint(data)
        result = writeAtraCnp(data)
        # print("atraCnpPostResult", result)
        return result

#atra_report route
@atraApp.route('/report', methods=['GET','POST'])
def atraReport():
    # atra_reported = current_app.atra_reported
    #GET에서 오면 서버 app context에 저장해둔 atraReport 정보를 회수함
    if request.method == 'GET':
        # print(current_app.atra_reported)
        print("reportMode", request.args.get("reportMode"))
        requested_reportMode = request.args.get("reportMode")
        if requested_reportMode == "atraMain":
            atra_reported_data = json.dumps(current_app.atra_reported_normal)
        else:
            atra_reported_data = json.dumps(current_app.atra_reported_watcher)
        # print(atra_reported_data)
        return atra_reported_data

    # POST 로 오면 atra또는 serverAtraBot 에서 report를 하러 옴
    if request.method == 'POST':

        data = request.get_json()
        # pprint(data)
        if data["start_mode"] == "START_NORMAL":
            current_app.atra_reported_normal = data
        if data["start_mode"] == "START_ATRAWATCHER":
            current_app.atra_reported_watcher = data
        return "ok"


#login route
@atraApp.route('/login', methods=['GET','POST'])
def login():
    # print(session, session.get("userId"))
    if request.method == 'POST':
        print(request.form["userId"], request.form["pass"])
        founded_user = readUserData(request.form["userId"])
        if founded_user == None:
            return "user Not founded"

        #hash값 동일 할 경우
        if check_password_hash(founded_user["password"], request.form["pass"]):
            session['userId'] = request.form['userId']
            # print(session, session.get("userId"))
            #url_for은 route 된 '메소드명'에 mapping됨.
            return redirect(url_for('atraMain'))
        else:
            return "password is not correct"

    return render_template('login.html')


@atraApp.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        print(request.form["userId"], request.form["pass"])
        existing_user = readUserData(request.form["userId"])
        if existing_user is None:
            # hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.gensalt())
            #werkzueg 에서 제공하는  hash 함수를 사용하며 pbkdf2:sha256을 사용 키 스트레칭 iteration은 자동으로 5000임
            hashed_pass = generate_password_hash(request.form["pass"], method='pbkdf2:sha256', salt_length=8)
            writeUserData(request.form["userId"], hashed_pass)
            return redirect(url_for('login'))
        return 'That userId already exists!'

    if session.get("userId") != None:
        return "you already logged in"

    return render_template('register.html')

# #atraWatcher route
# @atraApp.route('/atraWatcher', methods=['GET','POST'])
# @atraLoginRequired
# def atraWatcher():
#     # print("atraMain",session,session.get("userId"))
#     # if request.method == 'POST':
#     #     return 'ok'
#     return render_template("atraWatcher.html")


# @atraApp.route('/register', methods=['POST', 'GET'])
# def register():
#     if request.method == 'POST':
#         print(request.form["userId"], request.form["pass"])
#         existing_user = readUserData(request.form["userId"])
#         if existing_user is None:
#             # hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.gensalt())
#             #werkzueg 에서 제공하는  hash 함수를 사용하며 pbkdf2:sha256을 사용 키 스트레칭 iteration은 자동으로 5000임
#             hashed_pass = generate_password_hash(request.form["pass"], method='pbkdf2:sha256', salt_length=8)
#             writeUserData(request.form["userId"], hashed_pass)
#             return redirect(url_for('login'))
#         return 'That userId already exists!'
#
#     return render_template('register.html')
