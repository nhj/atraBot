### 디렉터리 구조
.botA/  arbitrageBot           

.boaA/atraBot_exchangeObj.py  실행파일  
.boaA/atraStart_atraWatcher.py atraServer에 호가창 정보만 전송  수익그래프 파악용  
.botA/atraOuter.py  기타 실행flag 관리 , AES256 기반 apikey 관리   
.botA/atraLogging.py  logging module을  이용한 log 설정  

.botA/setting/    -> 기타 api 키값 and   
.botA/setting /keys.ini   AES256으로 암호화된 개별 거래소의  api_key 들과   
.botA/setting /keys_server.ini     atraServer에서 사용  
.botA/setting /shared.json      개별 coin들의  fee등을 json으로 관리  
.botA/setting/shared_atraWatcher.json     

.botA/exchangeAPI/  #개별 거래소 api wrapper  

.atraServer/     flask기반 웹서버(history 확인용 가상수익 arbitrage 그래프)  
./runServer.py   server 시작   http https 모두실행가능  

written by  나혁진


